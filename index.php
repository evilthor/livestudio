<?php
    include('config.php');
    $error = "";
    if( isset($_SESSION['id']) && $_SESSION['id'] > 0 ){
        header('Location:./apps.php');exit;
    }
    if(isset($_POST['email'])){
    	$email = $_POST['email'];
    	$password = $_POST['password'];
    	if($email == ''){
    	      $error .= "Please enter email address.";
    	}elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    	      $error .= "Invalid email format";
    	}
    	if($password == ''){
        	if($error != ''){
                $error.="<br>";
            }
            $error .= "Please enter password.";
    	}
    	if($error == ''){
            //escaping filter input
    		$login = $db->get_row("SELECT * FROM tbl_users WHERE username = '".$db->escape($email)."' AND password = '".md5($db->escape($password))."'",ARRAY_A);
    		if($login){
    			$_SESSION['id'] = $login['id'];
    			$_SESSION['email'] = $login['username'];
    			header('Location:apps.php');exit;
    		}else{
    			$error .= "Invalid email and/or password.";
    		}
    	}	
    }
    include 'includes/header.php'; 
?>
<body>
    <script>
        <?php if(isset($error) && $error != ''){ ?>
        window.messages = {
            type : "Error",
            message: "<?php echo $error;?>",
        }
        <?php } ?>
        <?php /*if(isset($_GET['success']) && $_GET['success'] != ''){ ?>
        window.messages = {
            type : "Success",
            message: "Logout successfully.",
            redirect: './index.php',
        }
        <?php }*/ ?>
        <?php if(isset($_GET['error']) && $_GET['error'] != ''){ ?>
        window.messages = {
            type : "Error",
            message: "Please Login.",
            redirect: './index.php',

        }
        <?php } ?> 
    </script>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post" action="">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" value="<?php echo @$_POST['email']; ?>" placeholder="E-mail" name="email" type="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit" class="btn btn-lg btn-success btn-block">Login</a>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<?php include 'includes/footer.php'; ?>
</body>
</html>