<?php include 'config.php'; ?>
<?php include 'includes/session_check.php'; ?>
<?php if(isset($_GET['id']) && $_GET['id'] != ''){
	$checkDB = "SELECT * FROM tbl_block WHERE id = '".$_GET['id']."'";
	$checkDB = $db->get_row($checkDB);
	
	$sql_block_status = "SELECT is_default FROM tbl_static_block_status WHERE block_id = '".$_GET['id']."' AND app_id='".$_GET['app_id']."'";
	$res_block_status = $db->get_row($sql_block_status);

	if($res_block_status){if($res_block_status->is_default == 1){
		$res = false;
		$active = true;
		}
	else{
		if($yes_global==1){
			if( file_exists($checkDB->image_url) ){
					unlink($checkDB->image_url);		
				}
			$sql_del = "DELETE FROM tbl_block WHERE id = '".$checkDB->id."'";
			$db->query($sql_del);
		}else{

			if($checkDB->app_id == GLOBAL_EN || $checkDB->app_id == GLOBAL_AR){
					$sql = "INSERT INTO tbl_delete_data SET app_id = '".$_GET['app_id']."',
															row_id = '".$_GET['id']."',
															row_type = 'static_block',
															show_in_app = '0'";
					$db->query($sql);
			}else{
				if( file_exists($checkDB->image_url) ){
					unlink($checkDB->image_url);		
				}
				$sql_del = "DELETE FROM tbl_block WHERE id = '".$checkDB->id."'";
				$db->query($sql_del);
			}
		}
		
		/*if( file_exists($checkDB->image_url) ){
			unlink($checkDB->image_url);		
		}
		$sql_del = "DELETE FROM tbl_block WHERE id = '".$checkDB->id."'";
		$db->query($sql_del);*/
		//$sql = "UPDATE tbl_block SET show_in_app = 0 WHERE id = '".$checkDB->id."'";
		//$db->query($sql);

		$res = true;		
	}
	}else{
			$res = false;
			}
	}?>
<?php include 'includes/header.php'; ?>
<script>
	<?php if(isset($res) && $res == true){ ?>
	window.messages = {
		type : "success",
		message: "Record deleted successfully.",
	}
	<?php }elseif(isset($res) && $res == false && $active == true){ ?>
	window.messages = {
		type : "warning",
		message: "This block is currently active.",
	}
	<?php }elseif(isset($_GET['add'])){ ?>
	window.messages = {
		type : "success",
		message: "Block added sucessfully..",
	}
	<?php }elseif(isset($res) && $res == false){ ?>
	window.messages = {
		type : "warning",
		message: "Invalid id provided, please try again later.",
	}
	<?php } ?>
</script>
<body>
<div id="wrapper">
	<?php include('includes/navigation.php'); ?>
	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="apps">
						<div class="title">
							<h2>Static blocks</h2>
							<a class="btn btn-primary" href="./new-static-block.php?app_id=<?php echo $_GET['app_id']; ?>">new static block</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<?php 

				$sql = "SELECT GROUP_CONCAT(row_id) as row_ids FROM tbl_delete_data WHERE app_id ='".$_GET['app_id']."' AND row_type='static_block'";
				$skip_data = $db->get_row($sql);
				$row_ids_arr = $skip_data->row_ids;
				if($_GET['app_id'] == GLOBAL_EN || $_GET['app_id'] == GLOBAL_AR){
					$sql = "SELECT * FROM tbl_block  WHERE app_id IN(".@$_GET['app_id'].", ".$global_app_id." ) AND show_in_app='1' AND is_global='1' AND id NOT IN('".$row_ids_arr."')";
				}else{
					$sql = "SELECT * FROM tbl_block  WHERE app_id IN(".@$_GET['app_id'].", ".$global_app_id." ) AND show_in_app='1' AND id NOT IN('".$row_ids_arr."')";
				}
					$blocks = $db->get_results($sql);
						 if($blocks){
						 	$count=0;
							foreach($blocks as $block){
								if( $count != 0 && $count % 4 == 0 ){
									echo "</div><hr><div class=\"row\">";
								}
								$count++;
								?>
								<div class="col-lg-3">
                                    <div class="block" id="static-block-<?php echo $block->id; ?>">
                                        <div class="head">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                	<?php
                            	$sql_sb_status = "SELECT is_default FROM tbl_static_block_status WHERE block_id = '".$block->id."' AND app_id='".$_GET['app_id']."'";
          						$res_sb_status = $db->get_row($sql_sb_status);
                            	?>
                                                  <div class="radio">
                                                    <label>
                                                      <input 
                                                      	type="radio" 
                                                      	data-id="<?php echo $block->id; ?>" 
                                                      	data-app-id="<?php echo $_GET['app_id']; ?>" 
                                                      	data-parent-ele="#static-block-<?php echo $block->id; ?>"
                                                      	name="is_published" 
                                                      	value="1" 
                                                      	<?php if($res_sb_status->is_default == 1){ ?> checked<?php } ?>
                                                      > Published
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="col-lg-6">
                                                	<?php
														if( $block->is_global==0 || $yes_global=='1'){
														?> 
                                                    <a 
                                                        href="./static-block.php?id=<?php echo $block->id; ?>&app_id=<?php echo $_GET['app_id']; ?>" 
                                                        class="btn btn-danger btn-xs pull-right is-delete"
                                                    ><i class="fa fa-trash-o" aria-hidden="true"></i> delete</a>
                                                    <?php
														}
                                                    ?>
                                                    <?php
														if( $block->is_global==0 || $yes_global=='1'){
														?> 
                                                    <a 
                                                        href="./edit-static-block.php?id=<?php echo $block->id; ?>&app_id=<?php echo $_GET['app_id']; ?>" 
                                                        class="btn btn-primary btn-xs pull-right"
                                                    ><i class="fa fa-pencil-square-o" aria-hidden="true"></i> edit</a> 
                                                    <?php
                                                	}
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="block-img">
                                            <img src="<?php echo $block->image_url; ?>" class="img-responsive" alt="">
                                        </div>
                                        <?php if($block->has_text == 1){?>
                                        <div class="block-label">
                                           <?php echo $block->welcome_text;?>
                                        </div>
										<?php } ?>
                                    </div>
                                </div>
								<?php
							}
						 }
				?>
			</div>
		</div>
	</div>
</div>
<?php include 'includes/footer.php'; ?>
</body>
</html>
