<?php
include 'config.php';
include 'includes/session_check.php';

$sql_slider = "SELECT * FROM tbl_desktop_lander_slider WHERE cat_id = '".$_GET['cat_id']."' ORDER BY row_order ASC";
$res_slider = $db->get_results($sql_slider);
$layout_slider = perviewLayouts($res_slider); // this function in common_php.php

$sql_cat = "SELECT * FROM tbl_desktop_lander_rows WHERE cat_id = '".$_GET['cat_id']."' ORDER BY row_order ASC";
$res_cat = $db->get_results($sql_cat);
$layout_cat = perviewLayouts($res_cat); // this function in common_php.php
?>
<?php include 'includes/header.php'; ?>
<body>
<div id="wrapper">
	<?php include('includes/navigation.php'); ?>
	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="apps">
						<div class="title">
							<h2>Desktop lander manager</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
                <?php
                $sections = array(
                            '0' => 'Slider',
                            '1' => 'Categories',
                );
                foreach ($sections as $section){
                    if($section=='Slider'){
                        $tbl = 'tbl_desktop_lander_slider';
                    }else{
                        $tbl = 'tbl_desktop_lander_rows';
                    }
                    $sql_slider = "SELECT * FROM $tbl WHERE cat_id = '".$_GET['cat_id']."' ORDER BY row_order ASC";
                    $results = $db->get_results($sql_slider);

                    $sql = "SELECT * FROM $tbl WHERE cat_id = '".$_GET['cat_id']."' ORDER BY row_order ASC LIMIT 1";
                    $result = $db->get_row($sql);
                ?>
				<div class="col-md-3">
					<div class="box">
						<div class="merchandise-box" id="merchandise-<?php //echo $lander_cat->id; ?>" data-command="merchandise" data-id="<?php //echo $lander_cat->id; ?>">
							<div class="head">
								<div class="row">
									<div class="col-md-9">
										<div class="title"><?php echo $section; ?></div>
										<div class="sub-title"><?php echo count($results); ?> Row(s)</div>
									</div>
									<div class="col-md-3">
										 
									</div>
								</div>
							</div>
					   		<div class="content" onClick="window.location.href='desktop-lander-overview.php?app_id=<?php echo $_GET['app_id'] ?>&cat_id=<?php echo $_GET['cat_id']; ?>&type=<?php echo strtolower($section); ?>';">
								<?php
									$images = json_decode(@$result->images);

									if(@$result->layout == "single" || @$result->layout == "singleBar" || @$result->layout == "singleShort"){
								?>
								<div class="single">
									<div class="row">
										<div class="col-md-12">
											<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
										</div>
									</div>
								</div>
								<?php }elseif(@$result->layout == "double" || @$result->layout == "doubleShort"){ ?>
								<div class="double">
									<div class="row">
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
										</div>
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
										</div>
									</div>
								</div>
								<?php }elseif(@$result->layout == "tripleLeft"){ ?>
								<div class="triple">
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[2]; ?>" class="img-responsive"> </div>
										</div>
									</div>
								</div>
								<?php }elseif(@$result->layout == "tripleRight"){ ?>
								<div class="triple">
									<div class="row">
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[2]; ?>" class="img-responsive"> </div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<?php
						}
			 	?>
			</div>
		</div>
	</div>
</div>
<?php include 'includes/footer.php'; ?>
</body>
</html>