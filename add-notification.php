<?php
	include 'config.php';
	include 'includes/session_check.php';
	include 'includes/header.php';

/*	$and = $ios = '';
	$sql = "SELECT * FROM tbl_apps";
	$results = $db->get_results($sql);
	if($results){
		foreach($results as $result){
			$and .= '<li id="and-'.$result->id.'">'.$result->app_name.' ('.$result->app_store.')</li>';	
			$ios .= '<li id="ios-'.$result->id.'">'.$result->app_name.' ('.$result->app_store.')</li>';
		}
	}*/

	$sql = "SELECT * FROM tbl_stores 
                                WHERE status='1' 
                                order by store_lang";
    $res_stores = $db->get_results($sql);
    $stores = array();
    $and_en = $ios_en = '';
    $and_ar = $ios_ar = '';
  	foreach ($res_stores as $res_store) {

    	if($res_store->store_lang == "en"){
        	$stores['EN']['en_'.$res_store->country_code] = $res_store->country_code;
        }elseif($res_store->store_lang == "ar"){
            $stores['AR']['ar_'.$res_store->country_code] = $res_store->country_code;
        }else{
            $stores['EN']['en_'.$res_store->country_code] = $res_store->country_code;
            $stores['AR']['ar_'.$res_store->country_code] = $res_store->country_code;
        }
    }

    /*if($stores){
		foreach($stores as $key =>$store_single){
			if($key=='EN'){
				foreach($stores['EN'] as $index =>$store_en){
					$and_en .= '<li id="and-'.$index.'">Elabelz (en_'.strtolower($store_en).')</li>';	
					$ios_en .= '<li id="ios-'.$index.'">Elabelz (en_'.strtolower($store_en).')</li>';
				}
			}else{
				foreach($stores['AR'] as $index =>$store_ar){
					$and_ar .= '<li id="and-'.$index.'">Elabelz (ar_'.strtolower($store_ar).')</li>';	
					$ios_ar .= '<li id="ios-'.$index.'">Elabelz (ar_'.strtolower($store_ar).')</li>';
				}
			}
			
		}
	}*/
     //echo "<pre>";print_r($stores['EN']);echo "<hr>";
    //echo "<pre>";print_r($stores_ar);echo "<hr>";
   //exit;
?>
<body class="<?php echo bodyClass(array('notification')); ?>">
<div id="wrapper">
	<?php include('includes/navigation.php'); ?>
	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="title">
				<h1><strong>SEND</strong> PUSH NOTIFICATION</h1>
			</div>
			<div class="clearfix devicePreview">
				<div class="ipadPreviewContainer">
					<div id="carouselOuter" class="carouselRowsManager">
						<div id="carouselCircleLeft"></div>
						<div id="carousel" class="mCustomScrollbar">
							<div class="carouselTimeDateBox">
								<div class="moment-time"></div>
								<div class="moment-date"></div>
							</div>
							<div class="carouselNotification">
								<div class="notificationAppTitle clearfix">
									<div class="notificationAppIcon"> <img src="./dist/images/app_icon_placeholder.png" class="img-responsive"> </div>
									<div class="brandTitle"> Elabelz </div>
									<div class="notificationTime"> now </div>
								</div>
								<div class="notificationBody">
									<div class="notificationTitle"> {{ title.length > 0 ? title : 'Enter notification title' }} </div>
									<div class="notificationMessage"> {{ message.length > 0 ? message : 'Enter your notification message' }} </div>
									<div class="notificationFooter"> Press for more </div>
								</div>
							</div>
						</div>
						<div id="carouselCircleRight"></div>
					</div>
				</div>
				<div class="iphonePreviewContainer">
					<div id="carouselOuter" class="carouselRowsManager">
						<div class="carouselCircleTop"></div>
						<div class="carouselRectangleTop"></div>
						<div class="carouselCircleTopLeft"></div>
						<div id="carousel">
							<div class="carouselTimeDateBox">
								<div class="moment-time"></div>
								<div class="moment-date"></div>
							</div>
							<div class="carouselNotification">
								<div class="notificationAppTitle clearfix">
									<div class="notificationAppIcon"> <img src="./dist/images/app_icon_placeholder.png" class="img-responsive"> </div>
									<div class="brandTitle"> Elabelz </div>
									<div class="notificationTime"> now </div>
								</div>
								<div class="notificationBody">
                                             <div class="notificationTitle"> {{ title.length > 0 ? title : 'Enter notification title' }} </div>
                                             <div class="notificationMessage"> {{ message.length > 0 ? message : 'Enter your notification message' }} </div>
									<div class="notificationFooter"> Press for more </div>
								</div>
							</div>
						</div>
						<div class="carouselCircleBottom"></div>
					</div>
				</div>
			</div>
			<div class="layouts">
				<div class="layout-type">
					<div class="row">
                              <div class="col-lg-3">
							<div class="step step-1 clearfix">
								<span class="step-number">1</span>
								<div class="step-detail">
									<h6>Select your app</h6>
                                             <p>Select your app.</p>
		                                   <div id="container">
		                                     	<ul>
		                                       		<li id="all">All apps
		                                         		<ul>
		                                           			<li id="android">Android
		                                           				<ul>
		                                           					<?php foreach ($stores as $index => $store){ ?>
		                                           					<li id="android-all-<?php echo $index;?>">All <?php echo $index;?>
		                                           						<ul>
			                                           						<?php
			                                           							foreach ($store as $code => $country){
			                                           								$sql = "SELECT id FROM tbl_apps_new 
                                WHERE app_store='".strtolower($index)."_".strtolower($country)."'";
    																				$res_aps = $db->get_row($sql);
			                                           								echo '<li id="and-'.$res_aps->id.'">Elabelz ('.strtolower($index)."_".strtolower($country).')</li>';
			                                           							}
			                                           						?>
		                                           						</ul>
		                                           					</li>
		                                           						<?php
																		}
		                                           						?>
		                                           					
		                                           				</ul>
		                                             			
		                                           			</li>
		                                           			<li id="ios">iOS
		                                             			<ul>
		                                             				<?php foreach ($stores as $index => $store){ ?>
		                                           					<li id="ios-all-<?php echo $index;?>">All <?php echo $index;?>
		                                           						<ul>
			                                           						<?php
			                                           							foreach ($store as $code => $country){
			                                           								$sql = "SELECT id FROM tbl_apps_new 
                                WHERE app_store='".strtolower($index)."_".strtolower($country)."'";
    																				$res_aps = $db->get_row($sql);
			                                           								echo '<li id="ios-'.$res_aps->id.'">Elabelz ('.strtolower($index)."_".strtolower($country).')</li>';
			                                           							}
			                                           						?>
		                                           						</ul>
		                                           					</li>
		                                           						<?php
																		}
		                                           						?>
		                                           					<!-- <li>All EN<ul><?php echo $ios_en; ?></ul></li>
		                                           					<li>All AR <ul><?php echo $ios_ar; ?></ul></li> -->
		                                           				</ul>
		                                           			</li>
		                                         		</ul>
		                                       		</li>
		                                     	</ul>
		                                   </div>
		                              </div>
		                         </div>
                              </div>
						<div class="col-lg-3">
							<div class="step step-2 clearfix"> <span class="step-number">2</span>
								<div class="step-detail">
									<h6>Compose message</h6>
                                             <p>The message is required, the title is not.</p>
                                             <div class="form-group">
                                                  <input type="hidden" name="app_id" id="app_id" value="<?php echo $_REQUEST['app_id'] ?>">
                                                  <input type="text" ng-model="title" name="title" class="form-control" id="notification-title" placeholder="Enter notification title">
                                             </div>
                                             <div class="form-group">
                                                  <input type="checkbox" name="platform" id="platform" value="<?php if(isset($_GET['debug'])){echo"development";}else{echo"production";} ?>" style="display:none;">
                                                  <textarea name="message" ng-model="message" class="form-control" id="notification-message" placeholder="Enter your notification message" maxlength="215"></textarea>
                                             </div>
                                             <p>You have {{ message.length > 0 ? 215 - message.length : '215' }} characters left</p>
								</div>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="step step-3">
								<div class="clearfix"> <span class="step-number">3</span>
									<div class="step-detail">
										<h6>Notification settings</h6>
                                                  <p>Select the page, product or category in the app that should open when the user taps on the notification.</p>
                                                  <label for="">In-app landing page</label>
                                                  <div class="ddArea clearfix" data-click-show="#subjectContentDdPopup" data-ajax-url="./ajax/popupContent.php">
                                                       <div class="ddBtnText clipText">Choose notification subject</div>
                                                       <div class="ddBtnArrow">
                                                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                                                       </div>
                                                  </div>                                            
                                                  <div class="contentDdPopup" id="subjectContentDdPopup"></div>
                                                  <label for="">Send</label>
                                                  <div class="ddArea clearfix" data-click-show="#calendarContentDdPopup" data-ajax-url="./ajax/calendarContent.php">
                                                       <div class="ddBtnText clipText">Now</div>
                                                       <div class="ddBtnArrow">
                                                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                                                       </div>
                                                  </div>
                                                  <div class="contentDdPopup" style="height: 309px;" id="calendarContentDdPopup">
                                                       
                                                  </div>
                                             </div>

								</div>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="step step-4">
								<div class="clearfix"> <span class="step-number">4</span>
									<div class="step-detail">
										<h6>Send notification</h6>
										<p>The notification will be sent on <span id="selected-date">24 January</span> at <span id="selected-time">11:05</span> (in your local time zone).</p>									
                                             </div>
								</div>
								<div class="submit-button">
									<button id="notificationSaveRow" disabled class="btn btn-primary">SCHEDULE NOTIFICATION</button><img src="images/loader.gif" style="display:none;" id="push-loader">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
<?php include 'includes/footer.php'; ?>
</body>
</html>