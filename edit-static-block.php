<?php include 'config.php'; ?>
<?php include 'config.image.php'; ?>
<?php include 'includes/session_check.php'; ?>
<?php
    $sql_row = "SELECT * FROM tbl_block WHERE id = '".$_GET['id']."'";
    $result_row = $db->get_row($sql_row);
?>
<?php include 'includes/header.php'; ?>
<body>
    <div id="wrapper">
        <?php include('includes/navigation.php'); ?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="apps">
                            <div class="title" style="padding-bottom: 12px;">
                                <h2>Add static block</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix devicePreview" style="margin-top: -20px">
                    <div class="ipadPreviewContainer">
                        <div id="carouselOuter" class="carouselRowsManager">
                            <div id="carouselCircleLeft"></div>
                            <div id="carousel" class="mCustomScrollbar">
                            
                            <div class="row static-block-single-template-layout">
                                <div class="col-md-12">
                                    <div class="preview"><img src="<?php echo $result_row->image_url; ?>"></div>
                                    <div class="label" <?php if($result_row->has_text != 1){ ?>style="display:none"<?php } ?>><?php echo $result_row->welcome_text; ?></div>
                                </div>
                            </div>
                            
                            </div>
                            <div id="carouselCircleRight"></div>
                        </div>
                    </div>
                    <div class="iphonePreviewContainer">
                        <div id="carouselOuter" class="carouselRowsManager">
                            <div class="carouselCircleTop"></div>
                            <div class="carouselRectangleTop"></div>
                            <div class="carouselCircleTopLeft"></div>
                            <div id="carousel"><div class="row static-block-single-template-layout">
                                <div class="col-md-12">
                                    <div class="preview"><img src="<?php echo $result_row->image_url; ?>"></div>
                                    <div class="label" <?php if($result_row->has_text != 1){ ?>style="display:none"<?php } ?>><?php echo $result_row->welcome_text; ?></div>
                                </div>
                            </div>
                            </div>
                            <div class="carouselCircleBottom"></div>
                        </div>
                    </div>
                </div>
                <div class="layouts" id="static-block">
                    <div class="row">
                        <div class="col-lg-3 step step-1">
                            <div class="clearfix">
                                <span class="step-number">1</span>
                                <div class="step-detail">
                                    <h6>Choose image</h6>
                                </div>
                            </div>
                            <div class="uploader">
                                <div 
                                    id="fine-uploader-gallery-static-block" 
                                    data-option='{
                                          "layoutType":"single",
                                          "layoutTypeDB":"single",
                                          "viewTemplate":"static-block-single-layout.mt",
                                          "iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
                                          "iPhoneView": ".iphonePreviewContainer #carousel",
                                          "imagePosition": "0"
                                     }' 
                                     data-width="<?php echo $config_static_width; ?>" 
                                     data-height="<?php echo $config_static_height; ?>"
                                ></div>
                                <div class="have-image" id="single-<?php echo $result_row->id ?>">
                                    <button 
                                        type="button" 
                                        data-element-hide="#single-<?php echo $result_row->id ?>"
                                        data-button="#static-block #save_block"
                                    ><i class="fa fa-times" aria-hidden="true"></i></button>
                                    <img src="<?php echo $result_row->image_url; ?>" class="img-responsive" />
                               </div>
                            </div>
                        </div>
                        <?php /* ?>
                        <div class="col-lg-3 step step-2">
                            <div class="clearfix">
                                <span class="step-number">2</span>
                                <div class="step-detail">
                                    <h6>Choose label</h6>
                                </div>
                            </div>
                            <div class="blockLabel">
                                <div class="checkbox">
                                    <label>
                                        <input <?php if($result_row->has_text == 1){ ?>checked<?php } ?> type="checkbox" name="label_text" value="1" id="label-text" data-depend="#label-input"> Yes i have label text
                                    </label>
                                </div>
                                <div class="form-group"  id="label-input" <?php if($result_row->welcome_text == ""){ ?>style="display: none"<?php } ?>>
                                    <label for="static-block-text">Label</label>
                                    <input type="text" class="form-control" value="<?php echo $result_row->welcome_text; ?>" id="static-block-text" name="static_block_text">
                                </div>
                            </div>
                        </div>
                        <?php */ ?>
                        <div class="col-lg-3 step step-2">
                            <div class="clearfix">
                                <span class="step-number">2</span>
                                <div class="step-detail">
                                    <h6>Save block</h6>
                                </div>
                            </div>
                            <div class="saveBlock">
                                <button 
                                    data-app-id="<?php echo $result_row->app_id; ?>"
                                    data-id="<?php echo $result_row->id; ?>"
                                    data-checkbox="#static-block input[name='label_text']:checked";
                                    data-input="#static-block #static-block-text"
                                    data-ajax-url="./ajax/saveBlock.php"
                                    data-image="<?php echo $result_row->image_url; ?>"
                                    type="button" 
                                    name="save_block" 
                                    id="save_block" 
                                    disabled
                                    class="btn btn-primary"
                                >Save</button>
                                <button onClick="window.location.href='static-block.php?app_id=<?php echo $result_row->app_id; ?>';" type="button" class="btn btn-danger">Cancel</button>
                            </div>
                        </div>
                        <div class="col-lg-3 step step-4"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'includes/footer.php'; ?>
</body>
</html>