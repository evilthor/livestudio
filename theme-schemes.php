<?php include 'config.php'; ?>
<?php include 'includes/session_check.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="<?php echo bodyClass(); ?>">
    <div id="wrapper">
        <?php include 'includes/navigation.php'; ?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="apps">
                            <div class="title">
                                <h2><?php echo pageTitle(null, false); ?></h2>
                            </div>
                            <div class="clearfix">
                                <div class="row">
                               	<div class="col-lg-10" id="list">
                                        
                          		</div>
                               	<div class="col-lg-2">
                                        <a 
                                            data-data='{ "command" : "add-scheme-form" }' 
                                            data-loader="#add-scheme-form"
                                            href="#" 
                                            class="btn btn-primary btn-sm btn-block __get_popup"
                                        >Add scheme <span style="display:none" id="add-scheme-form"><i class="fa fa-spinner fa-spin"></i></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'includes/footer.php'; ?>
    <script>
    	jQuery(document).ready(function(){
            GET([{
                type: 'link',
                data: {
                    command: 'schemes-list',
                },
                callback: {
                    complete: function(XHR, textStatus) {
                        $('.theme-schemes #list').html(XHR.responseText);
                    }
                }
            }]);    		
    	})
    </script>
</body>
</html>