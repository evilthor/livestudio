<?php
	include 'config.php';
	include 'includes/session_check.php';

	if (isset($_POST["app_and_key"])):
		$devfile = $_POST['old_dev_ssl'];
		$profile = $_POST['old_pro_ssl'];
		if ($_FILES['pro_ssl']['tmp_name'] != '') {
			$targetnew = "ssl/";
			$targetnew = $targetnew . basename($_FILES['pro_ssl']['name']);
			if (move_uploaded_file($_FILES['pro_ssl']['tmp_name'], $targetnew)) {
				$profile = $targetnew;
			}
		}
		if ($_FILES['dev_ssl']['tmp_name'] != '') {
			$target = "ssl/";
			$target = $target . basename($_FILES['dev_ssl']['name']);
			if (move_uploaded_file($_FILES['dev_ssl']['tmp_name'], $target)) {
				$devfile = $target;
			}
		}
    		$sql = "UPDATE tbl_settings SET
			   app_and_key = '" . $_POST["app_and_key"] . "',
			   dev_ssl = '" . $devfile . "',
			   dev_passphrase = '" . $_POST["dev_passphrase"] . "',
			   pro_ssl = '" . $profile . "',
			   pro_passphrase = '" . $_POST["pro_passphrase"] . "'
			   WHERE id = '1'
			   ";
		$error = false;
		if ($db->query($sql) !== false) {
			header('Location:settings.php?message=update');exit;
		} else {
			$error = true;
		}
	endif;

	include 'includes/header.php';
?>
<body>
  	<div id="wrapper">
	 	<?php include 'includes/navigation.php';?>
	 	<div id="page-wrapper">
			<div class="container">
		    		<div class="row">
			   		<div class="col-lg-12">
				  		<div class="apps">
					 		<div class="title">
								<h2>Change settings</h2>
					 		</div>
						    	<div class="clearfix">
							   	<div class="row">
								  	<div class="col-sm-offset-4 col-md-4">
								 		<?php if (@$error) { ?>
									  	<p class="alert alert-danger">Error occured, Please try again later.</p>
										<?php } ?>
									 	<?php
											$sql = "SELECT * FROM tbl_settings WHERE id = '1'";
											$result = $db->get_row($sql);
										?>
										 <form id="frmAddApp" method="post" action="" enctype="multipart/form-data">
											<div class="form-group">
											    	<label for="app_and_key" class="control-label">API Key(Android)</label>
											   	<input type="text" class="form-control" id="app_and_key" name="app_and_key" value="<?php echo $result->app_and_key; ?>">
											</div>
											<div class="form-group">
											    	<label for="dev_ssl" class="control-label">SSL Dev</label>
											   	<input type="file" id="dev_ssl" name="dev_ssl">
											</div>
											<div class="form-group">
											    	<label for="dev_passphrase" class="control-label">Dev Passphrase</label>
											   	<input type="text" class="form-control" id="dev_passphrase" name="dev_passphrase" value="<?php echo $result->dev_passphrase; ?>">
											</div>
											<div class="form-group">
											    	<label for="pro_ssl" class="control-label">SSL Pro</label>
											   	<input type="file" id="pro_ssl" name="pro_ssl">
											</div>
											<div class="form-group">
											    	<label for="pro_passphrase" class="control-label">Pro Passphrase</label>
											   	<input type="text" class="form-control" id="pro_passphrase" name="pro_passphrase" value="<?php echo $result->pro_passphrase; ?>">
											</div>
										   	<input type="hidden" name="old_dev_ssl" value="<?php echo $result->dev_ssl; ?>">
										   	<input type="hidden" name="old_pro_ssl" value="<?php echo $result->pro_ssl; ?>">
										   	<button type="submit" class="btn btn-primary">Save</button>
									 	</form>
								  	</div>
							   	</div>
						    	</div>
				  		</div>
			   		</div>
		    		</div>
			</div>
	 	</div>
  	</div>
  	<?php include 'includes/footer.php';?>
</body>
</html>