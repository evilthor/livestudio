window.submitMerchandiser = {};
window.layout = null;
window.tmpAttachments = [];
window.categories = null;
window.brands = null;
window.products = null;
window.tickActive = null;
window.notification = null;
window.topSectionData = {
     buttons: new Array(),
     image: new Array(),
};
window.images = [];
window.smallPreviewArray = [];
window.flags = [
     {
          "id": "AF",
          "text": "Afghanistan"
     },
     {
          "id": "AX",
          "text": "Åland Islands"
     },
     {
          "id": "AL",
          "text": "Albania"
     },
     {
          "id": "DZ",
          "text": "Algeria"
     },
     {
          "id": "AS",
          "text": "American Samoa"
     },
     {
          "id": "AD",
          "text": "Andorra"
     },
     {
          "id": "AO",
          "text": "Angola"
     },
     {
          "id": "AI",
          "text": "Anguilla"
     },
     {
          "id": "AQ",
          "text": "Antarctica"
     },
     {
          "id": "AG",
          "text": "Antigua and Barbuda"
     },
     {
          "id": "AR",
          "text": "Argentina"
     },
     {
          "id": "AM",
          "text": "Armenia"
     },
     {
          "id": "AW",
          "text": "Aruba"
     },
     {
          "id": "AU",
          "text": "Australia"
     },
     {
          "id": "AT",
          "text": "Austria"
     },
     {
          "id": "AZ",
          "text": "Azerbaijan"
     },
     {
          "id": "BS",
          "text": "Bahamas"
     },
     {
          "id": "BH",
          "text": "Bahrain"
     },
     {
          "id": "BD",
          "text": "Bangladesh"
     },
     {
          "id": "BB",
          "text": "Barbados"
     },
     {
          "id": "BY",
          "text": "Belarus"
     },
     {
          "id": "BE",
          "text": "Belgium"
     },
     {
          "id": "BZ",
          "text": "Belize"
     },
     {
          "id": "BJ",
          "text": "Benin"
     },
     {
          "id": "BM",
          "text": "Bermuda"
     },
     {
          "id": "BT",
          "text": "Bhutan"
     },
     {
          "id": "BO",
          "text": "Bolivia, Plurinational State of"
     },
     {
          "id": "BQ",
          "text": "Bonaire, Sint Eustatius and Saba"
     },
     {
          "id": "BA",
          "text": "Bosnia and Herzegovina"
     },
     {
          "id": "BW",
          "text": "Botswana"
     },
     {
          "id": "BV",
          "text": "Bouvet Island"
     },
     {
          "id": "BR",
          "text": "Brazil"
     },
     {
          "id": "IO",
          "text": "British Indian Ocean Territory"
     },
     {
          "id": "BN",
          "text": "Brunei Darussalam"
     },
     {
          "id": "BG",
          "text": "Bulgaria"
     },
     {
          "id": "BF",
          "text": "Burkina Faso"
     },
     {
          "id": "BI",
          "text": "Burundi"
     },
     {
          "id": "KH",
          "text": "Cambodia"
     },
     {
          "id": "CM",
          "text": "Cameroon"
     },
     {
          "id": "CA",
          "text": "Canada"
     },
     {
          "id": "CV",
          "text": "Cape Verde"
     },
     {
          "id": "KY",
          "text": "Cayman Islands"
     },
     {
          "id": "CF",
          "text": "Central African Republic"
     },
     {
          "id": "TD",
          "text": "Chad"
     },
     {
          "id": "CL",
          "text": "Chile"
     },
     {
          "id": "CN",
          "text": "China"
     },
     {
          "id": "CX",
          "text": "Christmas Island"
     },
     {
          "id": "CC",
          "text": "Cocos (Keeling) Islands"
     },
     {
          "id": "CO",
          "text": "Colombia"
     },
     {
          "id": "KM",
          "text": "Comoros"
     },
     {
          "id": "CG",
          "text": "Congo"
     },
     {
          "id": "CD",
          "text": "Congo, the Democratic Republic of the"
     },
     {
          "id": "CK",
          "text": "Cook Islands"
     },
     {
          "id": "CR",
          "text": "Costa Rica"
     },
     {
          "id": "CI",
          "text": "Côte d'Ivoire"
     },
     {
          "id": "HR",
          "text": "Croatia"
     },
     {
          "id": "CU",
          "text": "Cuba"
     },
     {
          "id": "CW",
          "text": "Curaçao"
     },
     {
          "id": "CY",
          "text": "Cyprus"
     },
     {
          "id": "CZ",
          "text": "Czech Republic"
     },
     {
          "id": "DK",
          "text": "Denmark"
     },
     {
          "id": "DJ",
          "text": "Djibouti"
     },
     {
          "id": "DM",
          "text": "Dominica"
     },
     {
          "id": "DO",
          "text": "Dominican Republic"
     },
     {
          "id": "EC",
          "text": "Ecuador"
     },
     {
          "id": "EG",
          "text": "Egypt"
     },
     {
          "id": "SV",
          "text": "El Salvador"
     },
     {
          "id": "GQ",
          "text": "Equatorial Guinea"
     },
     {
          "id": "ER",
          "text": "Eritrea"
     },
     {
          "id": "EE",
          "text": "Estonia"
     },
     {
          "id": "ET",
          "text": "Ethiopia"
     },
     {
          "id": "FK",
          "text": "Falkland Islands (Malvinas)"
     },
     {
          "id": "FO",
          "text": "Faroe Islands"
     },
     {
          "id": "FJ",
          "text": "Fiji"
     },
     {
          "id": "FI",
          "text": "Finland"
     },
     {
          "id": "FR",
          "text": "France"
     },
     {
          "id": "GF",
          "text": "French Guiana"
     },
     {
          "id": "PF",
          "text": "French Polynesia"
     },
     {
          "id": "TF",
          "text": "French Southern Territories"
     },
     {
          "id": "GA",
          "text": "Gabon"
     },
     {
          "id": "GM",
          "text": "Gambia"
     },
     {
          "id": "GE",
          "text": "Georgia"
     },
     {
          "id": "DE",
          "text": "Germany"
     },
     {
          "id": "GH",
          "text": "Ghana"
     },
     {
          "id": "GI",
          "text": "Gibraltar"
     },
     {
          "id": "GR",
          "text": "Greece"
     },
     {
          "id": "GL",
          "text": "Greenland"
     },
     {
          "id": "GD",
          "text": "Grenada"
     },
     {
          "id": "GP",
          "text": "Guadeloupe"
     },
     {
          "id": "GU",
          "text": "Guam"
     },
     {
          "id": "GT",
          "text": "Guatemala"
     },
     {
          "id": "GG",
          "text": "Guernsey"
     },
     {
          "id": "GN",
          "text": "Guinea"
     },
     {
          "id": "GW",
          "text": "Guinea-Bissau"
     },
     {
          "id": "GY",
          "text": "Guyana"
     },
     {
          "id": "HT",
          "text": "Haiti"
     },
     {
          "id": "HM",
          "text": "Heard Island and McDonald Islands"
     },
     {
          "id": "VA",
          "text": "Holy See (Vatican City State)"
     },
     {
          "id": "HN",
          "text": "Honduras"
     },
     {
          "id": "HK",
          "text": "Hong Kong"
     },
     {
          "id": "HU",
          "text": "Hungary"
     },
     {
          "id": "IS",
          "text": "Iceland"
     },
     {
          "id": "IN",
          "text": "India"
     },
     {
          "id": "ID",
          "text": "Indonesia"
     },
     {
          "id": "IR",
          "text": "Iran, Islamic Republic of"
     },
     {
          "id": "IQ",
          "text": "Iraq"
     },
     {
          "id": "IE",
          "text": "Ireland"
     },
     {
          "id": "IM",
          "text": "Isle of Man"
     },
     {
          "id": "IL",
          "text": "Israel"
     },
     {
          "id": "IT",
          "text": "Italy"
     },
     {
          "id": "JM",
          "text": "Jamaica"
     },
     {
          "id": "JP",
          "text": "Japan"
     },
     {
          "id": "JE",
          "text": "Jersey"
     },
     {
          "id": "JO",
          "text": "Jordan"
     },
     {
          "id": "KZ",
          "text": "Kazakhstan"
     },
     {
          "id": "KE",
          "text": "Kenya"
     },
     {
          "id": "KI",
          "text": "Kiribati"
     },
     {
          "id": "KP",
          "text": "Korea, Democratic People's Republic of"
     },
     {
          "id": "KR",
          "text": "Korea, Republic of"
     },
     {
          "id": "KW",
          "text": "Kuwait"
     },
     {
          "id": "KG",
          "text": "Kyrgyzstan"
     },
     {
          "id": "LA",
          "text": "Lao People's Democratic Republic"
     },
     {
          "id": "LV",
          "text": "Latvia"
     },
     {
          "id": "LB",
          "text": "Lebanon"
     },
     {
          "id": "LS",
          "text": "Lesotho"
     },
     {
          "id": "LR",
          "text": "Liberia"
     },
     {
          "id": "LY",
          "text": "Libya"
     },
     {
          "id": "LI",
          "text": "Liechtenstein"
     },
     {
          "id": "LT",
          "text": "Lithuania"
     },
     {
          "id": "LU",
          "text": "Luxembourg"
     },
     {
          "id": "MO",
          "text": "Macao"
     },
     {
          "id": "MK",
          "text": "Macedonia, the Former Yugoslav Republic of"
     },
     {
          "id": "MG",
          "text": "Madagascar"
     },
     {
          "id": "MW",
          "text": "Malawi"
     },
     {
          "id": "MY",
          "text": "Malaysia"
     },
     {
          "id": "MV",
          "text": "Maldives"
     },
     {
          "id": "ML",
          "text": "Mali"
     },
     {
          "id": "MT",
          "text": "Malta"
     },
     {
          "id": "MH",
          "text": "Marshall Islands"
     },
     {
          "id": "MQ",
          "text": "Martinique"
     },
     {
          "id": "MR",
          "text": "Mauritania"
     },
     {
          "id": "MU",
          "text": "Mauritius"
     },
     {
          "id": "YT",
          "text": "Mayotte"
     },
     {
          "id": "MX",
          "text": "Mexico"
     },
     {
          "id": "FM",
          "text": "Micronesia, Federated States of"
     },
     {
          "id": "MD",
          "text": "Moldova, Republic of"
     },
     {
          "id": "MC",
          "text": "Monaco"
     },
     {
          "id": "MN",
          "text": "Mongolia"
     },
     {
          "id": "ME",
          "text": "Montenegro"
     },
     {
          "id": "MS",
          "text": "Montserrat"
     },
     {
          "id": "MA",
          "text": "Morocco"
     },
     {
          "id": "MZ",
          "text": "Mozambique"
     },
     {
          "id": "MM",
          "text": "Myanmar"
     },
     {
          "id": "NA",
          "text": "Namibia"
     },
     {
          "id": "NR",
          "text": "Nauru"
     },
     {
          "id": "NP",
          "text": "Nepal"
     },
     {
          "id": "NL",
          "text": "Netherlands"
     },
     {
          "id": "NC",
          "text": "New Caledonia"
     },
     {
          "id": "NZ",
          "text": "New Zealand"
     },
     {
          "id": "NI",
          "text": "Nicaragua"
     },
     {
          "id": "NE",
          "text": "Niger"
     },
     {
          "id": "NG",
          "text": "Nigeria"
     },
     {
          "id": "NU",
          "text": "Niue"
     },
     {
          "id": "NF",
          "text": "Norfolk Island"
     },
     {
          "id": "MP",
          "text": "Northern Mariana Islands"
     },
     {
          "id": "NO",
          "text": "Norway"
     },
     {
          "id": "OM",
          "text": "Oman"
     },
     {
          "id": "PK",
          "text": "Pakistan"
     },
     {
          "id": "PW",
          "text": "Palau"
     },
     {
          "id": "PS",
          "text": "Palestine, State of"
     },
     {
          "id": "PA",
          "text": "Panama"
     },
     {
          "id": "PG",
          "text": "Papua New Guinea"
     },
     {
          "id": "PY",
          "text": "Paraguay"
     },
     {
          "id": "PE",
          "text": "Peru"
     },
     {
          "id": "PH",
          "text": "Philippines"
     },
     {
          "id": "PN",
          "text": "Pitcairn"
     },
     {
          "id": "PL",
          "text": "Poland"
     },
     {
          "id": "PT",
          "text": "Portugal"
     },
     {
          "id": "PR",
          "text": "Puerto Rico"
     },
     {
          "id": "QA",
          "text": "Qatar"
     },
     {
          "id": "RE",
          "text": "Réunion"
     },
     {
          "id": "RO",
          "text": "Romania"
     },
     {
          "id": "RU",
          "text": "Russian Federation"
     },
     {
          "id": "RW",
          "text": "Rwanda"
     },
     {
          "id": "BL",
          "text": "Saint Barthélemy"
     },
     {
          "id": "SH",
          "text": "Saint Helena, Ascension and Tristan da Cunha"
     },
     {
          "id": "KN",
          "text": "Saint Kitts and Nevis"
     },
     {
          "id": "LC",
          "text": "Saint Lucia"
     },
     {
          "id": "MF",
          "text": "Saint Martin (French part)"
     },
     {
          "id": "PM",
          "text": "Saint Pierre and Miquelon"
     },
     {
          "id": "VC",
          "text": "Saint Vincent and the Grenadines"
     },
     {
          "id": "WS",
          "text": "Samoa"
     },
     {
          "id": "SM",
          "text": "San Marino"
     },
     {
          "id": "ST",
          "text": "Sao Tome and Principe"
     },
     {
          "id": "SA",
          "text": "Saudi Arabia"
     },
     {
          "id": "SN",
          "text": "Senegal"
     },
     {
          "id": "RS",
          "text": "Serbia"
     },
     {
          "id": "SC",
          "text": "Seychelles"
     },
     {
          "id": "SL",
          "text": "Sierra Leone"
     },
     {
          "id": "SG",
          "text": "Singapore"
     },
     {
          "id": "SX",
          "text": "Sint Maarten (Dutch part)"
     },
     {
          "id": "SK",
          "text": "Slovakia"
     },
     {
          "id": "SI",
          "text": "Slovenia"
     },
     {
          "id": "SB",
          "text": "Solomon Islands"
     },
     {
          "id": "SO",
          "text": "Somalia"
     },
     {
          "id": "ZA",
          "text": "South Africa"
     },
     {
          "id": "GS",
          "text": "South Georgia and the South Sandwich Islands"
     },
     {
          "id": "SS",
          "text": "South Sudan"
     },
     {
          "id": "ES",
          "text": "Spain"
     },
     {
          "id": "LK",
          "text": "Sri Lanka"
     },
     {
          "id": "SD",
          "text": "Sudan"
     },
     {
          "id": "SR",
          "text": "Suriname"
     },
     {
          "id": "SJ",
          "text": "Svalbard and Jan Mayen"
     },
     {
          "id": "SZ",
          "text": "Swaziland"
     },
     {
          "id": "SE",
          "text": "Sweden"
     },
     {
          "id": "CH",
          "text": "Switzerland"
     },
     {
          "id": "SY",
          "text": "Syrian Arab Republic"
     },
     {
          "id": "TW",
          "text": "Taiwan, Province of China"
     },
     {
          "id": "TJ",
          "text": "Tajikistan"
     },
     {
          "id": "TZ",
          "text": "Tanzania, United Republic of"
     },
     {
          "id": "TH",
          "text": "Thailand"
     },
     {
          "id": "TL",
          "text": "Timor-Leste"
     },
     {
          "id": "TG",
          "text": "Togo"
     },
     {
          "id": "TK",
          "text": "Tokelau"
     },
     {
          "id": "TO",
          "text": "Tonga"
     },
     {
          "id": "TT",
          "text": "Trinidad and Tobago"
     },
     {
          "id": "TN",
          "text": "Tunisia"
     },
     {
          "id": "TR",
          "text": "Turkey"
     },
     {
          "id": "TM",
          "text": "Turkmenistan"
     },
     {
          "id": "TC",
          "text": "Turks and Caicos Islands"
     },
     {
          "id": "TV",
          "text": "Tuvalu"
     },
     {
          "id": "UG",
          "text": "Uganda"
     },
     {
          "id": "UA",
          "text": "Ukraine"
     },
     {
          "id": "AE",
          "text": "United Arab Emirates"
     },
     {
          "id": "UK",
          "text": "United Kingdom"
     },
     {
          "id": "US",
          "text": "United States"
     },
     {
          "id": "UM",
          "text": "United States Minor Outlying Islands"
     },
     {
          "id": "UY",
          "text": "Uruguay"
     },
     {
          "id": "UZ",
          "text": "Uzbekistan"
     },
     {
          "id": "VU",
          "text": "Vanuatu"
     },
     {
          "id": "VE",
          "text": "Venezuela, Bolivarian Republic of"
     },
     {
          "id": "VN",
          "text": "Viet Nam"
     },
     {
          "id": "VG",
          "text": "Virgin Islands, British"
     },
     {
          "id": "VI",
          "text": "Virgin Islands, U.S."
     },
     {
          "id": "WF",
          "text": "Wallis and Futuna"
     },
     {
          "id": "EH",
          "text": "Western Sahara"
     },
     {
          "id": "YE",
          "text": "Yemen"
     },
     {
          "id": "ZM",
          "text": "Zambia"
     },
     {
          "id": "ZW",
          "text": "Zimbabwe"
     }
];
window.currencies = {
     "AED": {
          "code": "AED",
          "decimal_digits": 2,
          "name": "United Arab Emirates Dirham",
          "name_plural": "UAE dirhams",
          "rounding": 0,
          "symbol": "AED",
          "symbol_native": "د.إ.‏"
     },
     "AFN": {
          "code": "AFN",
          "decimal_digits": 0,
          "name": "Afghan Afghani",
          "name_plural": "Afghan Afghanis",
          "rounding": 0,
          "symbol": "Af",
          "symbol_native": "؋"
     },
     "ALL": {
          "code": "ALL",
          "decimal_digits": 0,
          "name": "Albanian Lek",
          "name_plural": "Albanian lekë",
          "rounding": 0,
          "symbol": "ALL",
          "symbol_native": "Lek"
     },
     "AMD": {
          "code": "AMD",
          "decimal_digits": 0,
          "name": "Armenian Dram",
          "name_plural": "Armenian drams",
          "rounding": 0,
          "symbol": "AMD",
          "symbol_native": "դր."
     },
     "ARS": {
          "code": "ARS",
          "decimal_digits": 2,
          "name": "Argentine Peso",
          "name_plural": "Argentine pesos",
          "rounding": 0,
          "symbol": "AR$",
          "symbol_native": "$"
     },
     "AUD": {
          "code": "AUD",
          "decimal_digits": 2,
          "name": "Australian Dollar",
          "name_plural": "Australian dollars",
          "rounding": 0,
          "symbol": "AU$",
          "symbol_native": "$"
     },
     "AZN": {
          "code": "AZN",
          "decimal_digits": 2,
          "name": "Azerbaijani Manat",
          "name_plural": "Azerbaijani manats",
          "rounding": 0,
          "symbol": "man.",
          "symbol_native": "ман."
     },
     "BAM": {
          "code": "BAM",
          "decimal_digits": 2,
          "name": "Bosnia-Herzegovina Convertible Mark",
          "name_plural": "Bosnia-Herzegovina convertible marks",
          "rounding": 0,
          "symbol": "KM",
          "symbol_native": "KM"
     },
     "BDT": {
          "code": "BDT",
          "decimal_digits": 2,
          "name": "Bangladeshi Taka",
          "name_plural": "Bangladeshi takas",
          "rounding": 0,
          "symbol": "Tk",
          "symbol_native": "৳"
     },
     "BGN": {
          "code": "BGN",
          "decimal_digits": 2,
          "name": "Bulgarian Lev",
          "name_plural": "Bulgarian leva",
          "rounding": 0,
          "symbol": "BGN",
          "symbol_native": "лв."
     },
     "BHD": {
          "code": "BHD",
          "decimal_digits": 3,
          "name": "Bahraini Dinar",
          "name_plural": "Bahraini dinars",
          "rounding": 0,
          "symbol": "BD",
          "symbol_native": "د.ب.‏"
     },
     "BIF": {
          "code": "BIF",
          "decimal_digits": 0,
          "name": "Burundian Franc",
          "name_plural": "Burundian francs",
          "rounding": 0,
          "symbol": "FBu",
          "symbol_native": "FBu"
     },
     "BND": {
          "code": "BND",
          "decimal_digits": 2,
          "name": "Brunei Dollar",
          "name_plural": "Brunei dollars",
          "rounding": 0,
          "symbol": "BN$",
          "symbol_native": "$"
     },
     "BOB": {
          "code": "BOB",
          "decimal_digits": 2,
          "name": "Bolivian Boliviano",
          "name_plural": "Bolivian bolivianos",
          "rounding": 0,
          "symbol": "Bs",
          "symbol_native": "Bs"
     },
     "BRL": {
          "code": "BRL",
          "decimal_digits": 2,
          "name": "Brazilian Real",
          "name_plural": "Brazilian reals",
          "rounding": 0,
          "symbol": "R$",
          "symbol_native": "R$"
     },
     "BWP": {
          "code": "BWP",
          "decimal_digits": 2,
          "name": "Botswanan Pula",
          "name_plural": "Botswanan pulas",
          "rounding": 0,
          "symbol": "BWP",
          "symbol_native": "P"
     },
     "BYR": {
          "code": "BYR",
          "decimal_digits": 0,
          "name": "Belarusian Ruble",
          "name_plural": "Belarusian rubles",
          "rounding": 0,
          "symbol": "BYR",
          "symbol_native": "BYR"
     },
     "BZD": {
          "code": "BZD",
          "decimal_digits": 2,
          "name": "Belize Dollar",
          "name_plural": "Belize dollars",
          "rounding": 0,
          "symbol": "BZ$",
          "symbol_native": "$"
     },
     "CAD": {
          "code": "CAD",
          "decimal_digits": 2,
          "name": "Canadian Dollar",
          "name_plural": "Canadian dollars",
          "rounding": 0,
          "symbol": "CA$",
          "symbol_native": "$"
     },
     "CDF": {
          "code": "CDF",
          "decimal_digits": 2,
          "name": "Congolese Franc",
          "name_plural": "Congolese francs",
          "rounding": 0,
          "symbol": "CDF",
          "symbol_native": "FrCD"
     },
     "CHF": {
          "code": "CHF",
          "decimal_digits": 2,
          "name": "Swiss Franc",
          "name_plural": "Swiss francs",
          "rounding": 0.05,
          "symbol": "CHF",
          "symbol_native": "CHF"
     },
     "CLP": {
          "code": "CLP",
          "decimal_digits": 0,
          "name": "Chilean Peso",
          "name_plural": "Chilean pesos",
          "rounding": 0,
          "symbol": "CL$",
          "symbol_native": "$"
     },
     "CNY": {
          "code": "CNY",
          "decimal_digits": 2,
          "name": "Chinese Yuan",
          "name_plural": "Chinese yuan",
          "rounding": 0,
          "symbol": "CN¥",
          "symbol_native": "CN¥"
     },
     "COP": {
          "code": "COP",
          "decimal_digits": 0,
          "name": "Colombian Peso",
          "name_plural": "Colombian pesos",
          "rounding": 0,
          "symbol": "CO$",
          "symbol_native": "$"
     },
     "CRC": {
          "code": "CRC",
          "decimal_digits": 0,
          "name": "Costa Rican Colón",
          "name_plural": "Costa Rican colóns",
          "rounding": 0,
          "symbol": "₡",
          "symbol_native": "₡"
     },
     "CVE": {
          "code": "CVE",
          "decimal_digits": 2,
          "name": "Cape Verdean Escudo",
          "name_plural": "Cape Verdean escudos",
          "rounding": 0,
          "symbol": "CV$",
          "symbol_native": "CV$"
     },
     "CZK": {
          "code": "CZK",
          "decimal_digits": 2,
          "name": "Czech Republic Koruna",
          "name_plural": "Czech Republic korunas",
          "rounding": 0,
          "symbol": "Kč",
          "symbol_native": "Kč"
     },
     "DJF": {
          "code": "DJF",
          "decimal_digits": 0,
          "name": "Djiboutian Franc",
          "name_plural": "Djiboutian francs",
          "rounding": 0,
          "symbol": "Fdj",
          "symbol_native": "Fdj"
     },
     "DKK": {
          "code": "DKK",
          "decimal_digits": 2,
          "name": "Danish Krone",
          "name_plural": "Danish kroner",
          "rounding": 0,
          "symbol": "Dkr",
          "symbol_native": "kr"
     },
     "DOP": {
          "code": "DOP",
          "decimal_digits": 2,
          "name": "Dominican Peso",
          "name_plural": "Dominican pesos",
          "rounding": 0,
          "symbol": "RD$",
          "symbol_native": "RD$"
     },
     "DZD": {
          "code": "DZD",
          "decimal_digits": 2,
          "name": "Algerian Dinar",
          "name_plural": "Algerian dinars",
          "rounding": 0,
          "symbol": "DA",
          "symbol_native": "د.ج.‏"
     },
     "EEK": {
          "code": "EEK",
          "decimal_digits": 2,
          "name": "Estonian Kroon",
          "name_plural": "Estonian kroons",
          "rounding": 0,
          "symbol": "Ekr",
          "symbol_native": "kr"
     },
     "EGP": {
          "code": "EGP",
          "decimal_digits": 2,
          "name": "Egyptian Pound",
          "name_plural": "Egyptian pounds",
          "rounding": 0,
          "symbol": "EGP",
          "symbol_native": "ج.م.‏"
     },
     "ERN": {
          "code": "ERN",
          "decimal_digits": 2,
          "name": "Eritrean Nakfa",
          "name_plural": "Eritrean nakfas",
          "rounding": 0,
          "symbol": "Nfk",
          "symbol_native": "Nfk"
     },
     "ETB": {
          "code": "ETB",
          "decimal_digits": 2,
          "name": "Ethiopian Birr",
          "name_plural": "Ethiopian birrs",
          "rounding": 0,
          "symbol": "Br",
          "symbol_native": "Br"
     },
     "EUR": {
          "code": "EUR",
          "decimal_digits": 2,
          "name": "Euro",
          "name_plural": "euros",
          "rounding": 0,
          "symbol": "€",
          "symbol_native": "€"
     },
     "GBP": {
          "code": "GBP",
          "decimal_digits": 2,
          "name": "British Pound Sterling",
          "name_plural": "British pounds sterling",
          "rounding": 0,
          "symbol": "£",
          "symbol_native": "£"
     },
     "GEL": {
          "code": "GEL",
          "decimal_digits": 2,
          "name": "Georgian Lari",
          "name_plural": "Georgian laris",
          "rounding": 0,
          "symbol": "GEL",
          "symbol_native": "GEL"
     },
     "GHS": {
          "code": "GHS",
          "decimal_digits": 2,
          "name": "Ghanaian Cedi",
          "name_plural": "Ghanaian cedis",
          "rounding": 0,
          "symbol": "GH₵",
          "symbol_native": "GH₵"
     },
     "GNF": {
          "code": "GNF",
          "decimal_digits": 0,
          "name": "Guinean Franc",
          "name_plural": "Guinean francs",
          "rounding": 0,
          "symbol": "FG",
          "symbol_native": "FG"
     },
     "GTQ": {
          "code": "GTQ",
          "decimal_digits": 2,
          "name": "Guatemalan Quetzal",
          "name_plural": "Guatemalan quetzals",
          "rounding": 0,
          "symbol": "GTQ",
          "symbol_native": "Q"
     },
     "HKD": {
          "code": "HKD",
          "decimal_digits": 2,
          "name": "Hong Kong Dollar",
          "name_plural": "Hong Kong dollars",
          "rounding": 0,
          "symbol": "HK$",
          "symbol_native": "$"
     },
     "HNL": {
          "code": "HNL",
          "decimal_digits": 2,
          "name": "Honduran Lempira",
          "name_plural": "Honduran lempiras",
          "rounding": 0,
          "symbol": "HNL",
          "symbol_native": "L"
     },
     "HRK": {
          "code": "HRK",
          "decimal_digits": 2,
          "name": "Croatian Kuna",
          "name_plural": "Croatian kunas",
          "rounding": 0,
          "symbol": "kn",
          "symbol_native": "kn"
     },
     "HUF": {
          "code": "HUF",
          "decimal_digits": 0,
          "name": "Hungarian Forint",
          "name_plural": "Hungarian forints",
          "rounding": 0,
          "symbol": "Ft",
          "symbol_native": "Ft"
     },
     "IDR": {
          "code": "IDR",
          "decimal_digits": 0,
          "name": "Indonesian Rupiah",
          "name_plural": "Indonesian rupiahs",
          "rounding": 0,
          "symbol": "Rp",
          "symbol_native": "Rp"
     },
     "ILS": {
          "code": "ILS",
          "decimal_digits": 2,
          "name": "Israeli New Sheqel",
          "name_plural": "Israeli new sheqels",
          "rounding": 0,
          "symbol": "₪",
          "symbol_native": "₪"
     },
     "INR": {
          "code": "INR",
          "decimal_digits": 2,
          "name": "Indian Rupee",
          "name_plural": "Indian rupees",
          "rounding": 0,
          "symbol": "Rs",
          "symbol_native": "টকা"
     },
     "IQD": {
          "code": "IQD",
          "decimal_digits": 0,
          "name": "Iraqi Dinar",
          "name_plural": "Iraqi dinars",
          "rounding": 0,
          "symbol": "IQD",
          "symbol_native": "د.ع.‏"
     },
     "IRR": {
          "code": "IRR",
          "decimal_digits": 0,
          "name": "Iranian Rial",
          "name_plural": "Iranian rials",
          "rounding": 0,
          "symbol": "IRR",
          "symbol_native": "﷼"
     },
     "ISK": {
          "code": "ISK",
          "decimal_digits": 0,
          "name": "Icelandic Króna",
          "name_plural": "Icelandic krónur",
          "rounding": 0,
          "symbol": "Ikr",
          "symbol_native": "kr"
     },
     "JMD": {
          "code": "JMD",
          "decimal_digits": 2,
          "name": "Jamaican Dollar",
          "name_plural": "Jamaican dollars",
          "rounding": 0,
          "symbol": "J$",
          "symbol_native": "$"
     },
     "JOD": {
          "code": "JOD",
          "decimal_digits": 3,
          "name": "Jordanian Dinar",
          "name_plural": "Jordanian dinars",
          "rounding": 0,
          "symbol": "JD",
          "symbol_native": "د.أ.‏"
     },
     "JPY": {
          "code": "JPY",
          "decimal_digits": 0,
          "name": "Japanese Yen",
          "name_plural": "Japanese yen",
          "rounding": 0,
          "symbol": "¥",
          "symbol_native": "￥"
     },
     "KES": {
          "code": "KES",
          "decimal_digits": 2,
          "name": "Kenyan Shilling",
          "name_plural": "Kenyan shillings",
          "rounding": 0,
          "symbol": "Ksh",
          "symbol_native": "Ksh"
     },
     "KHR": {
          "code": "KHR",
          "decimal_digits": 2,
          "name": "Cambodian Riel",
          "name_plural": "Cambodian riels",
          "rounding": 0,
          "symbol": "KHR",
          "symbol_native": "៛"
     },
     "KMF": {
          "code": "KMF",
          "decimal_digits": 0,
          "name": "Comorian Franc",
          "name_plural": "Comorian francs",
          "rounding": 0,
          "symbol": "CF",
          "symbol_native": "FC"
     },
     "KRW": {
          "code": "KRW",
          "decimal_digits": 0,
          "name": "South Korean Won",
          "name_plural": "South Korean won",
          "rounding": 0,
          "symbol": "₩",
          "symbol_native": "₩"
     },
     "KWD": {
          "code": "KWD",
          "decimal_digits": 3,
          "name": "Kuwaiti Dinar",
          "name_plural": "Kuwaiti dinars",
          "rounding": 0,
          "symbol": "KD",
          "symbol_native": "د.ك.‏"
     },
     "KZT": {
          "code": "KZT",
          "decimal_digits": 2,
          "name": "Kazakhstani Tenge",
          "name_plural": "Kazakhstani tenges",
          "rounding": 0,
          "symbol": "KZT",
          "symbol_native": "тңг."
     },
     "LBP": {
          "code": "LBP",
          "decimal_digits": 0,
          "name": "Lebanese Pound",
          "name_plural": "Lebanese pounds",
          "rounding": 0,
          "symbol": "LB£",
          "symbol_native": "ل.ل.‏"
     },
     "LKR": {
          "code": "LKR",
          "decimal_digits": 2,
          "name": "Sri Lankan Rupee",
          "name_plural": "Sri Lankan rupees",
          "rounding": 0,
          "symbol": "SLRs",
          "symbol_native": "SL Re"
     },
     "LTL": {
          "code": "LTL",
          "decimal_digits": 2,
          "name": "Lithuanian Litas",
          "name_plural": "Lithuanian litai",
          "rounding": 0,
          "symbol": "Lt",
          "symbol_native": "Lt"
     },
     "LVL": {
          "code": "LVL",
          "decimal_digits": 2,
          "name": "Latvian Lats",
          "name_plural": "Latvian lati",
          "rounding": 0,
          "symbol": "Ls",
          "symbol_native": "Ls"
     },
     "LYD": {
          "code": "LYD",
          "decimal_digits": 3,
          "name": "Libyan Dinar",
          "name_plural": "Libyan dinars",
          "rounding": 0,
          "symbol": "LD",
          "symbol_native": "د.ل.‏"
     },
     "MAD": {
          "code": "MAD",
          "decimal_digits": 2,
          "name": "Moroccan Dirham",
          "name_plural": "Moroccan dirhams",
          "rounding": 0,
          "symbol": "MAD",
          "symbol_native": "د.م.‏"
     },
     "MDL": {
          "code": "MDL",
          "decimal_digits": 2,
          "name": "Moldovan Leu",
          "name_plural": "Moldovan lei",
          "rounding": 0,
          "symbol": "MDL",
          "symbol_native": "MDL"
     },
     "MGA": {
          "code": "MGA",
          "decimal_digits": 0,
          "name": "Malagasy Ariary",
          "name_plural": "Malagasy Ariaries",
          "rounding": 0,
          "symbol": "MGA",
          "symbol_native": "MGA"
     },
     "MKD": {
          "code": "MKD",
          "decimal_digits": 2,
          "name": "Macedonian Denar",
          "name_plural": "Macedonian denari",
          "rounding": 0,
          "symbol": "MKD",
          "symbol_native": "MKD"
     },
     "MMK": {
          "code": "MMK",
          "decimal_digits": 0,
          "name": "Myanma Kyat",
          "name_plural": "Myanma kyats",
          "rounding": 0,
          "symbol": "MMK",
          "symbol_native": "K"
     },
     "MOP": {
          "code": "MOP",
          "decimal_digits": 2,
          "name": "Macanese Pataca",
          "name_plural": "Macanese patacas",
          "rounding": 0,
          "symbol": "MOP$",
          "symbol_native": "MOP$"
     },
     "MUR": {
          "code": "MUR",
          "decimal_digits": 0,
          "name": "Mauritian Rupee",
          "name_plural": "Mauritian rupees",
          "rounding": 0,
          "symbol": "MURs",
          "symbol_native": "MURs"
     },
     "MXN": {
          "code": "MXN",
          "decimal_digits": 2,
          "name": "Mexican Peso",
          "name_plural": "Mexican pesos",
          "rounding": 0,
          "symbol": "MX$",
          "symbol_native": "$"
     },
     "MYR": {
          "code": "MYR",
          "decimal_digits": 2,
          "name": "Malaysian Ringgit",
          "name_plural": "Malaysian ringgits",
          "rounding": 0,
          "symbol": "RM",
          "symbol_native": "RM"
     },
     "MZN": {
          "code": "MZN",
          "decimal_digits": 2,
          "name": "Mozambican Metical",
          "name_plural": "Mozambican meticals",
          "rounding": 0,
          "symbol": "MTn",
          "symbol_native": "MTn"
     },
     "NAD": {
          "code": "NAD",
          "decimal_digits": 2,
          "name": "Namibian Dollar",
          "name_plural": "Namibian dollars",
          "rounding": 0,
          "symbol": "N$",
          "symbol_native": "N$"
     },
     "NGN": {
          "code": "NGN",
          "decimal_digits": 2,
          "name": "Nigerian Naira",
          "name_plural": "Nigerian nairas",
          "rounding": 0,
          "symbol": "₦",
          "symbol_native": "₦"
     },
     "NIO": {
          "code": "NIO",
          "decimal_digits": 2,
          "name": "Nicaraguan Córdoba",
          "name_plural": "Nicaraguan córdobas",
          "rounding": 0,
          "symbol": "C$",
          "symbol_native": "C$"
     },
     "NOK": {
          "code": "NOK",
          "decimal_digits": 2,
          "name": "Norwegian Krone",
          "name_plural": "Norwegian kroner",
          "rounding": 0,
          "symbol": "Nkr",
          "symbol_native": "kr"
     },
     "NPR": {
          "code": "NPR",
          "decimal_digits": 2,
          "name": "Nepalese Rupee",
          "name_plural": "Nepalese rupees",
          "rounding": 0,
          "symbol": "NPRs",
          "symbol_native": "नेरू"
     },
     "NZD": {
          "code": "NZD",
          "decimal_digits": 2,
          "name": "New Zealand Dollar",
          "name_plural": "New Zealand dollars",
          "rounding": 0,
          "symbol": "NZ$",
          "symbol_native": "$"
     },
     "OMR": {
          "code": "OMR",
          "decimal_digits": 3,
          "name": "Omani Rial",
          "name_plural": "Omani rials",
          "rounding": 0,
          "symbol": "OMR",
          "symbol_native": "ر.ع.‏"
     },
     "PAB": {
          "code": "PAB",
          "decimal_digits": 2,
          "name": "Panamanian Balboa",
          "name_plural": "Panamanian balboas",
          "rounding": 0,
          "symbol": "B/.",
          "symbol_native": "B/."
     },
     "PEN": {
          "code": "PEN",
          "decimal_digits": 2,
          "name": "Peruvian Nuevo Sol",
          "name_plural": "Peruvian nuevos soles",
          "rounding": 0,
          "symbol": "S/.",
          "symbol_native": "S/."
     },
     "PHP": {
          "code": "PHP",
          "decimal_digits": 2,
          "name": "Philippine Peso",
          "name_plural": "Philippine pesos",
          "rounding": 0,
          "symbol": "₱",
          "symbol_native": "₱"
     },
     "PKR": {
          "code": "PKR",
          "decimal_digits": 0,
          "name": "Pakistani Rupee",
          "name_plural": "Pakistani rupees",
          "rounding": 0,
          "symbol": "PKRs",
          "symbol_native": "₨"
     },
     "PLN": {
          "code": "PLN",
          "decimal_digits": 2,
          "name": "Polish Zloty",
          "name_plural": "Polish zlotys",
          "rounding": 0,
          "symbol": "zł",
          "symbol_native": "zł"
     },
     "PYG": {
          "code": "PYG",
          "decimal_digits": 0,
          "name": "Paraguayan Guarani",
          "name_plural": "Paraguayan guaranis",
          "rounding": 0,
          "symbol": "₲",
          "symbol_native": "₲"
     },
     "QAR": {
          "code": "QAR",
          "decimal_digits": 2,
          "name": "Qatari Rial",
          "name_plural": "Qatari rials",
          "rounding": 0,
          "symbol": "QR",
          "symbol_native": "ر.ق.‏"
     },
     "RON": {
          "code": "RON",
          "decimal_digits": 2,
          "name": "Romanian Leu",
          "name_plural": "Romanian lei",
          "rounding": 0,
          "symbol": "RON",
          "symbol_native": "RON"
     },
     "RSD": {
          "code": "RSD",
          "decimal_digits": 0,
          "name": "Serbian Dinar",
          "name_plural": "Serbian dinars",
          "rounding": 0,
          "symbol": "din.",
          "symbol_native": "дин."
     },
     "RUB": {
          "code": "RUB",
          "decimal_digits": 2,
          "name": "Russian Ruble",
          "name_plural": "Russian rubles",
          "rounding": 0,
          "symbol": "RUB",
          "symbol_native": "руб."
     },
     "RWF": {
          "code": "RWF",
          "decimal_digits": 0,
          "name": "Rwandan Franc",
          "name_plural": "Rwandan francs",
          "rounding": 0,
          "symbol": "RWF",
          "symbol_native": "FR"
     },
     "SAR": {
          "code": "SAR",
          "decimal_digits": 2,
          "name": "Saudi Riyal",
          "name_plural": "Saudi riyals",
          "rounding": 0,
          "symbol": "SR",
          "symbol_native": "ر.س.‏"
     },
     "SDG": {
          "code": "SDG",
          "decimal_digits": 2,
          "name": "Sudanese Pound",
          "name_plural": "Sudanese pounds",
          "rounding": 0,
          "symbol": "SDG",
          "symbol_native": "SDG"
     },
     "SEK": {
          "code": "SEK",
          "decimal_digits": 2,
          "name": "Swedish Krona",
          "name_plural": "Swedish kronor",
          "rounding": 0,
          "symbol": "Skr",
          "symbol_native": "kr"
     },
     "SGD": {
          "code": "SGD",
          "decimal_digits": 2,
          "name": "Singapore Dollar",
          "name_plural": "Singapore dollars",
          "rounding": 0,
          "symbol": "S$",
          "symbol_native": "$"
     },
     "SOS": {
          "code": "SOS",
          "decimal_digits": 0,
          "name": "Somali Shilling",
          "name_plural": "Somali shillings",
          "rounding": 0,
          "symbol": "Ssh",
          "symbol_native": "Ssh"
     },
     "SYP": {
          "code": "SYP",
          "decimal_digits": 0,
          "name": "Syrian Pound",
          "name_plural": "Syrian pounds",
          "rounding": 0,
          "symbol": "SY£",
          "symbol_native": "ل.س.‏"
     },
     "THB": {
          "code": "THB",
          "decimal_digits": 2,
          "name": "Thai Baht",
          "name_plural": "Thai baht",
          "rounding": 0,
          "symbol": "฿",
          "symbol_native": "฿"
     },
     "TND": {
          "code": "TND",
          "decimal_digits": 3,
          "name": "Tunisian Dinar",
          "name_plural": "Tunisian dinars",
          "rounding": 0,
          "symbol": "DT",
          "symbol_native": "د.ت.‏"
     },
     "TOP": {
          "code": "TOP",
          "decimal_digits": 2,
          "name": "Tongan Paʻanga",
          "name_plural": "Tongan paʻanga",
          "rounding": 0,
          "symbol": "T$",
          "symbol_native": "T$"
     },
     "TRY": {
          "code": "TRY",
          "decimal_digits": 2,
          "name": "Turkish Lira",
          "name_plural": "Turkish Lira",
          "rounding": 0,
          "symbol": "TL",
          "symbol_native": "TL"
     },
     "TTD": {
          "code": "TTD",
          "decimal_digits": 2,
          "name": "Trinidad and Tobago Dollar",
          "name_plural": "Trinidad and Tobago dollars",
          "rounding": 0,
          "symbol": "TT$",
          "symbol_native": "$"
     },
     "TWD": {
          "code": "TWD",
          "decimal_digits": 2,
          "name": "New Taiwan Dollar",
          "name_plural": "New Taiwan dollars",
          "rounding": 0,
          "symbol": "NT$",
          "symbol_native": "NT$"
     },
     "TZS": {
          "code": "TZS",
          "decimal_digits": 0,
          "name": "Tanzanian Shilling",
          "name_plural": "Tanzanian shillings",
          "rounding": 0,
          "symbol": "TSh",
          "symbol_native": "TSh"
     },
     "UAH": {
          "code": "UAH",
          "decimal_digits": 2,
          "name": "Ukrainian Hryvnia",
          "name_plural": "Ukrainian hryvnias",
          "rounding": 0,
          "symbol": "₴",
          "symbol_native": "₴"
     },
     "UGX": {
          "code": "UGX",
          "decimal_digits": 0,
          "name": "Ugandan Shilling",
          "name_plural": "Ugandan shillings",
          "rounding": 0,
          "symbol": "USh",
          "symbol_native": "USh"
     },
     "USD": {
          "code": "USD",
          "decimal_digits": 2,
          "name": "US Dollar",
          "name_plural": "US dollars",
          "rounding": 0,
          "symbol": "$",
          "symbol_native": "$"
     },
     "UYU": {
          "code": "UYU",
          "decimal_digits": 2,
          "name": "Uruguayan Peso",
          "name_plural": "Uruguayan pesos",
          "rounding": 0,
          "symbol": "$U",
          "symbol_native": "$"
     },
     "UZS": {
          "code": "UZS",
          "decimal_digits": 0,
          "name": "Uzbekistan Som",
          "name_plural": "Uzbekistan som",
          "rounding": 0,
          "symbol": "UZS",
          "symbol_native": "UZS"
     },
     "VEF": {
          "code": "VEF",
          "decimal_digits": 2,
          "name": "Venezuelan Bolívar",
          "name_plural": "Venezuelan bolívars",
          "rounding": 0,
          "symbol": "Bs.F.",
          "symbol_native": "Bs.F."
     },
     "VND": {
          "code": "VND",
          "decimal_digits": 0,
          "name": "Vietnamese Dong",
          "name_plural": "Vietnamese dong",
          "rounding": 0,
          "symbol": "₫",
          "symbol_native": "₫"
     },
     "XAF": {
          "code": "XAF",
          "decimal_digits": 0,
          "name": "CFA Franc BEAC",
          "name_plural": "CFA francs BEAC",
          "rounding": 0,
          "symbol": "FCFA",
          "symbol_native": "FCFA"
     },
     "XOF": {
          "code": "XOF",
          "decimal_digits": 0,
          "name": "CFA Franc BCEAO",
          "name_plural": "CFA francs BCEAO",
          "rounding": 0,
          "symbol": "CFA",
          "symbol_native": "CFA"
     },
     "YER": {
          "code": "YER",
          "decimal_digits": 0,
          "name": "Yemeni Rial",
          "name_plural": "Yemeni rials",
          "rounding": 0,
          "symbol": "YR",
          "symbol_native": "ر.ي.‏"
     },
     "ZAR": {
          "code": "ZAR",
          "decimal_digits": 2,
          "name": "South African Rand",
          "name_plural": "South African rand",
          "rounding": 0,
          "symbol": "R",
          "symbol_native": "R"
     },
     "ZMK": {
          "code": "ZMK",
          "decimal_digits": 0,
          "name": "Zambian Kwacha",
          "name_plural": "Zambian kwachas",
          "rounding": 0,
          "symbol": "ZK",
          "symbol_native": "ZK"
     }
};