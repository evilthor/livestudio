$(function() {
	jQuery('#inner-noti span.content').each(function(index, ele) {
		var myregex = /[\u0600-\u06FF]/;
		var text = jQuery(ele).text();
		var matches = text.match(myregex);
		if (jQuery.type(matches) !== "null") {
			console.log();
			jQuery($(ele).parent()).addClass('arabic');
		}
	});
	setInterval(function() {
		if (Object.keys(tmpAttachments).length) {
			notification.require('content', JSON.stringify(tmpAttachments));
		}
	}, 1);
	$(document).on('click', '#notificationSaveRow', function() {
		var apps = notification.data.apps;
		var title = notification.data.title;
		var message = notification.data.message;
		var content = notification.data.content;
		var platform = jQuery('#platform').val();
		var dateTime = "";
		var date = "";
		var time = "";
		if (notification.data.dateTime) {
			dateTime = notification.data.dateTime;
			date = notification.data.date;
			time = notification.data.time;
		}
		//alert(dateTime); return;    
		var app_id = $('#app_id').val();
		jQuery('#push-loader').show();
		$('#notificationSaveRow').attr('disabled', 'disabled');
		$.ajax({
			method: "POST",
			url: 'ajax/notifications.php',
			data: {
				"apps": apps,
				"title": title,
				"message": message,
				"content": content,
				"platform": platform,
				"dateTime": dateTime,
				"date": date,
				"time": time
			},
			cache: false,
			dataType: 'html',
			beforeSend: function() {},
			success: function(res) {
				window.location.href = 'notifications.php?app_id=' + app_id;
			}
		});
	});
	$('#container').jstree({
		"core": {
			"themes": {},
			"animation": 0,
		},
		"plugins": ["checkbox"]
	}).on("changed.jstree", function(e, data) {
		var i, j, r = [];
		for (i = 0, j = data.selected.length; i < j; i++) {
			r.push(data.instance.get_node(data.selected[i]).id.trim());
		}
		notification.require('apps', r.join(','));
	});
	$('#notification-title').on('blur keyup', function() {
		notification.require('title', $(this).val());
	});
	$('#notification-message').on('blur keyup', function() {
		notification.require('message', $(this).val());
	});
	$(document).on('click', '.notification .contentDdPopup .ddList li a', function() {
		var page = $(this).attr('data-page-show'),
			current = $(this).attr('data-page-current'),
			list = $(this).attr('data-list-hide'),
			val = $(this).attr('data-val'),
			section = $(this).attr('data-page'),
			img_index = 0;
		if (val == 4) {
			$(this).addClass('tick-active');
			if (tickActive != null) {
				tickActive.addClass('tick-active');
			}
			tmpAttachments = [];
			tmpAttachments[0] = val;
			tmpAttachments[1] = "Static Image";
		}
		if (section !== undefined) {
			$(section).hide();
		}
		if (current !== undefined) {
			$(current).show();
		}
		$(page).show(100, function() {
			$(list).hide();
			if (val == 1) {
				$("input[name='category']").click(function() {
					var _this = $(this),
						_getText = _this.data('text'),
						_text = $(_getText).text();
					if (tickActive != null) {
						tickActive.addClass('tick-active');
					}
					tmpAttachments = [];
					tmpAttachments[0] = val;
					tmpAttachments[1] = _this.val();
					tmpAttachments[2] = _text.trim();
					$('.contentDdPopup, .ddPopup').hide();
				});
			} else if (val == 2) {
				$(document).delegate("a.btn-xsProduct", "click", function() {
					if (tickActive != null) {
						tickActive.addClass('tick-active');
					}
					tmpAttachments = [];
					tmpAttachments[0] = val;
					tmpAttachments[1] = $(this).attr("data-productId");
					tmpAttachments[2] = $(this).attr("data-productName");
					$('.contentDdPopup, .ddPopup').hide();
				});
			} else if (val == 3) {
				$("#customurl").blur(function() {
					var _this = $(this),
						_url = _this.val(),
						_isValid = validateUrl(_url),
						_button = _this.data('button');
					if (_isValid === true) {
						_this.addClass('valid').removeClass('invalid');
						if (tickActive != null) {
							tickActive.addClass('tick-active');
						}
						tmpAttachments = [];
						tmpAttachments[0] = val;
						tmpAttachments[1] = _url;
						tmpAttachments[2] = "External URL";
						//$('.contentDdPopup, .ddPopup').hide();
						$('.error-url').remove();
						$(_button).removeAttr('disabled');
					} else {
						_this.addClass('invalid').removeClass('valid');
						$('.error-url').remove();
						_this.after('<span class="error error-url">Please enter valid URL</span>');
						$('#saveRow').attr('disabled', 'disabled');
						$(_button).attr('disabled', 'disabled');
					}
				});
			} else if (val == 5) {
				$("input[name='brand']").click(function() {
					var _this = $(this),
						_getText = _this.data('text'),
						_text = $(_getText).text();
					if (tickActive != null) {
						tickActive.addClass('tick-active');
					}
					tmpAttachments = [];
					tmpAttachments[0] = val;
					tmpAttachments[1] = _this.val();
					tmpAttachments[2] = _text.trim();
					$('.contentDdPopup, .ddPopup').hide();
				});
			}
		});
	});
});