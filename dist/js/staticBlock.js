$(function() {
	jQuery(document).ready(function($) {
		$('#static-block-text').on('blur keyup keydwon focus', function(event) {
			$(document).find('.static-block-single-template-layout .label').html($(this).val().trim());
		});
	});
	$(document).on("click", "#static-block #save_block", function() {
		var _this = $(this),
			data = _this.data();
		saveBlock(data);
		return false;
	});
	$(document).on("click", "input[name='is_published']", function() {
		var _this = $(this),
			data = _this.data();
		$("input[name='is_published']").not(_this).removeAttr('checked');
		if (_this.attr('checked') != undefined) {
			return false;
		}
		$.ajax({
			method: "POST",
			url: './ajax/updateBlockStatus.php',
			cache: false,
			dataType: 'json',
			data: {
				"id": data.id,
				"app_id": data.appId
			},
			success: function(res) {
				_this.attr('checked', true);
				$(data.parentEle).notify(res.message, {
					position: 'top right',
					className: res.type,
					showAnimation: 'fadeIn',
					hideAnimation: 'fadeOut',
				});
			},
		});
	});
	if ($('#fine-uploader-gallery-static-block').length) {
		var minWidth, minHeight, image_path;
		var galleryUploader = new qq.FineUploader({
			element: document.getElementById("fine-uploader-gallery-static-block"),
			template: 'qq-simple-thumbnails-template',
			debug: false,
			multiple: false,
			request: {
				endpoint: './vendor/fineuploader/php-traditional-server/endpoint.php',
			},
			deleteFile: {
				enabled: true,
				endpoint: "./vendor/fineuploader/php-traditional-server/endpoint.php"
			},
			thumbnails: {
				placeholders: {
					waitingPath: './bower_components/fine-uploader/dist/placeholders/waiting-generic.png',
					notAvailablePath: './bower_components/fine-uploader/dist/placeholders/not_available-generic.png'
				}
			},
			validation: {
				allowedExtensions: ['jpeg', 'jpg', 'gif', 'png'],
			},
			failedUploadTextDisplay: {
				mode: 'custom'
			},
			showMessage: function(msg) {
				$.alert({
					title: 'Message',
					content: msg,
					type: 'red',
				});
			},
			callbacks: {
				onCancel: function(id, name) {
					var element = $(this)[0]._options.element,
						eleID = '#' + $(element).attr('id'),
						options = $(eleID).attr('data-option');
					$(eleID + ' .qq-upload-button').show();
					$(eleID + ' .qq-upload-list').hide();
				},
				onSubmit: function(id, name) {
					var details = $(this);
					var promise = new qq.Promise();
					var file = this.getFile(id);
					var image = new Image();
					var url = window.URL && window.URL.createObjectURL ? window.URL : window.webkitURL && window.webkitURL.createObjectURL ? window.webkitURL : null;
					image.onload = function() {
						var vali = my_custom_validation(this.width, this.height, details),
							require = {
								width: $(details[0]._options.element).attr('data-width'),
								height: $(details[0]._options.element).attr('data-height'),
							};
						if (vali) {
							promise.success();
						} else {
							promise.failure();
							$.alert({
								title: 'Message',
								content: 'The required image size is ' + require.width + 'px x ' + require.height + 'px',
								type: 'red',
							});
						}
					};
					image.src = url.createObjectURL(file);
					return promise;
					var file = this.getFile(id);
				},
				onDeleteComplete: function(id, xhr, isError) {
					var element = $(this)[0]._options.element,
						eleID = '#' + $(element).attr('id'),
						options = $(eleID).attr('data-option');
					options = $.parseJSON(options);
					$(options.iPadView + " .mCSB_container, " + options.iPhoneView + " .mCSB_container").html('');
					$(eleID + ' .qq-upload-button').show();
					$(eleID + ' .qq-upload-list').hide();
					$(options.smallPreview).html('');
					$('#save_block').attr("disabled", "disabled").removeData('image');
					// $('#label-text').attr("disabled", "disabled");
				},
				onComplete: function(id, name, responseJSON, xhr) {
					var element = $(this)[0]._options.element,
						eleID = '#' + $(element).attr('id'),
						options = $(eleID).attr('data-option');
					options = $.parseJSON(options);
					layout = options.layoutTypeDB;
					$(eleID + ' .qq-upload-button').hide();
					$(eleID + ' .qq-upload-list').show();
					var uuid = responseJSON.uuid;
					var fname = responseJSON.uploadName;
					image_path = "./vendor/fineuploader/php-traditional-server/files/" + uuid + "/" + fname;
					if (options.imagePosition == 0) {
						images[0] = image_path;
					} else if (options.imagePosition == 1) {
						images[1] = image_path;
					} else if (options.imagePosition == 2) {
						images[2] = image_path;
					}
					var length = 0;
					for (var cc = 0; cc < images.length; cc++) {
						if (images[cc] != undefined) {
							length++;
						}
					}
					$('#save_block').removeAttr('disabled').data('image', images[0]);
					// $('#label-text').removeAttr('disabled');
					window.smallPreviewArray[options.imagePosition] = options.smallPreview;
					templateRendering(options, images);
				},
				onError: function(id, name, errorReason, xhr) {
					//console.log(id, name, errorReason, xhr);
				}
			}
		});
	}
});