window.notification = {
    button: 'button#notificationSaveRow',
    data: {},
    init: function() {
        this.disabled();
    },
    empty: function(key, value) {
        if (value == '') {
            delete this.data[key];
        } else {
            this.data[key] = value;
        }
    },
    require: function(key, value) {
        this.empty(key, value);
        this.disabled();
    },
    disabled: function() {
        $(this.button).removeAttr('disabled');
        if (Object.keys(this.data).length < 4) {
            $(this.button).attr('disabled', 'disabled');
        }
    }
};