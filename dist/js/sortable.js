jQuery(document).ready(function($) {
     $("#sortable").sortable({
          // axis: 'x',
          cursor: "move",
          update: function(event, ui) {
               var data = $(this).sortable('toArray'),
                    order = $('input[name="row_order"]');
               order.val(data.join(','));
               data = $('#sortableForm').serialize();
               var url = './ajax/saveData.php';
               var path = window.location.pathname;
               var page = path.split("/").pop();
               if (page == 'add-category.php' || page == 'edit-category.php') {
                    var url = './ajax/saveDataCat.php';
               }
               $.ajax({
                    method: "POST",
                    url: url,
                    data: data,
                    cache: false,
                    dataType: 'html',
                    beforeSend: function() {},
                    success: function(res) {
                         if (res != '') {
                              res = $.parseJSON(res);
                              var ipad = $('.ipadPreviewContainer #carousel .mCSB_container');
                              if( ipad.length ){
                                   ipad.html(res.html);
                              }else{
                                   $('.ipadPreviewContainer #carousel').html(res.html).mCustomScrollbar();
                              }
                              var iphone = $('.iphonePreviewContainer #carousel .mCSB_container');
                              if(iphone.length){
                                   iphone.html(res.html);
                              }else{
                                   $('.iphonePreviewContainer #carousel').html(res.html).mCustomScrollbar();
                              }
                         }
                         $.notify(res.message.message, {
                              position: 'top right',
                              className: res.message.type,
                              showAnimation: 'fadeIn',
                              hideAnimation: 'fadeOut',
                         });
                    }
               });
          }
     });
});