function my_custom_validation(imageWidth, imageHeight, data) {
    if (imageWidth == $(data[0]._options.element).attr('data-width') && imageHeight == $(data[0]._options.element).attr('data-height')) {
        return true;
    } else {
        return false;
    }
}
var templateRendering = function(options, images) {
    var length = 0,
        object = {};
    for (var cc = 0; cc < images.length; cc++) {
        if (images[cc] != undefined) {
            length++;
        }
    }
    if (options.viewTemplate != '' && options.viewTemplate !== undefined) {
        if (options.layoutType == 'single' && length == 1) {
            object.perviewOne = images[options.imagePosition];
            $('#btnDoneImageUpload').removeAttr('disabled');
            $('#smallImgOverlay').show();
            rendered = getTemplate(options, object);
            var img = new Image();
            img.src = images[0];
            $(window.smallPreviewArray[0]).html(img);
        } else if (options.layoutType == 'double' && length == 2) {
            object.perviewOne = images[0];
            object.perviewTwo = images[1];
            $('#btnDoneImageUpload').removeAttr('disabled');
            $('#smallImgOverlay').show();
            rendered = getTemplate(options, object);
            var img = new Image();
            img.src = images[0];
            $(window.smallPreviewArray[0]).html(img);
            var img = new Image();
            img.src = images[1];
            $(window.smallPreviewArray[1]).html(img);
        } else if (options.layoutType == 'triple' && length == 3) {
            object.perviewOne = images[0];
            object.perviewTwo = images[1];
            object.perviewThree = images[2];
            $('#btnDoneImageUpload').removeAttr('disabled');
            $('#smallImgOverlay').show();
            rendered = getTemplate(options, object);
            var img = new Image();
            img.src = images[0];
            $(window.smallPreviewArray[0]).html(img);
            var img = new Image();
            img.src = images[1];
            $(window.smallPreviewArray[1]).html(img);
            var img = new Image();
            img.src = images[2];
            $(window.smallPreviewArray[2]).html(img);
        }
        return true;
    } else {
        return false;
    }
}
var getTemplate = function(options, object) {
    $.get('./templates/' + options.viewTemplate, function(template) {
        var rendered = Mustache.render(template, object);
        $(options.iPadView + " .mCSB_container, " + options.iPhoneView + " .mCSB_container").html(rendered);
    })
};

function addMerchandise(app_id, merchandise_id, active) {
    var title = $('#merchandise_title').val().trim();
    $.ajax({
        method: "POST",
        url: './ajax/saveMerchandise.php',
        cache: false,
        data: {
            "id": merchandise_id,
            "title": title,
            "app_id": app_id,
        },
        beforeSend: function() {
            /*$(element).html('<div class="loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');*/
        },
        success: function(res) {
            if (res.trim() == "error") {
                $.alert('Error occured, please try again later.');
            } else {
                window.location.href = 'add-merchandise.php?app_id=' + app_id + '&id=' + res + '&active=' + active;
            }
        },
    });
}

function addLander(data) {
    if (typeof data !== "object" && data === '') return false;
    var title = $(data.titleField).val().trim(),
        category = $(data.dropdownField).select2('data')[0],
        lander_id = data.id,
        app_id = data.app,
        active = data.column;
    if (title == "") {
        $(data.titleField).parents('.form-group').addClass('has-error');
        $.alert({
            title: 'Alert',
            content: 'Title is reqired',
            onClose: function() {
                $(data.titleField).focus();
            },
        });
        return false;
    } else {
        $(data.titleField).parents('.form-group').removeClass('has-error');
    }
    if (category === undefined || category.id == "") {
        $(data.dropdownField).parents('.form-group').addClass('has-error');
        $.alert({
            title: 'Alert',
            content: 'Category is reqired',
            onClose: function() {
                $(data.dropdownField).select2('open');
            },
        });
        return false;
    } else {
        $(data.dropdownField).parents('.form-group').removeClass('has-error');
    }
    $.ajax({
        method: "POST",
        url: './ajax/saveLander.php',
        cache: false,
        data: {
            "id": lander_id,
            "title": title,
            "app_id": app_id,
            "cat_id": category.id,
            "cat_name": category.title,
            "cat_relation": category.text,
        },
        beforeSend: function() {
            /*$(element).html('<div class="loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');*/
        },
        success: function(res) {
            console.log(res);
            if (res.trim() == "error") {
                $.alert('Error occured, please try again later.');
            } else {
                window.location.href = 'add-lander.php?app_id=' + app_id + '&id=' + res + '&active=' + active;
            }
        },
    });
    return false;
}

function addCategory(app_id, merchandise_id, active) {
    var title = $('#merchandise_title').val().trim();
    $.ajax({
        method: "POST",
        url: './ajax/saveCategory.php',
        cache: false,
        data: {
            "id": merchandise_id,
            "title": title,
            "app_id": app_id,
        },
        beforeSend: function() {
            /*$(element).html('<div class="loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');*/
        },
        success: function(res) {
            if (res.trim() == "error") {
                $.alert('Error occured, please try again later.');
            } else {
                window.location.href = 'add-category.php?app_id=' + app_id + '&id=' + res + '&active=' + active;
            }
        },
    });
}
function addSearchCategory(app_id, category_id, active) {
    var title = $('#merchandise_title').val().trim();
    $.ajax({
        method: "POST",
        url: './ajax/saveCategory.php',
        cache: false,
        data: {
            "id": category_id,
            "title": title,
            "app_id": app_id,
        },
        beforeSend: function() {
            /*$(element).html('<div class="loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');*/
        },
        success: function(res) {
            if (res.trim() == "error") {
                $.alert('Error occured, please try again later.');
            } else {
                window.location.href = 'add-search-category.php?app_id=' + app_id + '&id=' + res + '&active=' + active;
            }
        },
    });
}
function validateUrl(url) {
    var urlregex = new RegExp("^(http:\/\/|https:\/\/|ftp:\/\/){1}([0-9A-Za-z]+\.)");
    return urlregex.test(url);
}

function deleteLander(id, appid, rowid) {
    if (rowid == null || rowid == '') return false;
    $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to delete this layout row?',
        buttons: {
            confirm: {
                btnClass: 'btn-primary',
                action: function() {
                    $.ajax({
                        method: "POST",
                        url: './ajax/deleteLanderRow.php',
                        cache: false,
                        data: {
                            "row_id": rowid
                        },
                        success: function(res) {
                            location.href = './lander-overview.php?app_id=' + appid + '&id=' + id;
                        },
                    });
                }
            },
            cancel: {
                btnClass: 'btn-danger',
                action: function() {
                    $.alert('Canceled!');
                }
            },
        }
    });
}

function buttonLayouts() {
    var html = '';
    html += '<ul id="buttonSortable">';
    $.each(topSectionData.buttons, function(i, v) {
        tmpAttachments[i] = v.attachment;
        var sel = ($.isArray(v.attachment) && v.attachment[1] != undefined) ? v.attachment[1] : '';
        var val = ($.isArray(v.attachment) && v.attachment[2] != undefined) ? v.attachment[2] : '';
        var selectColor = (sel != '' || val != '') ? 'border-color:#24a724;' : 'border-color:#f0e68c;';
        if (v.appid == currentApp) {
            html += '<li id="' + i + '" data-attachment="[' + v.attachment + ']">';
            html += '<div class="previewPayouts button-layout single-layout" id="single-layout-' + i + '" style="display:inline-block">';
            if (v.btnid == undefined) {
                html += '<a class="close-this" data-preview-container="#smallImgOverlay" data-button-index="' + i + '" href="#"><i class="fa fa-times"></i></a>';
            } else {
                html += '<a class="close-global" data-preview-container="#smallImgOverlay" data-btn-id="' + v.btnid + '" data-button-type="self" data-button-index="' + i + '" href="#"><i class="fa fa-times"></i></a>';
            }
            html += '<div style="' + selectColor + '" class="button-content" id="singleLayout-' + i + '" data-ajax-url="./ajax/popupContentTopSection.php" data-all-hide=".contentDdPopup" data-click-show="#contentDdPopup-' + i + '" data-attachment-pos="' + i + '" data-option-val="' + val + '" data-option-sel="' + sel + '">' + v.value + '</div>';
            html += '<div class="contentDdPopup" id="contentDdPopup-' + i + '"></div>';
            html += '</div>';
            html += '</li>';
        } else {
            var selectColor = 'border-color:#337ab7;';
            html += '<li id="' + i + '" data-attachment="[' + v.attachment + ']">';
            html += '<div class="previewPayouts button-layout single-layout" id="single-layout-' + i + '" style="display:inline-block">';
            html += '<a class="close-global" data-preview-container="#smallImgOverlay" data-btn-id="' + v.btnid + '" data-button-type="global" data-button-index="' + i + '" href="#"><i class="fa fa-times"></i></a>';
            html += '<div style="' + selectColor + '" class="button-content" id="singleLayout-' + i + '" data-all-hide=".contentDdPopup" data-attachment-pos="' + i + '" data-option-val="' + val + '" data-option-sel="' + sel + '">' + v.value + '</div>';
            html += '<div class="contentDdPopup" id="contentDdPopup-' + i + '"></div>';
            html += '</div>';
            html += '</li>';
        }
    });
    html += '</ul>';
    return html;
}
var GET = function(options) {
    if ($.isArray(options)) {
        $.each(options, function(index, option) {
            var data = "";
            if (option.type == 'link') {
                data = $.param(option.data, true);
            } else if (option.type == 'form') {
                data = $(option.formId).serialize();
            }
            if (data != '') {
                $.ajax({
                    type: "POST",
                    url: base_url + '/ajax/get.php',
                    data: data,
                    cache: false,
                    dataType: (option.dataType != '') ? option.dataType : "html",
                    beforeSend: function(xhr) {
                        if (option.hasOwnProperty('callback')) {
                            if (typeof option.callback.beforeSend == 'function') {
                                option.callback.beforeSend(xhr);
                            } else {
                                option.callback.beforeSend;
                            }
                        }
                    },
                    error: function(XHR, textStatus, errorThrown) {
                        if (option.hasOwnProperty('callback')) {
                            if (typeof option.callback.error == 'function') {
                                option.callback.error(XHR, textStatus, errorThrown);
                            } else {
                                option.callback.error;
                            }
                        }
                    },
                    complete: function(XHR, textStatus) {
                        if (option.hasOwnProperty('callback')) {
                            if (typeof option.callback.complete == 'function') {
                                option.callback.complete(XHR, textStatus);
                            } else {
                                option.callback.complete;
                            }
                        }
                    },
                    success: function(data, textStatus, XHR) {
                        if (option.hasOwnProperty('callback')) {
                            if (typeof option.callback.success == 'function') {
                                option.callback.success(data, textStatus, XHR);
                            } else {
                                option.callback.success;
                            }
                        }
                    }
                });
            }
        });
    } else {
        console.warn('Options object require within array');
    }
    return false;
}
var Add = function(option) {
    var data = "";
    if (option.type == 'link') {
        data = $.param(option.data, true);
    } else if (option.type == 'form') {
        data = $(option.formId).serialize();
    }
    if (data != '') {
        $.ajax({
            type: "POST",
            url: base_url + "/ajax/add.php",
            data: data,
            cache: false,
            dataType: (option.dataType != '') ? option.dataType : "html",
            beforeSend: function(xhr) {
                if (typeof option.callback.beforeSend == 'function') {
                    option.callback.beforeSend(xhr);
                } else {
                    option.callback.beforeSend;
                }
            },
            error: function(response) {},
            complete: function(XHR, textStatus) {
                if (typeof option.callback.complete == 'function') {
                    option.callback.complete(XHR, textStatus);
                } else {
                    option.callback.complete;
                }
            },
            success: function(data, textStatus, XHR) {
                if (typeof option.callback.success == 'function') {
                    option.callback.success(data, textStatus, XHR);
                } else {
                    option.callback.success;
                }
            }
        });
    }
    return false;
}
var Edit = function(option) {
    var data = "";
    if (option.type == 'link') {
        data = $.param(option.data, true);
    } else if (option.type == 'form') {
        var data = $(option.formId).serialize();
    }
    if (data != '') {
        $.ajax({
            type: "POST",
            url: base_url + '/ajax/edit.php',
            data: data,
            cache: false,
            dataType: (option.dataType != '') ? option.dataType : "html",
            beforeSend: function(xhr) {
                if (typeof option.callback.beforeSend == 'function') {
                    option.callback.beforeSend(xhr);
                } else {
                    option.callback.beforeSend;
                }
            },
            error: function(response) {},
            complete: function(XHR, textStatus) {
                if (typeof option.callback.complete == 'function') {
                    option.callback.complete(XHR, textStatus);
                } else {
                    option.callback.complete;
                }
            },
            success: function(data, textStatus, XHR) {
                if (typeof option.callback.success == 'function') {
                    option.callback.success(data, textStatus, XHR);
                } else {
                    option.callback.success;
                }
            }
        });
    }
    return false;
}
var Delete = function(option) {
    var setting = {
        confirm: {
            title: 'Confirm!',
            content: 'Are you sure to delete this',
        }
    };
    option = $.extend(setting, option);
    $.confirm({
        title: option.confirm.title,
        content: option.confirm.content,
        buttons: {
            confirm: {
                btnClass: 'btn-primary',
                action: function() {
                    var data = "";
                    if (option.type == 'link') {
                        data = $.param(option.data, true);
                    } else if (option.type == 'form') {
                        data = $(option.formId).serialize();
                    }
                    if (data != '') {
                        $.ajax({
                            type: "POST",
                            url: base_url + '/ajax/delete.php',
                            data: data,
                            cache: false,
                            beforeSend: function(xhr) {
                                if (typeof option.callback.beforeSend == 'function') {
                                    option.callback.beforeSend(xhr);
                                } else {
                                    option.callback.beforeSend;
                                }
                            },
                            error: function(response) {},
                            complete: function(XHR, textStatus) {
                                if (typeof option.callback.complete == 'function') {
                                    option.callback.complete(XHR, textStatus);
                                } else {
                                    option.callback.complete;
                                }
                            },
                            success: function(data, textStatus, XHR) {
                                if (typeof option.callback.success == 'function') {
                                    option.callback.success(data, textStatus, XHR);
                                } else {
                                    option.callback.success;
                                }
                            }
                        });
                    }
                }
            },
            cancel: {
                btnClass: 'btn-danger',
                action: function() {
                    // $.alert('Canceled!');
                }
            },
        }
    });
    return false;
}

function saveBlock(data) {
    var title = '';
    var has_title = $(data.checkbox).val();
    if (has_title == 1) {
        title = $(data.input).val().trim();
    } else {
        has_title = 0;
    }
    $.ajax({
        method: "POST",
        url: data.ajaxUrl,
        cache: false,
        data: {
            "id": data.id,
            "title": title,
            "has_title": has_title,
            "image_url": data.image,
            "app_id": data.appId,
        },
        beforeSend: function() {
            /*$(element).html('<div class="loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');*/
        },
        success: function(res) {
            // $.alert(res);
            if (res.trim() == "error") {
                $.alert('Error occured, please try again later.');
            } else {
                window.location.href = 'static-block.php?app_id=' + data.appId;
            }
        },
    });
    return false
}

function countTopSectionData(indexes) {
    if (page_global == 'new-top-section' || 'edit-top-section' == page_global) {
        var buttonsLength = $('.button-content').length;
        var msg = $('#btn-msg');
        if (msg.length) {
            m = '';
            if (buttonsLength < 3) {
                var m = 'add min 3 butons, '
            }
            msg.html('Please ' + m + 'attach links then drag & drop sorting will enabled.');
        }
        if (indexes != null && $.isArray(indexes)) {
            $.each(indexes, function(i, v) {
                var inx = parseInt(v);
                if (tmpAttachments[inx] !== undefined) {
                    topSectionData.buttons[i].attachment = tmpAttachments[inx];
                }
            });
            indexes = null
        } else {
            $.each(tmpAttachments, function(i, v) {
                if (v !== undefined) {
                    topSectionData.buttons[i].attachment = v;
                }
            });
        }
        var count = 0;
        $.each(topSectionData.buttons, function(i, v) {
            if (v.hasOwnProperty('attachment') == true) {
                count++;
                if (v.appid == currentApp) {
                    $('#single-layout-' + i + ' .button-content').css('border-color', '#24a724');
                }
            }
        });
        if (count >= 3) {
            if (buttonsLength == count && count == tmpAttachments.length) {
                $('#smallImgOverlay').html(buttonLayouts()).show();
                sortButtons();
                var msg = $('#btn-msg');
                if (msg.length) {
                    msg.html('');
                }
                $('#save-top-section').removeAttr('disabled');
            } else {
                $('#save-top-section').attr('disabled', 'disabled');
            }
        } else {
            $('#save-top-section').attr('disabled', 'disabled');
        }
    }
}

function getBrandsforCategory(app_store_chk, c_id, img_index) {
    var loader = $('#categories-loader'),
        brandUrl = './ajax/brands.php';
    $.ajax({
        method: "POST",
        url: brandUrl,
        data: {
            'lang': app_store_chk,
            'cid': c_id,
            'img_index': img_index,
        },
        cache: false,
        dataType: 'json',
        beforeSend: function() {
            if (loader.length) {
                loader.fadeIn();
            }
        },
        success: function(res) {
            if (loader.length) {
                loader.fadeOut();
            }
            if (typeof res !== 'object' || res.length < 1) {
                $('.contentDdPopup, .ddPopup').hide();
                return false;
            }
            window.resultsAbc = res;
            $('.contentPage').hide();
            $('#suggest-brand').show(100, function() {
                var markup = '';
                $.each(resultsAbc, function(index, val) {
                    markup += '<li>';
                    markup += '<label for="suggest-brand-' + val.id + '" class="clearfix relation">';
                    markup += '<div class="ptText name"><span class="text suggest-brand-text-' + val.id + '">' + val.name + '</span><span class="hide">' + val.id + '</span></div>';
                    markup += '<div class="ptCheckbox"><input data-text=".suggest-brand-list .suggest-brand-text-' + val.id + '" data-imgIndex="' + img_index + '" type="radio" name="suggest_brand" id="suggest-brand-' + val.id + '" value="' + val.id + '"></div>';
                    markup += '</label>';
                    markup += '</li>';
                });
                $('.suggest-brand-list').html(markup);
                var userList, options = {
                    valueNames: ['name'],
                };
                userList = new List('suggest-brands', options);
                var _backup = $('#suggest-brands .suggest-brand-list .selected').clone().remove();
                $('#suggest-brands .suggest-brand-list').prepend(_backup);
            });
        },
        error: function(req, status, err) {
            if (loader.length) {
                loader.fadeOut();
            }
            $.notify(status + ': error occurred', {
                position: 'top right',
                className: 'error',
            });
        },
    });
}

function syncCategoriesBrands(syncType) {
    if (syncType == '1') {
        $.ajax({
            method: "POST",
            url: './cron/categories.php',
            cache: false,
            beforeSend: function() {
                $('.ajax-loader').fadeIn();
            },
            success: function(res) {
                $('.ajax-loader').fadeOut();
            },
        });
    } else {
        $.ajax({
            method: "POST",
            url: './cron/brands.php',
            cache: false,
            beforeSend: function() {
                $('.ajax-loader').fadeIn();
            },
            success: function(res) {
                $('.ajax-loader').fadeOut();
            },
        });
    }
}

function callPopup(el) {
    if (el.length) {
        // $.magnificPopup.close();
        $.magnificPopup.open({
            items: {
                src: el
            },
            type: 'inline',
            closeOnBgClick: false,
            closeBtnInside: true,
            enableEscapeKey: false,
            fixedContentPos: true,
            mainClass: 'my-mfp-zoom-in'
        });
    }
}

function sortButtons() {
    $("#buttonSortable").sortable({
        cursor: "move",
        update: function(event, ui) {
            var data = $(this).sortable('toArray');
            var buttons = [];
            $.each(topSectionData.buttons, function(i, v) {
                var index = parseInt(data[i]);
                buttons[i] = topSectionData.buttons[index];
            });
            topSectionData.buttons = buttons;
            // $('#smallImgOverlay').html(buttonLayouts()).show();
            countTopSectionData(data);
            // sortButtons();
        }
    });
}

function appendLayouts(availableData) {
    images = jQuery.parseJSON(availableData.images);
    tmpAttachments = jQuery.parseJSON(availableData.attachments);
    layout = availableData.layout;
    if (availableData.layout == 'singleShort') {
        $(".layoutItemContainer.singleShort").click();
        var options = {
            layoutType: "single",
            layoutTypeDB: "singleShort",
            viewTemplate: "single-layout.mt",
            iPadView: ".ipadPreviewContainer #carouselOuter #carousel",
            iPhoneView: ".iphonePreviewContainer #carousel",
            smallPreview: "#singleShortLayout",
            imagePosition: "0"
        };
        window.smallPreviewArray[0] = '#singleShortLayout';
        templateRendering(options, images);
    } else if (availableData.layout == 'single') {
        $(".layoutItemContainer.singleTall").click();
        var options = $.parseJSON('{"layoutType":"single","layoutTypeDB":"single","viewTemplate":"single-layout.mt","iPadView": ".ipadPreviewContainer #carouselOuter #carousel","iPhoneView": ".iphonePreviewContainer #carousel","smallPreview": "#singleLayout","imagePosition": "0"}');
        window.smallPreviewArray[0] = '#singleLayout';
        templateRendering(options, images);
    } else if (availableData.layout == 'doubleShort') {
        $(".layoutItemContainer.doubleShort").trigger('click');
        var options = $.parseJSON('{"layoutType":"double","layoutTypeDB":"doubleShort","viewTemplate":"double-layout.mt","iPadView": ".ipadPreviewContainer #carouselOuter #carousel","iPhoneView": ".iphonePreviewContainer #carousel","smallPreview": "#twoCol-left-short","imagePosition": "0"}');
        window.smallPreviewArray[0] = '#twoCol-left-short';
        templateRendering(options, images);
        var options = $.parseJSON('{"layoutType":"double","layoutTypeDB":"doubleShort","viewTemplate":"double-layout.mt","iPadView": ".ipadPreviewContainer #carouselOuter #carousel","iPhoneView": ".iphonePreviewContainer #carousel","smallPreview": "#twoCol-right-short","imagePosition": "1"}');
        window.smallPreviewArray[1] = '#twoCol-right-short';
        templateRendering(options, images);
    } else if (availableData.layout == 'double') {
        $(".layoutItemContainer.doubleTall").trigger('click');
        var options = $.parseJSON('{"layoutType":"double","layoutTypeDB":"double","viewTemplate":"double-layout.mt","iPadView": ".ipadPreviewContainer #carouselOuter #carousel","iPhoneView": ".iphonePreviewContainer #carousel","smallPreview": "#twoCol-left","imagePosition": "0"}');
        window.smallPreviewArray[0] = '#twoCol-left';
        templateRendering(options, images);
        var options = $.parseJSON('{"layoutType":"double","layoutTypeDB":"double","viewTemplate":"double-layout.mt","iPadView": ".ipadPreviewContainer #carouselOuter #carousel","iPhoneView": ".iphonePreviewContainer #carousel","smallPreview": "#twoCol-right","imagePosition": "1"}');
        window.smallPreviewArray[1] = '#twoCol-right';
        templateRendering(options, images);
    } else if (availableData.layout == 'tripleLeft') {
        $(".layoutItemContainer.tripleLeft").trigger('click');
        var options = $.parseJSON('{"layoutType":"triple","layoutTypeDB":"tripleLeft","viewTemplate":"triple-left-layout.mt","iPadView": ".ipadPreviewContainer #carouselOuter #carousel","iPhoneView": ".iphonePreviewContainer #carousel","smallPreview": "#threeCol-left-1","imagePosition": "0"}');
        window.smallPreviewArray[0] = '#threeCol-left-1';
        templateRendering(options, images);
        var options = $.parseJSON('{"layoutType":"triple","layoutTypeDB":"tripleLeft","viewTemplate":"triple-left-layout.mt","iPadView": ".ipadPreviewContainer #carouselOuter #carousel","iPhoneView": ".iphonePreviewContainer #carousel","smallPreview": "#threeCol-left-2","imagePosition": "1"}');
        window.smallPreviewArray[1] = '#threeCol-left-2';
        templateRendering(options, images);
        var options = $.parseJSON('{"layoutType":"triple","layoutTypeDB":"tripleLeft","viewTemplate":"triple-left-layout.mt","iPadView": ".ipadPreviewContainer #carouselOuter #carousel","iPhoneView": ".iphonePreviewContainer #carousel","smallPreview": "#threeCol-left-3","imagePosition": "2"}');
        window.smallPreviewArray[2] = '#threeCol-left-3';
        templateRendering(options, images);
    } else if (availableData.layout == 'tripleRight') {
        $(".layoutItemContainer.tripleRight").trigger('click');
        var options = $.parseJSON('{"layoutType":"triple","layoutTypeDB":"tripleRight","viewTemplate":"triple-right-layout.mt","iPadView": ".ipadPreviewContainer #carouselOuter #carousel","iPhoneView": ".iphonePreviewContainer #carousel","smallPreview": "#threeCol-right-1","imagePosition": "0"}');
        window.smallPreviewArray[0] = '#threeCol-right-1';
        templateRendering(options, images);
        var options = $.parseJSON('{"layoutType":"triple","layoutTypeDB":"tripleRight","viewTemplate":"triple-right-layout.mt","iPadView": ".ipadPreviewContainer #carouselOuter #carousel","iPhoneView": ".iphonePreviewContainer #carousel","smallPreview": "#threeCol-right-2","imagePosition": "1"}');
        window.smallPreviewArray[1] = '#threeCol-right-2';
        templateRendering(options, images);
        var options = $.parseJSON('{"layoutType":"triple","layoutTypeDB":"tripleRight","viewTemplate":"triple-right-layout.mt","iPadView": ".ipadPreviewContainer #carouselOuter #carousel","iPhoneView": ".iphonePreviewContainer #carousel","smallPreview": "#threeCol-right-3","imagePosition": "2"}');
        window.smallPreviewArray[2] = '#threeCol-right-3';
        templateRendering(options, images);
    } else if (availableData.layout == 'singleBar') {
        $(".layoutItemContainer.singleBar").trigger('click');
        var options = $.parseJSON('{"layoutType":"single","layoutTypeDB":"singleBar","viewTemplate":"single-layout.mt","iPadView": ".ipadPreviewContainer #carouselOuter #carousel","iPhoneView": ".iphonePreviewContainer #carousel","smallPreview": "#barLayout","imagePosition": "0"}');
        window.smallPreviewArray[0] = '#barLayout';
        templateRendering(options, images);
    }
    $('.layout-type').show();
}