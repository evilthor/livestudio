$(function() {
    $(document).on('click', '.__delete', function(event) {
        var _this = $(this);
        var options = _this.data();
        Delete(options);
        return false;
    });
    $(document).on('click', '.delete-app', function(event) {
        var _this = $(this),
            data = _this.data();
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure to delete this',
            buttons: {
                confirm: {
                    btnClass: 'btn-primary',
                    action: function() {
                        $.ajax({
                            method: "POST",
                            url: data.ajaxUrl,
                            cache: false,
                            data: {
                                "id": data.id,
                            },
                            beforeSend: function() {
                                $(data.loader).show();
                            },
                            success: function(res) {
                                $(data.loader).hide();
                                window.location.href = 'apps.php';
                            },
                        });
                    }
                },
                cancel: {
                    btnClass: 'btn-danger',
                    action: function() {
                        // $.alert('Canceled!');
                    }
                },
            }
        });
        return false;
    });
    $(window).on("load", function() {
        $(".ipadPreviewContainer #carouselOuter #carousel, .iphonePreviewContainer #carousel").mCustomScrollbar({
            scrollInertia: 0
        });
    });
    $(document).on('click', '.pageSection .page-title .back, .ptContentPageSection .contentPage .page-title .back', function() {
        var backTo = $(this).attr('data-backTo'),
            hide = $(this).attr('data-hide');
        $(backTo).show(1, function() {
            $(hide).hide();
        });
        if ($(this).attr("data-images") == 1) {
            $('.qq-upload-delete-selector').each(function(index, value) {
                $(this).trigger('click');
            });
            tmpAttachments = [];
            $('#btnDoneImageUpload, #saveRow').attr("disabled", "disabled");
            $('#smallImgOverlay').hide();
            $('.tick-active').removeClass('tick-active');
        }
        return false;
    });
    $(document).on('click', '#saveRow', function() {
        var title = $('#merchandise_title').val(),
            app_id = $('#app_id').val()
        row_id = $('#row_id').val()
        id = $('#merchandise_id').val();
        if (row_id == undefined) {
            row_id = '';
        }
        var url = './ajax/saveData.php';
        var path = window.location.pathname;
        var page = path.split("/").pop();
        if (page == 'add-category.php' || page == 'edit-category.php') {
            var url = './ajax/saveDataCat.php';
        }
        if (page == 'add-lander.php' || page == 'edit-lander.php') {
            id = $('#lander_id').val();
            var url = './ajax/saveDataLander.php';
        }
        if (page == 'add-search-category.php' || page == 'edit-search-category.php') {
            var url = './ajax/saveDataCatNew.php';
        }
        $.ajax({
            method: "POST",
            url: url,
            cache: false,
            data: {
                "command": "saveRow",
                "images": JSON.stringify(images),
                "id": id,
                "row_id": row_id,
                "attachments": JSON.stringify(tmpAttachments),
                "title": title,
                "app_id": app_id,
                "layout": layout
            },
            beforeSend: function() {
                /*$(element).html('<div class="loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');*/
            },
            success: function(res) {
                if (res.trim() == "error") {
                    $.alert('Error occured, please try again later.');
                } else {
                    if (page == 'add-category.php' || page == 'edit-category.php') {
                        window.location.href = "category-overview.php?app_id=" + app_id + "&id=" + res;
                    } else if (page == 'add-lander.php' || page == 'edit-lander.php') {
                        window.location.href = "lander-overview.php?app_id=" + app_id + "&id=" + res;
                    } else if (page == 'add-search-category.php' || page == 'edit-search-category.php') {
                        window.location.href = "search-category-overview.php?app_id=" + app_id + "&id=" + res;
                    } else {
                        window.location.href = "merchandise-overview.php?app_id=" + app_id + "&id=" + res;
                    }
                    console.log(window.location.href);
                }
            },
        });
    });
    $(document).on('click', '.layoutItemContainer', function() {
        var options = $(this).attr('data-option');
        options = $.parseJSON(options);
        if ($(options.pageHide).length) {
            $(options.pageHide).hide(100, function() {
                if ($(options.pageShow).length) {
                    $(options.pageShow).show(100, function() {
                        if ($(options.layoutHide).length && $(options.layoutShow).length) {
                            $(options.layoutHide).hide();
                            $(options.layoutShow).show(100, function() {
                                $(options.smallPreviewHide).hide();
                                $(options.smallPreviewShow).show();
                            });
                        } else {}
                    });
                } else {}
            });
        } else {}
    });
    $(document).on('click', '.ddArea', function() {
        var element = $(this).attr('data-click-show'),
            url = $(this).attr('data-ajax-url'),
            offset = $(this).offset();
        $(element).css({
            'left': (offset.left),
            'top': (offset.top - $(element).height() - 5)
        });
        $('.contentDdPopup').hide();
        $(element).show(50, function() {
            $.ajax({
                method: "GET",
                url: url,
                cache: false,
                beforeSend: function() {
                    $(element).html('<div class="loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');
                },
                success: function(res) {
                    $(element).html(res);
                },
            });
        });
        return false;
    });
    $(document).on('click', '.close-popup', function() {
        var _this = $(this),
            hide = _this.attr('data-hide');
        $(hide).hide();
        return false;
    });
    $('.ddBtn, .img-thumb').on('click', function() {
        var popup = $(this).attr('data-click-show');
        $(popup).toggle();
    });
    $(document).on('click', '.have-image button', function(event) {
        var _this = $(this),
            data = _this.data();
        $(data.elementHide).fadeOut();
    });
    $(document).on('click', '.is-delete', function(event) {
        var _this = $(this),
            elementType = _this.attr('data-element-type'),
            href = _this.attr('href');
        elementType = elementType != undefined ? elementType : '';
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure to delete this ' + elementType + '?',
            buttons: {
                confirm: {
                    btnClass: 'btn-primary',
                    action: function() {
                        location.href = href;
                    }
                },
                cancel: {
                    btnClass: 'btn-danger',
                    action: function() {
                        // $.alert('Canceled!');
                    }
                },
            }
        });
        return false;
    });
    $(document).on('click', '#btnDoneImageUpload, #btnDoneSuggestBrands, .done-btn .done', function(event) {
        var _this = $(this);
        var e = _this.data('hide');
        $(e).hide();
        countTopSectionData();
        return false;
    });
    $("#label-text").on('click', function(event) {
        var _this = $(this);
        var e = _this.data('depend');
        if (_this.prop('checked')) {
            $(document).find(e).show();
            $(document).find('.static-block-single-template-layout .label').show();
        } else {
            $(document).find(e).hide();
            $(document).find('.static-block-single-template-layout .label').hide();
        }
    });
    $(document).on('click', '.add-lander.active', function() {
        var _this = $(this);
        var data = _this.data();
        addLander(data);
        return false;
    });
    $(document).on('click', '.__get_popup', function(event) {
        var _this = $(this),
            data = _this.attr('data-data'),
            loader = _this.attr('data-loader'),
            options = [{
                "type": "link",
                "data": $.parseJSON(data),
                "callback": {
                    beforeSend: function(xhr) {
                        $(loader).show();
                    },
                    success: function(data, textStatus, XHR) {
                        $(loader).hide();
                    },
                    complete: function(XHR, textStatus) {
                        var el = $(XHR.responseText);
                        callPopup(el);
                    }
                }
            }];
        window.GET(options);
        return false;
    });
    $(document).on('click', "input[name='static_lander_category']", function(event) {
        var _this = $(this),
            _getText = _this.data('text'),
            _text = $(_getText).text(),
            imgIndex = _this.data('imgindex');
        if (tickActive != null) {
            tickActive.addClass('tick-active');
        }
        tmpAttachments[imgIndex][4] = _this.val();
        $('.contentDdPopup, .ddPopup').hide();
        countTopSectionData();
        if (
            (layout == 'single' && $('#single-layout>.tick-active').length == 1) || (layout == 'singleShort' && $('#single-short-layout>.tick-active').length == 1) || (layout == 'singleBar' && $('#bar-layout>.tick-active').length == 1) || (layout == 'double' && $('#two-col-layout>.tick-active').length == 2) || (layout == 'doubleShort' && $('#two-col-layout-short>.tick-active').length == 2) || (layout == 'tripleLeft' && $('#three-col-layout-left>.tick-active').length == 3) || (layout == 'tripleRight' && $('#three-col-layout-right>.tick-active').length == 3)) {
            $('#saveRow').removeAttr('disabled');
        } else {
            $('#saveRow').attr('disabled', 'disabled');
        }
    });
});