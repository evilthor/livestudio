$(function() {

	// $(document).on('change', '.store-management [name="store-language"]', function(event) {
	// 	var _this = $(this),
	// 	data = _this.data(),
	// 	val = _this.val();
	// 	console.log(data, val);
	// 	if( val == 'en' ){
	// 		$(data.eleDisabled).attr('disabled', true);
	// 	}else{
	// 		$(data.eleDisabled).removeAttr('disabled');
	// 	}
	// 	return false;
	// });

	$(document).on('change', '.store-management [name="store-language"]', function(event) {
		var _this = $(this),
		data = _this.data(),
		val = _this.val();
		//console.log(data, val);
			$.ajax({
				method: "POST",
				url: base_url+'/ajax/get.php',
				cache: false,
				data: {
					"lang": val,
					"command": 'getMagentoStores',
				},
				beforeSend: function() {
				},
				success: function(res) {
					$(data.loader).hide();
					var res = $.parseJSON(res);
					var nameHtml = ''
					$.each(res, function(i, v){
						nameHtml += '<option value="'+v.name+'">'+v.name+'</option>';
					});
					$('#country-name').html(nameHtml);
					$("#country-name").select2({
					  	dropdownParent: $('.white-popup'),
					});

					var codeHtml = '';
					$.each(res, function(i, v){
						codeHtml += '<option value="'+v.country_code+'" selected>'+v.country_code+'</option>';
					});
					$('#country-code').html(codeHtml);
					$("#country-code").select2({
					  	dropdownParent: $('.white-popup'),
					});
				},
			});
		
		return false;
	});

	$(document).on('click', '.delete-store', function(event) {
		var _this = $(this),
			data = _this.data();
		$.confirm({
			title: 'Confirm!',
			content: 'Are you sure to delete this',
			buttons: {
				confirm: {
					btnClass: 'btn-primary',
					action: function() {
						$.ajax({
							method: "POST",
							url: data.ajaxUrl,
							cache: false,
							data: {
								"id": data.id,
							},
							beforeSend: function() {
								$(data.loader).show();
							},
							success: function(res) {
								$(data.loader).hide();
								window.location.href = 'store-management.php';
							},
						});
					}
				},
				cancel: {
					btnClass: 'btn-danger',
					action: function() {
						// $.alert('Canceled!');
					}
				},
			}
		});
		return false;
	});
});