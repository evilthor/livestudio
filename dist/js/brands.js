var brandUrl = './ajax/brands.php';
jQuery(document).ready(function($) {
     $.ajax({
          method: "POST",
          url: brandUrl,
          data: 'lang=' + app_store_chk,
          cache: false,
          dataType: 'json',
          success: function(res) {
               brands = res;
          }
     });
     $(document).on('click', "input[name='suggest_brand']", function(e) {
          var _this = $(this),
               _getText = _this.data('text'),
               _text = $(_getText).text();
          index = _this.attr('data-imgindex');
          if (tickActive != null) {
               tickActive.addClass('tick-active');
          }
          tmpAttachments[index][4] = _this.val();
          setTimeout(function() {
               countTopSectionData();
               $('.contentDdPopup, .ddPopup').hide();
          }, 500);
     });
});