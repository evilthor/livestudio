$(function() {
    $(document).on("click", ".btn-delete-lander", function() {
        var _this = $(this);
        var data = _this.data();
        deleteLander(data.landerid, data.appid, data.rowid);
    });
});