var catUrl = './categories.json';
var storeType = app_store_chk.split('_')[0];
if (storeType == 'ar') {
     catUrl = './arcategories.json';
}
jQuery(document).ready(function($) {
     $.ajax({
          method: "GET",
          url: catUrl,
          cache: false,
          dataType: 'json',
          success: function(res) {
               window.categories = res.sort(function(a, b) {
                    var nameA = a.relation[0].toUpperCase(); // ignore upper and lowercase
                    var nameB = b.relation[0].toUpperCase(); // ignore upper and lowercase
                    if (nameA < nameB) {
                         return -1;
                    }
                    if (nameA > nameB) {
                         return 1;
                    }
                    // names must be equal
                    return 0;
               });
          }
     });
     $(document).on("click", ".btn-delete-category", function() {
          var merchandiseId = $(this).attr('data-merchandiseId');
          var appId = $(this).attr('data-appId');
          var rowId = $(this).attr('data-rowId');
          var rowGlobal = $(this).attr('data-global');
          var path = window.location.pathname;
          var page = path.split("/").pop();
          if (page == 'search-category-overview.php') {
               var url = './ajax/deleteRowCatNew.php';
          } else {
               var url = './ajax/deleteRowCat.php';
          }
          $.confirm({
               title: 'Confirm!',
               content: 'Are you sure to delete this layout row?',
               buttons: {
                    confirm: {
                         btnClass: 'btn-primary',
                         action: function() {
                              $.ajax({
                                   method: "POST",
                                   url: url,
                                   cache: false,
                                   data: {
                                        "row_id": rowId,
                                        "app_id": appId,
                                        "row_global": rowGlobal
                                   },
                                   success: function(res) {
                                        if (page == 'search-category-overview.php') {
                                             location.href = './search-category-overview.php?app_id=' + appId + '&id=' + merchandiseId;
                                        } else {
                                             location.href = './category-overview.php?app_id=' + appId + '&id=' + merchandiseId;
                                        }
                                   },
                              });
                         }
                    },
                    cancel: {
                         btnClass: 'btn-danger',
                         action: function() {
                              $.alert('Canceled!');
                         }
                    },
               }
          });
     });
});