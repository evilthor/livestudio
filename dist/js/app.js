notification.init();
jQuery(window).on('load', function() {
	jQuery('.ajax-loader').fadeOut();
	$('.loader-text').textillate('stop');
});
$(function() {
	$('.loader-text').textillate({
		loop: true,
		minDisplayTime: 500,
		in : {
			effect: 'fadeIn',
		},
		out: {
			effect: 'fadeOut',
		}
	});
	$(document).ajaxComplete(function(event, xhr, settings) {
		console.log(settings.url);
		var selectData = new Array();
		if (typeof categories === "object" && categories !== '') {
			$.each(categories, function(i, v) {
				var text = v.relation.toString().split(',').join(' → ');
				selectData.push({
					id: v.id,
					text: text,
					title: text
				});
			});
		}
		$('#old-apps').select2({
			placeholder: 'Select a categories',
			data: selectData,
			minimumResultsForSearch: Infinity
		});
		var selectedCat = $('#hiddenSelectedCat').val();
		$('#old-apps').val(selectedCat).trigger('change')
	});
	setInterval(function() {
		var date = moment().format("dddd DD MMMM");
		$('.moment-date').html(date);
		var time = moment().format("hh:mm");
		$('.moment-time').html(time);
	}, 1000);
	if (!jQuery.isEmptyObject(window.messages)) {
		$.alert({
			title: window.messages.type,
			content: window.messages.message,
			onAction: function(btnName) {
				if (window.messages.redirect !== undefined && window.messages.redirect !== '') {
					location.href = window.messages.redirect;
				}
			},
		});
	}
	$(document).find('[data-toggle="tooltip"]').tooltip();
	var path = window.location.pathname;
	var page = path.split("/").pop();
	if (page == 'add-category.php' || page == 'edit-category.php') {
		$('.category.img-thumb').on('click', function() {
			tickActive = $(this);
			$('.ddPopup').hide();
			var element = $(this).attr('data-click-show'),
				allPopup = $(this).attr("data-all-hide"),
				offset = $(this).offset();
			var img_index = $(this).attr("data-attachmentPos");
			var optionSel = "";
			var optionVal = "";
			$(element).css({
				'left': (offset.left - ($(this).width())),
				'top': (offset.top - $(element).height() - 5)
			});
			$(allPopup).html('').hide();
			$(element).show(50, function() {
				var optionVal = '';
				var optionSel = '';
				if (tmpAttachments[img_index] != undefined) {
					if (tmpAttachments[img_index][1] != undefined) {
						optionSel = tmpAttachments[img_index][1];
					}
					if (tmpAttachments[img_index][2] != undefined) {
						optionVal = tmpAttachments[img_index][2];
					}
				}
				$.ajax({
					method: "GET",
					url: './ajax/popupContentCat.php',
					cache: false,
					data: {
						"img_index": img_index,
						"optionVal": optionVal,
						"optionSel": optionSel,
						"appid_global": appid_global,
						"page_global": page_global
					},
					beforeSend: function() {
						$(element).html('<div class="loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');
					},
					success: function(res) {
						$(element).html(res);
						if (tmpAttachments[img_index] != undefined) {
							if (tmpAttachments[img_index][1] != undefined) {
								$(".ddList li a[data-val='" + tmpAttachments[img_index][1] + "']").trigger("click");
							}
						}
						/*if (optionSel != "" && optionSel != undefined) {
						 $(".ddList li a[data-val='" + optionSel + "']").trigger("click");
						 }*/
					},
				});
			});
			return false;
		});
	} else {
		$('.img-thumb').on('click', function() {
			tickActive = $(this);
			$('.ddPopup').hide();
			var element = $(this).attr('data-click-show'),
				allPopup = $(this).attr("data-all-hide"),
				pid = $(this).attr("data-pid"),
				offset = $(this).offset();
			var img_index = $(this).attr("data-attachmentPos");
			var optionSel = "";
			var optionVal = "";
			$(element).css({
				'left': (offset.left - ($(this).width())),
				'top': (offset.top - $(element).height() - 5)
			});
			$(allPopup).html('').hide();
			$(element).show(50, function() {
				var optionVal = '';
				var optionSel = '';
				if (tmpAttachments[img_index] != undefined) {
					if (tmpAttachments[img_index][1] != undefined) {
						optionSel = tmpAttachments[img_index][1];
					}
					if (tmpAttachments[img_index][2] != undefined) {
						optionVal = tmpAttachments[img_index][2];
					}
				}
				$.ajax({
					method: "GET",
					url: './ajax/popupContent.php',
					cache: false,
					data: {
						"pid": pid,
						"img_index": img_index,
						"optionVal": optionVal,
						"optionSel": optionSel,
						"appid_global": appid_global,
						"page_global": page_global
					},
					beforeSend: function() {
						$(element).html('<div class="loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');
					},
					success: function(res) {
						$(element).html(res);
						if (tmpAttachments[img_index] != undefined) {
							if (tmpAttachments[img_index][1] != undefined) {
								$(".ddList li a[data-val='" + tmpAttachments[img_index][1] + "']").trigger("click");
							}
						}
						/*if (optionSel != "" && optionSel != undefined) {
						 $(".ddList li a[data-val='" + optionSel + "']").trigger("click");
						 }*/
					},
				});
			});
			return false;
		});
	}
	// $(document).click(function(e) {
	//     if ($(e.target).closest('.contentDdPopup, .ddPopup').length != 0) return false;
	//     $('.contentDdPopup, .ddPopup').hide();
	// });
	$(document).on('click', '.contentDdPopup .ddList li a', function() {
		var path = window.location.pathname;
		var page = path.split("/").pop();
		if (page != 'add-notification.php') {
			var page = $(this).attr('data-page-show'),
				current = $(this).attr('data-page-current'),
				list = $(this).attr('data-list-hide'),
				val = $(this).attr('data-val'),
				img_index = $(this).attr('data-imgIndex'),
				section = $(this).attr('data-page');
			if (val == 4) {
				$(this).addClass('tick-active');
				if (tickActive != null) {
					tickActive.addClass('tick-active');
				}
				tmpAttachments[img_index] = [];
				tmpAttachments[img_index][0] = img_index;
				tmpAttachments[img_index][1] = val;
				tmpAttachments[img_index][3] = "Static Image";
				// topSectionData.buttons[img_index].attachment = tmpAttachments;
				if (
					(layout == 'single' && $('#single-layout>.tick-active').length == 1) || (layout == 'singleShort' && $('#single-short-layout>.tick-active').length == 1) || (layout == 'singleBar' && $('#bar-layout>.tick-active').length == 1) || (layout == 'double' && $('#two-col-layout>.tick-active').length == 2) || (layout == 'doubleShort' && $('#two-col-layout-short>.tick-active').length == 2) || (layout == 'tripleLeft' && $('#three-col-layout-left>.tick-active').length == 3) || (layout == 'tripleRight' && $('#three-col-layout-right>.tick-active').length == 3)) {
					$('#saveRow').removeAttr('disabled');
				} else {
					$('#saveRow').attr('disabled', 'disabled');
				}
				countTopSectionData();
			}
			if (section !== undefined) {
				$(section).hide();
			}
			if (current !== undefined) {
				$(current).show();
			}
			$(page).show(100, function() {
				$(list).hide();
				if (val == 1) {
					$("input[name='category']").click(function() {
						var _this = $(this),
							_getText = _this.data('text'),
							_text = $(_getText).data();
						if (tickActive != null) {
							tickActive.addClass('tick-active');
						}
						tmpAttachments[img_index] = [];
						tmpAttachments[img_index][0] = img_index;
						tmpAttachments[img_index][1] = val;
						tmpAttachments[img_index][2] = _this.val();
						tmpAttachments[img_index][3] = _text.title.trim();
						if (tmpAttachments[img_index][2] != undefined && tmpAttachments[img_index][2] != '') {
							getBrandsforCategory(app_store_chk, tmpAttachments[img_index][2], img_index)
						}
						// $('.contentDdPopup, .ddPopup').hide();
						if (
							(layout == 'single' && $('#single-layout>.tick-active').length == 1) || (layout == 'singleShort' && $('#single-short-layout>.tick-active').length == 1) || (layout == 'singleBar' && $('#bar-layout>.tick-active').length == 1) || (layout == 'double' && $('#two-col-layout>.tick-active').length == 2) || (layout == 'doubleShort' && $('#two-col-layout-short>.tick-active').length == 2) || (layout == 'tripleLeft' && $('#three-col-layout-left>.tick-active').length == 3) || (layout == 'tripleRight' && $('#three-col-layout-right>.tick-active').length == 3)) {
							$('#saveRow').removeAttr('disabled');
						} else {
							$('#saveRow').attr('disabled', 'disabled');
						}
					});
				} else if (val == 2) {
					$(document).delegate("a.btn-xsProduct", "click", function() {
						if (tickActive != null) {
							tickActive.addClass('tick-active');
						}
						tmpAttachments[img_index] = [];
						tmpAttachments[img_index][0] = img_index;
						tmpAttachments[img_index][1] = val;
						tmpAttachments[img_index][2] = $(this).attr("data-productId");
						tmpAttachments[img_index][3] = $(this).attr("data-productName");
						$('.contentDdPopup, .ddPopup').hide();
						if (
							(layout == 'single' && $('#single-layout>.tick-active').length == 1) || (layout == 'singleShort' && $('#single-short-layout>.tick-active').length == 1) || (layout == 'singleBar' && $('#bar-layout>.tick-active').length == 1) || (layout == 'double' && $('#two-col-layout>.tick-active').length == 2) || (layout == 'doubleShort' && $('#two-col-layout-short>.tick-active').length == 2) || (layout == 'tripleLeft' && $('#three-col-layout-left>.tick-active').length == 3) || (layout == 'tripleRight' && $('#three-col-layout-right>.tick-active').length == 3)) {
							$('#saveRow').removeAttr('disabled');
						} else {
							$('#saveRow').attr('disabled', 'disabled');
						}
						countTopSectionData();
					});
				} else if (val == 3) {
					$("#customurl").blur(function() {
						var _this = $(this),
							_url = _this.val(),
							_isValid = validateUrl(_url),
							_button = _this.data('button');
						if (_isValid === true) {
							_this.addClass('valid').removeClass('invalid');
							if (tickActive != null) {
								tickActive.addClass('tick-active');
							}
							tmpAttachments[img_index] = [];
							tmpAttachments[img_index][0] = img_index;
							tmpAttachments[img_index][1] = val;
							tmpAttachments[img_index][2] = _url;
							tmpAttachments[img_index][3] = "External URL";
							// $('.contentDdPopup, .ddPopup').hide();
							if (
								(layout == 'single' && $('#single-layout>.tick-active').length == 1) || (layout == 'singleShort' && $('#single-short-layout>.tick-active').length == 1) || (layout == 'singleBar' && $('#bar-layout>.tick-active').length == 1) || (layout == 'double' && $('#two-col-layout>.tick-active').length == 2) || (layout == 'doubleShort' && $('#two-col-layout-short>.tick-active').length == 2) || (layout == 'tripleLeft' && $('#three-col-layout-left>.tick-active').length == 3) || (layout == 'tripleRight' && $('#three-col-layout-right>.tick-active').length == 3)) {
								$('#saveRow').removeAttr('disabled');
							} else {
								$('#saveRow').attr('disabled', 'disabled');
							}
							countTopSectionData();
							$('.error-url').remove();
							$(_button).removeAttr('disabled');
						} else {
							_this.addClass('invalid').removeClass('valid');
							$('.error-url').remove();
							_this.after('<span class="error error-url">Please enter valid URL</span>');
							$('#saveRow').attr('disabled', 'disabled');
							$(_button).attr('disabled', 'disabled');
						}
					});
				} else if (val == 5) {
					$("input[name='brand']").click(function() {
						var _this = $(this),
							_getText = _this.data('text'),
							_text = $(_getText).text();
						if (tickActive != null) {
							tickActive.addClass('tick-active');
						}
						tmpAttachments[img_index] = [];
						tmpAttachments[img_index][0] = img_index;
						tmpAttachments[img_index][1] = val;
						tmpAttachments[img_index][2] = _this.val();
						tmpAttachments[img_index][3] = _text.trim();
						$('.contentDdPopup, .ddPopup').hide();
						if (
							(layout == 'single' && $('#single-layout>.tick-active').length == 1) || (layout == 'singleShort' && $('#single-short-layout>.tick-active').length == 1) || (layout == 'singleBar' && $('#bar-layout>.tick-active').length == 1) || (layout == 'double' && $('#two-col-layout>.tick-active').length == 2) || (layout == 'doubleShort' && $('#two-col-layout-short>.tick-active').length == 2) || (layout == 'tripleLeft' && $('#three-col-layout-left>.tick-active').length == 3) || (layout == 'tripleRight' && $('#three-col-layout-right>.tick-active').length == 3)) {
							$('#saveRow').removeAttr('disabled');
						} else {
							$('#saveRow').attr('disabled', 'disabled');
						}
						countTopSectionData();
					});
				} else if (val == 6) {
					$("input[name='lander']").click(function() {
						var _this = $(this),
							_getText = _this.data('text'),
							_text = $(_getText).text();
						tmpAttachments[img_index] = [];
						tmpAttachments[img_index][0] = img_index;
						tmpAttachments[img_index][1] = val;
						tmpAttachments[img_index][2] = _this.val();
						tmpAttachments[img_index][3] = _text.trim();
						var page_path = window.location.pathname;
						var page_name = page_path.split("/").pop();
						if (('sw' == _this.val() || 'sm' == _this.val() || 'sk' == _this.val()) && (page_name != 'edit-top-section.php' && page_name != 'new-top-section.php')) {
							var valueIs = '';
							if ('sw' == _this.val()) {
								valueIs = 'WOMEN ';
							} else if ('sm' == _this.val()) {
								valueIs = 'MEN ';
							} else if ('sk' == _this.val()) {
								valueIs = 'KIDS ';
							}
							$('.contentPage').hide();
							$('#static-lander-category').show(100, function() {
								var markup = '';
								var i = 0;
								$.each(window.categories, function(index, val) {
									var relation = val.relation,
										tip = val.id + ": " + val.name;
									rel = '';
									var count = relation.length;
									var i = 0;
									$.each(relation, function(index, el) {
										i++;
										var classA = '';
										if (count == i) {
											classA = "class='last'";
										}
										rel += '<em ' + classA + '>' + el.toString() + '</em>';
									});
									relation = rel;
									markup += '<li data-toggle="tooltip" data-placement="right" title="' + tip + '">';
									markup += '<label for="static-lander-category-list-' + val.id + '" class="clearfix relation">';
									markup += '<div class="ptText name text text-' + val.id + '" data-id="' + val.id + '">' + relation + '</div>';
									markup += '<div class="ptCheckbox"><input data-imgIndex="' + img_index + '" data-text=".static-lander-categories-list .text-' + val.id + '" type="radio" name="static_lander_category" id="static-lander-category-list-' + val.id + '" value="' + val.id + '"></div>';
									markup += '</label>';
									markup += '</li>';
								});
								$('.static-lander-categories-list').html(markup);
								$('[data-toggle="tooltip"]').tooltip();
								var userList, options = {
									valueNames: [
                                        'name'
                                    ],
									fuzzySearch: {
										searchClass: "search",
										location: 0,
										distance: 1000,
										threshold: 0.4,
										multiSearch: true
									}
								};
								var LanderCategories = new List('static-lander-categories', options);
								LanderCategories.fuzzySearch(valueIs);
								// var selected = $('#categories .category-list .selected'),
								// _backup = selected.clone();
								// selected.remove();
								// $('#categories .category-list').prepend(_backup)
							});
						} else {
							if (tickActive != null) {
								tickActive.addClass('tick-active');
							}
							$('.contentDdPopup, .ddPopup').hide();
							countTopSectionData();
							if (
								(layout == 'single' && $('#single-layout>.tick-active').length == 1) || (layout == 'singleShort' && $('#single-short-layout>.tick-active').length == 1) || (layout == 'singleBar' && $('#bar-layout>.tick-active').length == 1) || (layout == 'double' && $('#two-col-layout>.tick-active').length == 2) || (layout == 'doubleShort' && $('#two-col-layout-short>.tick-active').length == 2) || (layout == 'tripleLeft' && $('#three-col-layout-left>.tick-active').length == 3) || (layout == 'tripleRight' && $('#three-col-layout-right>.tick-active').length == 3)) {
								$('#saveRow').removeAttr('disabled');
							} else {
								$('#saveRow').attr('disabled', 'disabled');
							}
						}
					});
				}
			});
		}
	});
	var minWidth, minHeight;
	window.images = [];
	window.smallPreviewArray = [];
	$('.fine-uploader').fineUploader({
		template: 'qq-simple-thumbnails-template',
		debug: false,
		multiple: false,
		request: {
			endpoint: './vendor/fineuploader/php-traditional-server/endpoint.php',
		},
		deleteFile: {
			enabled: true,
			endpoint: "./vendor/fineuploader/php-traditional-server/endpoint.php"
		},
		thumbnails: {
			placeholders: {
				waitingPath: './bower_components/fine-uploader/dist/placeholders/waiting-generic.png',
				notAvailablePath: './bower_components/fine-uploader/dist/placeholders/not_available-generic.png'
			}
		},
		validation: {
			allowedExtensions: ['jpeg', 'jpg', 'gif', 'png'],
			//image:{minWidth:200,maxWidth:200,minHeight:200,maxHeight:200}
		},
		failedUploadTextDisplay: {
			mode: 'custom'
		},
		showMessage: function(msg) {
			$.alert({
				title: 'Message',
				content: msg,
				type: 'red',
			});
		},
		callbacks: {
			onCancel: function(id, name) {
				var element = $(this)[0]._options.element,
					eleID = '#' + $(element).attr('id'),
					options = $(eleID).attr('data-option');
				$(eleID + ' .qq-upload-button').show();
				$(eleID + ' .qq-upload-list').hide();
			},
			onSubmit: function(id, name) {
				var details = $(this);
				var promise = new qq.Promise();
				var file = this.getFile(id);
				var image = new Image();
				var url = window.URL && window.URL.createObjectURL ? window.URL : window.webkitURL && window.webkitURL.createObjectURL ? window.webkitURL : null;
				image.onload = function() {
					var vali = my_custom_validation(this.width, this.height, details),
						require = {
							width: $(details[0]._options.element).attr('data-width'),
							height: $(details[0]._options.element).attr('data-height'),
						};
					if (vali) {
						promise.success();
					} else {
						promise.failure();
						$.alert({
							title: 'Message',
							content: 'The required image size is ' + require.width + 'px x ' + require.height + 'px',
							type: 'red',
						});
					}
				};
				image.src = url.createObjectURL(file);
				return promise;
				var file = this.getFile(id);
			},
			onDeleteComplete: function(id, xhr, isError) {
				var element = $(this)[0]._options.element,
					eleID = '#' + $(element).attr('id'),
					options = $(eleID).attr('data-option');
				options = $.parseJSON(options);
				$(options.iPadView + " .mCSB_container, " + options.iPhoneView + " .mCSB_container").html('');
				$(eleID + ' .qq-upload-button').show();
				$(eleID + ' .qq-upload-list').hide();
				$(options.smallPreview).html('');
				if (options.imagePosition == 0) {
					images[0] = undefined;
					$('#btnDoneImageUpload, #saveRow').attr("disabled", "disabled");
					$('#smallImgOverlay').hide();
				} else if (options.imagePosition == 1) {
					images[1] = undefined;
					$('#btnDoneImageUpload, #saveRow').attr("disabled", "disabled");
					$('#smallImgOverlay').hide();
				} else if (options.imagePosition == 2) {
					images[2] = undefined;
					$('#btnDoneImageUpload, #saveRow').attr("disabled", "disabled");
					$('#smallImgOverlay').hide();
				}
			},
			onComplete: function(id, name, responseJSON, xhr) {
				var element = $(this)[0]._options.element,
					eleID = '#' + $(element).attr('id'),
					options = $(eleID).attr('data-option');
				options = $.parseJSON(options);
				layout = options.layoutTypeDB;
				$(eleID + ' .qq-upload-button').hide();
				$(eleID + ' .qq-upload-list').show();
				var uuid = responseJSON.uuid;
				var fname = responseJSON.uploadName;
				var image_path = "./vendor/fineuploader/php-traditional-server/files/" + uuid + "/" + fname;
				if (options.imagePosition == 0) {
					images[0] = image_path;
				} else if (options.imagePosition == 1) {
					images[1] = image_path;
				} else if (options.imagePosition == 2) {
					images[2] = image_path;
				}
				var length = 0;
				for (var cc = 0; cc < images.length; cc++) {
					if (images[cc] != undefined) {
						length++;
					}
				}
				if (
					(layout == 'single' && $('#single-layout>.tick-active').length == length) || (layout == 'singleShort' && $('#single-short-layout>.tick-active').length == length) || (layout == 'singleBar' && $('#bar-layout .tick-active').length == length) || (layout == 'double' && $('#two-col-layout .tick-active').length == length) || (layout == 'doubleShort' && $('#two-col-layout-short .tick-active').length == length) || (layout == 'tripleLeft' && $('#three-col-layout-left .tick-active').length == length) || (layout == 'tripleRight' && $('#three-col-layout-right .tick-active').length == length)) {
					$('#saveRow').removeAttr('disabled');
				} else {
					$('#saveRow').attr('disabled', 'disabled');
				}
				window.smallPreviewArray[options.imagePosition] = options.smallPreview;
				templateRendering(options, images);
			},
			onError: function(id, name, errorReason, xhr) {
				//
			}
		}
	});
	$("#frmAddApp").validate();
});