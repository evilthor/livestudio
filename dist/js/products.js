jQuery(document).ready(function($) {
	$.ajax({
		method: "GET",
		url: "./products.json",
		cache: false,
		dataType: 'json',
		success: function(res) {
			products = res;
		}
	});
	$(document).on('keyup', '#product-search', function(event) {
		var keyword = $(this).val(),
			key = null;
		if (keyword !== '') {
			if ($.isNumeric(keyword) === true) {
				key = "id";
			}
			if ($.isNumeric(keyword) === false) {
				key = "sku";
			}
			var results = $.grep(products, function(e) {
				if (key == 'sku') {
					return e.sku.indexOf(keyword) !== -1;
				}
				if (key == 'id') {
					return e.id.indexOf(keyword) !== -1;
				}
			});
			$('.product-list').empty();
			if (keyword.length > 3) {
				$.each(results, function(index, val) {
					$('.product-list').eq(0).append('<li><span>' + val.name + '</span><a class="btn btn-xs btn-primary btn-xsProduct" href="javascript:;" data-productId="' + val.id + '" data-productName="' + val.name + '">Add</a></li>');
				});
			}
		} else {
			$('.product-list').empty();
		}
	});
});