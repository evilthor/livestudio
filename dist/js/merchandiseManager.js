$(function() {
	$(document).on("click", "input[name='status']", function() {
		var _this = $(this),
			data = _this.data();
		$("input[name='status']").not(_this).removeAttr('checked');
		if (_this.attr('checked') != undefined) {
			return false;
		}
		$.ajax({
			method: "POST",
			url: './ajax/updateMerchandiseStatus.php',
			dataType: 'json',
			data: {
				"id": data.id,
				"app_id": data.appId
			},
			success: function(res) {
				_this.attr('checked', true);
				$(data.parentEle).notify(res.message, {
					position: 'top right',
					className: res.type,
					showAnimation: 'fadeIn',
					hideAnimation: 'fadeOut',
				});
			},
		});
	});
	$(document).on("click", ".btn-delete-merchandise", function() {
		var merchandiseId = $(this).attr('data-merchandiseId');
		var appId = $(this).attr('data-appId');
		var rowId = $(this).attr('data-rowId');
		var rowGlobal = $(this).attr('data-global');
		$.confirm({
			title: 'Confirm!',
			content: 'Are you sure to delete this layout row?',
			buttons: {
				confirm: {
					btnClass: 'btn-primary',
					action: function() {
						$.ajax({
							method: "POST",
							url: './ajax/deleteRow.php',
							cache: false,
							data: {
								"row_id": rowId,
								"app_id": appId,
								"row_global": rowGlobal
							},
							success: function(res) {
								location.href = './merchandise-overview.php?app_id=' + appId + '&id=' + merchandiseId;
							},
						});
					}
				},
				cancel: {
					btnClass: 'btn-danger',
					action: function() {
						$.alert('Canceled!');
					}
				},
			}
		});
	});
});