$(document).ready(function() {
	$(document).on("click", ".btn-add-button", function() {
		if (($.isArray(images) && images.length > 0) != false) {
			topSectionData.image = [];
			topSectionData.image.push(images[0]);
		}
		if (($.isArray(topSectionData.image) && topSectionData.image.length > 0) == false) {
			$.alert({
				title: 'Message',
				content: 'Please upload image first',
				type: 'red',
			});
			return false;
		}
		var _this = $(this);
		var data = _this.data();
		var val = $(data.textfield).val();
		var appid = $(data.appid).val();
		if (val != null && val != '') {
			topSectionData.buttons.push({
				value: val,
				appid: appid
			});
			$(data.textfield).val('');
		} else {
			return false;
		}
		var html = buttonLayouts();
		if ($(data.previewContainer).length) {
			$(data.previewContainer).show().html(html);
		}
		countTopSectionData();
		// sortButtons();
	});
	// $(document).click(function(event) {
	//     var target = $(event.target);
	//     console.log(target);
	// });
	$(document).on('click', '.button-content', function() {
		$('.ddPopup').hide();
		var _this = $(this),
			offset = _this.offset(),
			optionVal = "",
			data = _this.data(),
			tickActive = _this;
		$('.button-content').removeClass('active-ajax-content-box');
		_this.addClass('active-ajax-content-box');
		$(data.allHide).html('').hide();
		$(data.clickShow).show(50, function() {
			var optionVal = data.optionVal;
			var optionSel = data.optionSel;
			$.ajax({
				method: "GET",
				url: data.ajaxUrl,
				cache: false,
				data: {
					"img_index": data.attachmentPos,
					"optionVal": optionVal,
					"optionSel": optionSel,
					"appid_global": appid_global,
					"page_global": page_global
				},
				beforeSend: function() {
					$(data.clickShow).html('<div class="loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');
				},
				success: function(res) {
					$(data.clickShow).html(res);
				},
			});
		});
		return false;
	});
	$(document).on("click", "#save-top-section", function() {
		var rowID = $('#row_id');
		if (rowID.length > 0) {
			topSectionData._id = parseInt(rowID.val());
		}
		topSectionData.appid = appid_global;
		// $.each(topSectionData.buttons, function(i, v) {
		// 	topSectionData.buttons[i].attachment = tmpAttachments[i];
		// });
		if ($.isEmptyObject(topSectionData) == false && typeof topSectionData == "object") {
			$.ajax({
				method: "POST",
				url: './ajax/saveDataTopSection.php',
				cache: false,
				data: topSectionData,
				beforeSend: function() {},
				success: function(res) {
					if (res.trim() == "error") {
						$.alert('Error occured, please try again later.');
					} else {
						window.location.href = 'top-sections.php?app_id=' + appid_global;
					}
				},
			});
		}
		return false;
	});
	var h = window.innerHeight;
	h = h - $('nav.navbar').outerHeight();
	$('.layouts#top-section').css({
		'max-height': 'inherit',
		'min-height': 'inherit',
		'height': h
	});
	$(window).resize(function() {
		var h = window.innerHeight;
		h = h - $('nav.navbar').outerHeight();
		$('.layouts#top-section').css({
			'max-height': 'inherit',
			'min-height': 'inherit',
			'height': h
		});
		console.log(window.innerHeight, $('nav.navbar').outerHeight())
	})
	$(document).on('click', '.button-layout .close-this', function() {
		var _this = $(this),
			data = _this.data();
		if ($.isEmptyObject(topSectionData) == false && typeof topSectionData == "object") {
			topSectionData.buttons.splice(data.buttonIndex, 1);
			tmpAttachments.splice(data.buttonIndex, 1);
			if (topSectionData.buttons.length == 0) {
				$('#save-top-section').attr('disabled', 'disabled');
			}
			var html = buttonLayouts();
			if ($(data.previewContainer).length) {
				$(data.previewContainer).show().html(html);
			}
			sortButtons();
			countTopSectionData();
		}
		return false;
	});
	$(document).on('click', '.button-layout .close-global', function() {
		var count = 0;
		$.each(topSectionData.buttons, function(i, v) {
			if (v.hasOwnProperty('btnid') == true) {
				count++;
			}
		});
		if (count <= 3) {
			$.alert('Buttons should be atleast 3. If you want to delete existing buttons, please add new then delete.');
		} else {
			var data = $(this).data();
			$.ajax({
				method: "POST",
				url: './ajax/delete.php',
				cache: false,
				dataType: 'json',
				data: {
					"id": data.btnId,
					"app_id": currentApp,
					"btnType": data.buttonType,
					"command": 'deleteTopSectionButtons'
				},
				success: function(res) {
					topSectionData = '';
					location.href = './edit-top-section.php?app_id=' + currentApp + '&id=' + currentSec;
				},
			});
		}
	});
	if ($('#fine-uploader-gallery').length) {
		var minWidth, minHeight, image_path;
		var galleryUploader = new qq.FineUploader({
			element: document.getElementById("fine-uploader-gallery"),
			template: 'qq-simple-thumbnails-template',
			debug: false,
			multiple: false,
			request: {
				endpoint: gUploader.endpoint,
			},
			deleteFile: {
				enabled: true,
				endpoint: gUploader.deleteEndpoint
			},
			thumbnails: {
				placeholders: {
					waitingPath: gUploader.thumbnails.placeholders.waitingPath,
					notAvailablePath: gUploader.thumbnails.placeholders.notAvailablePath
				}
			},
			validation: {
				allowedExtensions: ['jpeg', 'jpg', 'gif', 'png'],
			},
			failedUploadTextDisplay: {
				mode: 'custom'
			},
			showMessage: function(msg) {
				$.alert({
					title: 'Message',
					content: msg,
					type: 'red',
				});
			},
			callbacks: {
				onCancel: function(id, name) {
					var element = $(this)[0]._options.element,
						eleID = '#' + $(element).attr('id'),
						options = $(eleID).attr('data-option');
					$(eleID + ' .qq-upload-button').show();
					$(eleID + ' .qq-upload-list').hide();
				},
				onSubmit: function(id, name) {
					var details = $(this);
					var promise = new qq.Promise();
					var file = this.getFile(id);
					var image = new Image();
					var url = window.URL && window.URL.createObjectURL ? window.URL : window.webkitURL && window.webkitURL.createObjectURL ? window.webkitURL : null;
					image.onload = function() {
						var vali = my_custom_validation(this.width, this.height, details),
							require = {
								width: $(details[0]._options.element).attr('data-width'),
								height: $(details[0]._options.element).attr('data-height'),
							};
						if (vali) {
							promise.success();
						} else {
							promise.failure();
							$.alert({
								title: 'Message',
								content: 'The required image size is ' + require.width + 'px x ' + require.height + 'px',
								type: 'red',
							});
						}
					};
					image.src = url.createObjectURL(file);
					return promise;
					var file = this.getFile(id);
				},
				onDeleteComplete: function(id, xhr, isError) {
					var element = $(this)[0]._options.element,
						eleID = '#' + $(element).attr('id'),
						options = $(eleID).attr('data-option');
					options = $.parseJSON(options);
					$(options.iPadView + " .mCSB_container, " + options.iPhoneView + " .mCSB_container").html('');
					$(eleID + ' .qq-upload-button').show();
					$(eleID + ' .qq-upload-list').hide();
					$(options.smallPreview).html('');
					$('#save-top-section').attr('disabled', 'disabled');
				},
				onComplete: function(id, name, responseJSON, xhr) {
					var element = $(this)[0]._options.element,
						eleID = '#' + $(element).attr('id'),
						options = $(eleID).attr('data-option');
					options = $.parseJSON(options);
					layout = options.layoutTypeDB;
					$(eleID + ' .qq-upload-button').hide();
					$(eleID + ' .qq-upload-list').show();
					var uuid = responseJSON.uuid;
					var fname = responseJSON.uploadName;
					image_path = "./vendor/fineuploader/php-traditional-server/files/" + uuid + "/" + fname;
					if (options.imagePosition == 0) {
						images[0] = image_path;
					} else if (options.imagePosition == 1) {
						images[1] = image_path;
					} else if (options.imagePosition == 2) {
						images[2] = image_path;
					}
					var length = 0;
					for (var cc = 0; cc < images.length; cc++) {
						if (images[cc] != undefined) {
							length++;
						}
					}
					var index = parseInt(options.imagePosition);
					window.smallPreviewArray[index] = options.smallPreview;
					templateRendering(options, images);
					// button = topSectionData.buttons;
					// if (button.length) {
					// 	$.each(button, function(index, val) {
					// 		if (val.attachment != undefined) {
					// 			tmpAttachments.push(val.attachment);
					// 		}
					// 	});
					// }
					topSectionData.image = [];
					topSectionData.image.push(images[0]);
					countTopSectionData();
				},
				onError: function(id, name, errorReason, xhr) {}
			}
		});
	}
	$(document).on("click", ".top-section-toggle", function() {
		
		var _this = $(this),
			data = _this.data();
			//console.log(data);
		// $("input[name='top_status']").not(_this).removeAttr('checked');
		// if (_this.attr('checked') != undefined) {
		// 	return false;
		// }
		$.ajax({
			method: "POST",
			url: './ajax/updateTopSectionStatus.php',
			cache: false,
			dataType: 'json',
			data: {
				"id": data.id,
				"app_id": data.appId,
				"status": data.status,
				"global_check": data.global
			},
			success: function(res) {
				//_this.attr('checked', true);
				$(data.parentEle).notify(res.message, {
					position: 'top right',
					className: res.type,
					showAnimation: 'fadeIn',
					hideAnimation: 'fadeOut',
				});
				location.href = './top-sections.php?app_id=' + data.appId;
			},
		});
	});
});