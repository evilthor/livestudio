<?php
	include 'config.php';
	include 'includes/session_check.php';
	include 'includes/header.php';

	$is_edit = false;
	if(isset($_GET['id'])){
		$sql_cats = "SELECT * FROM tbl_categories WHERE id = '".$_GET['id']."'";
		$result_cats = $db->get_row($sql_cats);

		$sql = "SELECT GROUP_CONCAT(row_id) as row_ids FROM tbl_delete_data WHERE app_id ='".$_GET['app_id']."' AND row_type='categories_row_new'";
		$skip_data = $db->get_row($sql);
		$row_ids_arr_temp = $skip_data->row_ids;

		if($row_ids_arr_temp){
			$row_ids_temp_arr = explode(',', $row_ids_arr_temp);
			$row_ids_arr = "'" . implode ( "', '", $row_ids_temp_arr ) . "'";
			$row_ids_arr = rtrim($row_ids_arr,"'");
			$row_ids_arr = ltrim($row_ids_arr,"'");
		}else{
			$row_ids_arr='';
		}
		
		$layout = '';
		$data = array();
		$sort_sql = "SELECT * FROM tbl_sort_rows WHERE app_id = '" . @$_GET['app_id'] . "' AND row_type_id = '" . @$_GET['id'] . "' AND row_type = 'search_category_new' ORDER BY sort_order ASC";
		$sort_results = $db->get_results($sort_sql);
		if( count($sort_results) ){
				if($yes_global==1){
					$sql = "SELECT tbl_cat_rows_new.* FROM tbl_cat_rows_new JOIN tbl_sort_rows ON tbl_cat_rows_new.id=tbl_sort_rows.row_id where tbl_cat_rows_new.cat_id = '".$result_cats->id."' AND tbl_cat_rows_new.show_in_app = '1' AND tbl_cat_rows_new.is_global='1' AND tbl_cat_rows_new.id NOT IN ('".$row_ids_arr."') AND tbl_cat_rows_new.app_id IN(".@$_GET['app_id'].", ".$global_app_id.") AND tbl_sort_rows.id IN ( SELECT id FROM tbl_sort_rows WHERE tbl_sort_rows.row_type='search_category_new' AND tbl_sort_rows.app_id = '".@$_GET['app_id']."') ORDER BY tbl_sort_rows.sort_order ASC";
					$results = $db->get_results($sql);
				}else{
					$sql = "SELECT tbl_cat_rows_new.* FROM tbl_cat_rows_new JOIN tbl_sort_rows ON tbl_cat_rows_new.id=tbl_sort_rows.row_id where tbl_cat_rows_new.cat_id = '".$result_cats->id."' AND tbl_cat_rows_new.show_in_app = '1' AND tbl_cat_rows_new.id NOT IN ('".$row_ids_arr."') AND tbl_cat_rows_new.app_id IN(".@$_GET['app_id'].", ".$global_app_id.") AND tbl_sort_rows.id IN ( SELECT id FROM tbl_sort_rows WHERE tbl_sort_rows.row_type='search_category_new' AND tbl_sort_rows.app_id = '".@$_GET['app_id']."') ORDER BY tbl_sort_rows.sort_order ASC";
					$results = $db->get_results($sql);
				}
				if(@$results){
					foreach($results as $result){
						$data[] = $result;
						$images = json_decode($result->images);
						$attachments = json_decode($result->attachment);
						$ctitle = $ctitle1 = $ctitle2 = '';
						if($result->layout == "single" || $result->layout == "singleBar" || $result->layout == "singleShort"){
							if( count($attachments) ){
								if($attachments[0][1] == 1){
									$ctitle = 'Category id: '.$attachments[0][2].' Title: '.$attachments[0][3];
								}elseif($attachments[0][1] == 5){
									$ctitle = 'Brand id: '.$attachments[0][2].' Title: '.$attachments[0][3];
								}elseif($attachments[0][1] == 6){
									$ctitle = 'Lander id: '.$attachments[0][2].' Title: '.$attachments[0][3];
								}
							}
							$layout.='<div class="row single-template-layout">
								<div class="col-md-12">
									<div class="preview"><img src="'.$images[0].'" title="'.$ctitle.'"></div>
								</div>
							</div>';
						}elseif($result->layout == "double" || $result->layout == "doubleShort"){
							if( count($attachments) ){
								if($attachments[0][1] == 1){
									$ctitle = 'Category id: '.$attachments[0][2].' Title: '.$attachments[0][3];
								}elseif($attachments[0][1] == 5){
									$ctitle = 'Brand id: '.$attachments[0][2].' Title: '.$attachments[0][3];
								}elseif($attachments[0][1] == 6){
									$ctitle = 'Lander id: '.$attachments[0][2].' Title: '.$attachments[0][3];
								}
								if($attachments[1][1] == 1){
									$ctitle1 = 'Category id: '.$attachments[1][2].' Title: '.$attachments[1][3];
								}elseif($attachments[1][1] == 5){
									$ctitle1 = 'Brand id: '.$attachments[1][2].' Title: '.$attachments[1][3];
								}elseif($attachments[1][1] == 6){
									$ctitle1 = 'Lander id: '.$attachments[1][2].' Title: '.$attachments[1][3];
								}
							}
							$doubleShort  = ( $result->layout == 'doubleShort' ) ?  'short' :  '';
							$layout.='<div class="row double-template-layout '.$doubleShort.'">
								<div class="col-md-6">
									<div class="preview"><img src="'.$images[0].'" title="'.$ctitle.'"></div>
								</div>
								<div class="col-md-6">
									<div class="preview"><img src="'.$images[1].'" title="'.$ctitle1.'"></div>
								</div>
							</div>';
						}elseif($result->layout == "tripleLeft"){
							if( count($attachments) ){
								if($attachments[0][1] == 1){
									$ctitle = 'Category id: '.$attachments[0][2].' Title: '.$attachments[0][3];
								}elseif($attachments[0][1] == 5){
									$ctitle = 'Brand id: '.$attachments[0][2].' Title: '.$attachments[0][3];
								}elseif($attachments[0][1] == 6){
									$ctitle = 'Lander id: '.$attachments[0][2].' Title: '.$attachments[0][3];
								}
								if($attachments[1][1] == 1){
									$ctitle1 = 'Category id: '.$attachments[1][2].' Title: '.$attachments[1][3];
								}elseif($attachments[1][1] == 5){
									$ctitle1 = 'Brand id: '.$attachments[1][2].' Title: '.$attachments[1][3];
								}elseif($attachments[1][1] == 6){
									$ctitle1 = 'Lander id: '.$attachments[1][2].' Title: '.$attachments[1][3];
								}
								if($attachments[2][1] == 1){
									$ctitle2 = 'Category id: '.$attachments[2][2].' Title: '.$attachments[2][3];
								}elseif($attachments[2][1] == 5){
									$ctitle2 = 'Brand id: '.$attachments[2][2].' Title: '.$attachments[2][3];
								}elseif($attachments[2][1] == 6){
									$ctitle2 = 'Lander id: '.$attachments[2][2].' Title: '.$attachments[2][3];
								}
							}
							$layout.='<div class="row preview-triple-left">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-12">
											<div class="preview _1024X704"><img src="'.$images[0].'" title="'.$ctitle.'"></div>
										</div>
										<div class="col-md-12">
											<div class="preview _1024X704"><img src="'.$images[1].'" title="'.$ctitle1.'"></div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="preview _1024X1310"><img src="'.$images[2].'" title="'.$ctitle2.'"></div>
								</div>
							</div>';
						}elseif($result->layout == "tripleRight"){
							if( count($attachments) ){
								if($attachments[0][1] == 1){
									$ctitle = 'Category id: '.$attachments[0][2].' Title: '.$attachments[0][3];
								}elseif($attachments[0][1] == 5){
									$ctitle = 'Brand id: '.$attachments[0][2].' Title: '.$attachments[0][3];
								}elseif($attachments[0][1] == 6){
									$ctitle = 'Lander id: '.$attachments[0][2].' Title: '.$attachments[0][3];
								}
								if($attachments[1][1] == 1){
									$ctitle1 = 'Category id: '.$attachments[1][2].' Title: '.$attachments[1][3];
								}elseif($attachments[1][1] == 5){
									$ctitle1 = 'Brand id: '.$attachments[1][2].' Title: '.$attachments[1][3];
								}elseif($attachments[1][1] == 6){
									$ctitle1 = 'Lander id: '.$attachments[1][2].' Title: '.$attachments[1][3];
								}
								if($attachments[2][1] == 1){
									$ctitle2 = 'Category id: '.$attachments[2][2].' Title: '.$attachments[2][3];
								}elseif($attachments[2][1] == 5){
									$ctitle2 = 'Brand id: '.$attachments[2][2].' Title: '.$attachments[2][3];
								}elseif($attachments[2][1] == 6){
									$ctitle2 = 'Lander id: '.$attachments[2][2].' Title: '.$attachments[2][3];
								}
							}
							$layout.='<div class="row preview-triple-right">
								<div class="col-md-6">
									<div class="preview _1024X1310"><img src="'.$images[0].'" title="'.$ctitle.'"></div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-12">
											<div class="preview _1024X704"><img src="'.$images[1].'" title="'.$ctitle1.'"></div>
										</div>
							
										<div class="col-md-12">
											<div class="preview _1024X704"><img src="'.$images[2].'" title="'.$ctitle2.'"></div>
										</div>
									</div>
								</div>
							</div>';
						}
					} 
				}
			//}
		}else{
			if($yes_global==1){
				$sql = "SELECT * FROM tbl_cat_rows_new WHERE cat_id = '".@$_GET['id']."' AND show_in_app = '1' AND is_global='1' AND id NOT IN ('".$row_ids_arr."')";
				$results = $db->get_results($sql);
			}else{
				$sql = "SELECT * FROM tbl_cat_rows_new WHERE cat_id = '".@$_GET['id']."' AND show_in_app = '1' AND id NOT IN ('".$row_ids_arr."')";
				$results = $db->get_results($sql);
			}
			if(@$results){
				foreach($results as $result){
					$data[] = $result;
					$images = json_decode($result->images);
					$attachments = json_decode($result->attachment);
					$ctitle = $ctitle1 = $ctitle2 = '';
					if($result->layout == "single" || $result->layout == "singleBar"){
						if( count($attachments) ){
							if($attachments[0][1] == 1){
								$ctitle = 'Category id: '.$attachments[0][2].' Title: '.$attachments[0][3];
							}elseif($attachments[0][1] == 5){
								$ctitle = 'Brand id: '.$attachments[0][2].' Title: '.$attachments[0][3];
							}elseif($attachments[0][1] == 6){
								$ctitle = 'Lander id: '.$attachments[0][2].' Title: '.$attachments[0][3];
							}
						}
						$layout.='<div class="row single-template-layout">
							<div class="col-md-12">
								<div class="preview"><img src="'.$images[0].'" title="'.$ctitle.'"></div>
							</div>
						</div>';
					}elseif($result->layout == "double" || $result->layout == "doubleShort"){
						if( count($attachments) ){
							if($attachments[0][1] == 1){
								$ctitle = 'Category id: '.$attachments[0][2].' Title: '.$attachments[0][3];
							}elseif($attachments[0][1] == 5){
								$ctitle = 'Brand id: '.$attachments[0][2].' Title: '.$attachments[0][3];
							}elseif($attachments[0][1] == 6){
								$ctitle = 'Lander id: '.$attachments[0][2].' Title: '.$attachments[0][3];
							}
							if($attachments[1][1] == 1){
								$ctitle1 = 'Category id: '.$attachments[1][2].' Title: '.$attachments[1][3];
							}elseif($attachments[1][1] == 5){
								$ctitle1 = 'Brand id: '.$attachments[1][2].' Title: '.$attachments[1][3];
							}elseif($attachments[1][1] == 6){
								$ctitle1 = 'Lander id: '.$attachments[1][2].' Title: '.$attachments[1][3];
							}
						}
						$doubleShort  = ( $result->layout == 'doubleShort' ) ?  'short' :  '';
						$layout.='<div class="row double-template-layout '.$doubleShort.'">
							<div class="col-md-6">
								<div class="preview"><img src="'.$images[0].'" title="'.$ctitle.'"></div>
							</div>
							<div class="col-md-6">
								<div class="preview"><img src="'.$images[1].'" title="'.$ctitle1.'"></div>
							</div>
						</div>';
					}elseif($result->layout == "tripleLeft"){
						if( count($attachments) ){
							if($attachments[0][1] == 1){
								$ctitle = 'Category id: '.$attachments[0][2].' Title: '.$attachments[0][3];
							}elseif($attachments[0][1] == 5){
								$ctitle = 'Brand id: '.$attachments[0][2].' Title: '.$attachments[0][3];
							}elseif($attachments[0][1] == 6){
								$ctitle = 'Lander id: '.$attachments[0][2].' Title: '.$attachments[0][3];
							}
							if($attachments[1][1] == 1){
								$ctitle1 = 'Category id: '.$attachments[1][2].' Title: '.$attachments[1][3];
							}elseif($attachments[1][1] == 5){
								$ctitle1 = 'Brand id: '.$attachments[1][2].' Title: '.$attachments[1][3];
							}elseif($attachments[1][1] == 6){
								$ctitle1 = 'Lander id: '.$attachments[1][2].' Title: '.$attachments[1][3];
							}
							if($attachments[2][1] == 1){
								$ctitle2 = 'Category id: '.$attachments[2][2].' Title: '.$attachments[2][3];
							}elseif($attachments[2][1] == 5){
								$ctitle2 = 'Brand id: '.$attachments[2][2].' Title: '.$attachments[2][3];
							}elseif($attachments[2][1] == 6){
								$ctitle2 = 'Lander id: '.$attachments[2][2].' Title: '.$attachments[2][3];
							}
						}
						$layout.='<div class="row preview-triple-left">
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-12">
										<div class="preview _1024X704"><img src="'.$images[0].'" title="'.$ctitle.'"></div>
									</div>

									<div class="col-md-12">
										<div class="preview _1024X704"><img src="'.$images[1].'" title="'.$ctitle1.'"></div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="preview _1024X1310"><img src="'.$images[2].'" title="'.$ctitle2.'"></div>
							</div>
						</div>';
					}elseif($result->layout == "tripleRight"){
						if( count($attachments) ){
							if($attachments[0][1] == 1){
								$ctitle = 'Category id: '.$attachments[0][2].' Title: '.$attachments[0][3];
							}elseif($attachments[0][1] == 5){
								$ctitle = 'Brand id: '.$attachments[0][2].' Title: '.$attachments[0][3];
							}elseif($attachments[0][1] == 6){
								$ctitle = 'Lander id: '.$attachments[0][2].' Title: '.$attachments[0][3];
							}
							if($attachments[1][1] == 1){
								$ctitle1 = 'Category id: '.$attachments[1][2].' Title: '.$attachments[1][3];
							}elseif($attachments[1][1] == 5){
								$ctitle1 = 'Brand id: '.$attachments[1][2].' Title: '.$attachments[1][3];
							}elseif($attachments[1][1] == 6){
								$ctitle1 = 'Lander id: '.$attachments[1][2].' Title: '.$attachments[1][3];
							}
							if($attachments[2][1] == 1){
								$ctitle2 = 'Category id: '.$attachments[2][2].' Title: '.$attachments[2][3];
							}elseif($attachments[2][1] == 5){
								$ctitle2 = 'Brand id: '.$attachments[2][2].' Title: '.$attachments[2][3];
							}elseif($attachments[2][1] == 6){
								$ctitle2 = 'Lander id: '.$attachments[2][2].' Title: '.$attachments[2][3];
							}
						}
						$layout.='<div class="row preview-triple-right">
							<div class="col-md-6">
								<div class="preview _1024X1310"><img src="'.$images[0].'" title="'.$ctitle.'"></div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-12">
										<div class="preview _1024X704"><img src="'.$images[1].'" title="'.$ctitle1.'"></div>
									</div>
						
									<div class="col-md-12">
										<div class="preview _1024X704"><img src="'.$images[2].'" title="'.$ctitle2.'"></div>
									</div>
								</div>
							</div>
						</div>';
					}
				} 
			}			
		}
	}
?>
<body>
<form action="" id="sortableForm">
	<input type="hidden" name="command" value="layoutSortCatNew">
	<input type="hidden" name="app_id" value="<?php echo intval($_GET['app_id']) ?>">
	<input type="hidden" name="search_category_id" value="<?php echo intval($_GET['id']) ?>">
	<input type="hidden" name="row_order" value="">
	<input type="hidden" name="row_type" value="search_category_new">
</form>
<div id="wrapper">
	<?php include('includes/navigation.php'); ?>
	<!-- Page Content -->
	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="layoutTitle">
				<div class="form-group">
					<input id="merchandise_title" name="merchandise_title" type="text" class="form-control" placeholder="please fill in title" data-toggle="tooltip" data-placement="right" readonly title="This title is for your reference only and will not be used in the app" value="<?php echo @$result_cats->title; ?>">
					<input type="hidden" name="merchandise_id" id="merchandise_id" value="<?php echo @$_GET['id']; ?>" />
					<input type="hidden" name="app_id" id="app_id" value="<?php echo $_GET['app_id']; ?>" />
				</div>
			</div>
			<div class="clearfix devicePreview">
				<div class="ipadPreviewContainer">
					<div id="carouselOuter" class="carouselRowsManager">
						<div id="carouselCircleLeft"></div>
						<div id="carousel" class="mCustomScrollbar">
							<?php echo $layout; ?>
						</div>
						<div id="carouselCircleRight"></div>
					</div>
				</div>
				<div class="iphonePreviewContainer">
					<div id="carouselOuter" class="carouselRowsManager">
						<div class="carouselCircleTop"></div>
						<div class="carouselRectangleTop"></div>
						<div class="carouselCircleTopLeft"></div>
						<div id="carousel" class="mCustomScrollbar">
							<?php echo $layout; ?>
						</div>
						<div class="carouselCircleBottom"></div>
					</div>
				</div>
			</div>
			<div class="layouts" style="max-height:inherit;">
				<div class="layout-boxs clearfix">
					<ul id="sortable">
						<?php
							$c = 1;
							if(@$data){
								$totalRows = count($data);
								foreach($data as $result){
						?>
						<li id="<?php echo $result->id; ?>">
							<div class="layout-box">
								<div class="overlay" id="single-edit-popup">
									<?php
									if( $result->is_global==0 || $yes_global=='1'){
									?>
									<a class="btn btn-xs btn-primary" href="edit-search-category.php?app_id=<?php echo $_GET['app_id']; ?>&id=<?php echo $_GET['id']; ?>&row_id=<?php echo $result->id; ?>&active=<?php echo $c; ?>">Edit</a>
									<?php
									}
									?>
									<a data-section="searchCatNew" data-appid="<?php echo $_GET['app_id']; ?>" data-merchandiseid="<?php echo $_GET['id']; ?>" data-rowid="<?php echo $result->id; ?>" <?php if($result->is_global==1){?> data-global="1" <?php }?> class="btn btn-xs btn-danger btn-delete-category" href="javascript:;">Delete</a>
								</div>
								<span data-popup="#single-edit-popup">
									<?php
											if($result->layout == "single" || $result->layout == "singleBar" || $result->layout == "singleShort"){
												$images = json_decode($result->images);
									?>
									<div class="single">
										<div class="row">
											<div class="col-md-12">
												<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
											</div>
										</div>
									</div>
									<?php
				                                   }elseif($result->layout == "double" || $result->layout == "doubleShort"){
												$images = json_decode($result->images);
									?>
									<div class="double">
										<div class="row">
											<div class="col-md-6">
												<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
											</div>
											<div class="col-md-6">
												<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
											</div>
										</div>
									</div>
									<?php
			                                      	}elseif($result->layout == "tripleLeft"){
												$images = json_decode($result->images);
									?>
									<div class="triple">
										<div class="row">
											<div class="col-md-6">
												<div class="row">
													<div class="col-md-12">
														<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="img"> <img src="<?php echo $images[2]; ?>" class="img-responsive"> </div>
											</div>
										</div>
									</div>
									<?php
			                                      	}elseif($result->layout == "tripleRight"){
												$images = json_decode($result->images);
									?>
									<div class="triple">
										<div class="row">
											<div class="col-md-6">
												<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
											</div>
											<div class="col-md-6">
												<div class="row">
													<div class="col-md-12">
														<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="img"> <img src="<?php echo $images[2]; ?>" class="img-responsive"> </div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<?php
			                                      	}
											$c++;
									?>
								</span>
							</div>
						</li>
						<?php
									}
								}
						?>
					</ul>
					<div onClick="addSearchCategory('<?php echo @$_GET['app_id']; ?>','<?php echo @$_GET['id']; ?>','<?php echo $c; ?>');" class="layout-box active">
						<span><?php echo $c; ?></span> 
					</div>
					<?php
							$c++;
							if($c < 11){
								while($c <= 10){
					?>
					<div class="layout-box"> <span><?php echo $c; ?></span> </div>
					<?php 
							  		$c++;
							  	}
							} 
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include 'includes/footer.php'; ?>
</body>
</html>
