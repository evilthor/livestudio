<?php include 'config.php'; ?>
<?php include 'includes/session_check.php'; ?>
<?php if(isset($_GET['id']) && $_GET['id'] != ''){
	$checkDB = "SELECT id,status FROM tbl_lander_pages WHERE id = '".$_GET['id']."'";
	$checkDB = $db->get_row($checkDB);
	if($checkDB){
		$sql_rows = "SELECT * FROM tbl_lander_rows WHERE lander_id = '".$_GET['id']."'";
		$result_rows = $db->get_results($sql_rows);
		if($result_rows){
			foreach($result_rows as $result_row){
				$images = $result_row->images;
				$images = json_decode($images);
				if( count($images) > 0 ){
					foreach($images as $image){
						if(file_exists($image)){
							unlink($image);
						}
					}
					$sql_del = "DELETE FROM tbl_lander_rows WHERE id = '".$result_row->id."'";
					$db->query($sql_del);
					}
				}
			}
		$sql_del = "DELETE FROM tbl_lander_pages WHERE id = '".$_GET['id']."'";
		$db->query($sql_del);
		$res = true;
	}
} ?>
<?php include 'includes/header.php'; ?>
<script>
	<?php if(isset($res) && $res == true){ ?>
	window.messages = {
		type : "success",
		message: "Record deleted successfully.",
	}
	<?php }elseif(isset($res) && $res == false && $active == true){ ?>
	window.messages = {
		type : "warning",
		message: "This home layout is currently active.",
	}
	<?php }elseif(isset($res) && $res == false){ ?>
	window.messages = {
		type : "warning",
		message: "Invalid id provided, please try again later.",
	}
	<?php } ?>
</script>
<body>
<div id="wrapper">
	<?php include('includes/navigation.php'); ?>
	<!-- Page Content -->
	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="apps">
						<div class="title">
							<h2>Lander pages manager</h2>
							<a class="btn btn-primary" href="./lander-overview.php?app_id=<?php echo $_GET['app_id']; ?>">New lander page</a>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<div class="row">
				<?php 
						 $sql = "SELECT a.* FROM tbl_lander_pages a WHERE app_id = '".@$_GET['app_id']."'";
						 $landers = $db->get_results($sql);
						 if($landers){
						 	$count=0;
							foreach($landers as $lander){
								if( $count != 0 && $count % 4 == 0 ){
									echo "</div><hr><div class=\"row\">";
								}
								$count++;
							$sql = "SELECT * FROM tbl_lander_rows WHERE lander_id = '".$lander->id."' ORDER BY row_order ASC";
							$results = $db->get_results($sql);
							$result = $results[0];
							if(@$result->layout == "singleBar"){
								$result = @$results[1];
								}
							?>
							<div class="col-md-3">
					<div class="box">
						<div class="merchandise-box" id="merchandise-<?php echo $lander->id; ?>" data-command="merchandise" data-id="<?php echo $lander->id; ?>">
							<div class="head">
								<div class="row">
									<div class="col-md-9">
										<div class="title"><?php echo $lander->title; ?></div>
                                        		<div class="sub-title"><?php echo count($results); ?> Rows&nbsp;|&nbsp;Lander id: <?php echo $lander->id; ?></div>
									</div>
									<div class="col-md-3">
									<script>
										var del_option_<?php echo $lander->id; ?> = {
											type: 'link',
											data :{
												"command" : "delete-lander-pages", 
												"id" : "<?php echo $lander->id; ?>"
											},
											callback: {
												beforeSend: function(xhr) {
													$('#delete-lander-pages-<?php echo $lander->id; ?>').fadeIn();
												},
												complete: function(XHR, textStatus) {
												},
												success: function(data, textStatus, XHR) {
													//alert(data);
													$.alert({
															content:data,
															onClose: function(){
																location.href = 'lander-manager.php?app_id=<?php echo $_GET['app_id']?>';
															}
														
													});
													$('#delete-lander-pages-<?php echo $lander->id; ?>').fadeOut();
													
												},
											}
										}
									</script>
										<a 
											data-merchandiseId="<?php echo $lander->id; ?>" 
											data-appId="<?php echo $lander->app_id; ?>" 
											href="javascript:;" 
											class="btn btn-danger btn-xs pull-right"
											data-element-type="merchandise"
											onClick="Delete(del_option_<?php echo $lander->id; ?>); return false;"
										>delete <span style="display:none" id="delete-lander-pages-<?php echo $lander->id; ?>"><i class="fa fa-spinner fa-spin"></i></span></a> 
									</div>
								</div>
							</div>
                            <div class="content" onClick="window.location.href='lander-overview.php?app_id=<?php echo $_GET['app_id'] ?>&id=<?php echo $lander->id; ?>';">
							<?php
							$images = json_decode(@$result->images);
							if(@$result->layout == "single" || @$result->layout == "singleShort" || @$result->layout == "singleBar"){
								?>
							
								<div class="single">
									<div class="row">
										<div class="col-md-12">
											<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
										</div>
									</div>
								</div>
								<?php
								}elseif(@$result->layout == "double" || @$result->layout == "doubleShort"){
								?>
					
							<div class="double">
									<div class="row">
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
										</div>
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
										</div>
									</div>
								</div>
								<?php
								}elseif(@$result->layout == "tripleLeft"){
								?>
							<div class="triple">
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[2]; ?>" class="img-responsive"> </div>
										</div>
									</div>
								</div>
							<?php
								}elseif(@$result->layout == "tripleRight"){
								?>
								<div class="triple">
									<div class="row">
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[2]; ?>" class="img-responsive"> </div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php
								}
							?>
							</div>
                            <!-- <div class="option">
								<label for="status">Published</label>
								<input data-merchandiseId="<?php echo $lander->id; ?>" data-appId="<?php echo $lander->app_id; ?>" type="radio" id="status" name="status" value="1" <?php if($lander->status == 1){echo"checked";} ?>>
							</div> -->
						</div>
					</div>
				</div>
							<?php
							}
						 }else{
							 ?>
				<div class="col-md-12">
					<p class="alert alert-success">No record found.</p>
				</div>
				<?php
							 }
						 ?>
				
				
				
				
			</div>
		</div>
		<!-- /.container-fluid --> 
	</div>
	<!-- /#page-wrapper --> 
</div>
<!-- /#wrapper -->
<?php include 'includes/footer.php'; ?>
</body>
</html>
