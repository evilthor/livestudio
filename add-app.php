<?php include 'config.php'; ?>
<?php include 'includes/session_check.php'; ?>
<?php

if(isset($_POST["app_name"])): 

  $sql_app_check = "SELECT id FROM tbl_apps WHERE app_store = '".trim($_POST["app_store"])."'";
  $res_app_check = $db->get_row($sql_app_check);
  if($res_app_check){
      header("Location:add-app.php?error=".base64_encode($_POST["app_store"]." app already exists!"));
      exit;
  }

  $res_app_arr = explode('_', $_POST["app_store"]);

  $check_gl_app = "SELECT id FROM tbl_apps WHERE app_store = '".$res_app_arr[0]."_global'";
  $res_gl_app = $db->get_row($check_gl_app);
  $globalAppId = $res_gl_app->id;

	$targetimage = "images/";
	$targetimage = $targetimage . basename( $_FILES['appimage']['name']); 
	$appimage = $targetimage;
	$imageFileType = pathinfo($targetimage,PATHINFO_EXTENSION);
	move_uploaded_file($_FILES['appimage']['tmp_name'], $targetimage);
	$sql = "INSERT INTO tbl_apps SET	
								user_id = '".$_SESSION["id"]."',
								app_name = '".$_POST["app_name"]."',
								app_desc = '".$_POST["app_desc"]."',
								app_store = '".$_POST["app_store"]."',
								appimage = '".$appimage."',
                parent_app = '".$globalAppId."'";
	$error = false;
	if($db->query($sql)){
		$id = $db->insert_id;
    // get gobal app data and inherit it.
      $selected_app = trim($_POST["app_store"]);
      $selected_app_arr = explode("_", $selected_app);
      $lang_app = $selected_app_arr[0];
      $global_app = $lang_app."_global";

      $sql_global = "SELECT id FROM tbl_apps WHERE app_store = '".$global_app."'";
      $res_global = $db->get_row($sql_global);
      if ($res_global) {
          $global_app_id = $res_global->id;

          //merchandise status
          $sql_merch_status = "SELECT * FROM tbl_merchandise_status WHERE app_id = '".$global_app_id."'";
          $res_merch_status = $db->get_results($sql_merch_status);
          if($res_merch_status){
            foreach ($res_merch_status as $key => $res_merch) {
              
              $sql = "INSERT INTO tbl_merchandise_status SET app_id = '".$id."',
                                                      merchandise_id = '".$res_merch->merchandise_id."',
                                                      status = '".$res_merch->status."'";
              $db->query($sql);
            }
          }

          //merchandise/category/top section sort
          $sql_row_sort = "SELECT * FROM tbl_sort_rows WHERE app_id = '".$global_app_id."'";
          $res_row_sort = $db->get_results($sql_row_sort);
          if($res_row_sort){
            foreach ($res_row_sort as $key => $res_row_srt) {
              
              $sql = "INSERT INTO tbl_sort_rows SET
                                    app_id = '" . $id . "',
                                    row_id = '" . $res_row_srt->row_id . "',
                                    row_type = '" . $res_row_srt->row_type . "',
                                    row_type_id = '" . $res_row_srt->row_type_id . "',
                                    sort_order = '" . $res_row_srt->sort_order . "'";

              $db->query($sql);
            }
          }

          //static block status
          $sql_sb_status = "SELECT * FROM tbl_static_block_status WHERE app_id = '".$global_app_id."'";
          $res_sb_status = $db->get_results($sql_sb_status);
          if($res_sb_status){
            foreach ($res_sb_status as $key => $res_sb) {
              
              $sql = "INSERT INTO tbl_static_block_status SET app_id = '".$id."',
                                                      block_id = '".$res_sb->block_id."',
                                                      is_default = '".$res_sb->is_default."'";
              $db->query($sql);
            }
          }

          //top sections status
          $sql_ts_status = "SELECT * FROM tbl_top_sections_status WHERE app_id = '".$global_app_id."'";
          $res_ts_status = $db->get_results($sql_ts_status);
          if($res_ts_status){
            foreach ($res_ts_status as $key => $res_ts) {
              
              $sql = "INSERT INTO tbl_top_sections_status SET app_id = '".$id."',
                                                      top_section_id = '".$res_ts->top_section_id."',
                                                      status = '".$res_ts->status."'";
              $db->query($sql);
            }
          }

           //lander_pages
        /*  $sql_lander = "SELECT * FROM tbl_lander_pages WHERE app_id = '".$global_app_id."'";
          $res_lander = $db->get_results($sql_lander);
          $i=0;
          if($res_lander){
            $temp_landers = array();
            foreach ($res_lander as $key => $lander) {
              //$temp_landers[$i]['old'] = $lander->id;
              $sql = "INSERT INTO tbl_lander_pages SET app_id = '".$id."',
                                                      title = '".$lander->title."',
                                                      cat_id = '".$lander->cat_id."',
                                                      cat_name = '".$lander->cat_name."',
                                                      cat_relation = '".$lander->cat_relation."',
                                                      status = '".$lander->status."'";
              $db->query($sql);
              $lander_id = $db->insert_id;
              $temp_landers[$lander->id] = $lander_id;
              
              $sql_lander_rows = "SELECT * FROM tbl_lander_rows WHERE lander_id = '".$lander->id."' AND show_in_app='1'";
              $res_lander_rows = $db->get_results($sql_lander_rows);
              if($res_lander_rows){
                $attachments='';
                foreach ($res_lander_rows as $key => $lander_row) {
                  $attachments = json_decode($lander_row->attachment);
                  if($attachments[0][1]==6){
                    $attachments[0][2]=$lander_id;
                  }

                  $sql = "INSERT INTO tbl_lander_rows SET lander_id = '".$lander_id."',
                                                    layout = '".$lander_row->layout."',
                                                    images = '".$lander_row->images."',
                                                    attachment = '".json_encode($attachments,JSON_UNESCAPED_UNICODE)."',
                                                    is_global = '1',
                                                    show_in_app = '1',
                                                    row_order = '".$lander_row->row_order."'";
                  $db->query($sql);
                }
              }//if($res_lander_rows)
            $i++;
            }//foreach ($res_lander as $key => $lander)
          }//if($res_lander)

          //blocks
          $sql_blocks = "SELECT * FROM tbl_block WHERE app_id = '".$global_app_id."' AND show_in_app='1'";
          $res_blocks = $db->get_results($sql_blocks);
          if($res_blocks){
            foreach ($res_blocks as $key => $block) {
              
              $sql = "INSERT INTO tbl_block SET app_id = '".$id."',
                                                image_url = '".$block->image_url."',
                                                has_text = '".$block->has_text."',
                                                welcome_text = '".$block->welcome_text."',
                                                is_global = '1',
                                                show_in_app = '1',
                                                is_default = '".$block->is_default."'";
              $db->query($sql);
            }
          }//if($res_blocks)

          //top_sections
          $sql_top_sections = "SELECT * FROM tbl_top_sections WHERE app_id = '".$global_app_id."'";
          $res_top_sections = $db->get_results($sql_top_sections);
          if($res_top_sections){
            $t=0;
            foreach ($res_top_sections as $key => $top_sections) {
              $attachments = json_decode($top_sections->attachments,true);
              $finalAttachments = array();
              foreach($attachments as $attach){
                $attach['value'] = $attach['value'];
                if($attach['attachment'][1] == 6){
                  if($attach['attachment'][2]=='sk' || $attach['attachment'][2]=='sm' || $attach['attachment'][2]=='sw'){
                    // do not update ids in case of static lander.
                    $attach['attachment'][2] = $attach['attachment'][2];
                  }else{
                    $ids = $temp_landers[$attach['attachment'][2]];
                    $attach['attachment'][2] = $ids;
                  }
                  
                }
                $finalAttachments[] = $attach;
                
              }
              $sql = "INSERT INTO tbl_top_sections SET app_id = '".$id."',
                                                image_url = '".$top_sections->image_url."',
                                                attachments = '".json_encode($finalAttachments,JSON_UNESCAPED_UNICODE)."',
                                                status = '".$top_sections->status."'";
              $db->query($sql);
            }
          }//if($res_top_sections)

           //merchandise
          $sql_merchandise = "SELECT * FROM tbl_merchandise WHERE app_id = '".$global_app_id."'";
          $res_merchandise = $db->get_results($sql_merchandise);
          if($res_merchandise){
            foreach ($res_merchandise as $key => $merchandise) {
              
              $sql = "INSERT INTO tbl_merchandise SET app_id = '".$id."',
                                                      title = '".$merchandise->title."',
                                                      status = '".$merchandise->status."'";
              $db->query($sql);
              $merchandise_id = $db->insert_id;

              $sql_merchandise_rows = "SELECT * FROM tbl_rows WHERE merchandise_id = '".$merchandise->id."' AND show_in_app='1'";
              $res_merchandise_rows = $db->get_results($sql_merchandise_rows);
              if($res_merchandise_rows){
                foreach ($res_merchandise_rows as $key => $merchandise_row) {
                 
                  $attachments = json_decode($merchandise_row->attachment,true);
                  $finalAttachments = array();
                  foreach($attachments as $attach){
                    if($attach[1] == 6){
                      $ids = $temp_landers[$attach[2]];
                      $attach[2] = $ids;
                    }
                    $finalAttachments[] = $attach;
                    
                  }
                  $sql = "INSERT INTO tbl_rows SET merchandise_id = '".$merchandise_id."',
                                                    layout = '".$merchandise_row->layout."',
                                                    images = '".$merchandise_row->images."',
                                                    attachment = '".json_encode($finalAttachments,JSON_UNESCAPED_UNICODE)."',
                                                    is_global = '1',
                                                    show_in_app = '1',
                                                    row_order = '".$merchandise_row->row_order."'";
                  $db->query($sql);
                }
              }//if($res_merchandise_rows)
            }//foreach ($res_merchandise as $key => $merchandise)
          }//if($res_merchandise)

          //categories
          $sql_categories = "SELECT * FROM tbl_categories WHERE app_id = '".$global_app_id."'";
          $res_categories = $db->get_results($sql_categories);
          if($res_categories){
            foreach ($res_categories as $key => $category) {
              
              $sql = "INSERT INTO tbl_categories SET app_id = '".$id."',
                                                      title = '".$category->title."',
                                                      status = '".$category->status."'";
              $db->query($sql);
              $category_id = $db->insert_id;

              $sql_category_rows = "SELECT * FROM tbl_cat_rows WHERE merchandise_id = '".$category->id."' AND show_in_app='1'";
              $res_category_rows = $db->get_results($sql_category_rows);
              if($res_category_rows){
                foreach ($res_category_rows as $key => $category_row) {
                  
                  $attachments = json_decode($category_row->attachment,true);
                  $finalAttachments = array();
                  foreach($attachments as $attach){
                    if($attach[1] == 6){
                      $ids = $temp_landers[$attach[2]];
                      $attach[2] = $ids;
                    }
                    $finalAttachments[] = $attach;
                    
                  }

                  $sql = "INSERT INTO tbl_cat_rows SET merchandise_id = '".$category_id."',
                                                    layout = '".$category_row->layout."',
                                                    images = '".$category_row->images."',
                                                    attachment = '".json_encode($finalAttachments,JSON_UNESCAPED_UNICODE)."',
                                                    is_global = '1',
                                                    show_in_app = '1',
                                                    row_order = '".$category_row->row_order."'";
                  $db->query($sql);
                }
              }//if($res_category_rows)
            }//foreach ($res_categories as $key => $category)
          }//if($res_categories)*/
      }//if ($res_global)
     

		/*$sql = "INSERT INTO tbl_categories
				(app_id,title,status)
				VALUES
				('".$id."','Men','1'),
				('".$id."','Women','1')
				";
		$db->query($sql);		*/
		header('Location:apps.php?message=success');exit;
		}else{
			$error = true;
			}
	endif;
?>
<?php include 'includes/header.php'; ?>
<body>
<script>
    <?php if(isset($error) && $error != ''){ ?>
    window.messages = {
        type : "Error",
        message: "Error occured, Please try again later.",
    }
    <?php } ?>

    <?php if(isset($_GET['error']) && $_GET['error'] != ''){ ?>
    window.messages = {
        type : "Error",
        message: "<?php echo base64_decode($_GET['error']);?>",
    }
    <?php } ?>

</script>
     <div id="wrapper">
          <?php include('includes/navigation.php'); ?>
          <div id="page-wrapper">
               <div class="container-fluid">
                    <div class="row">
                         <div class="col-lg-12">
                              <div class="apps">
                                   <div class="title">
                                        <h2>Add new app</h2>
                                   </div>
                                   <div class="clearfix">
                                        <div class="row">
                                             <div class="col-md-2"></div>
                                             <div class="col-md-8">
                                                  <form id="frmAddApp" class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                      <label for="" class="col-sm-2 control-label">App name</label>
                                                      <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="app_name" name="app_name" required placeholder="App name">
                                                      </div>
                                                    </div>
                                                    <div class="form-group">
                                                      <label for="" class="col-sm-2 control-label">App Description</label>
                                                      <div class="col-sm-10">
                                                        <textarea class="form-control" id="app_desc" name="app_desc" required placeholder="App description"></textarea>
                                                      </div>
                                                    </div>
<div class="form-group">
                                                                <?php
                                                          $sql = "SELECT * FROM tbl_stores 
                                                                                WHERE status='1' 
                                                                                order by store_lang";
                                                          $res_stores = $db->get_results($sql);
                                                                $stores = array();
                                                          foreach ($res_stores as $res_store) {
                                                                    if($res_store->store_lang == "en"){
                                                                        $stores['EN'][$res_store->country_code] = $res_store->country_code;
                                                                    }elseif($res_store->store_lang == "ar"){
                                                                        $stores['AR'][$res_store->country_code] = $res_store->country_code;
                                                                    }else{
                                                                        $stores['EN'][$res_store->country_code] = $res_store->country_code;
                                                                        $stores['AR'][$res_store->country_code] = $res_store->country_code;
                                                                    }
                                                                }
                                                       ?>
                                                                    <label for="" class="col-sm-2 control-label">Store</label>
                                                                    <div class="col-sm-10">
                                                                        <select id="app_store" name="app_store" class="form-control" required>
                                                                            <?php foreach ($stores as $index => $store) { ?>
                                                                            <optgroup label="<?php echo $index ?>">
                                                                                <?php
                                                                           foreach ($store as $code => $country) {
                                                                                $value = strtolower($index). "_". strtolower($code);
                                                                      ?>
                                                                                    <option value="<?php echo $value; ?>">
                                                                                        <?php echo strtoupper($country); ?>
                                                                                    </option>
                                                                                    <?php } ?>
                                                                            </optgroup>
                                                                            <?php } ?>

                                                                        </select>
                                                                    </div>
                                                            </div>
                                                    
                                                    <div class="form-group">
                                                      <label for="" class="col-sm-2 control-label">App image</label>
                                                      <div class="col-sm-10">
                                                        <input type="file" class="form-control" id="appimage" name="appimage" required>
                                                      </div>
                                                    </div>
                                                    <div class="form-group">
                                                      <div class="col-sm-offset-2 col-sm-10">
                                                        <button type="submit" class="btn btn-primary">Save</button>
                                                      </div>
                                                    </div>
                                                  </form>                                                  
                                             </div>
                                             <div class="col-md-2"></div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
     <?php include 'includes/footer.php'; ?>
</body>
</html>
