<?php
	include 'config.php';
	include 'includes/session_check.php';
?>
<?php include 'includes/header.php'; ?>
<body>
<div id="wrapper">
	<?php include('includes/navigation.php'); ?>
	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="apps">
						<div class="title">
							<h2>Search Category manager</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<?php

			 		$sql = "SELECT * FROM tbl_categories  WHERE app_id IN(".@$_GET['app_id'].", ".$global_app_id." ) ORDER BY app_id";

			 		$categories = $db->get_results($sql);

			 		$sql = "SELECT GROUP_CONCAT(row_id) as row_ids FROM tbl_delete_data WHERE app_id ='".$_GET['app_id']."' AND row_type='categories_row_new'";
							$skip_data = $db->get_row($sql);
							$row_ids_arr_temp = $skip_data->row_ids;

					if($row_ids_arr_temp){
						$row_ids_temp_arr = explode(',', $row_ids_arr_temp);
						$row_ids_arr = "'" . implode ( "', '", $row_ids_temp_arr ) . "'";
						$row_ids_arr = rtrim($row_ids_arr,"'");
						$row_ids_arr = ltrim($row_ids_arr,"'");
					}else{
						$row_ids_arr='';
					}

				 	if($categories){
						foreach($categories as $category){
							$sql = "SELECT * FROM tbl_sort_rows WHERE app_id = '".@$_GET['app_id']."' AND row_type_id = '".$category->id."' AND row_type='search_category_new'";
							$check = $db->get_results($sql);
							if(count($check) > 0){
								if($yes_global==1){
									$sql = "SELECT * FROM tbl_cat_rows_new JOIN tbl_sort_rows ON tbl_cat_rows_new.id=tbl_sort_rows.row_id AND tbl_sort_rows.row_type='search_category_new' AND tbl_cat_rows_new.cat_id = '$category->id' AND tbl_cat_rows_new.show_in_app = '1' AND tbl_cat_rows_new.is_global='1' AND tbl_cat_rows_new.app_id=tbl_sort_rows.app_id AND tbl_cat_rows_new.app_id IN(".@$_GET['app_id'].", ".$global_app_id." ) ORDER BY tbl_sort_rows.sort_order ASC";
								}else{
									$sql = "SELECT * FROM tbl_cat_rows_new JOIN tbl_sort_rows ON tbl_cat_rows_new.id=tbl_sort_rows.row_id AND tbl_sort_rows.row_type='search_category_new' AND tbl_cat_rows_new.cat_id = '$category->id' AND tbl_cat_rows_new.show_in_app = '1' AND tbl_cat_rows_new.id NOT IN('".$row_ids_arr."') AND tbl_cat_rows_new.app_id=tbl_sort_rows.app_id AND tbl_cat_rows_new.app_id IN(".@$_GET['app_id'].", ".$global_app_id." ) ORDER BY tbl_sort_rows.sort_order ASC";
								}
							}else{
								if($yes_global==1){
									$sql = "SELECT * FROM tbl_cat_rows_new WHERE cat_id = '".$category->id."' AND show_in_app = '1' AND is_global='1' AND id NOT IN('".$row_ids_arr."')";
								}else{
									$sql = "SELECT * FROM tbl_cat_rows WHERE cat_id = '".$category->id."' AND show_in_app = '1' AND id NOT IN('".$row_ids_arr."')";
								}
							}
							$results = $db->get_results($sql);
							$result = $results[0];
							if(@$result->layout == "singleBar"){
								$result = @$results[1];
							}
				?>
				<div class="col-md-3">
					<div class="box">
						<div class="merchandise-box" id="merchandise-<?php echo $category->id; ?>" data-command="category_new" data-id="<?php echo $category->id; ?>">
							<div class="head">
								<div class="row">
									<div class="col-md-9">
										<div class="title"><?php echo $category->title; ?></div>
										<div class="sub-title"><?php echo count($results); ?> Rows</div>
									</div>
									<div class="col-md-3">
										 
									</div>
								</div>
							</div>
					   		<div class="content" onClick="window.location.href='search-category-overview.php?app_id=<?php echo $_GET['app_id'] ?>&id=<?php echo $category->id; ?>';">
								<?php
									$images = json_decode(@$result->images);
									if(@$result->layout == "single" || @$result->layout == "singleBar"){
								?>
								<div class="single">
									<div class="row">
										<div class="col-md-12">
											<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
										</div>
									</div>
								</div>
								<?php }elseif(@$result->layout == "double" || @$result->layout == "doubleShort"){ ?>
								<div class="double">
									<div class="row">
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
										</div>
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
										</div>
									</div>
								</div>
								<?php }elseif(@$result->layout == "tripleLeft"){ ?>
								<div class="triple">
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[2]; ?>" class="img-responsive"> </div>
										</div>
									</div>
								</div>
								<?php }elseif(@$result->layout == "tripleRight"){ ?>
								<div class="triple">
									<div class="row">
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[2]; ?>" class="img-responsive"> </div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<?php
						}
				 	}else{
			 	?>
				<div class="col-md-12">
					<p class="alert alert-success">No record found.</p>
				</div>
				<?php
				 	}
			 	?>
			</div>
		</div>
	</div>
</div>
<?php include 'includes/footer.php'; ?>
</body>
</html>