<?php
     $showOne = isset($showLayoutOne) ? $showLayoutOne : 'both';
     if( $showOne == 'both' || $showOne == 'tall' ){
		$cssClasses = isset($previewLayout) ? $previewLayout : '';
		$id = $uploaderLayoutData['single']['id'];
		$p_id = isset($uploaderLayoutData['single']['p_id']) ? $uploaderLayoutData['single']['p_id'] : '';
?>
<div class="previewPayouts single-layout" id="single-layout" style="<?php echo ($id != '') ? 'display:block;' : ''; ?>">
	<div 
		class="img-thumb <?php echo $cssClasses; ?>" 
		id="singleLayout" 
		data-all-hide=".contentDdPopup" 
		data-click-show="#singleContentDdPopup" 
		data-attachmentpos="0"
		data-pid="<?php echo $p_id ?>"
	></div>
	<div class="contentDdPopup" id="singleContentDdPopup"></div>
</div>
<?php
     }
     if( $showOne == 'both' || $showOne == 'short' ){
		$id = $uploaderLayoutData['singleShort']['id'];
?>
<div class="previewPayouts single-short-layout" id="single-short-layout" style="<?php echo ($id != '') ? 'display:block;' : ''; ?>">
	<div 
		class="img-thumb <?php echo $cssClasses; ?>" 
		id="singleShortLayout" 
		data-all-hide=".contentDdPopup" 
		data-click-show="#singleShortContentDdPopup" 
		data-attachmentpos="0"
		data-pid="<?php echo $p_id ?>"
	></div>
	<div class="contentDdPopup" id="singleShortContentDdPopup"></div>
</div>
<?php } ?>