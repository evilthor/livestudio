<?php
     $showOne = isset($showLayoutOne) ? $showLayoutOne : 'both';
?>
<li class="ddItem singleLayoutContainer">
	<div class="templateTitle">one image:</div>
	<?php
		if( $showOne == 'both' || $showOne == 'short' ){
		     $id = $uploaderLayoutData['singleShort']['id'];
		     $layout = $uploaderLayoutData['singleShort']['layout'];
		     $images = $uploaderLayoutData['singleShort']['images'];
		     $width = $uploaderLayoutData['singleShort']['size'][0];
		     $height = $uploaderLayoutData['singleShort']['size'][1];
	?>	
	<div
		class="layoutItemContainer singleShort"
		data-option='{
			"pageHide":".ddPage",
			"pageShow":".pageSection",
			"layoutShow":".page-7",
			"layoutHide":".upload-section",
			"smallPreviewShow":".previewPayouts#single-short-layout",
			"smallPreviewHide":".previewPayouts"
		}'
		title="Single Short Layout(w: <?php echo $width; ?>px, h: <?php echo $height; ?>px)"
		data-toggle="tooltip" 
		data-placement="right"
	>
		<div class="single-layout">
			<div class="layoutTemplate">
				<div class="singleLayout">
					<div class="layoutItem">
						<div class="grayBlock">
							<div class="blockTitle"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
		}
		if( $showOne == 'both' || $showOne == 'tall' ){
		     $id = $uploaderLayoutData['single']['id'];
		     $layout = $uploaderLayoutData['single']['layout'];
		     $images = $uploaderLayoutData['single']['images'];
		     $width = $uploaderLayoutData['single']['size'][0];
		     $height = $uploaderLayoutData['single']['size'][1];
	?>
	<div
		class="layoutItemContainer singleTall"
		data-option='{
			"pageHide":".ddPage",
			"pageShow":".pageSection",
			"layoutShow":".page-1",
			"layoutHide":".upload-section",
			"smallPreviewShow":".previewPayouts#single-layout",
			"smallPreviewHide":".previewPayouts"
		}'
		title="Single Layout(w: <?php echo $width; ?>px, h: <?php echo $height; ?>px)"
		data-toggle="tooltip" 
		data-placement="right"
	>
		<div class="single-layout">
			<div class="layoutTemplate">
				<div class="singleLayout">
					<div class="layoutItem">
						<div class="grayBlock">
							<div class="blockTitle"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
</li>