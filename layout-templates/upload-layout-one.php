<?php
     $showOne = isset($showLayoutOne) ? $showLayoutOne : 'both';
     if( $showOne == 'both' || $showOne == 'tall' ){
          $id = $uploaderLayoutData['single']['id'];
          $layout = $uploaderLayoutData['single']['layout'];
          $images = $uploaderLayoutData['single']['images'];
          $width = $uploaderLayoutData['single']['size'][0];
          $height = $uploaderLayoutData['single']['size'][1];
?>
<div class="page-1 upload-section">
	<div class="layoutItem">
		<div class="grayBlock">
			<div class="blockTitle">1</div>
		</div>
		<div class="file-uploader">
			<div class="file-uploader-box">
				<div 
                         id="fine-uploader" 
                         data-option='{
                              "layoutType":"single",
                              "layoutTypeDB":"single",
                              "viewTemplate":"single-layout.mt",
                              "iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
                              "iPhoneView": ".iphonePreviewContainer #carousel",
                              "smallPreview": "#singleLayout",
                              "imagePosition": "0"
                         }'
                         data-width="<?php echo $width ?>"
                         data-height="<?php echo $height ?>"
                         class="fine-uploader"
                    > </div>
				<?php if($layout == "single"){ ?>
				<div class="have-image" id="single-<?php echo $id ?>">
                         <button type="button" data-element-hide="#single-<?php echo $id ?>"><i class="fa fa-times" aria-hidden="true"></i></button>
                         <img src="<?php echo $images; ?>" class="img-responsive" />
                    </div>
				<?php } ?>
               </div>
          </div>
	</div>
</div>
<?php
     }
     if( $showOne == 'both' || $showOne == 'short' ){
          $id = $uploaderLayoutData['singleShort']['id'];
          $layout = $uploaderLayoutData['singleShort']['layout'];
          $images = $uploaderLayoutData['singleShort']['images'];
          $width = $uploaderLayoutData['singleShort']['size'][0];
          $height = $uploaderLayoutData['singleShort']['size'][1];
?>
<div class="page-7 upload-section">
     <div class="layoutItem">
          <div class="grayBlock">
               <div class="blockTitle">1</div>
          </div>
          <div class="file-uploader">
               <div class="file-uploader-box">
                    <div 
                         id="fine-uploader-short" 
                         data-option='{
                              "layoutType":"single",
                              "layoutTypeDB":"singleShort",
                              "viewTemplate":"single-layout.mt",
                              "iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
                              "iPhoneView": ".iphonePreviewContainer #carousel",
                              "smallPreview": "#singleShortLayout",
                              "imagePosition": "0"
                         }'
                         data-width="<?php echo $width ?>"
                         data-height="<?php echo $height ?>"
                         class="fine-uploader"
                    > </div>
                    <?php if($layout == "singleShort"){ ?>
                    <div class="have-image" id="singleShort-<?php echo $id ?>">
                         <button type="button" data-element-hide="#singleShort-<?php echo $id ?>"><i class="fa fa-times" aria-hidden="true"></i></button>
                         <img src="<?php echo $images; ?>" class="img-responsive" />
                    </div>
                    <?php } ?>
               </div>
          </div>
     </div>
</div>
<?php } ?>