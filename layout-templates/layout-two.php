<?php
	$showTwo = isset($showLayoutTwo) ? $showLayoutTwo : 'both';
?>
<li class="ddItem twoLayoutContainer">
	<div class="templateTitle">two images:</div>
	<?php
		if( $showTwo == 'both' || $showTwo == 'short' ){ 
			$id = $uploaderLayoutData['doubleShort']['id'];
			$layout = $uploaderLayoutData['doubleShort']['layout'];
			$images = $uploaderLayoutData['doubleShort']['images'];
			$leftWidth = $uploaderLayoutData['doubleShort']['size']['left'][0];
			$leftHeight = $uploaderLayoutData['doubleShort']['size']['left'][1];
			$rightWidth = $uploaderLayoutData['doubleShort']['size']['right'][0];
			$rightHeight = $uploaderLayoutData['doubleShort']['size']['right'][1];
	?>
	<div 
		class="layoutItemContainer doubleShort short" 
		data-option='{
			"pageHide":".ddPage",
			"pageShow":".pageSection",
			"layoutShow":".page-3",
			"layoutHide":".upload-section",
			"smallPreviewShow":".previewPayouts#two-col-layout-short",
			"smallPreviewHide":".previewPayouts"
		}'
	>
		<div class="two-layout">
			<div class="layoutTemplate">
				<div class="twoLayout clearfix">
					<div 
						class="leftItem pull-left"
						title="Left Double Short Layout(w: <?php echo $leftWidth; ?>px, h: <?php echo $leftHeight; ?>px)"
						data-toggle="tooltip" 
						data-placement="right"
					>
						<div class="grayBlock">
							<div class="blockTitle"></div>
						</div>
					</div>
					<div 
						class="rightItem pull-right"
						title="Right Double Short Layout(w: <?php echo $rightWidth; ?>px, h: <?php echo $rightHeight; ?>px)"
						data-toggle="tooltip" 
						data-placement="right"
					>
						<div class="grayBlock">
							<div class="blockTitle"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
		}
		if( $showTwo == 'both' || $showTwo == 'tall' ){
			$id = $uploaderLayoutData['double']['id'];
			$layout = $uploaderLayoutData['double']['layout'];
			$images = $uploaderLayoutData['double']['images'];
			$leftWidth = $uploaderLayoutData['double']['size']['left'][0];
			$leftHeight = $uploaderLayoutData['double']['size']['left'][1];
			$rightWidth = $uploaderLayoutData['double']['size']['right'][0];
			$rightHeight = $uploaderLayoutData['double']['size']['right'][1];
	?>
	<div 
		class="layoutItemContainer doubleTall" 
		data-option='{
			"pageHide":".ddPage",
			"pageShow":".pageSection",
			"layoutShow":".page-2",
			"layoutHide":".upload-section",
			"smallPreviewShow":".previewPayouts#two-col-layout",
			"smallPreviewHide":".previewPayouts"
		}'
	>
		<div class="two-layout">
			<div class="layoutTemplate">
				<div class="twoLayout clearfix">
					<div 
						class="leftItem pull-left"
						title="Left Double Layout(w: <?php echo $leftWidth; ?>px, h: <?php echo $leftHeight; ?>px)"
						data-toggle="tooltip" 
						data-placement="right"
					>
						<div class="grayBlock">
							<div class="blockTitle"></div>
						</div>
					</div>
					<div 
						class="rightItem pull-right"
						title="Right Double Layout(w: <?php echo $rightWidth; ?>px, h: <?php echo $rightHeight; ?>px)"
						data-toggle="tooltip" 
						data-placement="right"
					>
						<div class="grayBlock">
							<div class="blockTitle"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
</li>