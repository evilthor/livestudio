<?php
     $showBar = isset($showSingleBar) ? $showSingleBar : 'yes';
     if( $showBar == 'yes' ){
		$cssClasses = isset($previewLayout) ? $previewLayout : '';
		$id = $uploaderLayoutData['singleBar']['id'];
		$p_id = isset($uploaderLayoutData['singleBar']['p_id']) ? $uploaderLayoutData['singleBar']['p_id'] : '';
?>
<div class="previewPayouts bar-layout" id="bar-layout" style="<?php echo ($id != '') ? 'display:block;' : ''; ?>">
	<div class="img-thumb <?php echo $cssClasses; ?>" id="barLayout" data-all-hide=".contentDdPopup" data-click-show="#barContentDdPopup" data-attachmentpos="0" data-pid="<?php echo $p_id ?>"></div>
	<div class="contentDdPopup" id="barContentDdPopup"></div>
</div>
<?php } ?>