<?php
	$cssClasses = isset($previewLayout) ? $previewLayout : '';
	$showLayoutTwo = isset($showLayoutTwo) ? $showLayoutTwo : 'both';
	if( $showLayoutTwo == 'both' || $showLayoutTwo == 'tall' ){
		$id = $uploaderLayoutData['double']['id'];
		$p_id = isset($uploaderLayoutData['double']['p_id']) ? $uploaderLayoutData['double']['p_id'] : '';
?>
<div class="previewPayouts two-col-layout clearfix" id="two-col-layout" style="<?php echo ($id != '') ? 'display:block;' : ''; ?>">
	<div 
		class="img-thumb pull-left <?php echo $cssClasses; ?>" 
		id="twoCol-left" 
		data-all-hide=".contentDdPopup" 
		data-click-show="#twoColLeftContentDdPopup" 
		data-attachmentpos="0"
		data-id="<?php echo $id ?>"
	></div>
	<div class="contentDdPopup" id="twoColLeftContentDdPopup"></div>
	<div 
		class="img-thumb pull-left <?php echo $cssClasses; ?>" 
		id="twoCol-right" 
		data-all-hide=".contentDdPopup" 
		data-click-show="#twoColRightContentDdPopup" 
		data-attachmentpos="1"
		data-id="<?php echo $id ?>"
	></div>
	<div class="contentDdPopup" id="twoColRightContentDdPopup"></div>
</div>
<?php
	}
	if( $showLayoutTwo == 'both' || $showLayoutTwo == 'short' ){
		$id = $uploaderLayoutData['doubleShort']['id'];
		$p_id = isset($uploaderLayoutData['doubleShort']['p_id']) ? $uploaderLayoutData['doubleShort']['p_id'] : '';
?>
<div class="previewPayouts two-col-layout-short clearfix" id="two-col-layout-short" style="<?php echo ($id != '') ? 'display:block;' : ''; ?>">
	<div 
		class="img-thumb pull-left <?php echo $cssClasses; ?>" 
		id="twoCol-left-short" 
		data-all-hide=".contentDdPopup" 
		data-click-show="#twoColLeftContentDdPopupShort" 
		data-attachmentpos="0"
		data-pid="<?php echo $p_id ?>"
	></div>
	<div class="contentDdPopup" id="twoColLeftContentDdPopupShort"></div>
	<div 
		class="img-thumb pull-left <?php echo $cssClasses; ?>" 
		id="twoCol-right-short" 
		data-all-hide=".contentDdPopup" 
		data-click-show="#twoColRightContentDdPopupShort" 
		data-attachmentpos="1"
		data-pid="<?php echo $p_id ?>"
	></div>
	<div class="contentDdPopup" id="twoColRightContentDdPopupShort"></div>
</div>
<?php } ?>