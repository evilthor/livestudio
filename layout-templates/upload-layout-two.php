<?php
	$showLayoutTwo = isset($showLayoutTwo) ? $showLayoutTwo : 'both';
	if( $showLayoutTwo == 'both' || $showLayoutTwo == 'short' ){
		$id = $uploaderLayoutData['doubleShort']['id'];
		$layout = $uploaderLayoutData['doubleShort']['layout'];
		$images = $uploaderLayoutData['doubleShort']['images'];
		$leftWidth = $uploaderLayoutData['doubleShort']['size']['left'][0];
		$leftHeight = $uploaderLayoutData['doubleShort']['size']['left'][1];
		$rightWidth = $uploaderLayoutData['doubleShort']['size']['right'][0];
		$rightHeight = $uploaderLayoutData['doubleShort']['size']['right'][1];
?>
<div class="page-3 upload-section">
	<div class="layoutTemplate">
		<div class="twoLayoutShort clearfix">
			<div class="leftItem pull-left">
				<div class="grayBlock">
					<div class="blockTitle">1</div>
				</div>
			</div>
			<div class="rightItem pull-right">
				<div class="grayBlock">
					<div class="blockTitle">2</div>
				</div>
			</div>
		</div>
		<div class="file-uploader clearfix">
			<div class="file-uploader-box">
				<div
					id="twoLayoutShort-fine-uploader-left"
					class="fine-uploader"
					data-width="<?php echo $leftWidth; ?>"
					data-height="<?php echo $leftHeight; ?>"
					data-option='{
						"layoutType":"double",
						"layoutTypeDB":"doubleShort",
						"viewTemplate":"double-layout.mt",
						"iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
						"iPhoneView": ".iphonePreviewContainer #carousel",
						"smallPreview": "#twoCol-left-short",
						"imagePosition": "0"
					}'
				> </div>
				<?php if($layout == "doubleShort"){ ?>
				<div class="have-image" id="doubleShort-left-<?php echo $id ?>">
                         <button type="button" data-element-hide="#doubleShort-left-<?php echo $id ?>"><i class="fa fa-times" aria-hidden="true"></i></button>
                         <img src="<?php echo $images[0]; ?>" class="img-responsive" />
                    </div>
				<?php } ?>
			</div>
			<div class="file-uploader-box">
				<div
					id="twoLayoutShort-fine-uploader-right"
					class="fine-uploader"
					data-width="<?php echo $rightWidth; ?>"
					data-height="<?php echo $rightHeight; ?>"
					data-option='{
						"layoutType":"double",
						"layoutTypeDB":"doubleShort",
						"viewTemplate":"double-layout.mt",
						"iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
						"iPhoneView": ".iphonePreviewContainer #carousel",
						"smallPreview": "#twoCol-right-short",
						"imagePosition": "1"
					}'
				> </div>
				<?php if($layout == "doubleShort"){ ?>
				<div class="have-image" id="doubleShort-right-<?php echo $id ?>">
                         <button type="button" data-element-hide="#doubleShort-right-<?php echo $id ?>"><i class="fa fa-times" aria-hidden="true"></i></button>
                         <img src="<?php echo $images[1]; ?>" class="img-responsive" />
                    </div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php
	}
	if( $showLayoutTwo == 'both' || $showLayoutTwo == 'tall' ){
		$id = $uploaderLayoutData['double']['id'];
		$layout = $uploaderLayoutData['double']['layout'];
		$images = $uploaderLayoutData['double']['images'];
		$leftWidth = $uploaderLayoutData['double']['size']['left'][0];
		$leftHeight = $uploaderLayoutData['double']['size']['left'][1];
		$rightWidth = $uploaderLayoutData['double']['size']['right'][0];
		$rightHeight = $uploaderLayoutData['double']['size']['right'][1];
?>
<div class="page-2 upload-section">
	<div class="layoutTemplate">
		<div class="twoLayout clearfix">
			<div class="leftItem pull-left">
				<div class="grayBlock">
					<div class="blockTitle">1</div>
				</div>
			</div>
			<div class="rightItem pull-right">
				<div class="grayBlock">
					<div class="blockTitle">2</div>
				</div>
			</div>
		</div>
		<div class="file-uploader clearfix">
			<div class="file-uploader-box">
				<div
                         id="twoLayout-fine-uploader-left"
                         class="fine-uploader"
                         data-width="<?php echo $leftWidth; ?>"
                         data-height="<?php echo $leftHeight; ?>"
                         data-option='{
                              "layoutType":"double",
                              "layoutTypeDB":"double",
                              "viewTemplate":"double-layout.mt",
                              "iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
                              "iPhoneView": ".iphonePreviewContainer #carousel",
                              "smallPreview": "#twoCol-left",
                              "imagePosition": "0"
                         }'
                    > </div>
                         <?php if($layout == "double"){ ?>
                         <div class="have-image" id="double-left-<?php echo $id ?>">
                              <button type="button" data-element-hide="#double-left-<?php echo $id ?>"><i class="fa fa-times" aria-hidden="true"></i></button>
                              <img src="<?php echo $images[0]; ?>" class="img-responsive" />
                         </div>
                         <?php } ?>
			</div>
			<div class="file-uploader-box">
				<div
                         id="twoLayout-fine-uploader-right"
                         class="fine-uploader"
                         data-width="<?php echo $rightWidth; ?>"
                         data-height="<?php echo $rightHeight; ?>"
                         data-option='{
                              "layoutType":"double",
                              "layoutTypeDB":"double",
                              "viewTemplate":"double-layout.mt",
                              "iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
                              "iPhoneView": ".iphonePreviewContainer #carousel",
                              "smallPreview": "#twoCol-right",
                              "imagePosition": "1"
                         }'
                    > </div>
                    <?php if($layout == "double"){ ?>
                    <div class="have-image" id="double-right-<?php echo $id ?>">
                         <button type="button" data-element-hide="#double-right-<?php echo $id ?>"><i class="fa fa-times" aria-hidden="true"></i></button>
                         <img src="<?php echo $images[1]; ?>" class="img-responsive" />
                    </div>
                    <?php } ?>
			</div>
		</div>
	</div>
</div>
<?php } ?>