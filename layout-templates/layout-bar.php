<?php
     $showBar = isset($showSingleBar) ? $showSingleBar : 'yes';
     if( $showBar == 'yes' ){
		$id = $uploaderLayoutData['singleBar']['id'];
		$layout = $uploaderLayoutData['singleBar']['layout'];
		$images = $uploaderLayoutData['singleBar']['images'];
		$width = $uploaderLayoutData['singleBar']['size'][0];
		$height = $uploaderLayoutData['singleBar']['size'][1];
?>
<li class="ddItem barLayoutContainer">
	<div class="templateTitle">bar:</div>
	<div
          class="layoutItemContainer singleBar"
          data-option='{
               "pageHide":".ddPage",
               "pageShow":".pageSection",
               "layoutShow":".page-6",
               "layoutHide":".upload-section",
               "smallPreviewShow":".previewPayouts#bar-layout",
               "smallPreviewHide":".previewPayouts"
          }'
		title="Single Bar Layout(w: <?php echo $width; ?>px, h: <?php echo $height; ?>px)"
		data-toggle="tooltip" 
		data-placement="right"
     >
		<div class="bar-layout">
			<div class="layoutTemplate">
				<div class="barLayout">
					<div class="layoutItem">
						<div class="grayBlock">
							<div class="blockTitle"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</li>
<?php } ?>