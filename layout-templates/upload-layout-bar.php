<?php
     $showBar = isset($showSingleBar) ? $showSingleBar : 'yes';
     if( $showBar == 'yes' ){
		$id = $uploaderLayoutData['singleBar']['id'];
		$layout = $uploaderLayoutData['singleBar']['layout'];
		$images = $uploaderLayoutData['singleBar']['images'];
		$width = $uploaderLayoutData['singleBar']['size'][0];
		$height = $uploaderLayoutData['singleBar']['size'][1];
?>
<div class="page-6 upload-section">
	<div class="layoutItem">
		<div class="grayBlock">
			<div class="blockTitle">1</div>
		</div>
		<div class="file-uploader">
			<div class="file-uploader-box">
				<div
					id="fine-uploader-bar"
					class="fine-uploader"
					data-width="<?php echo $width; ?>"
					data-height="<?php echo $height; ?>"
					data-option='{
						"layoutType":"single",
						"layoutTypeDB":"singleBar",
						"viewTemplate":"single-layout.mt",
						"iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
						"iPhoneView": ".iphonePreviewContainer #carousel",
						"smallPreview": "#barLayout",
						"imagePosition": "0"
					}'
				> </div>
				<?php if($layout == "singleBar"){ ?>
				<div class="have-image" id="singleBar-1-<?php echo $id ?>">
					<button type="button" data-element-hide="#singleBar-1-<?php echo $id ?>"><i class="fa fa-times" aria-hidden="true"></i></button>
					<img src="<?php echo $images; ?>" class="img-responsive" />
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php } ?>