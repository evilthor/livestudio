<?php
	$cssClasses = isset($previewLayout) ? $previewLayout : '';
	$showLayoutThree = isset($showLayoutThree) ? $showLayoutThree : 'both';
	if( $showLayoutThree == 'both' || $showLayoutThree == 'left' ){
		$id = $uploaderLayoutData['tripleLeft']['id'];
		$p_id = isset($uploaderLayoutData['tripleLeft']['p_id']) ? $uploaderLayoutData['tripleLeft']['p_id'] : '';
?>
<div class="previewPayouts three-col-layout-left clearfix" id="three-col-layout-left" style="<?php echo ($id != '') ? 'display:block;' : ''; ?>">
	<div class="img-thumb pull-left <?php echo $cssClasses; ?>" id="threeCol-left-1" data-all-hide=".contentDdPopup" data-click-show="#threeColLeftContentDdPopupShort-1" data-attachmentpos="0" data-pid="<?php echo $p_id ?>"></div>
	<div class="contentDdPopup" id="threeColLeftContentDdPopupShort-1"></div>
	<div class="img-thumb pull-left <?php echo $cssClasses; ?>" id="threeCol-left-2" data-all-hide=".contentDdPopup" data-click-show="#threeColLeftContentDdPopupShort-2" data-attachmentpos="1" data-pid="<?php echo $p_id ?>"></div>
	<div class="contentDdPopup" id="threeColLeftContentDdPopupShort-2"></div>
	<div class="img-thumb pull-left <?php echo $cssClasses; ?>" id="threeCol-left-3" data-all-hide=".contentDdPopup" data-click-show="#threeColLeftContentDdPopupShort-3" data-attachmentpos="2" data-pid="<?php echo $p_id ?>"></div>
	<div class="contentDdPopup" id="threeColLeftContentDdPopupShort-3"></div>
</div>
<?php
	}
	if( $showLayoutThree == 'both' || $showLayoutThree == 'right' ){
		$id = $uploaderLayoutData['tripleRight']['id'];
		$p_id = isset($uploaderLayoutData['tripleRight']['p_id']) ? $uploaderLayoutData['tripleRight']['p_id'] : '';
?>
<div class="previewPayouts three-col-layout-right clearfix" id="three-col-layout-right" style="<?php echo ($id != '') ? 'display:block;' : ''; ?>">
	<div class="img-thumb pull-left <?php echo $cssClasses; ?>" id="threeCol-right-1" data-all-hide=".contentDdPopup" data-click-show="#threeColRightContentDdPopupShort-1" data-attachmentpos="0" data-pid="<?php echo $p_id ?>"></div>
	<div class="contentDdPopup" id="threeColRightContentDdPopupShort-1"></div>
	<div class="img-thumb pull-left <?php echo $cssClasses; ?>" id="threeCol-right-2" data-all-hide=".contentDdPopup" data-click-show="#threeColRightContentDdPopupShort-2" data-attachmentpos="1" data-pid="<?php echo $p_id ?>"></div>
	<div class="contentDdPopup" id="threeColRightContentDdPopupShort-2"></div>
	<div class="img-thumb pull-left <?php echo $cssClasses; ?>" id="threeCol-right-3" data-all-hide=".contentDdPopup" data-click-show="#threeColRightContentDdPopupShort-3" data-attachmentpos="2" data-pid="<?php echo $p_id ?>"></div>
	<div class="contentDdPopup" id="threeColRightContentDdPopupShort-3"></div>
</div>
<?php } ?>