<?php
	$showLayoutThree = isset($showLayoutThree) ? $showLayoutThree : 'both';
	if( $showLayoutThree == 'both' || $showLayoutThree == 'left' ){
		$id = $uploaderLayoutData['tripleLeft']['id'];
		$layout = $uploaderLayoutData['tripleLeft']['layout'];
		$images = $uploaderLayoutData['tripleLeft']['images'];
		$leftWidth_1 = $uploaderLayoutData['tripleLeft']['size']['left'][0][0];
		$leftHeight_1 = $uploaderLayoutData['tripleLeft']['size']['left'][0][1];
		$leftWidth_2 = $uploaderLayoutData['tripleLeft']['size']['left'][1][0];
		$leftHeight_2 = $uploaderLayoutData['tripleLeft']['size']['left'][1][1];
		$rightWidth = $uploaderLayoutData['tripleLeft']['size']['right'][0];
		$rightHeight = $uploaderLayoutData['tripleLeft']['size']['right'][1];
?>
<div class="page-4 upload-section">
	<div class="layoutTemplate">
		<div class="threeLayout clearfix">
			<div class="leftItem pull-left">
				<div class="grayBlock">
					<div class="blockTitle">1</div>
				</div>
				<div class="grayBlock">
					<div class="blockTitle">2</div>
				</div>
			</div>
			<div class="rightItem pull-right">
				<div class="grayBlock">
					<div class="blockTitle">3</div>
				</div>
			</div>
		</div>
		<div class="file-uploader clearfix">
			<div class="leftCol pull-left">
				<div class="file-uploader-box">
					<div
						id="threeLayout-fine-uploader-left-1"
						class="fine-uploader"
						data-option='{
							"layoutType":"triple",
							"layoutTypeDB":"tripleLeft",
							"viewTemplate":"triple-left-layout.mt",
							"iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
							"iPhoneView": ".iphonePreviewContainer #carousel",
							"smallPreview": "#threeCol-left-1",
							"imagePosition": "0"
						}'
						data-width="<?php echo $leftWidth_1 ?>"
						data-height="<?php echo $leftHeight_1 ?>"
					> </div>
					<?php if($layout == "tripleLeft"){ ?>
					<div class="have-image" id="tripleLeft-1-<?php echo $id ?>">
						<button type="button" data-element-hide="#tripleLeft-1-<?php echo $id ?>"><i class="fa fa-times" aria-hidden="true"></i></button>
						<img src="<?php echo $images[0]; ?>" class="img-responsive" />
					</div>
					<?php } ?>
				</div>
				<div class="file-uploader-box">
					<div
						id="threeLayout-fine-uploader-left-2"
						class="fine-uploader"
						data-option='{
							"layoutType":"triple",
							"layoutTypeDB":"tripleLeft",
							"viewTemplate":"triple-left-layout.mt",
							"iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
							"iPhoneView": ".iphonePreviewContainer #carousel",
							"smallPreview": "#threeCol-left-2",
							"imagePosition": "1"
						}'
						data-width="<?php echo $leftWidth_2 ?>"
						data-height="<?php echo $leftHeight_2 ?>"
					> </div>
					<?php if($layout == "tripleLeft"){ ?>
					<div class="have-image" id="tripleLeft-2-<?php echo $id ?>">
						<button type="button" data-element-hide="#tripleLeft-2-<?php echo $id ?>"><i class="fa fa-times" aria-hidden="true"></i></button>
						<img src="<?php echo $images[1]; ?>" class="img-responsive" />
					</div>
					<?php } ?>
				</div>
			</div>
			<div class="rightCol pull-right">
				<div class="file-uploader-box">
					<div
						id="threeLayout-fine-uploader-left-3"
						class="fine-uploader"
						data-option='{
							"layoutType":"triple",
							"layoutTypeDB":"tripleLeft",
							"viewTemplate":"triple-left-layout.mt",
							"iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
							"iPhoneView": ".iphonePreviewContainer #carousel",
							"smallPreview": "#threeCol-left-3",
							"imagePosition": "2"
						}'
						data-width="<?php echo $rightWidth ?>"
						data-height="<?php echo $rightHeight ?>"
					> </div>
					<?php if($layout == "tripleLeft"){ ?>
					<div class="have-image" id="tripleLeft-3-<?php echo $id ?>">
						<button type="button" data-element-hide="#tripleLeft-3-<?php echo $id ?>"><i class="fa fa-times" aria-hidden="true"></i></button>
						<img src="<?php echo $images[2]; ?>" class="img-responsive" />
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	}
	if( $showLayoutThree == 'both' || $showLayoutThree == 'right' ){
		$id = $uploaderLayoutData['tripleRight']['id'];
		$layout = $uploaderLayoutData['tripleRight']['layout'];
		$images = $uploaderLayoutData['tripleRight']['images'];
		$leftWidth = $uploaderLayoutData['tripleRight']['size']['left'][0];
		$leftHeight = $uploaderLayoutData['tripleRight']['size']['left'][1];
		$rightWidth_1 = $uploaderLayoutData['tripleRight']['size']['right'][0][0];
		$rightHeight_1 = $uploaderLayoutData['tripleRight']['size']['right'][0][1];
		$rightWidth_2 = $uploaderLayoutData['tripleRight']['size']['right'][1][0];
		$rightHeight_2 = $uploaderLayoutData['tripleRight']['size']['right'][1][1];
?>
<div class="page-5 upload-section">
	<div class="layoutTemplate">
		<div class="threeLayout clearfix">
			<div class="leftItem pull-left">
				<div class="grayBlock">
					<div class="blockTitle">1</div>
				</div>
			</div>
			<div class="rightItem pull-right">
				<div class="grayBlock">
					<div class="blockTitle">2</div>
				</div>
				<div class="grayBlock">
					<div class="blockTitle">3</div>
				</div>
			</div>
		</div>
		<div class="file-uploader clearfix">
			<div class="leftCol pull-left">
				<div class="file-uploader-box">
					<div 
						id="threeLayout-fine-uploader-right-1"
						class="fine-uploader"
						data-option='{
							"layoutType":"triple",
							"layoutTypeDB":"tripleRight",
							"viewTemplate":"triple-right-layout.mt",
							"iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
							"iPhoneView": ".iphonePreviewContainer #carousel",
							"smallPreview": "#threeCol-right-1",
							"imagePosition": "0"
						}'
						data-width="<?php echo $leftWidth ?>"
						data-height="<?php echo $leftHeight ?>"
					> </div>
					<?php if($layout == "tripleRight"){ ?>
					<div class="have-image" id="tripleRight-1-<?php echo $id ?>">
						<button type="button" data-element-hide="#tripleRight-1-<?php echo $id ?>"><i class="fa fa-times" aria-hidden="true"></i></button>
						<img src="<?php echo $images[0]; ?>" class="img-responsive" />
					</div>
					<?php } ?>
				</div>
			</div>
			<div class="rightCol pull-right">
				<div class="file-uploader-box">
					<div 
						id="threeLayout-fine-uploader-right-2" 
						class="fine-uploader"
						data-option='{
								"layoutType":"triple",
								"layoutTypeDB":"tripleRight",
								"viewTemplate":"triple-right-layout.mt",
								"iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
								"iPhoneView": ".iphonePreviewContainer #carousel",
								"smallPreview": "#threeCol-right-2",
								"imagePosition": "1"
							}'
							data-width="<?php echo $rightWidth_1 ?>"
							data-height="<?php echo $rightHeight_1 ?>"
					></div>
					<?php if($layout == "tripleRight"){ ?>
					<div class="have-image" id="tripleRight-2-<?php echo $id ?>">
						<button type="button" data-element-hide="#tripleRight-2-<?php echo $id ?>"><i class="fa fa-times" aria-hidden="true"></i></button>
						<img src="<?php echo $images[1]; ?>" class="img-responsive" />
					</div>
					<?php } ?>
				</div>
				<div class="file-uploader-box">
					<div 
						id="threeLayout-fine-uploader-right-3" 
						class="fine-uploader"
						data-option='{
								"layoutType":"triple",
								"layoutTypeDB":"tripleRight",
								"viewTemplate":"triple-right-layout.mt",
								"iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
								"iPhoneView": ".iphonePreviewContainer #carousel",
								"smallPreview": "#threeCol-right-3",
								"imagePosition": "2"
							}'
							data-width="<?php echo $rightWidth_2 ?>"
							data-height="<?php echo $rightHeight_2 ?>"
					></div>
					<?php if($layout == "tripleRight"){ ?>
					<div class="have-image" id="tripleRight-3-<?php echo $id ?>">
						<button type="button" data-element-hide="#tripleRight-3-<?php echo $id ?>"><i class="fa fa-times" aria-hidden="true"></i></button>
						<img src="<?php echo $images[2]; ?>" class="img-responsive" />
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>