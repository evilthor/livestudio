<?php
	$showThree = isset($showLayoutThree) ? $showLayoutThree : 'both';
?>
<li class="ddItem threeLayoutContainer">
	<?php if( $showThree != 'none' ){  ?>
	<div class="templateTitle">three images:</div>
	<?php } ?>
	<?php
		if( $showThree == 'both' || $showThree == 'left' ){ 
			$id = $uploaderLayoutData['tripleLeft']['id'];
			$layout = $uploaderLayoutData['tripleLeft']['layout'];
			$images = $uploaderLayoutData['tripleLeft']['images'];
			$leftWidth_1 = $uploaderLayoutData['tripleLeft']['size']['left'][0][0];
			$leftHeight_1 = $uploaderLayoutData['tripleLeft']['size']['left'][0][1];
			$leftWidth_2 = $uploaderLayoutData['tripleLeft']['size']['left'][1][0];
			$leftHeight_2 = $uploaderLayoutData['tripleLeft']['size']['left'][1][1];
			$rightWidth = $uploaderLayoutData['tripleLeft']['size']['right'][0];
			$rightHeight = $uploaderLayoutData['tripleLeft']['size']['right'][1];
	?>
	<div 
		class="layoutItemContainer tripleLeft" 
          data-option='{
               "pageHide":".ddPage",
               "pageShow":".pageSection",
               "layoutShow":".page-4",
               "layoutHide":".upload-section",
               "smallPreviewShow":".previewPayouts#three-col-layout-left",
               "smallPreviewHide":".previewPayouts"
          }'
     >
		<div class="three-layout-left">
			<div class="layoutTemplate">
				<div class="threeLayout clearfix">
					<div
						class="leftItem pull-left"
						title="Left Short Layout(w: <?php echo $leftWidth_1; ?>px, h: <?php echo $leftHeight_1; ?>px)"
						data-toggle="tooltip" 
						data-placement="right"
					>
						<div class="grayBlock">
							<div class="blockTitle"></div>
						</div>
						<div class="grayBlock">
							<div class="blockTitle"></div>
						</div>
					</div>
					<div
						class="rightItem pull-right"
						title="Right Large Layout(w: <?php echo $rightWidth; ?>px, h: <?php echo $rightHeight; ?>px)"
						data-toggle="tooltip" 
						data-placement="right"
					>
						<div class="grayBlock">
							<div class="blockTitle"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
		}
		if( $showThree == 'both' || $showThree == 'right' ){
			$id = $uploaderLayoutData['tripleRight']['id'];
			$layout = $uploaderLayoutData['tripleRight']['layout'];
			$images = $uploaderLayoutData['tripleRight']['images'];
			$leftWidth = $uploaderLayoutData['tripleRight']['size']['left'][0];
			$leftHeight = $uploaderLayoutData['tripleRight']['size']['left'][1];
			$rightWidth_1 = $uploaderLayoutData['tripleRight']['size']['right'][0][0];
			$rightHeight_1 = $uploaderLayoutData['tripleRight']['size']['right'][0][1];
			$rightWidth_2 = $uploaderLayoutData['tripleRight']['size']['right'][1][0];
			$rightHeight_2 = $uploaderLayoutData['tripleRight']['size']['right'][1][1];
	?>
	<div 
		class="layoutItemContainer tripleRight" 
          data-option='{
               "pageHide":".ddPage",
               "pageShow":".pageSection",
               "layoutShow":".page-5",
               "layoutHide":".upload-section",
               "smallPreviewShow":".previewPayouts#three-col-layout-right",
               "smallPreviewHide":".previewPayouts"
          }'
     >
		<div class="three-layout-right">
			<div class="layoutTemplate">
				<div class="threeLayout clearfix">
					<div
						class="leftItem pull-left"
						title="Left Large Layout(w: <?php echo $rightWidth; ?>px, h: <?php echo $rightHeight; ?>px)"
						data-toggle="tooltip" 
						data-placement="right"
					>
						<div class="grayBlock">
							<div class="blockTitle"></div>
						</div>
					</div>
					<div
						class="rightItem pull-right"
						title="Right Short Layout(w: <?php echo $leftWidth_1; ?>px, h: <?php echo $leftHeight_1; ?>px)"
						data-toggle="tooltip" 
						data-placement="right"
					>
						<div class="grayBlock">
							<div class="blockTitle"></div>
						</div>
						<div class="grayBlock">
							<div class="blockTitle"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
</li>