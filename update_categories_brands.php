<?php include 'config.php'; ?>
<?php include 'includes/session_check.php'; ?>
<?php include 'includes/header.php'; ?>
<body>
<div id="wrapper">
    <?php include 'includes/navigation.php'; ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="apps">
                        <div class="title">
                            <h2>Sync Categories and Brands</h2>
                        </div>
                        <?php
                        if (@$error) {
                            ?>
                            <div class="clearfix">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <p class="alert-danger">Error occured, Please try again later.</p>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="clearfix">
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button onclick="syncCategoriesBrands('1');" type="button" class="btn btn-primary">Sync Categories</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button onclick="syncCategoriesBrands('2');" type="button" class="btn btn-primary">Sync Brands</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button onclick="fetchTopBrands();" type="button" class="btn btn-primary">Fetch Top Brands</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'includes/footer.php'; ?>
</body>
</html>