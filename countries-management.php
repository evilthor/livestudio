<?php include 'config.php'; ?>
<?php include 'includes/session_check.php'; ?>
<?php include 'includes/header.php'; ?>
<?php
    $sql = "SELECT * FROM tbl_countries WHERE store='".$_GET['store']."'";
    $res_countries = $db->get_results($sql);

    $sql = "SELECT store FROM tbl_countries GROUP BY store ORDER BY store DESC";
    $res = $db->get_results($sql);
?>
<script>
  <?php if(isset($res) && $res == true){ ?>
  /*window.messages = {
    type : "success",
    message: "Record deleted successfully.",
  }*/
  <?php }elseif(isset($res) && $res == false){ ?>
  window.messages = {
    type : "warning",
    message: "Invalid id provided, please try again later.",
  }
  <?php } ?>
</script>
<body class="<?php echo bodyClass(); ?>">
    <div id="wrapper">
        <?php include 'includes/navigation.php'; ?>
        <div class="loading" style="display: none;">Loading&#8230;</div>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="apps">
                            <div class="title">
                                <h2><?php echo pageTitle(array('Countries List Management'), false); ?></h2>
                            </div>
                            <div class="clearfix">
                                <div class="row">
                                    <div class="col-lg-10" style=" text-align: center; margin-bottom: 20px;">
                                        <select name="all_countries" id="all_countries" onchange="redirectSection(this.value);">
                                            <?php foreach($res as $store_single){?>
                                            <option value="<?php echo $store_single->store;?>" <?php if($_GET['store']==$store_single->store){ echo "selected";}?>><?php echo $store_single->store;?></option>
                                            <?php }?>
                                        </select>

                                    </div>
                                </div>
                                <div class="row">

                                        <?php
                                            if($res_countries){
                                                //$res_countries = array_chunk($res_countries, 2);
                                                    $count=0;
                                                //foreach($res_countries as $res_country){
                                        ?>

                                            <?php
                                                foreach ($res_countries as $key => $row) {
                                                    $count++;
                                            ?>
                                    <div class="col-lg-6" style="padding-bottom:4px;">
                                                    <div class="col-lg-1"><?php echo $count;?></div>
                                                    <div class="col-lg-5"><span><?php echo $row->country_name;?></span></div>
                                                    <div class="col-lg-1"><span class="label label-primary"> <?php echo $row->short_code;?></span>
                                                        </div>
                                                    <div class="col-lg-1"><span class="label label-primary"> <?php echo $row->country_code;?></span></div>
                                                    <div class="col-lg-2"><?php
                                                        if( $row->status == '1' ){
                                                            $class = "primary";
                                                            $status = "Published";
                                                        }else{
                                                            $class = "warning";
                                                            $status = "Disabled";
                                                        }
                                                        ?>
                                                        <span class="label label-<?php echo $class; ?>"> <?php echo $status;?></span></div>
                                            <div class="col-lg-2">

                                                    <a
                                                            class="__get_popup btn btn-primary btn-xs"
                                                            data-data='{ "command" : "edit-country-form", "store" : "<?php echo $_GET['store'];?>", "ID": "<?php echo $row->id;?>" }'
                                                            data-loader="#update-loader-<?php echo $row->id;?>"
                                                            href="#"
                                                    >edit <span style="display:none" id="update-loader-<?php echo $row->id;?>"><i class="fa fa-spinner fa-spin"></i></span></a>
                                            </div>
                                    </div>
                                                    <?php
                                                    if($count%2==0){

                                                        echo "</div><div class=\"row\">";echo "<hr>";
                                                    }
                                                    ?>
                                                <!--<div class="col-lg-6">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td style="width: 30px"><?php /*echo $count;*/?></th>
                                                                <td>
                                                                    <div style="padding-bottom: 6px;">
                                                                        <span><?php /*echo $row->country_name;*/?></span>
                                                                    </div>
                                                                    <div style="padding-bottom: 6px;">
                                                                        <span class="label label-primary">Short code: <?php /*echo $row->short_code;*/?></span>
                                                                        <span class="label label-primary">Country code: <?php /*echo $row->country_code;*/?></span>
                                                                        <?php
/*                                                                            if( $row->status == '1' ){
                                                                                $class = "primary";
                                                                                $status = "Published";
                                                                            }else{
                                                                                $class = "warning";
                                                                                 $status = "Disabled";
                                                                            }
                                                                        */?>
                                                                        <span class="label label-<?php /*echo $class; */?>">status: <?php /*echo $status;*/?></span>
                                                                    </div>
                                                                    <div>
                                                                        <a 
                                                                            class="__get_popup btn btn-primary btn-sm" 
                                                                            data-data='{ "command" : "edit-country-form", "store" : "<?php /*echo $_GET['store'];*/?>", "ID": "<?php /*echo $row->id;*/?>" }'
                                                                            data-loader="#update-loader-<?php /*echo $row->id;*/?>"
                                                                            href="#"
                                                                        >edit <span style="display:none" id="update-loader-<?php /*echo $row->id;*/?>"><i class="fa fa-spinner fa-spin"></i></span></a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>                                                
                                            </div>-->
                                            <?php } ?>

                                        <?php
                                                //}
                                            }
                                        ?>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function redirectSection(val) {
            window.location.href = 'countries-management.php?store='+val;
        }
    </script>
    <?php include 'includes/footer.php'; ?>
</body>
</html>