<?php include 'config.php'; ?>
<?php include 'includes/session_check.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="<?php echo bodyClass(); ?>">
    <script>
      <?php if(isset($res) && $res == true){ ?>
      window.messages = {
        type : "success",
        message: "Record deleted successfully.",
      }
      <?php }elseif(isset($res) && $res == false){ ?>
      window.messages = {
        type : "warning",
        message: "Invalid id provided, please try again later.",
      }
      <?php } ?>
    </script>
    <div id="wrapper">
        <?php include 'includes/navigation.php'; ?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="apps">
                            <div class="title">
                                <h2><?php echo pageTitle(null, false); ?></h2>
                            </div>
                            <div class="clearfix">
                                <div class="row">
                               	<div class="col-lg-10" id="list">
                                        
                          		</div>
                               	<div class="col-lg-2">
                                        <a 
                                            data-data='{ "command" : "cache-category" }' 
                                            data-loader="#cache-category"
                                            href="#" 
                                            class="btn btn-primary btn-sm btn-block __get_popup"
                                        >Cache category <span style="display:none" id="cache-category"><i class="fa fa-spinner fa-spin"></i></span></a>
                                        <a 
                                            data-data='{ "command" : "add-api-service-form" }' 
                                            data-loader="#add-api-service-form"
                                            href="#" 
                                            class="btn btn-primary btn-sm btn-block __get_popup"
                                        >Add service <span style="display:none" id="add-api-service-form"><i class="fa fa-spinner fa-spin"></i></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'includes/footer.php'; ?>
    <script>
    	jQuery(document).ready(function(){
    GET([{
        type: 'link',
        data: {
            command: 'cache-management-list',
        },
        callback: {
            complete: function(XHR, textStatus) {
                $('.cache-management #list').html(XHR.responseText);
            }
        }
    }]);    		
    	})
    </script>
</body>
</html>