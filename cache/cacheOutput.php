<?php
$store = isset($_REQUEST['store']) ? $_REQUEST['store'] : "";
$id    = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
$desktop_lander_page    = isset($_REQUEST['lander']) ? $_REQUEST['lander'] : "";
$uri   = $_SERVER['SCRIPT_NAME'];
$domain_name = $_SERVER['SERVER_NAME'];
if(!empty($store)) {
    if(!empty($desktop_lander_page)){
        $cache_key = sha1($domain_name.$uri."?store=".$store."&lander=".$desktop_lander_page);
    }else{
        $cache_key = sha1($domain_name.$uri."?store=".$store);
    }
} elseif(!empty($id)) {
    $cache_key = sha1($domain_name.$uri."?id=".$id);
}
$cacheRefresh = (bool) !empty($_REQUEST['clear']) ? $_REQUEST['clear'] : false;

// if cache enable & we have valid key
/*if ($cacheObject->bEnabled && !$cacheRefresh) {
    $data = $cacheObject->getData($cache_key);
    if($data) {
        header("Content-Type: application/json");
        echo $data;   
        die;        
    }
}*/
?>
