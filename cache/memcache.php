<?php
/*
* Mobile Studio
* @author: Babar ali <babar.ali@progos.org>
*/
class CacheMemcache {

    var $iTtl = 2592000; // Time To Live 1 month
    var $bEnabled = false; // Memcache enabled?
    var $oCache = null;

    // constructor
    function CacheMemcache() {
        if (class_exists('Memcached')) {
            $this->oCache = new Memcached();
            $this->bEnabled = true;
            if (! $this->oCache->addServer('127.0.0.1', 11211))  {
                $this->oCache = null;
                $this->bEnabled = true;
            }
        }
    }

    // get data from cache server
    function getData($sKey) {
        $vData = $this->oCache->get($sKey);
        return false === $vData ? null : $vData;
    }

    // save data to cache server
    function setData($sKey, $vData) {
        //Use MEMCACHE_COMPRESSED to store the item compressed (uses zlib).
        return $this->oCache->set($sKey, $vData, $this->iTtl);
    }

    // delete data from cache server
    function delData($sKey) {
        return $this->oCache->delete($sKey);
    }
}

$cacheObject = new CacheMemcache();
?>
