<?php
    include 'config.php';
    include 'includes/session_check.php';

    if (isset($_POST["username"])):
        $sql_current = "SELECT * FROM tbl_users WHERE id = '".$_SESSION['id']."' AND password = '".$_REQUEST['password']."'";
        $res_current = $db->get_row($sql_current);
        if(!$res_current){
		      $error = "Invalid current password entered.";
        }elseif(strlen($_REQUEST['npassword']) < 8){
			$error = "Password mus be at least 8 characters long.";	 
        }elseif($_REQUEST['npassword'] != $_REQUEST['cnpassword']){
			$error = "New password and confirm new password must match.";	 
        }else{
			$sql = "UPDATE tbl_users SET password = '".$_REQUEST['npassword']."' WHERE id = '".$_SESSION['id']."'";
			if ($db->query($sql) !== false) {
                header('Location:profile.php?message=update');exit;
            } else {
                $error = "Error occured, please try again later.";	 
            }
        }
    endif;
    include 'includes/header.php';
?>
<body>
    <div id="wrapper">
        <?php include 'includes/navigation.php';?>
        <div id="page-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="apps">
                            <div class="title">
                                <h2>Profile</h2>
                            </div>
                            <div class="clearfix">
                                <div class="row">
                                    <div class="col-sm-offset-4 col-md-4">
                                        <?php if (@$error != "") {?>
                                        <p class="alert alert-danger"><?php echo $error; ?></p>
                                        <?php } ?>
                                        <?php if (@$_GET['message'] == "update") {?>
                                        <p class="alert alert-success">Record updated successfully.</p>
                                        <?php } ?>
                                        <?php
                                            $sql = "SELECT * FROM tbl_users WHERE id = '".$_SESSION['id']."'";
                                            $result = $db->get_row($sql);
										?>
                                        <form id="frmAddApp" method="post" action="profile.php" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label for="" class="control-label">Email</label>
                                                <input readonly type="text" class="form-control" id="username" name="username" value="<?php echo $result->username; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label">Current Password</label>
                                                <input type="password" class="form-control" id="password" name="password" value="">
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label">New Password</label>
                                                <input type="password" class="form-control" id="npassword" name="npassword" value="">
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="control-label">Confirm New Password</label>
                                                <input type="password" class="form-control" id="cnpassword" name="cnpassword" value="">
                                            </div>
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'includes/footer.php';?>
</body>
</html>