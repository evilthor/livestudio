<?php
$mode = $_REQUEST['mode'];
$type = $_REQUEST['type'];

$push_url = "";
$cid="";
$pid="";
if($type==1){
	$cid = $_REQUEST['category'];
	}elseif($type==2){
		$pid = $_REQUEST['product'];
	}elseif($type==3){
		$push_url = $_REQUEST['url'];
	}

$dev_ssl = 'ssl/aps_dis.pem';
$dev_pass = '123';

$pro_ssl = 'ssl/aps_dis.pem';
$pro_pass = '123';
$message = $_REQUEST['message'];
$deviceToken = $_REQUEST['device'];

// Put your alert message here:
$ctx = stream_context_create();
////////////////////////////////////////////////////////////////////////////////
if($mode == 'development'){

// ckpro.pem for prduction
stream_context_set_option($ctx, 'ssl', 'local_cert', $dev_ssl);
//stream_context_set_option($ctx, 'ssl', 'local_cert', 'ckDev.pem');
stream_context_set_option($ctx, 'ssl', 'passphrase', $dev_pass);
}
if($mode == 'production'){
stream_context_set_option($ctx, 'ssl', 'local_cert', $pro_ssl);
//stream_context_set_option($ctx, 'ssl', 'local_cert', 'ckDev.pem');
stream_context_set_option($ctx, 'ssl', 'passphrase', $pro_pass);
}
// production environment, Open a connection to the APNS server
if($mode == "development"):
$fp = stream_socket_client(
	'ssl://gateway.sandbox.push.apple.com:2195', $err,
	$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
elseif($mode == "production"):
$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
endif;

if (!$fp)
	//header("Location: notification.php?message=failure");
	exit("Failed to connect: $err $errstr" . PHP_EOL);

// Create the payload body
$body['aps'] = array(
	'alert' => $message,
	'sound' => 'default'
	);
if($type == '1'){
	$body['type'] = 'category';
	$body['value'] = $cid;
}elseif($type == '2'){
	$body['type'] = 'product';
	$body['value'] = $pid;
}elseif($type == '3'){
	$body['type'] = 'url';
	$body['value'] = $push_url;
}elseif($type == '4'){
	$body['type'] = 'static';
	$body['value'] = '';
}


// Encode the payload as JSON
$payload = json_encode($body);

// Build the binary notification
$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
$result = fwrite($fp, $msg, strlen($msg));

if (!$result){
	echo $message = 'failure';
}
else{
	echo $message = 'success';
}

// Close the connection to the server
fclose($fp);
?>