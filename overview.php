<?php include 'config.php'; ?>
<?php include 'includes/session_check.php'; ?>
<?php include 'includes/header.php'; ?>
<?php

     $app_id = (isset($_REQUEST['app_id']) && !empty($_REQUEST['app_id'])) ? $_REQUEST['app_id'] : 0;

     $sql_apps = "SELECT * FROM tbl_apps WHERE id = '".$app_id."'";
     $res_apps = $db->get_row($sql_apps);

     $sections = array(
          // 'notifications' => array(
          //      'url' => './notifications.php?app_id='.$app_id,
          //      'icon' => './dist/images/notifications.jpg',
          //      'title' => 'Notification manager',
          //      'sub_title' => 'Manage and send push notifications',
          // ),
          'merchandise' => array(
               'url' => './merchandise-manager.php?app_id='.$app_id,
               'icon' => './dist/images/layouts.jpg',
               'title' => 'Merchandise manager',
               'sub_title' => 'Create and manage home merchandise',
          ),
          'static-block' => array(
               'url' => './static-block.php?app_id='.$app_id,
               'icon' => './dist/images/staticblock.jpg',
               'title' => 'Static block',
               'sub_title' => 'Create and manage static block',
          ),
          'category' => array(
               'url' => './category-manager.php?app_id='.$app_id,
               'icon' => './dist/images/layouts.jpg',
               'title' => 'Search Category Manager',
               'sub_title' => 'Create and manage categories layout for app',
          ),
          'top_section' => array(
               'url' => './top-sections.php?app_id='.$app_id,
               'icon' => './dist/images/staticblock.jpg',
               'title' => 'Top section',
               'sub_title' => 'Create and manage top section',
          ),
          'lander_page' => array(
               'url' => './lander-manager.php?app_id='.$app_id,
               'icon' => './dist/images/layouts.jpg',
               'title' => 'Lander manager',
               'sub_title' => 'Create and manage lander pages',
          ),
         'desktop_lander' => array(
             'url' => './desktop-lander-manager.php?app_id='.$app_id,
             'icon' => './dist/images/layouts.jpg',
             'title' => 'Desktop Lander manager',
             'sub_title' => 'Create and manage lander pages for Elabelz desktop',
         ),
          'search_category' => array(
               'url' => './search-category-manager.php?app_id='.$app_id,
               'icon' => './dist/images/layouts.jpg',
               'title' => 'Search Category Manager(All Layouts)',
               'sub_title' => 'Create and manage categories layout for app',
          ),
     );

     if($res_apps->global_app==0){
          unset($sections['lander_page']);
     }
     $sections = array_chunk($sections, 3);
?>
<body>
     <div id="wrapper">
          <?php include('includes/navigation.php'); ?>
          <div id="page-wrapper">
               <div class="container-fluid">
                    <div class="row">
                         <div class="col-lg-12">
                              <div class="overview">
                                   <div class="clearfix">
                                        <?php foreach ($sections as $key => $rows) {?>
                                        <div class="row">
                                             <?php foreach ($rows as $index => $section) {?>
                                             <div class="col-md-4">
                                                  <div class="item">
                                                       <a href="<?php echo $section['url'] ?>">
                                                            <span><img src="<?php echo $section['icon'] ?>" alt="" class="img-responsive"></span>
                                                            <span class="title">
                                                                 <?php echo $section['title'] ?>
                                                                 <span class="sub-text">
                                                                      <?php echo $section['sub_title'] ?>
                                                                 </span>
                                                            </span>
                                                       </a>
                                                  </div>
                                             </div>
                                             <?php } ?>
                                        </div>
                                        <hr>
                                        <?php } ?>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
     <?php include 'includes/footer.php'; ?>
</body>
</html>
