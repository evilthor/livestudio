<?php include 'config.php'; ?>
<?php include 'config.image.php'; ?>
<?php include 'includes/session_check.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="<?php echo bodyClass(array('top-section')); ?>">
    <script>
     var currentApp = '<?php echo trim($_GET['app_id'])?>';        
    </script>
    <div id="wrapper">
        <?php include('includes/navigation.php'); ?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="layouts" id="top-section">
                            <div class="inner-layouts">
                                <div class="row">
                                    <div class="col-lg-12 step step-1">
                                        <div class="clearfix">
                                            <span class="step-number">1</span>
                                            <div class="step-detail">
                                                <h6>Choose image</h6>
                                            </div>
                                        </div>
                                        <div class="uploader">
                                            <div 
                                                id="fine-uploader-gallery" 
                                                data-option='{
                                                      "layoutType":"single",
                                                      "layoutTypeDB":"single",
                                                      "viewTemplate":"static-block-single-layout.mt",
                                                      "iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
                                                      "iPhoneView": ".iphonePreviewContainer #carousel",
                                                      "imagePosition": "0"
                                                 }' 
                                                 data-width="<?php echo $config_static_width; ?>" 
                                                 data-height="<?php echo $config_static_height; ?>"
                                            ></div>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 step step-2">
                                        <div class="clearfix">
                                            <span class="step-number">2</span>
                                            <div class="step-detail">
                                                <h6>Add buttons</h6>
                                            </div>
                                        </div>
                                        <div class="blockLabel">
                                            <div class="form-inline">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="button-text" required="" style="width: 130px;">
                                                     <input type="hidden" id="button-app" value="<?php echo $_GET['app_id'];?>">
                                                </div>
                                                <button 
                                                    type="button" 
                                                    class="btn btn-link btn-add-button" 
                                                    data-textfield="#button-text"
                                                    data-appid="#button-app"
                                                    data-preview-container="#smallImgOverlay"
                                                    data-save-button="#save-top-section"
                                                >add</button>                                            
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 step step-3">
                                        <div class="clearfix">
                                              <span class="step-number">3</span>
                                              <div class="step-detail">
                                                   <h6>Add content</h6>
                                                   <!-- <p>
                                                        Tap the images below to add a content link to them. Images can link to content like a category, product, website or can be static.
                                                   </p> -->
                                              </div>
                                        </div>
                                        <div class="img-link">
                                          <div class="image-preview-small">
                                                <div id="btn-msg"></div>
                                                <div id="smallImgOverlay" class="preview-layout" style="display:none;">
                                                    <div class="previewPayouts single-layout" id="single-layout">
                                                         <div class="img-thumb" id="singleLayout" data-all-hide=".contentDdPopup" data-click-show="#singleContentDdPopup" data-attachmentPos="0"></div>
                                                         <div class="contentDdPopup" id="singleContentDdPopup"></div>
                                                    </div>
                                                </div>
                                          </div>

                                         </div> 
                                         <hr>                           
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 step step-4">
                                        <div class="clearfix">
                                            <span class="step-number">4</span>
                                            <div class="step-detail">
                                                <h6>Save block</h6>
                                            </div>
                                        </div>
                                        <div class="saveBlock">
                                            <button 
                                                type="button" 
                                                name="save-top-section" 
                                                id="save-top-section" 
                                                disabled 
                                                class="btn btn-primary"
                                            >Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="apps">
                                    <div class="title" style="padding-bottom: 12px;">
                                        <h2>Add top section</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix devicePreview" style="margin-top: -20px">
                            <div class="ipadPreviewContainer">
                                <div id="carouselOuter" class="carouselRowsManager">
                                    <div id="carouselCircleLeft"></div>
                                    <div id="carousel" class="mCustomScrollbar">
                                    </div>
                                    <div id="carouselCircleRight"></div>
                                </div>
                            </div>
                            <div class="iphonePreviewContainer">
                                <div id="carouselOuter" class="carouselRowsManager">
                                    <div class="carouselCircleTop"></div>
                                    <div class="carouselRectangleTop"></div>
                                    <div class="carouselCircleTopLeft"></div>
                                    <div id="carousel" class="mCustomScrollbar">
                                    </div>
                                    <div class="carouselCircleBottom"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'includes/footer.php'; ?>
</body>
</html>