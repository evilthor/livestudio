<?php include 'config.php'; ?>
<?php include 'includes/session_check.php'; ?>
<?php include 'includes/header.php'; ?>

<?php if(isset($_GET['id']) && $_GET['id'] != ''){
  $checkDB = "SELECT * FROM tbl_top_sections WHERE id = '".$_GET['id']."'";
  $checkDB = $db->get_row($checkDB);

 /* $sql_block_status = "SELECT status FROM tbl_top_sections_status WHERE top_section_id = '".$_GET['id']."' AND app_id='".$_GET['app_id']."'";
  $res_block_status = $db->get_row($sql_block_status);

  if($res_block_status->status == 1){

    $res = false;
    $active = true;
    
  }else{*/
    if($checkDB){
      if($yes_global==1){
        $sql_del = "DELETE FROM tbl_top_sections WHERE id = '".$_GET['id']."'";
        $db->query($sql_del);
        $res = true;
      }else{

        if($checkDB->app_id == GLOBAL_EN || $checkDB->app_id == GLOBAL_AR){
         $sql = "INSERT INTO tbl_delete_data SET app_id = '".$_GET['app_id']."',
                                  row_id = '".$_GET['id']."',
                                  row_type = 'top_section',
                                  show_in_app = '0'";
          $db->query($sql);
        }else{

         $sql_del = "DELETE FROM tbl_top_sections WHERE id = '".$_GET['id']."'";
          $db->query($sql_del);
          $res = true;
        }
      }
    }
  //}
} 
?>
<script>
  <?php if(isset($res) && $res == true){ ?>
  window.messages = {
    type : "success",
    message: "Record deleted successfully.",
  }
  <?php }elseif(isset($res) && $res == false){ ?>
  window.messages = {
    type : "warning",
    message: "Invalid id provided, please try again later.",
  }
  <?php } ?>
</script>
<body>
<div id="wrapper">
	<?php include('includes/navigation.php'); ?>
	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="apps">
						<div class="title">
							<h2>Top Sections</h2>
							<a class="btn btn-primary" href="./new-top-section.php?app_id=<?php echo @$_GET['app_id'] ?>">new top section</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
                  <?php 
                    $sql = "SELECT GROUP_CONCAT(row_id) as row_ids FROM tbl_delete_data WHERE app_id ='".$_GET['app_id']."' AND row_type='top_section'";
                    $skip_data = $db->get_row($sql);
                    $row_ids_arr = $skip_data->row_ids;

                    if($_GET['app_id'] == GLOBAL_EN || $_GET['app_id'] == GLOBAL_AR){
                         $sql = "SELECT * FROM tbl_top_sections WHERE app_id IN(".@$_GET['app_id'].", ".$global_app_id." ) AND show_in_app='1' AND is_global='1' AND id NOT IN('".$row_ids_arr."')";
                    }else{

                        $sql = "SELECT * FROM tbl_top_sections WHERE app_id IN(".@$_GET['app_id'].", ".$global_app_id." ) AND show_in_app='1' AND id NOT IN('".$row_ids_arr."')";
                    }
                    $top_sections = $db->get_results($sql);
                    if($top_sections){
                    $count=0;
                    foreach($top_sections as $top_section){
                        if( $count != 0 && $count % 4 == 0 ){
                          echo "</div><hr><div class=\"row\">";
                        }
                    ?>
                    <div class="col-lg-3">
                         <div class="block" id="top-section-<?php echo $top_section->id;?>">
                              <div class="head">
                                   <div class="row">
                                        <div class="col-lg-6">
                                          <?php
                                            $sql_ts_status = "SELECT status FROM                  tbl_top_sections_status WHERE              top_section_id = '".$top_section->id."'                 AND app_id='".$_GET['app_id']."'";
                                    $res_ts_status = $db->get_row($sql_ts_status);
                                            ?>
                                             <div class="radio">

                                                  <label>
                                                       <?php if($res_ts_status->status=='1'){?>
                                                     <i 
                                                            id="topSectionStatus-<?php echo $top_section->id ?>"
                                                            data-id="<?php echo $top_section->id;?>" 
                                                            data-status="<?php echo $res_ts_status->status;?>" 
                                                            data-app-id="<?php echo $_GET['app_id'];?>" 
                                                            data-parent-ele="#top-section-<?php echo $top_section->id;?>" 
                                                            <?php if($top_section->is_global==1){?> data-global="1" <?php }?>
                                                            class="top-section-toggle fa fa-toggle-on" 
                                                            style="font-size: 30px; cursor: pointer; color: green;" 
                                                            title="Click to Disable"></i>
                                                       <?php
                                                       }else{?>
                                                           <i 
                                                            id="topSectionStatus-<?php echo $top_section->id ?>"
                                                            data-id="<?php echo $top_section->id;?>" 
                                                            data-status="<?php echo $res_ts_status->status;?>" 
                                                            data-app-id="<?php echo $_GET['app_id'];?>" 
                                                            data-parent-ele="#top-section-<?php echo $top_section->id;?>"
                                                            <?php if($top_section->is_global==1){?> data-global="1" <?php }?> 
                                                            class="top-section-toggle fa fa-toggle-off" 
                                                            style="font-size: 30px; cursor: pointer; color: darkgray;" 
                                                            title="Click to Enable"></i>
                                                       <?php 
                                                       }
                                                       ?>
                                                     

                                                     <!--   <input 
                                                          type="radio" 
                                                          data-id="<?php echo $top_section->id;?>" 
                                                          data-app-id="<?php echo $_GET['app_id'];?>" 
                                                          data-parent-ele="#top-section-<?php echo $top_section->id;?>"
                                                          name="top_status" 
                                                          id="top_status" 
                                                          value="<?php echo $top_section->status;?>" 
                                                          <?php if($res_ts_status->status=='1'){ echo "checked=checked"; }?>
                                                       > Published -->
                                                  </label>
                                             </div>
                                        </div>
                                        <div class="col-lg-6">
                                          <?php
                                          if( $top_section->is_global==0 || $yes_global=='1'){
                                          ?> 
                                             <a 
                                                  href="./top-sections.php?app_id=<?php echo @$_GET['app_id'];?>&id=<?php echo $top_section->id;?>" 
                                                  class="btn btn-danger btn-xs pull-right is-delete"
                                             ><i class="fa fa-trash-o" aria-hidden="true"></i> delete</a> 
                                              <?php
                                            }
                                             ?>
                                             <a 
                                                  href="./edit-top-section.php?app_id=<?php echo @$_GET['app_id'];?>&id=<?php echo $top_section->id;?>" 
                                                  class="btn btn-primary btn-xs pull-right"
                                             ><i class="fa fa-pencil-square-o" aria-hidden="true"></i> edit</a> 
                                            
                                        </div>
                                   </div>
                              </div>
                              <div class="block-img">
                                  <img src="<?php echo $top_section->image_url;?>" class="img-responsive" alt="">
                              </div>
                         </div>  
                    </div>
                    <?php
                    $count++;
                              }

                         }
                    ?>
               </div>
		</div>
	</div>
</div>
<?php include 'includes/footer.php'; ?>
</body>
</html>