<?php
	include 'config.php';
	include 'config.image.php';
	include 'includes/session_check.php';
	include 'includes/header.php';

	$sql_row = "SELECT * FROM tbl_cat_rows WHERE id = '".$_GET['row_id']."'";
	$result_row = $db->get_row($sql_row);
	$layout = '';
	$images = json_decode($result_row->images);
	if($result_row->layout == "single" || $result_row->layout == "singleBar"){
		$layout.='<div class="row single-template-layout">';
			$layout.='<div class="col-md-12">';
				$layout.='<div class="preview"><img src="'.$images[0].'"></div>';
			$layout.='</div>';
		$layout.='</div>';
	}elseif($result_row->layout == "double" || $result_row->layout == "doubleShort"){
		$layout.='<div class="row double-template-layout">';
			$layout.='<div class="col-md-6">';
				$layout.='<div class="preview"><img src="'.$images[0].'"></div>';
			$layout.='</div>';
			$layout.='<div class="col-md-6">';
				$layout.='<div class="preview"><img src="'.$images[1].'"></div>';
			$layout.='</div>';
		$layout.='</div>';
	}elseif($result_row->layout == "tripleLeft"){
		$layout.='<div class="row preview-triple-left">';
			$layout.='<div class="col-md-6">';
				$layout.='<div class="row">';
					$layout.='<div class="col-md-12">';
						$layout.='<div class="preview _1024X704"><img src="'.$images[0].'"></div>';
					$layout.='</div>';
					$layout.='<div class="col-md-12">';
						$layout.='<div class="preview _1024X704"><img src="'.$images[1].'"></div>';
					$layout.='</div>';
				$layout.='</div>';
			$layout.='</div>';
			$layout.='<div class="col-md-6">';
				$layout.='<div class="preview _1024X1310"><img src="'.$images[2].'"></div>';
			$layout.='</div>';
		$layout.='</div>';
	}elseif($result_row->layout == "tripleRight"){
		$layout.='<div class="row preview-triple-right">';
			$layout.='<div class="col-md-6">';
				$layout.='<div class="preview _1024X1310"><img src="'.$images[0].'"></div>';
			$layout.='</div>';
			$layout.='<div class="col-md-6">';
				$layout.='<div class="row">';
					$layout.='<div class="col-md-12">';
						$layout.='<div class="preview _1024X704"><img src="'.$images[1].'"></div>';
					$layout.='</div>';
					$layout.='<div class="col-md-12">';
						$layout.='<div class="preview _1024X704"><img src="'.$images[2].'"></div>';
					$layout.='</div>';
				$layout.='</div>';
			$layout.='</div>';
		$layout.='</div>';
	}

	$showLayoutOne = 'tall'; // both, short, tall, none;
	$showLayoutTwo = 'tall'; // both, short, tall, none;
	$showLayoutThree = 'none'; // both, left, right, none;
     $showSingleBar = 'no'; // yes, no;

	$previewLayout = array('category', 'tick-active');
	$previewLayout = implode(' ', $previewLayout);
	$uploaderLayoutData = array();

	$uploaderLayoutData['single']['layout'] = ( $result_row->layout == 'single' ) ? $result_row->layout : '';
	$uploaderLayoutData['single']['id'] = ( $result_row->layout == 'single' ) ? $result_row->id : '';
	$uploaderLayoutData['single']['images'] = ( $result_row->layout == 'single' ) ? $images[0] : '';
	$uploaderLayoutData['single']['size'] = array($config_cat_width*2, $config_cat_height);

	$uploaderLayoutData['singleShort']['layout'] = ( $result_row->layout == 'singleShort' ) ? $result_row->layout : '';
	$uploaderLayoutData['singleShort']['id'] = ( $result_row->layout == 'singleShort' ) ? $result_row->id : '';
	$uploaderLayoutData['singleShort']['images'] = ( $result_row->layout == 'singleShort' ) ? $images[0] : '';
     $uploaderLayoutData['singleShort']['size'] = array(819, 282);

	$uploaderLayoutData['double']['layout'] = ( $result_row->layout == 'double' ) ? $result_row->layout : '';
	$uploaderLayoutData['double']['id'] = ( $result_row->layout == 'double' ) ? $result_row->id : '';
	$uploaderLayoutData['double']['images'] = ( $result_row->layout == 'double' ) ? $images : '';
	$uploaderLayoutData['double']['size']['left'] = array($config_cat_width, $config_cat_height);
	$uploaderLayoutData['double']['size']['right'] = array($config_cat_width, $config_cat_height);

	$uploaderLayoutData['doubleShort']['layout'] = ( $result_row->layout == 'doubleShort' ) ? $result_row->layout : '';
	$uploaderLayoutData['doubleShort']['id'] = ( $result_row->layout == 'doubleShort' ) ? $result_row->id : '';
	$uploaderLayoutData['doubleShort']['images'] = ( $result_row->layout == 'doubleShort' ) ? $images : '';
	$uploaderLayoutData['doubleShort']['size']['left'] = array($config_homedoubleshort_width, $config_homedoubleshort_height);
	$uploaderLayoutData['doubleShort']['size']['right'] = array($config_homedoubleshort_width, $config_homedoubleshort_height);

	$uploaderLayoutData['tripleLeft']['layout'] = ( $result_row->layout == 'tripleLeft' ) ? $result_row->layout : '';
	$uploaderLayoutData['tripleLeft']['id'] = ( $result_row->layout == 'tripleLeft' ) ? $result_row->id : '';
	$uploaderLayoutData['tripleLeft']['images'] = ( $result_row->layout == 'tripleLeft' ) ? $images : '';
	$uploaderLayoutData['tripleLeft']['size']['left'] = array(
		array($config_tripleshort_width, $config_tripleshort_height),
		array($config_tripleshort_width, $config_tripleshort_height)
	);
	$uploaderLayoutData['tripleLeft']['size']['right'] = array($config_triplelarge_width, $config_triplelarge_height);

	$uploaderLayoutData['tripleRight']['layout'] = ( $result_row->layout == 'tripleRight' ) ? $result_row->layout : '';
	$uploaderLayoutData['tripleRight']['id'] = ( $result_row->layout == 'tripleRight' ) ? $result_row->id : '';
	$uploaderLayoutData['tripleRight']['images'] = ( $result_row->layout == 'tripleRight' ) ? $images : '';
	$uploaderLayoutData['tripleRight']['size']['left'] = array($config_triplelarge_width, $config_triplelarge_height);
	$uploaderLayoutData['tripleRight']['size']['right'] = array(
		array($config_tripleshort_width, $config_tripleshort_height),
		array($config_tripleshort_width, $config_tripleshort_height)
	);

	$uploaderLayoutData['singleBar']['layout'] = ( $result_row->layout == 'singleBar' ) ? $result_row->layout : '';
	$uploaderLayoutData['singleBar']['id'] = ( $result_row->layout == 'singleBar' ) ? $result_row->id : '';
	$uploaderLayoutData['singleBar']['images'] = ( $result_row->layout == 'singleBar' ) ? $images[0] : '';
	$uploaderLayoutData['singleBar']['size'] = array($config_banner_width, $config_banner_height);
?>
<body>
<div id="wrapper">
	<?php include('includes/navigation.php'); ?>
	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="layoutTitle">
				<div class="form-group">
					<input id="merchandise_title" name="merchandise_title" type="text" class="form-control" placeholder="Row <?php echo $_GET['active']; ?>" data-toggle="tooltip" readonly data-placement="right" title="This title is for your reference only and will not be used in the app">
					<input type="hidden" name="merchandise_id" id="merchandise_id" value="<?php echo @$_GET['id'] ?>" />
					<input type="hidden" name="app_id" id="app_id" value="<?php echo $_GET['app_id']; ?>" />
					<input type="hidden" name="row_id" id="row_id" value="<?php echo $_GET['row_id']; ?>" />
				</div>
			</div>
			<div class="clearfix devicePreview">
				<div class="ipadPreviewContainer">
					<div id="carouselOuter" class="carouselRowsManager">
						<div id="carouselCircleLeft"></div>
						<div id="carousel" class="mCustomScrollbar"><?php echo $layout; ?></div>
						<div id="carouselCircleRight"></div>
					</div>
				</div>
				<div class="iphonePreviewContainer">
					<div id="carouselOuter" class="carouselRowsManager">
						<div class="carouselCircleTop"></div>
						<div class="carouselRectangleTop"></div>
						<div class="carouselCircleTopLeft"></div>
						<div id="carousel" class="mCustomScrollbar"> <?php echo $layout; ?> </div>
						<div class="carouselCircleBottom"></div>
					</div>
				</div>
			</div>
			<div class="layouts">
				<div class="layout-type" style="display:none">
					<div class="row">
						<div class="col-lg-4">
							<div class="step step-1 clearfix"> <span class="step-number">1</span>
								<div class="step-detail">
									<h6>Choose layout & add images</h6>
									<p>Choose one of the layouts with one, two or three images and add the images.</p>
								</div>
							</div>
							<div class="ddContainer">
								<div class="ddBtn clearfix" data-click-show=".ddPopup">
									<div class="ddBtnText clipText">LAYOUT + IMAGES</div>
									<div class="ddBtnArrow"> <i class="fa fa-angle-down" aria-hidden="true"></i> </div>
								</div>
								<div class="ddPopup">
									<div class="ddPopupPageContainer">
										<div class="pageSection">
											<div class="page-title clearfix"> <a href="javascript:;" class="back" data-images="1" data-backto=".ddPage" data-hide=".pageSection"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
												<h6>ADD IMAGE</h6>
											</div>
											<?php
												require('./layout-templates/upload-layout-one.php');
												require('./layout-templates/upload-layout-two.php');
												require('./layout-templates/upload-layout-three.php');
												require('./layout-templates/upload-layout-bar.php');
											?>
											<div class="done-section">
												<button disabled id="btnDoneImageUpload" data-hide=".ddPopup" class="done btn btn-primary btn-block">done</button>
											</div>
										</div>
										<div class="ddPage">
											<div class="ddTitle"> <span class="ddTitleText"> CHOOSE TEMPLATE </span> </div>
											<ul class="ddList">
												<?php
													require('./layout-templates/layout-one.php');
													require('./layout-templates/layout-two.php');
													require('./layout-templates/layout-three.php'); 
													require('./layout-templates/layout-bar.php'); 
												?>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="step step-2">
								<div class="clearfix"> <span class="step-number">2</span>
									<div class="step-detail">
										<h6>Add content</h6>
										<p> Tap the images below to add a content link to them. Images can link to content like a category, product, website or can be static. </p>
									</div>
								</div>
								<div class="img-link">
									<div class="image-preview-small">
										<div id="smallImgOverlay" class="preview-layout">
											<?php
												require('./layout-templates/preview-layout-one.php');
												require('./layout-templates/preview-layout-two.php');
												require('./layout-templates/preview-layout-three.php');
												require('./layout-templates/preview-layout-bar.php'); 
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="step step-3">
								<div class="clearfix"> <span class="step-number">3</span>
									<div class="step-detail">
										<h6>Finalize row</h6>
										<p> All changes will be saved and published </p>
									</div>
								</div>
								<div class="submit-button">
									<button id="saveRow" class="btn btn-primary" disabled>Update</button>
									<button onClick="window.location.href='category-overview.php?app_id=<?php echo $_GET['app_id']; ?>&id=<?php echo $_GET['id']; ?>';" class="btn btn-danger">Cancel</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include 'includes/footer.php'; ?>
<script>
	$(document).ready(function(){
		window.availableData = {
			layout : '<?php echo $result_row->layout; ?>',
			images : '<?php echo $result_row->images; ?>',
			attachments : '<?php echo $result_row->attachment; ?>'
		};
		appendLayouts(availableData);
	});
</script>
</body>
</html>