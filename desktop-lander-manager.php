<?php
	include 'config.php';
	include 'includes/session_check.php';
?>
<?php include 'includes/header.php'; ?>
<script>
	<?php if(isset($res) && $res == true){ ?>
	window.messages = {
		type : "success",
		message: "Record deleted successfully.",
	}
	<?php }elseif(isset($res) && $res == false && $active == true){ ?>
	window.messages = {
		type : "warning",
		message: "This home layout is currently active.",
	}
	<?php }elseif(isset($res) && $res == false){ ?>
	window.messages = {
		type : "warning",
		message: "Invalid id provided, please try again later.",
	}
	<?php } ?>
</script>
<body>
<div id="wrapper">
	<?php include('includes/navigation.php'); ?>
	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="apps">
						<div class="title">
							<h2>Desktop lander manager</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<?php
			 		$sql = "SELECT * FROM tbl_categories  WHERE app_id IN(".@$_GET['app_id'].", ".$global_app_id." ) ORDER BY app_id";
			 		$lander_cats = $db->get_results($sql);

			 		$sql = "SELECT GROUP_CONCAT(row_id) as row_ids FROM tbl_delete_data WHERE app_id ='".$_GET['app_id']."' AND row_type='desktop_lander_row'";
                    $skip_data = $db->get_row($sql);
                    $row_ids_arr_temp = $skip_data->row_ids;

					if($row_ids_arr_temp){
						$row_ids_temp_arr = explode(',', $row_ids_arr_temp);
						$row_ids_arr = "'" . implode ( "', '", $row_ids_temp_arr ) . "'";
						$row_ids_arr = rtrim($row_ids_arr,"'");
						$row_ids_arr = ltrim($row_ids_arr,"'");
					}else{
						$row_ids_arr='';
					}

				 	if($lander_cats){
						foreach($lander_cats as $lander_cat){
							$sql = "SELECT * FROM tbl_sort_rows WHERE app_id = '".@$_GET['app_id']."' AND row_type_id = '".$lander_cat->id."'";
							$check = $db->get_results($sql);
							if(count($check) > 0){
								if($yes_global==1){
									$sql = "SELECT * FROM tbl_desktop_lander_rows JOIN tbl_sort_rows ON tbl_desktop_lander_rows.id=tbl_sort_rows.row_id AND 
                                                                                          tbl_sort_rows.row_type='desktop_lander_row' AND 
                                                                                          tbl_desktop_lander_rows.cat_id = '$lander_cat->id' AND 
                                                                                          tbl_desktop_lander_rows.show_in_app = '1' AND 
                                                                                          tbl_desktop_lander_rows.is_global='1' AND 
                                                                                          tbl_desktop_lander_rows.app_id=tbl_sort_rows.app_id AND 
                                                                                          tbl_desktop_lander_rows.app_id IN(".@$_GET['app_id'].", ".$global_app_id." ) 
                                                                                          ORDER BY tbl_sort_rows.sort_order ASC";
								}else{
									$sql = "SELECT * FROM tbl_desktop_lander_rows JOIN tbl_sort_rows ON tbl_desktop_lander_rows.id=tbl_sort_rows.row_id AND 
                                                                                          tbl_sort_rows.row_type='desktop_lander_row' AND 
                                                                                          tbl_desktop_lander_rows.cat_id = '$lander_cat->id' AND 
                                                                                          tbl_desktop_lander_rows.show_in_app = '1' AND 
                                                                                          tbl_desktop_lander_rows.id NOT IN('".$row_ids_arr."') AND 
                                                                                          tbl_desktop_lander_rows.app_id=tbl_sort_rows.app_id AND 
                                                                                          tbl_desktop_lander_rows.app_id IN(".@$_GET['app_id'].", ".$global_app_id." ) 
                                                                                          ORDER BY tbl_sort_rows.sort_order ASC";
								}
							}else{
								if($yes_global==1){
									$sql = "SELECT * FROM tbl_desktop_lander_rows WHERE cat_id = '".$lander_cat->id."' AND show_in_app = '1' AND is_global='1' AND id NOT IN('".$row_ids_arr."')";
								}else{
									$sql = "SELECT * FROM tbl_desktop_lander_rows WHERE cat_id = '".$lander_cat->id."' AND show_in_app = '1' AND id NOT IN('".$row_ids_arr."')";
								}
							}
							$results = $db->get_results($sql);
							$result = $results[0];
							if(@$result->layout == "singleBar"){
								$result = @$results[1];
							}

                            $sql_lander_cats = "SELECT * FROM tbl_desktop_lander_rows WHERE cat_id = '".$lander_cat->id."'";
                            $result_lander_cats = $db->get_results($sql_lander_cats);
                            $sql_lander_sliders = "SELECT * FROM tbl_desktop_lander_slider WHERE cat_id = '".$lander_cat->id."'";
                            $result_lander_sliders = $db->get_results($sql_lander_sliders);
				?>
				<div class="col-md-3">
					<div class="box">
						<div class="" id="merchandise-<?php echo $lander_cat->id; ?>" data-command="merchandise" data-id="<?php echo $lander_cat->id; ?>">
					   		<div class="content">
                                <div class="col-md-12">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div style="cursor: pointer; display: inline-block" onClick="window.location.href='desktop-lander-sections.php?app_id=<?php echo $_GET['app_id'] ?>&cat_id=<?php echo $lander_cat->id; ?>';">
                                                    <div class="title" style="font-size: 16px; font-weight: bold;"><?php echo $lander_cat->title; ?></div>
                                                    <div class="sub-title" style="font-size: 13px;"><?php echo count($result_lander_cats); ?> Category Row(s)</div>
                                                    <div class="sub-title" style="font-size: 13px;"><?php echo count($result_lander_sliders); ?> Slider Row(s)</div>
                                                    <img style="margin-top: 12px; display: inline-block" src="./dist/images/layouts.jpg" class="img-responsive">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
									$images = json_decode(@$result->images);
									if(@$result->layout == "single" || @$result->layout == "singleBar"){
								?>
								<div class="single">
									<div class="row">
										<div class="col-md-12">
											<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
										</div>
									</div>
								</div>
								<?php }elseif(@$result->layout == "double" || @$result->layout == "doubleShort"){ ?>
								<div class="double">
									<div class="row">
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
										</div>
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
										</div>
									</div>
								</div>
								<?php }elseif(@$result->layout == "tripleLeft"){ ?>
								<div class="triple">
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[2]; ?>" class="img-responsive"> </div>
										</div>
									</div>
								</div>
								<?php }elseif(@$result->layout == "tripleRight"){ ?>
								<div class="triple">
									<div class="row">
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[2]; ?>" class="img-responsive"> </div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<?php
						}
				 	}else{
			 	?>
				<div class="col-md-12">
					<p class="alert alert-success">No record found.</p>
				</div>
				<?php
				 	}
			 	?>
			</div>
		</div>
	</div>
</div>
<?php include 'includes/footer.php'; ?>
</body>
</html>