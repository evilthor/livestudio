module.exports = function(grunt) {
     grunt.initConfig({
          pkg: grunt.file.readJSON('package.json'),
          concat: {
               dist: {
                    src: [
                         "./bower_components/mustache.js/mustache.min.js",
                         "./bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js",
                         "./node_modules/select2/dist/js/select2.full.min.js",
                         "./node_modules/list.js/dist/list.min.js",
                         "./bower_components/jquery-validation/dist/jquery.validate.min.js",
                         "./bower_components/jquery-confirm2/dist/jquery-confirm.min.js",
                         "./bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js",
                         "./bower_components/jstree/dist/jstree.min.js",
                         "./node_modules/magnific-popup/dist/jquery.magnific-popup.min.js",
                         "./node_modules/jquery-form/dist/jquery.form.min.js",
                         "./node_modules/notifyjs-browser/dist/notify.js",
                         "./node_modules/letteringjs/jquery.lettering.js",
                         "./node_modules/textillate/jquery.textillate.js",
                         "./dist/js/variables.js",
                         "./dist/js/functions.js",
                         "./dist/js/objects.js",
                         "./dist/js/brands.js",
                         "./dist/js/categories.js",
                         "./dist/js/products.js",
                         "./dist/js/sortable.js",
                         "./dist/js/topSection.js",
                         "./dist/js/commonEvents.js",
                         "./dist/js/landerPages.js",
                         "./dist/js/merchandiseManager.js",
                         "./dist/js/notifications.js",
                         "./dist/js/staticBlock.js",
                         "./dist/js/storeManagement.js",
                         "./dist/js/app.js",
                    ],
                    dest: './dest/js/apps.js'
               },
               css: {
                    src: [
                         "./bower_components/jquery-ui/themes/base/all.css",
                         // "./bower_components/bootstrap/dist/css/bootstrap.min.css",
                         "./bower_components/fine-uploader/dist/fine-uploader.min.css",
                         "./bower_components/font-awesome/css/font-awesome.min.css",
                         "./bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css",
                         "./node_modules/select2/dist/css/select2.min.css",
                         "./bower_components/jquery-confirm2/dist/jquery-confirm.min.css",
                         "./bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css",
                         "./bower_components/jstree/dist/themes/default/style.min.css",
                         "./node_modules/magnific-popup/dist/magnific-popup.css",
                         "./node_modules/flag-icon-css/css/flag-icon.min.css",
                         "./node_modules/loaders.css/loaders.min.css",
                         "./node_modules/animate.css/animate.min.css",
                         "./dist/css/app.css",
                    ],
                    dest: './dest/css/apps.css',
               }
          },
          uglify: {
               options: {
                    banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
               },
               dist: {
                    files: {
                         './dest/js/apps.min.js': ['<%= concat.dist.dest %>']
                    }
               }
          },
          cssmin: {
               target: {
                    files: {
                         './dest/css/apps.min.css': [
                              "./bower_components/jquery-ui/themes/base/all.css",
                              // "./bower_components/bootstrap/dist/css/bootstrap.min.css",
                              "./bower_components/fine-uploader/dist/fine-uploader.min.css",
                              "./bower_components/font-awesome/css/font-awesome.min.css",
                              "./bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css",
                              "./node_modules/select2/dist/css/select2.min.css",
                              "./bower_components/jquery-confirm2/dist/jquery-confirm.min.css",
                              "./bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css",
                              "./bower_components/jstree/dist/themes/default/style.min.css",
                              "./node_modules/magnific-popup/dist/magnific-popup.css",
                              "./node_modules/flag-icon-css/css/flag-icon.min.css",
                              "./node_modules/loaders.css/loaders.min.css",
                              "./node_modules/animate.css/animate.min.css",
                              "./dist/css/app.css",
                         ]
                    }
               }
          },
          copy: {
               fonts: {
                    files: [
                         // flattens results to a single level
                         {
                              expand: true,
                              flatten: true,
                              src: [
                                   './bower_components/font-awesome/fonts/**',
                                   './bower_components/bootstrap/fonts/**',
                              ],
                              dest: './dest/fonts/',
                              filter: 'isFile'
                         },
                    ],
               },
               jstree_images: {
                    files: [
                         {
                              expand: true,
                              flatten: true,
                              src: [
                                   './bower_components/jstree/src/themes/default/*.{png,jpg,gif,svg}',
                              ],
                              dest: './dest/css/',
                              filter: 'isFile'
                         },
                    ],
               },
               fineuploader_images: {
                    files: [
                         {
                              expand: true,
                              flatten: true,
                              src: [
                                   './bower_components/fine-uploader/dist/*.{png,jpg,gif,svg}',
                              ],
                              dest: './dest/css/',
                              filter: 'isFile'
                         },
                    ],
               },
               flags_images: {
                    files: [
                         {
                              expand: true,
                              cwd: './node_modules/flag-icon-css/flags/',
                              src: [
                                   '**',
                              ],
                              dest: './dest/flags/',
                         },
                    ],
               },
               images: {
                    files: [
                         {
                              expand: true,
                              flatten: true,
                              src: [
                                   './dist/images/*',
                              ],
                              dest: './dest/images/',
                              filter: 'isFile'
                         },
                    ],
               },
          },
     });
     grunt.loadNpmTasks('grunt-contrib-uglify');
     grunt.loadNpmTasks('grunt-contrib-concat');
     grunt.loadNpmTasks('grunt-contrib-cssmin');
     grunt.loadNpmTasks('grunt-contrib-copy');
     grunt.registerTask('min', ['concat', 'uglify', 'cssmin', 'copy']);
};