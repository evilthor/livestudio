<?php include 'config.php'; ?>
<?php include 'includes/session_check.php'; ?>
<?php include 'includes/header.php'; ?>
<?php
    $sql = "SELECT * FROM tbl_stores ORDER BY country_code";
    $res_stores = $db->get_results($sql);

    if(isset($_POST['id']) && $_POST['id'] != ''){
        $res = false; 
        $checkDB = "SELECT id,country_flag FROM tbl_stores WHERE id = '".$_POST['id']."'";
        $checkDB = $db->get_row($checkDB);
        if($checkDB){
            $image = getcwd()."/images/countries_flags/".$checkDB->country_flag;
            if( file_exists($image) ){
                unlink($image);
            }
            $sql_del = "DELETE FROM tbl_stores WHERE id = '".$_POST['id']."'";
            $db->query($sql_del);
            $res = true;
        }
    } 
?>
<script>
  <?php if(isset($res) && $res == true){ ?>
  window.messages = {
    type : "success",
    message: "Record deleted successfully.",
  }
  <?php }elseif(isset($res) && $res == false){ ?>
  window.messages = {
    type : "warning",
    message: "Invalid id provided, please try again later.",
  }
  <?php } ?>
</script>
<body class="<?php echo bodyClass(); ?>">
    <div id="wrapper">
        <?php include 'includes/navigation.php'; ?>
        <div class="loading" style="display: none;">Loading&#8230;</div>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="apps">
                            <div class="title">
                                <h2><?php echo pageTitle(array('Store countries management'), false); ?></h2>
                            </div>
                            <div class="clearfix">
                                <div class="row">
                                    <div class="col-lg-10">
                                        <?php
                                            if($res_stores){
                                                $res_stores = array_chunk($res_stores, 2);
                                                    $count=0;
                                                foreach($res_stores as $res_store){
                                        ?>
                                        <div class="row">
                                            <?php
                                                foreach ($res_store as $key => $row) { 
                                                    $count++;
                                            ?>
                                            <div class="col-lg-6">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td style="width: 30px"><?php echo $count;?></th>
                                                                <td>
                                                                    <div style="padding-bottom: 6px;">
                                                                        <?php if( $row->country_flag != '' ){ ?>
                                                                        <span><img style="max-width: 20px;" src="<?php echo $siteUrl?>/images/countries_flags/<?php echo $row->country_flag;?>"></span>
                                                                        <?php } ?>
                                                                        <span><?php echo $row->country_name;?></span> 
                                                                        <?php if( $row->country_name_arabic != '' ){ ?>
                                                                        (<span dir="rtl"><?php echo $row->country_name_arabic;?></span>)
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div style="padding-bottom: 6px;">
                                                                        <span class="label label-primary">store lang: <?php echo $row->store_lang;?></span>
                                                                        <span class="label label-primary">store code: <?php echo $row->store_code;?></span>
                                                                        <?php
                                                                            if( $row->status == '1' ){
                                                                                $class = "primary";
                                                                                $status = "Published";
                                                                            }else{
                                                                                $class = "warning";
                                                                                 $status = "Draft";
                                                                            }
                                                                        ?>
                                                                        <span class="label label-<?php echo $class; ?>">status: <?php echo $status;?></span>
                                                                    </div>
                                                                    <div>
                                                                        <a 
                                                                            class="__get_popup btn btn-primary btn-sm" 
                                                                            data-data='{ "command" : "edit-store-form", "ID": "<?php echo $row->id;?>" }' 
                                                                            data-loader="#update-loader-<?php echo $row->id;?>"
                                                                            href="#"
                                                                        >edit <span style="display:none" id="update-loader-<?php echo $row->id;?>"><i class="fa fa-spinner fa-spin"></i></span></a>
                                                                        <a 
                                                                            
                                                                            data-id="<?php echo $row->id;?>" 
                                                                            data-ajax-url="./store-management.php"
                                                                            href="#" 
                                                                            data-loader="#del-loader-<?php echo $row->id;?>"
                                                                            class="btn btn-danger btn-sm delete-store"
                                                                        >delete <span style="display:none" id="del-loader-<?php echo $row->id;?>"><i class="fa fa-spinner fa-spin"></i></span></a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>                                                
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <?php
                                                }
                                            }
                                        ?>
                                        
                                    </div>
                                    <div class="col-lg-2">
                                        <a 
                                            data-data='{ "command" : "add-new-store-form" }' 
                                            data-loader="#add-loader"
                                            href="#" 
                                            class="btn btn-primary btn-sm btn-block __get_popup"
                                        >Add new store <span style="display:none" id="add-loader"><i class="fa fa-spinner fa-spin"></i></span></a>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'includes/footer.php'; ?>
</body>
</html>