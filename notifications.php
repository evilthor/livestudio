<?php include 'config.php'; ?>
<?php include 'includes/session_check.php'; ?>
<?php include 'includes/header.php'; ?>
<body>
	<div id="wrapper" class="notification">
          <?php include('includes/navigation.php'); ?>
          <div id="page-wrapper">
               <div class="container-fluid" id="notifications">
                    <div class="row">
                         <div class="col-lg-12">
                         	<div class="apps">
                         		<div class="title">
                              		<h2>Notification manager</h2>
                                    	<a class="btn btn-primary" href="./add-notification.php">New notification</a>
                         		</div>
                         	</div>
                         </div>
                    </div>
                    <div class="row" id="inner-noti">
                         <div class="col-md-12">
                         	<div class="notification">
                         		<div class="scheduled">
	                         		<h3>Scheduled notifications</h3>
	                         		<div class="row">
	                         			<div class="col-md-12">
	                         				<ul>
                                            	<?php
												$sql = "SELECT * FROM tbl_notifications WHERE is_sent = '0' ORDER BY id DESC";
												$results = $db->get_results($sql);
												if($results){
													foreach($results as $result){
														?><li>
	                         						<span class="date-time">
	                         							<?php 
														date_default_timezone_set('Asia/Dubai');
														if($result->is_scheduled == 0){echo date('d M, H:i',strtotime($result->date_time));}else{echo date('d M, H:i',strtotime($result->schedule_date.' '.$result->schedule_time));} ?>
	                         						</span>
	                         						<span class="content"><?php echo $result->message; ?></span>
	                         					</li><?php
														}
													}else{
														echo"<li>No record found.</li>";
														}
												?>
	                         				</ul>
	                         			</div>
	                         		</div>
                         		</div>
                         		<div class="sent">
								<h3>Sent notifications</h3>
	                         		<div class="row">
	                         			<div class="col-md-12">
	                         				<ul>
                                            	<?php
												$sql = "SELECT * FROM tbl_notifications WHERE is_sent = '1' ORDER BY id DESC";
												$results = $db->get_results($sql);
												if($results){
													foreach($results as $result){
														?><li>
	                         						<span class="date-time">
	                         							<?php if($result->is_scheduled == 0){echo date('d M, H:i',strtotime($result->date_time));}else{echo date('d M, H:i',strtotime($result->schedule_date.' '.$result->schedule_time));} ?>
	                         						</span>
	                         						<span class="content"><?php echo $result->message; ?></span>
	                         					</li><?php
														}
													}else{
														echo"<li>No record found.</li>";
														}
												?>
	                         				</ul>
	                         			</div>
	                         		</div>
                         		</div>
                         	</div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
     <?php include 'includes/footer.php'; ?>
</body>
</html>