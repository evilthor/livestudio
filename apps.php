<?php
	include 'config.php';
	include 'includes/session_check.php';
	include 'includes/header.php';

	if(isset($_POST['id']) && $_POST['id'] != ''){

		// Delete from blocks
		$sql_del = "DELETE FROM tbl_block WHERE app_id = '".$_POST['id']."'";
		$db->query($sql_del);

		$sql_del_sb = "DELETE FROM tbl_static_block_status WHERE app_id = '".$_POST['id']."'";
		$db->query($sql_del_sb);

		// Delete from top sections
		$sql_del = "DELETE FROM tbl_top_sections WHERE app_id = '".$_POST['id']."'";
		$db->query($sql_del);

		// Delete from lander pages
		$sql_lander = "SELECT * FROM tbl_lander_pages WHERE app_id = '".$_POST['id']."'";
		$res_lander = $db->get_results($sql_lander);

		if($res_lander){
			foreach ($res_lander as $key => $value) {
				$sql_del = "DELETE FROM tbl_lander_rows WHERE lander_id = '".$value->id."'";
				$db->query($sql_del);
			}
			$sql_del = "DELETE FROM tbl_lander_pages WHERE app_id = '".$_POST['id']."'";
			$db->query($sql_del);
		}

		// Delete from merchandise
		$sql_merchandise = "SELECT * FROM tbl_merchandise WHERE app_id = '".$_POST['id']."'";
		$res_merchandise = $db->get_results($sql_merchandise);

		if($res_merchandise){
			foreach ($res_merchandise as $key => $value) {
				$sql_del = "DELETE FROM tbl_rows WHERE merchandise_id = '".$value->id."'";
				$db->query($sql_del);

				$sql_del = "DELETE FROM tbl_cat_rows WHERE merchandise_id = '".$value->id."'";
				$db->query($sql_del);
			}

			$sql_del = "DELETE FROM tbl_merchandise WHERE app_id = '".$_POST['id']."'";
			$db->query($sql_del);

			$sql_del_status = "DELETE FROM tbl_merchandise_status WHERE app_id = '".$_POST['id']."'";
			$db->query($sql_del_status);
		}

		// Delete from category
		$sql_categories = "SELECT * FROM tbl_categories WHERE app_id = '".$_POST['id']."'";
		$res_categories = $db->get_results($sql_categories);

		if($res_categories){
			foreach ($res_categories as $key => $value) {
				$sql_del = "DELETE FROM tbl_cat_rows WHERE merchandise_id = '".$value->id."'";
				$db->query($sql_del);
			}

			$sql_del = "DELETE FROM tbl_categories WHERE app_id = '".$_POST['id']."'";
			$db->query($sql_del);
		}

		// At last delete app
		$sql_del = "DELETE FROM tbl_apps WHERE id = '".$_POST['id']."'";
		$db->query($sql_del);
	}

	$sql = "SELECT * FROM tbl_apps WHERE user_id = '".@$_SESSION['id']."'";
 	$results = $db->get_results($sql);
 	if($results){
 		$global = $group = array();
 		foreach($results as $result){
 			$store = explode('_', $result->app_store);
 			if( $result->global_app == 0 ){
 				$group[$store[1]][$store[0]] = $result;
 			}
 			if( $result->global_app == 1 ){
 				$global[$store[1]][$store[0]] = $result;
 			}
		}
		asort($group);
		arsort($global['global']);
	}
?>
<body>
	<script>
		<?php if(isset($_GET['message']) && $_GET['message'] == 'success'){ ?>
		window.messages = {
			type : "Success",
			message: "App added successfully.",
		}
		<?php } ?>
		<?php if(isset($_GET['message']) && $_GET['message'] == 'update'){ ?>
		window.messages = {
			type : "Success",
			message: "App updated successfully.",
		}
		<?php } ?>
		<?php if(isset($_GET['message']) && $_GET['message'] == 'wrong'){ ?>
		window.messages = {
			type : "Error",
			message: "Invalid app id provided, please try again.",
		}
		<?php } ?>
	</script>
 	<div id="wrapper">
		<?php include('includes/navigation.php'); ?>
		<div id="page-wrapper">
		 	<div class="container-fluid">
				<div class="row">
				 	<div class="col-lg-12">
						<div class="apps">
						 	<div class="title">
								<h2>My Apps</h2>
								<a href="./add-app.php" class="btn btn-primary">Add app</a>
						 	</div>
						 	<div class="clearfix">
						 		<div class="row">
						 			<div class="col-lg-4 col-lg-offset-4">	
										<div class="panel panel-default">
											<div class="panel-heading"><i class="fa fa-globe" aria-hidden="true"></i> Global Apps</div>
											<div class="panel-body">
												<ul class="app-box clearfix">
											 		<?php 
													 	if($global['global']){
													 		$i = 1;
													 		$total = count($global['global']);
													 		foreach($global['global'] as $result){
																$class = '';
																if( $i == $total ){
																	$class = "last";
																}	
													?>
													<li class="<?php echo $class; ?>">
														<a href="./overview.php?app_id=<?php echo $result->id; ?>">
													 		<div class="img">
																<img src="<?php echo $result->appimage; ?>" alt="" class="img-responsive">
														 	</div>
														 	<div class="detail">
																<p>
															 		<?php echo $result->app_name; ?>
																 	<span class="country">
																		<?php echo $result->app_desc; ?><br><?php echo $result->app_store; ?>
																 	</span>
																</p>
															 </div>
														</a>
												 	</li>
													<?php
																$i++;
															}
														}
													?>
												</ul>
											</div>
										</div>
						 			</div>
						 		</div>
								<div class="row">
									<?php
										$count = 0;
										foreach ($group as $key => $gro) {
											arsort($gro);
											if( $count % 3 == 0 && $count != 0 ){
												echo '</div><div class="row">';
											}
									?>
									<div class="col-lg-4">
										<div class="panel panel-default">
											<div class="panel-heading"><span class="flag-icon flag-icon-<?php echo $key ?>"></span> <?php echo strtoupper($key); ?></div>
											<div class="panel-body">
												<ul class="app-box clearfix">
													<?php
														$total = count($gro);
														$i = 1;
														foreach ($gro as $key => $app) {
															$class = '';
															if( $i == $total ){
																$class = "last";
															}
													?>
													<li class="<?php echo $class; ?>">
														<a href="./overview.php?app_id=<?php echo $app->id; ?>">
													 		<div class="img">
																<img src="<?php echo $app->appimage; ?>" alt="" class="img-responsive">
														 	</div>
														 	<div class="detail">
																<p>
															 		<?php echo $app->app_name; ?>
																 	<span class="country">
																		<?php echo $app->app_desc; ?><br><?php echo $app->app_store; ?>
																 	</span>
																</p>
															 </div>
														</a>
														<div class="clearfix">
															<a class="pull-left" href="edit-app.php?app_id=<?php echo $app->id; ?>">Edit</a>
															<a 
																data-id="<?php echo $app->id;?>" 
																data-ajax-url="./apps.php"
																href="#" 
																data-loader="#del-loader-<?php echo $app->id;?>"
																class="pull-right delete-app"
															>Delete <span style="display:none" id="del-loader-<?php echo $app->id;?>"><i class="fa fa-spinner fa-spin"></i></span></a>
														</div>
												 	</li>
													<?php $i++; } ?>
												</ul>
											</div>
										</div>
									</div>
									<?php $count++; } ?>
								</div>
							</div>
					 	</div>
					</div>
			 	</div>
			</div>
 		</div>
 	</div>
 	<?php include 'includes/footer.php'; ?>
</body>
</html>