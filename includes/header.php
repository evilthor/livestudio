<!DOCTYPE html>
<html lang="en" ng-app>

<?php
    $ip = null;
    if ( !empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
        //check ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif ( !empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
        //to check ip is pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
// $ip = '123';
    define('IS_IP', $ip);
    function bodyClass($classes = array())
    {
        $arr = array();
        $arr[] = 'page';
        $bodyClass = $_SERVER['PHP_SELF'];
        $bodyClass = basename($bodyClass);
        $bodyClass = pathinfo($bodyClass);
        if( is_array($classes) && count($classes) > 0 ){
            foreach ($classes as $key => $class) {
                $arr[] = $class;
            }
        }
        $arr[] = str_replace(array('_'), '-', $bodyClass['filename']);
        $bodyClass = implode(' ', $arr);
        return $bodyClass;
    }
    function pageTitle($titles = array(), $prefix = true)
    {
        $arr = array();
        if( $prefix ){
            $arr[] = 'Mobile Studio - ';
        }
        $title = $_SERVER['PHP_SELF'];
        $title = basename($title);
        $title = pathinfo($title);
        if( is_array($titles) && count($titles) > 0 ){
            foreach ($titles as $key => $t) {
                $arr[] = $t;
            }
        }else{
            $arr[] = str_replace(array('-','_'), ' ', $title['filename']);
        }
        $title = implode(' ', $arr);
        return $title;
    }
    $sql_scheme = "SELECT * FROM tbl_color_schemes WHERE active='1' limit 1";
    $res_scheme = $db->get_results($sql_scheme);

    define("VERSION_STAMP", 21);
    $version_salt = '?v='.VERSION_STAMP;
?>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo pageTitle(); ?></title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/earlyaccess/notonaskharabic.css" rel="stylesheet">
    <?php /*if(count($res_scheme)){ ?>
    <!-- <link id="color-scheme" href="./color-schemes/<?php echo $res_scheme[0]->slug; ?>" rel="stylesheet"> -->
    <?php //}else{ ?>
    <!-- <link href="./bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> -->
    <?php }*/ ?>
    <!-- <link href="./bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> -->
    <link id="color-scheme" href="./bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <?php if( IS_IP == '127.0.0.1' || IS_IP == '::1' ){ ?>
    <link href="./bower_components/jquery-ui/themes/base/all.css<?php echo $version_salt;?>" rel="stylesheet">
    <link href="./bower_components/fine-uploader/dist/fine-uploader.min.css" rel="stylesheet">
    <link href="./bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="./bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet" />
    <link href="./node_modules/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="./bower_components/jquery-confirm2/dist/jquery-confirm.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="./bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="./bower_components/jstree/dist/themes/default/style.min.css" />
    <link rel="stylesheet" href="./node_modules/magnific-popup/dist/magnific-popup.css<?php echo $version_salt;?>" />
    <link rel="stylesheet" href="./node_modules/flag-icon-css/css/flag-icon.min.css" />
    <link rel="stylesheet" href="./node_modules/loaders.css/loaders.min.css" />
    <link rel="stylesheet" type="text/css" href="./node_modules/animate.css/animate.min.css">

    <link href="./dist/css/app.css<?php echo $version_salt;?>" rel="stylesheet">
    <?php }else{ ?>
    <link href="./dest/css/apps.min.css<?php echo $version_salt;?>" rel="stylesheet">
    <?php } ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<script type="text/template" id="qq-simple-thumbnails-template">
    <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="Drop files here">
        <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
            <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
        </div>
        <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
            <span class="qq-upload-drop-area-text-selector"></span>
        </div>
        <div class="qq-upload-button-selector qq-upload-button">
            <div class="qq-upload-text">Upload a file</div>
        </div>
        <span class="qq-drop-processing-selector qq-drop-processing">
            <span>Processing dropped files...</span>
            <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
        </span>
        <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
            <li>
                <div class="qq-progress-bar-container-selector">
                    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                </div>
                <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
                <span class="qq-upload-file-selector qq-upload-file"></span>
                <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                <span class="qq-upload-size-selector qq-upload-size"></span>
                <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel" data-toggle="tooltip" data-placement="top" title="Cancel"><i class="fa fa-times" aria-hidden="true"></i></button>
                <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry" data-toggle="tooltip" data-placement="top" title="Retry"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></button>
                <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
            </li>
        </ul>

        <dialog class="qq-alert-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Close</button>
            </div>
        </dialog>

        <dialog class="qq-confirm-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">No</button>
                <button type="button" class="qq-ok-button-selector">Yes</button>
            </div>
        </dialog>

        <dialog class="qq-prompt-dialog-selector">
            <div class="qq-dialog-message-selector"></div>
            <input type="text">
            <div class="qq-dialog-buttons">
                <button type="button" class="qq-cancel-button-selector">Cancel</button>
                <button type="button" class="qq-ok-button-selector">Ok</button>
            </div>
        </dialog>
    </div>
</script>
<script>
	<?php
        $app_id = isset($_REQUEST['app_id']) ? $_REQUEST['app_id'] : 0;
        $app_store = null;
    	$sql = "SELECT * FROM tbl_apps WHERE id = '".$app_id."'";
    	$result = $db->get_row($sql);
    	$data = array();
    	if($result){
    		$app_store = $result->app_store;
    	}
	?>
	window.app_store_chk = '<?php echo $app_store; ?>';
    window.base_url = './';
	window.messages = {};
    window.gUploader = {
        endpoint : './vendor/fineuploader/php-traditional-server/endpoint.php',
        deleteEndpoint: './vendor/fineuploader/php-traditional-server/endpoint.php',
        thumbnails: {
            placeholders: {
                waitingPath: './bower_components/fine-uploader/dist/placeholders/waiting-generic.png',
                notAvailablePath: './bower_components/fine-uploader/dist/placeholders/not_available-generic.png'
            }
        }
    }
    var appid_global = '<?php echo isset($_GET['app_id']) ? $_GET['app_id'] :'';?>';
    var page_global = '<?php echo $page_name;?>';
</script>
</head>