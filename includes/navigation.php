<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
	   <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
	   </button>
	   <a class="navbar-brand" href="./index.php">Mobile Studio</a>
	   <?php include('breadcrumb.php');?>
    </div>
    <ul class="nav navbar-top-links navbar-right">
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="notifications.php">
					<i class="fa fa-commenting fa-fw"></i> Notification Manager</a>
				</li>
				<li><a href="settings.php">
					<i class="fa fa-gear fa-fw"></i> Notification Settings</a>
				</li>
				<li class="divider"></li>
				<li><a href="update_categories_brands.php"><i class="fa fa-list fa-fw"></i> Update Categories & Brands</a></li>
				<li><a href="store-management.php"><i class="fa fa-globe fa-fw"></i> Store Countries Management</a></li>
				<li><a href="countries-management.php?store=en_ae"><i class="fa fa-globe fa-fw"></i> Allowed Countries Management</a></li>
				<li><a href="cache-management.php"><i class="fa fa-history fa-fw"></i> Cache Management</a></li>
				<!-- <li><a href="theme-schemes.php"><i class="fa fa-gear fa-fw"></i> Theme Schemes</a></li> -->
				<li class="divider"></li>
				<li><a href="profile.php">
					<i class="fa fa-user fa-fw"></i> User Profile</a>
				</li>
				<li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
			</ul>
		</li>
    </ul>
</nav>