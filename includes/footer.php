	<div class="ajax-loader" style="display: block;"><div class="loader-text">Loading...</div></div>
	<script src="./bower_components/jquery/dist/jquery.min.js"></script>
	<script src="./bower_components/jquery-ui/jquery-ui.min.js"></script>
	<script src="./bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="./bower_components/fine-uploader/dist/jquery.fine-uploader.min.js"></script>
	<script src="./bower_components/moment/min/moment.min.js"></script>
	<script src="./bower_components/moment-timezone/builds/moment-timezone-with-data.min.js"></script>
	<script src="./bower_components/angular/angular.min.js"></script>
	<?php if( IS_IP == '127.0.0.1' || IS_IP == '::1' ){ ?>
	<script src="./bower_components/mustache.js/mustache.min.js"></script>
	<script src="./bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="./node_modules/select2/dist/js/select2.full.min.js"></script>
	<script src="./node_modules/list.js/dist/list.min.js"></script>
	<script src="./bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="./bower_components/jquery-confirm2/dist/jquery-confirm.min.js"></script>
  	<script src="./bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
  	<script src="./bower_components/jstree/dist/jstree.min.js"></script>
  	<script src="./node_modules/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
  	<script src="./node_modules/jquery-form/dist/jquery.form.min.js"></script>
  	<script src="./node_modules/notifyjs-browser/dist/notify.js"></script>
	<script src="./node_modules/letteringjs/jquery.lettering.js"></script>
	<script src="./node_modules/textillate/jquery.textillate.js"></script>
	<!-- for global variable -->
	<script src="./dist/js/variables.js<?php echo $version_salt;?>"></script>
	<!-- for helper functions -->
	<script src="./dist/js/functions.js<?php echo $version_salt;?>"></script>
	<!-- for helper objects -->
	<script src="./dist/js/objects.js<?php echo $version_salt;?>"></script>
	<!-- for brands section -->
	<script src="./dist/js/brands.js<?php echo $version_salt;?>"></script>
	<!-- for categories section -->
	<script src="./dist/js/categories.js<?php echo $version_salt;?>"></script>
	<!-- for products search -->
	<script src="./dist/js/products.js<?php echo $version_salt;?>"></script>
	<!-- for sortable event -->
	<script src="./dist/js/sortable.js<?php echo $version_salt;?>"></script>
	<!-- for top section -->
	<script src="./dist/js/topSection.js<?php echo $version_salt;?>"></script>
	<!-- for all common events -->
	<script src="./dist/js/commonEvents.js<?php echo $version_salt;?>"></script>
	<!-- for lander page section -->
	<script src="./dist/js/landerPages.js<?php echo $version_salt;?>"></script>
	<!-- for merchandiser section -->
	<script src="./dist/js/merchandiseManager.js<?php echo $version_salt;?>"></script>
	<!-- for notifications section -->
	<script src="./dist/js/notifications.js<?php echo $version_salt;?>"></script>
	<!-- for static block section -->
	<script src="./dist/js/staticBlock.js<?php echo $version_salt;?>"></script>
	<!-- for store management section -->
	<script src="./dist/js/storeManagement.js<?php echo $version_salt;?>"></script>
	<!-- for init -->
	<script src="./dist/js/app.js<?php echo $version_salt;?>"></script>
	<?php }else{ ?>
	<script src="./dest/js/apps.min.js<?php echo $version_salt;?>"></script>
	<?php } ?>