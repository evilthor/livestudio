<?php
if(isset($_SESSION['id']) && isset($_SESSION['email'])){
	$bcrumb = array();

	$bcrumb['apps'][0]['url'] = '';
	$bcrumb['apps'][0]['title'] = '';
	
	$bcrumb['add-app'][0]['url'] = 'apps.php';
	$bcrumb['add-app'][0]['title'] = 'Home';
	$bcrumb['add-app'][1]['url'] = '';
	$bcrumb['add-app'][1]['title'] = 'Add new app';
	
	
	$bcrumb['settings'][0]['url'] = 'apps.php';
	$bcrumb['settings'][0]['title'] = 'Home';
	$bcrumb['settings'][1]['url'] = '';
	$bcrumb['settings'][1]['title'] = 'App Settings';
	
	$bcrumb['profile'][0]['url'] = 'apps.php';
	$bcrumb['profile'][0]['title'] = 'Home';
	$bcrumb['profile'][1]['url'] = '';
	$bcrumb['profile'][1]['title'] = 'Profile';
	
	
	$bcrumb['edit-app'][0]['url'] = 'apps.php';
	$bcrumb['edit-app'][0]['title'] = 'Home';
	$bcrumb['edit-app'][1]['url'] = '';
	$bcrumb['edit-app'][1]['title'] = 'Edit app';
	
	$bcrumb['overview'][0]['url'] = 'apps.php';
	$bcrumb['overview'][0]['title'] = 'Home';
	$bcrumb['overview'][1]['url'] = '';
	$bcrumb['overview'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	
	$bcrumb['notifications'][0]['url'] = 'apps.php';
	$bcrumb['notifications'][0]['title'] = 'Home';
	// $bcrumb['notifications'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	// $bcrumb['notifications'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['notifications'][2]['url'] = '';
	$bcrumb['notifications'][2]['title'] = 'Notifications';
	
	$bcrumb['add-notification'][0]['url'] = 'apps.php';
	$bcrumb['add-notification'][0]['title'] = 'Home';
	// $bcrumb['add-notification'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	// $bcrumb['add-notification'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['add-notification'][2]['url'] = 'notifications.php';
	$bcrumb['add-notification'][2]['title'] = 'Notifications';
	$bcrumb['add-notification'][3]['url'] = '';
	$bcrumb['add-notification'][3]['title'] = 'Add Notification';
	
	$bcrumb['merchandise-manager'][0]['url'] = 'apps.php';
	$bcrumb['merchandise-manager'][0]['title'] = 'Home';
	$bcrumb['merchandise-manager'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['merchandise-manager'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['merchandise-manager'][2]['url'] = '';
	$bcrumb['merchandise-manager'][2]['title'] = 'Merchandise manager';
	
	$bcrumb['merchandise-overview'][0]['url'] = 'apps.php';
	$bcrumb['merchandise-overview'][0]['title'] = 'Home';
	$bcrumb['merchandise-overview'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['merchandise-overview'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['merchandise-overview'][2]['url'] = 'merchandise-manager.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['merchandise-overview'][2]['title'] = 'Merchandise manager';
	$bcrumb['merchandise-overview'][3]['url'] = '';
	$bcrumb['merchandise-overview'][3]['title'] = 'Add Merchandise';
	
	$bcrumb['add-merchandise'][0]['url'] = 'apps.php';
	$bcrumb['add-merchandise'][0]['title'] = 'Home';
	$bcrumb['add-merchandise'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['add-merchandise'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['add-merchandise'][2]['url'] = 'merchandise-manager.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['add-merchandise'][2]['title'] = 'Merchandise manager';
	$bcrumb['add-merchandise'][3]['url'] = 'merchandise-overview.php?app_id='.@$_REQUEST['app_id'].'&id='.@$_REQUEST['id'];
	$bcrumb['add-merchandise'][3]['title'] = 'Add Merchandise';
	$bcrumb['add-merchandise'][4]['url'] = '';
	$bcrumb['add-merchandise'][4]['title'] = 'Add Row';
	
	$bcrumb['edit-merchandise'][0]['url'] = 'apps.php';
	$bcrumb['edit-merchandise'][0]['title'] = 'Home';
	$bcrumb['edit-merchandise'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['edit-merchandise'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['edit-merchandise'][2]['url'] = 'merchandise-manager.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['edit-merchandise'][2]['title'] = 'Merchandise manager';
	$bcrumb['edit-merchandise'][3]['url'] = 'merchandise-overview.php?app_id='.@$_REQUEST['app_id'].'&id='.@$_REQUEST['id'];
	$bcrumb['edit-merchandise'][3]['title'] = 'Add Merchandise';
	$bcrumb['edit-merchandise'][4]['url'] = '';
	$bcrumb['edit-merchandise'][4]['title'] = 'Edit Row';

	$bcrumb['lander-manager'][0]['url'] = 'apps.php';
	$bcrumb['lander-manager'][0]['title'] = 'Home';
	$bcrumb['lander-manager'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['lander-manager'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['lander-manager'][2]['url'] = '';
	$bcrumb['lander-manager'][2]['title'] = 'Lander manager';

	$bcrumb['lander-overview'][0]['url'] = 'apps.php';
	$bcrumb['lander-overview'][0]['title'] = 'Home';
	$bcrumb['lander-overview'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['lander-overview'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['lander-overview'][2]['url'] = 'lander-manager.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['lander-overview'][2]['title'] = 'Lander manager';
	$bcrumb['lander-overview'][3]['url'] = '';
	$bcrumb['lander-overview'][3]['title'] = 'Add Lander';

	$bcrumb['add-lander'][0]['url'] = 'apps.php';
	$bcrumb['add-lander'][0]['title'] = 'Home';
	$bcrumb['add-lander'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['add-lander'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['add-lander'][2]['url'] = 'lander-manager.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['add-lander'][2]['title'] = 'Lander manager';
	$bcrumb['add-lander'][3]['url'] = 'lander-overview.php?app_id='.@$_REQUEST['app_id'].'&id='.@$_REQUEST['id'];
	$bcrumb['add-lander'][3]['title'] = 'Add Lander';
	$bcrumb['add-lander'][4]['url'] = '';
	$bcrumb['add-lander'][4]['title'] = 'Add Row';
	
	$bcrumb['edit-lander'][0]['url'] = 'apps.php';
	$bcrumb['edit-lander'][0]['title'] = 'Home';
	$bcrumb['edit-lander'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['edit-lander'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['edit-lander'][2]['url'] = 'lander-manager.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['edit-lander'][2]['title'] = 'Lander manager';
	$bcrumb['edit-lander'][3]['url'] = 'lander-overview.php?app_id='.@$_REQUEST['app_id'].'&id='.@$_REQUEST['id'];
	$bcrumb['edit-lander'][3]['title'] = 'Add Lander';
	$bcrumb['edit-lander'][4]['url'] = '';
	$bcrumb['edit-lander'][4]['title'] = 'Edit Row';

	$bcrumb['top-sections'][0]['url'] = 'apps.php';
	$bcrumb['top-sections'][0]['title'] = 'Home';
	$bcrumb['top-sections'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['top-sections'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['top-sections'][2]['url'] = '';
	$bcrumb['top-sections'][2]['title'] = 'Top Sections';
	
	$bcrumb['new-top-section'][0]['url'] = 'apps.php';
	$bcrumb['new-top-section'][0]['title'] = 'Home';
	$bcrumb['new-top-section'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['new-top-section'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['new-top-section'][2]['url'] = 'top-sections.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['new-top-section'][2]['title'] = 'Top Sections';
	$bcrumb['new-top-section'][3]['url'] = '';
	$bcrumb['new-top-section'][3]['title'] = 'Add top section';

	$bcrumb['edit-top-section'][0]['url'] = 'apps.php';
	$bcrumb['edit-top-section'][0]['title'] = 'Home';
	$bcrumb['edit-top-section'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['edit-top-section'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['edit-top-section'][2]['url'] = 'top-sections.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['edit-top-section'][2]['title'] = 'Top Sections';
	$bcrumb['edit-top-section'][3]['url'] = '';
	$bcrumb['edit-top-section'][3]['title'] = 'Edit top section';

	$bcrumb['static-block'][0]['url'] = 'apps.php';
	$bcrumb['static-block'][0]['title'] = 'Home';
	$bcrumb['static-block'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['static-block'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['static-block'][2]['url'] = '';
	$bcrumb['static-block'][2]['title'] = 'Static Blocks';
	
	$bcrumb['new-static-block'][0]['url'] = 'apps.php';
	$bcrumb['new-static-block'][0]['title'] = 'Home';
	$bcrumb['new-static-block'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['new-static-block'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['new-static-block'][2]['url'] = 'static-block.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['new-static-block'][2]['title'] = 'Static Blocks';
	$bcrumb['new-static-block'][3]['url'] = '';
	$bcrumb['new-static-block'][3]['title'] = 'New Block';
	
	$bcrumb['edit-static-block'][0]['url'] = 'apps.php';
	$bcrumb['edit-static-block'][0]['title'] = 'Home';
	$bcrumb['edit-static-block'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['edit-static-block'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['edit-static-block'][2]['url'] = 'static-block.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['edit-static-block'][2]['title'] = 'Static Blocks';
	$bcrumb['edit-static-block'][3]['url'] = '';
	$bcrumb['edit-static-block'][3]['title'] = 'Edit Block';
	
	$bcrumb['category-manager'][0]['url'] = 'apps.php';
	$bcrumb['category-manager'][0]['title'] = 'Home';
	$bcrumb['category-manager'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['category-manager'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['category-manager'][2]['url'] = '';
	$bcrumb['category-manager'][2]['title'] = 'Search Category manager';
	
	$bcrumb['category-overview'][0]['url'] = 'apps.php';
	$bcrumb['category-overview'][0]['title'] = 'Home';
	$bcrumb['category-overview'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['category-overview'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['category-overview'][2]['url'] = 'category-manager.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['category-overview'][2]['title'] = 'Search Category manager';
	$bcrumb['category-overview'][3]['url'] = '';
	$bcrumb['category-overview'][3]['title'] = 'Add Category';
	
	$bcrumb['add-category'][0]['url'] = 'apps.php';
	$bcrumb['add-category'][0]['title'] = 'Home';
	$bcrumb['add-category'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['add-category'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['add-category'][2]['url'] = 'category-manager.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['add-category'][2]['title'] = 'Search Category manager';
	$bcrumb['add-category'][3]['url'] = 'category-overview.php?app_id='.@$_REQUEST['app_id'].'&id='.@$_REQUEST['id'];
	$bcrumb['add-category'][3]['title'] = 'Add Category';
	$bcrumb['add-category'][4]['url'] = '';
	$bcrumb['add-category'][4]['title'] = 'Add Row';
	
	$bcrumb['edit-category'][0]['url'] = 'apps.php';
	$bcrumb['edit-category'][0]['title'] = 'Home';
	$bcrumb['edit-category'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['edit-category'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['edit-category'][2]['url'] = 'category-manager.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['edit-category'][2]['title'] = 'Search Category manager';
	$bcrumb['edit-category'][3]['url'] = 'category-overview.php?app_id='.@$_REQUEST['app_id'].'&id='.@$_REQUEST['id'];
	$bcrumb['edit-category'][3]['title'] = 'Add Category';
	$bcrumb['edit-category'][4]['url'] = '';
	$bcrumb['edit-category'][4]['title'] = 'Edit Row';

/**/
	$bcrumb['search-category-manager'][0]['url'] = 'apps.php';
	$bcrumb['search-category-manager'][0]['title'] = 'Home';
	$bcrumb['search-category-manager'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['search-category-manager'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['search-category-manager'][2]['url'] = '';
	$bcrumb['search-category-manager'][2]['title'] = 'Search Category manager';
	
	$bcrumb['search-category-overview'][0]['url'] = 'apps.php';
	$bcrumb['search-category-overview'][0]['title'] = 'Home';
	$bcrumb['search-category-overview'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['search-category-overview'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['search-category-overview'][2]['url'] = 'search-category-manager.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['search-category-overview'][2]['title'] = 'Search Category manager';
	$bcrumb['search-category-overview'][3]['url'] = '';
	$bcrumb['search-category-overview'][3]['title'] = 'Add Category';
	
	$bcrumb['add-search-category'][0]['url'] = 'apps.php';
	$bcrumb['add-search-category'][0]['title'] = 'Home';
	$bcrumb['add-search-category'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['add-search-category'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['add-search-category'][2]['url'] = 'search-category-manager.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['add-search-category'][2]['title'] = 'Search Category manager';
	$bcrumb['add-search-category'][3]['url'] = 'search-category-overview.php?app_id='.@$_REQUEST['app_id'].'&id='.@$_REQUEST['id'];
	$bcrumb['add-search-category'][3]['title'] = 'Add Category';
	$bcrumb['add-search-category'][4]['url'] = '';
	$bcrumb['add-search-category'][4]['title'] = 'Add Row';
	
	$bcrumb['edit-search-category'][0]['url'] = 'apps.php';
	$bcrumb['edit-search-category'][0]['title'] = 'Home';
	$bcrumb['edit-search-category'][1]['url'] = 'overview.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['edit-search-category'][1]['title'] = strtoupper(str_replace('_', ' ', $app_store)) . ' Overview';
	$bcrumb['edit-search-category'][2]['url'] = 'search-category-manager.php?app_id='.@$_REQUEST['app_id'];
	$bcrumb['edit-search-category'][2]['title'] = 'Search Category manager';
	$bcrumb['edit-search-category'][3]['url'] = 'search-category-overview.php?app_id='.@$_REQUEST['app_id'].'&id='.@$_REQUEST['id'];
	$bcrumb['edit-search-category'][3]['title'] = 'Add Category';
	$bcrumb['edit-search-category'][4]['url'] = '';
	$bcrumb['edit-search-category'][4]['title'] = 'Edit Row';
/**/

	$bcrumb['store-management'][0]['url'] = 'apps.php';
	$bcrumb['store-management'][0]['title'] = 'Home';
	$bcrumb['store-management'][1]['url'] = '';
	$bcrumb['store-management'][1]['title'] = 'Store countries management';

	$bcrumb['update_categories_brands'][0]['url'] = 'apps.php';
	$bcrumb['update_categories_brands'][0]['title'] = 'Home';
	$bcrumb['update_categories_brands'][1]['url'] = '';
	$bcrumb['update_categories_brands'][1]['title'] = 'Sync Categories and Brands';

	$bcrumb['cache-management'][0]['url'] = 'apps.php';
	$bcrumb['cache-management'][0]['title'] = 'Home';
	$bcrumb['cache-management'][1]['url'] = '';
	$bcrumb['cache-management'][1]['title'] = 'cache management';
?>
	<ol class="breadcrumb navbar-left">
	  <?php 
	  $bIndex = str_replace('.php','',basename($_SERVER['PHP_SELF']));
	  if( isset($bcrumb[$bIndex])){
		  foreach($bcrumb[$bIndex] as $b){
				?>
                <li <?php if($b['url'] == ''){echo" class='active'";} ?>>
					<?php if($b['url'] != ''){?><a href="<?php echo $b['url']; ?>"><?php } ?>
						<?php echo $b['title']; ?>
					<?php if($b['url'] != ''){ ?></a><?php } ?>
                </li>
		<?php
			}
	  }
		?>
	</ol>
<?php }?>
