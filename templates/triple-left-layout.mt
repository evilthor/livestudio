<div class="row preview-triple-left">
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-12">
				<div class="preview _1024X704"><img src="{{ perviewOne }}"></div>
			</div>

			<div class="col-md-12">
				<div class="preview _1024X704"><img src="{{ perviewTwo }}"></div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="preview _1024X1310"><img src="{{ perviewThree }}"></div>
	</div>
</div>