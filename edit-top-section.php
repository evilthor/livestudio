<?php
    include 'config.php';
    include 'config.image.php';
    include 'includes/session_check.php';
    include 'includes/header.php';

    /*$sql_ts = "SELECT * FROM tbl_sort_rows WHERE app_id ='".$global_app_id."' AND row_type='top_section' AND row_type_id='".$_GET['id']."'";
    $res_ts = $db->get_results($sql_ts);
    if($res_ts){
        foreach($res_ts as $res_ts_single){
            //print_r($res_ts_single);
            $sql_tse = "SELECT id FROM tbl_sort_rows WHERE app_id ='".$_GET['app_id']."' AND row_type='top_section' AND row_type_id='".$_GET['id']."' AND row_id='".$res_ts_single->row_id."'";
            $res_tse = $db->get_row($sql_tse);
            //print_r($res_tse);
            if(!$res_tse){
                $sql = "INSERT INTO tbl_sort_rows SET
                        app_id = '" . $_GET['app_id'] . "',
                        row_id = '" . $res_ts_single->row_id . "',
                        row_type = '" . $res_ts_single->row_type . "',
                        row_type_id = '" . $res_ts_single->row_type_id. "',
                        sort_order = '" . $res_ts_single->sort_order."'";
                $res = $db->query($sql);
            }
        }
    }*/
        

    
    $sql_row = "SELECT * FROM tbl_top_sections WHERE id = '".$_GET['id']."'";
    $result_row = $db->get_row($sql_row);

if($yes_global==1){
    $global_check = "AND tbl_attachments.is_global=1";
    $row_ids_arr='';
    $deleted_check = "";
}else{
    $global_check = "";

    $sql = "SELECT GROUP_CONCAT(row_id) as row_ids FROM tbl_delete_data WHERE app_id ='".$_GET['app_id']."' AND row_type='top_section_buttons'";
        $skip_data = $db->get_row($sql);
        $row_ids_arr_temp = $skip_data->row_ids;

        if($row_ids_arr_temp){
            $row_ids_temp_arr = explode(',', $row_ids_arr_temp);
            $row_ids_arr = "'" . implode ( "', '", $row_ids_temp_arr ) . "'";
            $row_ids_arr = rtrim($row_ids_arr,"'");
            $row_ids_arr = ltrim($row_ids_arr,"'");
        }else{
            $row_ids_arr='';
        }
    $deleted_check = "AND tbl_attachments.id NOT IN ('".$row_ids_arr."')";
}



    // $sql = "SELECT * FROM tbl_attachments 
    //             WHERE app_id IN(".@$_GET['app_id'].", ".$global_app_id." ) 
    //             AND p_id = '" . $_GET['id'] . "'
    //             AND section_type = 'top_section' $global_check $deleted_check 
    //             ORDER BY sort_order ASC";
    $sql = "SELECT tbl_attachments.* FROM tbl_attachments JOIN tbl_sort_rows ON tbl_attachments.id=tbl_sort_rows.row_id where tbl_attachments.p_id = '".$result_row->id."' AND tbl_attachments.show_in_app = '1' AND tbl_attachments.app_id IN(".@$_GET['app_id'].", ".$global_app_id.") $deleted_check  $global_check AND tbl_sort_rows.id IN ( SELECT id FROM tbl_sort_rows WHERE tbl_sort_rows.row_type='top_section' AND tbl_sort_rows.app_id = '".@$_GET['app_id']."') ORDER BY tbl_sort_rows.sort_order ASC";
    $rows = $db->get_results($sql);
    $buttons = array();
    if($rows){
        foreach ($rows as $key => $row) {
            $buttons[$key]['value'] = $row->btn_text;
            $buttons[$key]['attachment'] = json_decode($row->attachment);
            $buttons[$key]['btnid'] = $row->id;
            $buttons[$key]['appid'] = $row->app_id;
        }
    }
    
?>
<body class="<?php echo bodyClass(array('top-section')); ?>">
    <div id="wrapper">
        <?php include('includes/navigation.php'); ?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="layouts" id="top-section">
                            <div class="inner-layouts">
                                <div class="row">
                                    <div class="col-lg-12 step step-1">
                                        <div class="clearfix">
                                            <span class="step-number">1</span>
                                            <div class="step-detail">
                                                <h6>Choose image</h6>
                                            </div>
                                        </div>
                                        <div class="uploader">
                                            <div 
                                                id="fine-uploader-gallery" 
                                                data-option='{
                                                      "layoutType":"single",
                                                      "layoutTypeDB":"single",
                                                      "viewTemplate":"static-block-single-layout.mt",
                                                      "iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
                                                      "iPhoneView": ".iphonePreviewContainer #carousel",
                                                      "imagePosition": "0"
                                                 }' 
                                                 data-width="<?php echo $config_static_width; ?>" 
                                                 data-height="<?php echo $config_static_height; ?>"
                                            ></div>

                                                            <div class="have-image" id="single-<?php echo $result_row->id ?>">
                                                                                <button type="button" data-element-hide="#single-<?php echo $result_row->id ?>"><i class="fa fa-times" aria-hidden="true"></i></button>
                                                                                <img src="<?php echo $result_row->image_url; ?>" class="img-responsive" />
                                                                           </div>
                                                           
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 step step-2">
                                        <div class="clearfix">
                                            <span class="step-number">2</span>
                                            <div class="step-detail">
                                                <h6>Add buttons</h6>
                                            </div>
                                        </div>
                                        <div class="blockLabel">
                                            <div class="form-inline">
                                                <div class="form-group">
                                                    <input type="hidden" name="row_id" id="row_id" value="<?php echo $_GET['id']; ?>" />
                                                    <!-- <input type="hidden" name="image_url_hidden" id="image_url_hidden" value="<?php echo $result_row->image_url; ?>" /> -->
                                                    <input type="text" class="form-control" id="button-text" required="" style="width: 130px;">
                                                    <input type="hidden" id="button-app" value="<?php echo $_GET['app_id'];?>">
                                                </div>
                                                <button 
                                                    type="button" 
                                                    class="btn btn-link btn-add-button" 
                                                    data-textfield="#button-text"
                                                    data-appid="#button-app"
                                                    data-preview-container="#smallImgOverlay"
                                                    data-save-button="#save-top-section"
                                                >add</button>                                            
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 step step-3">
                                        <div class="clearfix">
                                              <span class="step-number">3</span>
                                              <div class="step-detail">
                                                   <h6>Add content</h6>
                                                   <!-- <p>
                                                        Tap the images below to add a content link to them. Images can link to content like a category, product, website or can be static.
                                                   </p> -->
                                              </div>
                                        </div>
                                        <div class="img-link">
                                          <div class="image-preview-small">
                                                <div id="btn-msg"></div>
                                                <div id="smallImgOverlay" class="preview-layout" style="display:none;">
                                                    <div class="previewPayouts single-layout" id="single-layout">
                                                         <div class="img-thumb" id="singleLayout" data-all-hide=".contentDdPopup" data-click-show="#singleContentDdPopup" data-attachmentPos="0"></div>
                                                         <div class="contentDdPopup" id="singleContentDdPopup"></div>
                                                    </div>
                                                </div>
                                          </div>

                                         </div> 
                                         <hr>                           
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 step step-4">
                                        <div class="clearfix">
                                            <span class="step-number">4</span>
                                            <div class="step-detail">
                                                <h6>Save block</h6>
                                            </div>
                                        </div>
                                        <div class="saveBlock">
                                            <button 
                                                type="button" 
                                                name="save-top-section" 
                                                id="save-top-section" 
                                                disabled 
                                                class="btn btn-primary"
                                            >Save</button>
                                            <span id="btn-require-msg"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="apps">
                                    <div class="title" style="padding-bottom: 12px;">
                                        <h2>Edit top section</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix devicePreview" style="margin-top: -20px">
                            <div class="ipadPreviewContainer">
                                <div id="carouselOuter" class="carouselRowsManager">
                                    <div id="carouselCircleLeft"></div>
                                    <div id="carousel" class="mCustomScrollbar">
                                         <img src="<?php echo $result_row->image_url; ?>" class="img-responsive" />
                                    </div>
                                    <div id="carouselCircleRight"></div>
                                </div>
                            </div>
                            <div class="iphonePreviewContainer">
                                <div id="carouselOuter" class="carouselRowsManager">
                                    <div class="carouselCircleTop"></div>
                                    <div class="carouselRectangleTop"></div>
                                    <div class="carouselCircleTopLeft"></div>
                                    <div id="carousel" class="mCustomScrollbar">
                                         <img src="<?php echo $result_row->image_url; ?>" class="img-responsive" />
                                    </div>
                                    <div class="carouselCircleBottom"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'includes/footer.php'; ?>
    <script>
        <?php if( count($buttons) ){ ?>
        topSectionData.buttons = <?php echo json_encode($buttons, JSON_UNESCAPED_UNICODE); ?>;
        $.each(topSectionData.buttons, function(i, v) {
            if( v.attachment != undefined ){
                tmpAttachments[i] = v.attachment;
            }
        });
        <?php } ?>
        topSectionData.image.push('<?php echo $result_row->image_url; ?>');
        $(document).ready(function(){
            $('#smallImgOverlay').html(buttonLayouts()).show();
            sortButtons();
        })

        var currentApp = '<?php echo trim($_GET['app_id'])?>';
        var currentSec = '<?php echo trim($_GET['id'])?>';
    </script>
</body>
</html>