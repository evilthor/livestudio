<?php
     include 'config.php';
     include 'config.image.php';
     include 'includes/session_check.php';
     include 'includes/header.php';

     $showLayoutOne = 'both'; // both, short, tall, none;
     $showLayoutTwo = 'both'; // both, short, tall, none;
     $showLayoutThree = 'both'; // both, left, right, none;
     $showSingleBar = 'yes'; // yes, no;

     $previewLayout = array('lander');
     $previewLayout = implode(' ', $previewLayout);
     $uploaderLayoutData = array();

     $uploaderLayoutData['single']['layout'] = '';
     $uploaderLayoutData['single']['id'] = '';
     $uploaderLayoutData['single']['p_id'] = $_GET['id'];
     $uploaderLayoutData['single']['images'] = '';
     $uploaderLayoutData['single']['size'] = array(819, 524);

     $uploaderLayoutData['singleShort']['layout'] = '';
     $uploaderLayoutData['singleShort']['id'] = '';
     $uploaderLayoutData['singleShort']['p_id'] = $_GET['id'];
     $uploaderLayoutData['singleShort']['images'] = '';
     $uploaderLayoutData['singleShort']['size'] = array(819, 282);

     $uploaderLayoutData['double']['layout'] = '';
     $uploaderLayoutData['double']['id'] = '';
     $uploaderLayoutData['double']['p_id'] = $_GET['id'];
     $uploaderLayoutData['double']['images'] = '';
     $uploaderLayoutData['double']['size']['left'] = array($config_homedouble_width, $config_homedouble_height);
     $uploaderLayoutData['double']['size']['right'] = array($config_homedouble_width, $config_homedouble_height);

     $uploaderLayoutData['doubleShort']['layout'] = '';
     $uploaderLayoutData['doubleShort']['id'] = '';
     $uploaderLayoutData['doubleShort']['p_id'] = $_GET['id'];
     $uploaderLayoutData['doubleShort']['images'] = '';
     $uploaderLayoutData['doubleShort']['size']['left'] = array($config_homedoubleshort_width, $config_homedoubleshort_height);
     $uploaderLayoutData['doubleShort']['size']['right'] = array($config_homedoubleshort_width, $config_homedoubleshort_height);

     $uploaderLayoutData['tripleLeft']['layout'] = '';
     $uploaderLayoutData['tripleLeft']['id'] = '';
     $uploaderLayoutData['tripleLeft']['p_id'] = $_GET['id'];
     $uploaderLayoutData['tripleLeft']['images'] = '';
     $uploaderLayoutData['tripleLeft']['size']['left'] = array(
          array($config_tripleshort_width, $config_tripleshort_height),
          array($config_tripleshort_width, $config_tripleshort_height)
     );
     $uploaderLayoutData['tripleLeft']['size']['right'] = array($config_triplelarge_width, $config_triplelarge_height);

     $uploaderLayoutData['tripleRight']['layout'] = '';
     $uploaderLayoutData['tripleRight']['id'] = '';
     $uploaderLayoutData['tripleRight']['p_id'] = $_GET['id'];
     $uploaderLayoutData['tripleRight']['images'] = '';
     $uploaderLayoutData['tripleRight']['size']['left'] = array($config_triplelarge_width, $config_triplelarge_height);
     $uploaderLayoutData['tripleRight']['size']['right'] = array(
          array($config_tripleshort_width, $config_tripleshort_height),
          array($config_tripleshort_width, $config_tripleshort_height)
     );

     $uploaderLayoutData['singleBar']['layout'] = '';
     $uploaderLayoutData['singleBar']['id'] = '';
     $uploaderLayoutData['singleBar']['p_id'] = $_GET['id'];
     $uploaderLayoutData['singleBar']['images'] = '';
     $uploaderLayoutData['singleBar']['size'] = array($config_banner_width, $config_banner_height);

?>
<body class="<?php echo bodyClass(array('lander')); ?>">
     <div id="wrapper">
          <?php include('includes/navigation.php'); ?>
          <div id="page-wrapper">
               <div class="container-fluid">
                    <div class="layoutTitle">
                         <div class="form-group">
                              <input id="lander_title" name="lander_title" type="text" class="form-control" placeholder="lander Row <?php echo $_GET['active']; ?>" data-toggle="tooltip" readonly data-placement="right" title="This title is for your reference only and will not be used in the app">
                              <input type="hidden" name="lander_id" id="lander_id" value="<?php echo @$_GET['id'] ?>" />
                              <input type="hidden" name="app_id" id="app_id" value="<?php echo $_GET['app_id']; ?>" />
                         </div>
                    </div>
                    <div class="clearfix devicePreview">
                         <div class="ipadPreviewContainer">
                              <div id="carouselOuter" class="carouselRowsManager">
                                   <div id="carouselCircleLeft"></div>
                                   <div id="carousel" class="mCustomScrollbar"> </div>
                                   <div id="carouselCircleRight"></div>
                              </div>
                         </div>
                         <div class="iphonePreviewContainer">
                              <div id="carouselOuter" class="carouselRowsManager">
                                   <div class="carouselCircleTop"></div>
                                   <div class="carouselRectangleTop"></div>
                                   <div class="carouselCircleTopLeft"></div> 
                                   <div id="carousel" class="mCustomScrollbar"></div>
                                   <div class="carouselCircleBottom"></div>
                              </div>
                         </div>
                    </div>
                    <div class="layouts">
                         <div class="layout-boxs clearfix" style="visibility:hidden;">
                              <div class="layout-box active">
                                   <span>1</span>
                              </div>
                         </div>
                         <div class="layout-type" style="display:none">
                              <div class="row">
                                   <div class="col-lg-4">
                                        <div class="step step-1 clearfix">
                                             <span class="step-number">1</span>
                                             <div class="step-detail">
                                                  <h6>Choose layout & add images</h6>
                                                  <p>Choose one of the layouts with one, two or three images and add the images.</p>
                                             </div>
                                        </div>
                                        <div class="ddContainer">
                                             <div class="ddBtn clearfix" data-click-show=".ddPopup">
                                                  <div class="ddBtnText clipText">LAYOUT + IMAGES</div>
                                                  <div class="ddBtnArrow">
                                                       <i class="fa fa-angle-down" aria-hidden="true"></i>
                                                  </div>
                                             </div>
                                             <div class="ddPopup">
                                                  <div class="ddPopupPageContainer">
                                                       <div class="pageSection">
                                                            <div class="page-title clearfix">
                                                                 <a href="javascript:;" class="back" data-images="1" data-backTo=".ddPage" data-hide=".pageSection"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                                                                 <h6>ADD IMAGE</h6>
                                                            </div>
                                                            <?php
                                                                 require('./layout-templates/upload-layout-one.php');
                                                                 require('./layout-templates/upload-layout-two.php');
                                                                 require('./layout-templates/upload-layout-three.php');
                                                                 require('./layout-templates/upload-layout-bar.php');
                                                            ?>
                                                            <div class="done-section">
                                                                 <button disabled id="btnDoneImageUpload" data-hide=".ddPopup" class="done btn btn-primary btn-block">done</button>
                                                            </div>
                                                       </div>
                                                       <div class="ddPage">
                                                            <div class="ddTitle"> <span class="ddTitleText"> CHOOSE TEMPLATE </span> </div>
                                                            <ul class="ddList">
                                                                 <?php
                                                                      require('./layout-templates/layout-one.php');
                                                                      require('./layout-templates/layout-two.php');
                                                                      require('./layout-templates/layout-three.php'); 
                                                                      require('./layout-templates/layout-bar.php'); 
                                                                 ?>
                                                            </ul>
                                                       </div>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                                   <div class="col-lg-4">
                                        <div class="step step-2">
                                             <div class="clearfix">
                                                  <span class="step-number">2</span>
                                                  <div class="step-detail">
                                                       <h6>Add content</h6>
                                                       <p>
                                                            Tap the images below to add a content link to them. Images can link to content like a category, product, website or can be static.
                                                       </p>
                                                  </div>
                                             </div>
                                             <div class="img-link">
                                                  <div class="image-preview-small">
                                                       <div id="smallImgOverlay" class="preview-layout" style="display:none;">
                                                            <?php
                                                                 require('./layout-templates/preview-layout-one.php');
                                                                 require('./layout-templates/preview-layout-two.php');
                                                                 require('./layout-templates/preview-layout-three.php');
                                                                 require('./layout-templates/preview-layout-bar.php'); 
                                                            ?>
                                                       </div>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                                   <div class="col-lg-4">
                                        <div class="step step-3">
                                             <div class="clearfix">
                                                  <span class="step-number">3</span>
                                                  <div class="step-detail">
                                                       <h6>Finalize row</h6>
                                                       <p>
                                                            All changes wil be saved and published
                                                       </p>
                                                  </div>
                                             </div>
                                             <div class="submit-button">
                                                  <button id="saveRow" disabled class="btn btn-primary">Save changes</button>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
     <?php include 'includes/footer.php'; ?>
     <script>
          $(document).ready(function(){
          	$('.layout-boxs').hide();
          	$('.layout-type').show();
          });
     </script>
</body>
</html>