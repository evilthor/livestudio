<?php

/**
 * PHP Server-Side Example for Fine Uploader (traditional endpoint handler).
 * Maintained by Widen Enterprises.
 *
 * This example:
 *  - handles chunked and non-chunked requests
 *  - supports the concurrent chunking feature
 *  - assumes all upload requests are multipart encoded
 *  - supports the delete file feature
 *
 * Follow these steps to get up and running with Fine Uploader in a PHP environment:
 *
 * 1. Setup your client-side code, as documented on http://docs.fineuploader.com.
 *
 * 2. Copy this file and handler.php to your server.
 *
 * 3. Ensure your php.ini file contains appropriate values for
 *    max_input_time, upload_max_filesize and post_max_size.
 *
 * 4. Ensure your "chunks" and "files" folders exist and are writable.
 *    "chunks" is only needed if you have enabled the chunking feature client-side.
 *
 * 5. If you have chunking enabled in Fine Uploader, you MUST set a value for the `chunking.success.endpoint` option.
 *    This will be called by Fine Uploader when all chunks for a file have been successfully uploaded, triggering the
 *    PHP server to combine all parts into one file. This is particularly useful for the concurrent chunking feature,
 *    but is now required in all cases if you are making use of this PHP example.
 */

// Include the upload handler class
require_once "handler.php";
require_once "webp_gd.php";

$uploader = new UploadHandler();

// Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
$uploader->allowedExtensions = array(); // all files types allowed by default

// Specify max file size in bytes.
$uploader->sizeLimit = null;

// Specify the input name set in the javascript.
$uploader->inputName = "qqfile"; // matches Fine Uploader's default inputName value by default

// If you want to use the chunking/resume feature, specify the folder to temporarily save parts.
$uploader->chunksFolder = "chunks";

$method = $_SERVER["REQUEST_METHOD"];
if ($method == "POST") {
    header("Content-Type: text/plain");

    // Assumes you have a chunking.success.endpoint set to point here with a query parameter of "done".
    // For example: /myserver/handlers/endpoint.php?done
    if (isset($_GET["done"])) {
        $result = $uploader->combineChunks("files");
    }
    // Handles upload requests
    else {
        // Call handleUpload() with the name of the folder, relative to PHP's getcwd()
        $result = $uploader->handleUpload("files");

        // To return a name used for uploaded file you can use the following line.
        $result["uploadName"] = $uploader->getUploadName();
    }
    $result["LINK"] = "vendor/fineuploader/php-traditional-server/files/".$result['uuid']."/".$result["uploadName"];
    $sourceImgUrl = "../../../".$result["LINK"];
    $image_data = getimagesize($sourceImgUrl);
    $image_width = $image_data[0];
    $image_height = $image_data[1];
    $result["SIZE"] = $image_width."x".$image_height;
    $result["COMPRESSED"] = 'No';

    $imgick = new Imagick(__DIR__."/files/".$result['uuid']."/".$result["uploadName"]);
    $imgick_type_compression = $imgick->getImageCompressionQuality();
    $imgick_mime_type = $imgick->getImageMimeType();
    $result["tp"] = $imgick_mime_type;
    $result["cmp"] = $imgick_type_compression;
    if(($imgick_type_compression>94) && ($imgick_mime_type == 'image/jpeg' || $imgick_mime_type == 'image/jpg')){
        $cmp_level = $imgick_type_compression-5;
        lossLessOptimize(__DIR__."/files/".$result['uuid']."/".$result["uploadName"],$cmp_level);
        $result["COMPRESSED"] = 'Yes';
    }

    // create webp
    if(($image_width=='300' && $image_height=='380') || ($image_width=='1200' && $image_height=='276')) {
        $uploadNameArr = explode(".", $result["uploadName"]);
        $webPImgUrl = "../../../vendor/fineuploader/php-traditional-server/files/" . $result['uuid'] . "/" . $uploadNameArr[0] . ".webp";
        webpconvert_gd($sourceImgUrl, $webPImgUrl, '100', '');
    }
    ///
    echo json_encode($result);
}
// for delete file requests
else if ($method == "DELETE") {
    $result = $uploader->handleDelete("files");
    echo json_encode($result);
}
else {
    header("HTTP/1.0 405 Method Not Allowed");
}


function lossLessOptimize($source_url,$qLavel){
    header('Content-type: image/jpeg');
    $imagick = new Imagick($source_url);
    $imagick->setCompression(imagick::COMPRESSION_JPEG);
    $imagick->setImageCompressionQuality($qLavel);
    $profiles = $imagick->getImageProfiles("icc", true);
    $imagick->stripImage();
    //$imagick->despeckleImage();
    if(!empty($profiles))
        $imagick->profileImage("icc", $profiles['icc']);

    $imagick->writeImage($source_url);
}
?>
