<?php
$config_cat_width = '680';
$config_cat_height = '480';

$config_static_width = '1125';
$config_static_height = '810';

$config_homedouble_width = '564';
$config_homedouble_height = '564';

$config_homedoubleshort_width = '564';
$config_homedoubleshort_height = '282';

$config_triplelarge_width = '564';
$config_triplelarge_height = '1128';

$config_tripleshort_width = '564';
$config_tripleshort_height = '564';

$config_banner_width = '1125';
$config_banner_height = '138';
?>