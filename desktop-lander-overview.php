<?php include 'config.php'; ?>
<?php include 'includes/session_check.php'; ?>
<?php include 'includes/header.php'; ?>
<?php
$is_edit = false;
$layout = '';
if($_GET['type']=='slider'){
    $tbl = 'tbl_desktop_lander_slider';
}else{
    $tbl = 'tbl_desktop_lander_rows';
}
if(isset($_GET['cat_id'])){
    $sql_cat = "SELECT * FROM tbl_categories WHERE id = '".$_GET['cat_id']."'";
    $result_cat = $db->get_row($sql_cat);

    $sql = "SELECT * FROM $tbl WHERE cat_id = '".$_GET['cat_id']."' ORDER BY row_order ASC";
    $results = $db->get_results($sql);
    $layout = perviewLayouts($results); // this function in common_php.php
} ?>
<body class="<?php echo bodyClass(array('lander')); ?>">
<form action="" id="sortableForm">
    <input type="hidden" name="command" value="desktopLanderLayoutSort">
    <input type="hidden" name="app_id" value="<?php echo intval($_GET['app_id']) ?>">
    <input type="hidden" name="lander_cat_id" value="<?php echo intval($_GET['cat_id']) ?>">
    <input type="hidden" name="lander_type" value="<?php echo $_GET['type']; ?>">
    <input type="hidden" name="row_order" value="">
</form>
<div id="wrapper">
    <?php include('includes/navigation.php'); ?>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <!--<div id="cacheStatus" class="cacheStatusDiv">
                Please clear LANDER services cache for new changes to take place!<br>
                <a href="lander-overview.php?app_id=<?php /*echo $_GET['app_id']; */?>&id=<?php /*echo $_GET['id']; */?>">
                    <span class="cacheStatusSpan">OK</span>
                </a>
            </div>-->
            <div class="layoutTitle">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <!--<label for="lander_title">Title</label>
                            <input id="desktop_lander_title" name="desktop_lander_title" type="text" class="form-control" placeholder="please fill in title" data-toggle="tooltip" data-placement="right" title="This title is for your reference only and will not be used in the site" value="<?php /*echo @$result_lander->title; */?>">-->
                            <input type="hidden" name="lander_cat_id" id="lander_cat_id" value="<?php echo @$_GET['cat_id']; ?>" />
                            <input type="hidden" name="app_id" id="app_id" value="<?php echo $_GET['app_id']; ?>" />
                            <input type="hidden" name="lander_type" id="lander_type" value="<?php echo $_GET['type']; ?>" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix devicePreview">
                <div class="ipadPreviewContainer">
                    <div id="carouselOuter" class="carouselRowsManager">
                        <div id="carouselCircleLeft"></div>
                        <div id="carousel" class="mCustomScrollbar"> <?php echo $layout; ?> </div>
                        <div id="carouselCircleRight"></div>
                    </div>
                </div>
                <div class="iphonePreviewContainer">
                    <div id="carouselOuter" class="carouselRowsManager">
                        <div class="carouselCircleTop"></div>
                        <div class="carouselRectangleTop"></div>
                        <div class="carouselCircleTopLeft"></div>
                        <div id="carousel" class="mCustomScrollbar"> <?php echo $layout; ?> </div>
                        <div class="carouselCircleBottom"></div>
                    </div>
                </div>
            </div>
            <div class="layouts" style="max-height: inherit;">
                <div class="layout-boxs clearfix">
                    <ul id="sortable">
                        <?php
                        $c = 1;
                        if(@$results){
                            $totalRows = count($results);
                            foreach($results as $result){
                                ?>
                                <li id="<?php echo $result->id; ?>">
                                    <div class="layout-box">
                                        <div class="overlay" id="single-edit-popup">
                                            <a class="btn btn-xs btn-primary" href="desktop-edit-lander.php?app_id=<?php echo $_GET['app_id']; ?>&cat_id=<?php echo $_GET['cat_id']; ?>&row_id=<?php echo $result->id; ?>&lander_type=<?php echo $_GET['type']; ?>&active=<?php echo $c; ?>">Edit</a>
                                            <a data-appid="<?php echo $_GET['app_id']; ?>" data-catid="<?php echo $_GET['cat_id']; ?>" data-rowid="<?php echo $result->id; ?>" data-type="<?php echo trim($_GET['type']); ?>" class="btn btn-xs btn-danger btn-delete-lander-desktop" href="javascript:;">Delete</a>
                                        </div>
                                        <span data-popup="#single-edit-popup">
									<?php
                                    if($result->layout == "single" || $result->layout == "singleShort" || $result->layout == "singleBar"){
                                        $images = json_decode($result->images);
                                        ?>
                                        <div class="single">
										<div class="row">
											<div class="col-md-12">
												<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
											</div>
										</div>
									</div>
                                        <?php
                                    }elseif($result->layout == "double" || $result->layout == "doubleShort"){
                                        $images = json_decode($result->images);
                                        ?>
                                        <div class="double">
										<div class="row">
											<div class="col-md-6">
												<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
											</div>
											<div class="col-md-6">
												<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
											</div>
										</div>
									</div>
                                        <?php
                                    }elseif($result->layout == "tripleLeft"){
                                        $images = json_decode($result->images);
                                        ?>
                                        <div class="triple">
										<div class="row">
											<div class="col-md-6">
												<div class="row">
													<div class="col-md-12">
														<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="img"> <img src="<?php echo $images[2]; ?>" class="img-responsive"> </div>
											</div>
										</div>
									</div>
                                        <?php
                                    }elseif($result->layout == "tripleRight"){
                                        $images = json_decode($result->images);
                                        ?>
                                        <div class="triple">
										<div class="row">
											<div class="col-md-6">
												<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
											</div>
											<div class="col-md-6">
												<div class="row">
													<div class="col-md-12">
														<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="img"> <img src="<?php echo $images[2]; ?>" class="img-responsive"> </div>
													</div>
												</div>
											</div>
										</div>
									</div>
                                        <?php
                                    }
                                    $c++;
                                    ?>
								</span>
                                    </div>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                    <div
                            data-app="<?php echo @$_GET['app_id']; ?>"
                            data-catid="<?php echo @$_GET['cat_id']; ?>"
                            data-landertype="<?php echo @$_GET['type']; ?>"
                            data-column="<?php echo $c; ?>"
                            class="layout-box add-desktop-lander active"
                    >
                        <span><?php echo $c; ?></span>
                    </div>
                    <?php
                    $c++;
                    if($c < 11){
                        while($c <= 10){
                            ?>
                            <div class="layout-box"> <span><?php echo $c; ?></span> </div>
                            <?php
                            $c++;
                        }
                    }
                    ?>
                </div>
                <div class="layout-type" style="display:none">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="step step-1 clearfix"> <span class="step-number">1</span>
                                <div class="step-detail">
                                    <h6>Choose layout & add images</h6>
                                    <p>Choose one of the layouts with one, two or three images and add the images.</p>
                                </div>
                            </div>
                            <div class="ddContainer">
                                <div class="ddBtn clearfix" data-click-show=".ddPopup">
                                    <div class="ddBtnText clipText">LAYOUT + IMAGES</div>
                                    <div class="ddBtnArrow"> <i class="fa fa-angle-down" aria-hidden="true"></i> </div>
                                </div>
                                <div class="ddPopup">
                                    <div class="ddPopupPageContainer">
                                        <div class="pageSection">
                                            <div class="page-title clearfix"> <a href="javascript:;" class="back" data-images="1" data-backto=".ddPage" data-hide=".pageSection"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                                                <h6>ADD IMAGE</h6>
                                            </div>
                                            <div class="page-1 upload-section">
                                                <div class="layoutItem">
                                                    <div class="grayBlock">
                                                        <div class="blockTitle">1</div>
                                                    </div>
                                                    <div class="file-uploader">
                                                        <div class="file-uploader-box">
                                                            <div
                                                                    id="fine-uploader"
                                                                    data-option='{
                                                                                          "layoutType":"single",
                                                                                          "viewTemplate":"single-layout.mt",
                                                                                          "iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
                                                                                          "iPhoneView": ".iphonePreviewContainer #carousel",
                                                                                          "smallPreview": "#singleLayout",
                                                                                          "imagePosition": "0"
                                                                                     }'
                                                                    data-width="2048"
                                                                    data-height="1310"
                                                                    class="fine-uploader"
                                                            > </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="page-3 upload-section">
                                                <div class="layoutTemplate">
                                                    <div class="twoLayoutShort clearfix">
                                                        <div class="leftItem pull-left">
                                                            <div class="grayBlock">
                                                                <div class="blockTitle">1</div>
                                                            </div>
                                                        </div>
                                                        <div class="rightItem pull-right">
                                                            <div class="grayBlock">
                                                                <div class="blockTitle">2</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="file-uploader clearfix">
                                                        <div class="file-uploader-box">
                                                            <div
                                                                    id="twoLayoutShort-fine-uploader-left"
                                                                    class="fine-uploader"
                                                                    data-width="1024"
                                                                    data-height="704"
                                                                    data-option='{
                                                                                          "layoutType":"double",
                                                                                          "viewTemplate":"double-layout.mt",
                                                                                          "iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
                                                                                          "iPhoneView": ".iphonePreviewContainer #carousel",
                                                                                          "smallPreview": "#twoCol-left-short",
                                                                                          "imagePosition": "0"
                                                                                     }'
                                                            > </div>
                                                        </div>
                                                        <div class="file-uploader-box">
                                                            <div
                                                                    id="twoLayoutShort-fine-uploader-right"
                                                                    class="fine-uploader"
                                                                    data-width="1024"
                                                                    data-height="704"
                                                                    data-option='{
                                                                                          "layoutType":"double",
                                                                                          "viewTemplate":"double-layout.mt",
                                                                                          "iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
                                                                                          "iPhoneView": ".iphonePreviewContainer #carousel",
                                                                                          "smallPreview": "#twoCol-right-short",
                                                                                          "imagePosition": "1"
                                                                                     }'
                                                            > </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="page-2 upload-section">
                                                <div class="layoutTemplate">
                                                    <div class="twoLayout clearfix">
                                                        <div class="leftItem pull-left">
                                                            <div class="grayBlock">
                                                                <div class="blockTitle">1</div>
                                                            </div>
                                                        </div>
                                                        <div class="rightItem pull-right">
                                                            <div class="grayBlock">
                                                                <div class="blockTitle">2</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="file-uploader clearfix" data-width="200">
                                                        <div class="file-uploader-box">
                                                            <div
                                                                    id="twoLayout-fine-uploader-left"
                                                                    class="fine-uploader"
                                                                    data-width="1024"
                                                                    data-height="1310"
                                                                    data-option='{
                                                                                          "layoutType":"double",
                                                                                          "viewTemplate":"double-layout.mt",
                                                                                          "iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
                                                                                          "iPhoneView": ".iphonePreviewContainer #carousel",
                                                                                          "smallPreview": "#twoCol-left",
                                                                                          "imagePosition": "0"
                                                                                     }'
                                                            > </div>
                                                        </div>
                                                        <div class="file-uploader-box">
                                                            <div
                                                                    id="twoLayout-fine-uploader-right"
                                                                    class="fine-uploader"
                                                                    data-width="1024"
                                                                    data-height="1310"
                                                                    data-option='{
                                                                                          "layoutType":"double",
                                                                                          "viewTemplate":"double-layout.mt",
                                                                                          "iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
                                                                                          "iPhoneView": ".iphonePreviewContainer #carousel",
                                                                                          "smallPreview": "#twoCol-right",
                                                                                          "imagePosition": "1"
                                                                                     }'
                                                            > </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="page-4 upload-section">
                                                <div class="layoutTemplate">
                                                    <div class="threeLayout clearfix">
                                                        <div class="leftItem pull-left">
                                                            <div class="grayBlock">
                                                                <div class="blockTitle">1</div>
                                                            </div>
                                                            <div class="grayBlock">
                                                                <div class="blockTitle">2</div>
                                                            </div>
                                                        </div>
                                                        <div class="rightItem pull-right">
                                                            <div class="grayBlock">
                                                                <div class="blockTitle">3</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="file-uploader clearfix">
                                                        <div class="leftCol pull-left">
                                                            <div class="file-uploader-box">
                                                                <div
                                                                        id="threeLayout-fine-uploader-left-1"
                                                                        class="fine-uploader"
                                                                        data-option='{
                                                                                               "layoutType":"triple",
                                                                                               "viewTemplate":"triple-left-layout.mt",
                                                                                               "iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
                                                                                               "iPhoneView": ".iphonePreviewContainer #carousel",
                                                                                               "smallPreview": "#threeCol-left-1",
                                                                                               "imagePosition": "0"
                                                                                          }'
                                                                        data-width="1024"
                                                                        data-height="704"
                                                                > </div>
                                                            </div>
                                                            <div class="file-uploader-box">
                                                                <div
                                                                        id="threeLayout-fine-uploader-left-2"
                                                                        class="fine-uploader"
                                                                        data-option='{
                                                                                               "layoutType":"triple",
                                                                                               "viewTemplate":"triple-left-layout.mt",
                                                                                               "iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
                                                                                               "iPhoneView": ".iphonePreviewContainer #carousel",
                                                                                               "smallPreview": "#threeCol-left-2",
                                                                                               "imagePosition": "1"
                                                                                          }'
                                                                        data-width="1024"
                                                                        data-height="704"
                                                                > </div>
                                                            </div>
                                                        </div>
                                                        <div class="rightCol pull-right">
                                                            <div class="file-uploader-box">
                                                                <div
                                                                        id="threeLayout-fine-uploader-left-3"
                                                                        class="fine-uploader"
                                                                        data-option='{
                                                                                               "layoutType":"triple",
                                                                                               "viewTemplate":"triple-left-layout.mt",
                                                                                               "iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
                                                                                               "iPhoneView": ".iphonePreviewContainer #carousel",
                                                                                               "smallPreview": "#threeCol-left-3",
                                                                                               "imagePosition": "2"
                                                                                          }'
                                                                        data-width="1024"
                                                                        data-height="1310"
                                                                > </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="page-5 upload-section">
                                                <div class="layoutTemplate">
                                                    <div class="threeLayout clearfix">
                                                        <div class="leftItem pull-left">
                                                            <div class="grayBlock">
                                                                <div class="blockTitle">1</div>
                                                            </div>
                                                        </div>
                                                        <div class="rightItem pull-right">
                                                            <div class="grayBlock">
                                                                <div class="blockTitle">2</div>
                                                            </div>
                                                            <div class="grayBlock">
                                                                <div class="blockTitle">3</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="file-uploader clearfix">
                                                        <div class="leftCol pull-left">
                                                            <div class="file-uploader-box">
                                                                <div
                                                                        id="threeLayout-fine-uploader-right-1"
                                                                        class="fine-uploader"
                                                                        data-option='{
                                                                                               "layoutType":"triple",
                                                                                               "viewTemplate":"triple-right-layout.mt",
                                                                                               "iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
                                                                                               "iPhoneView": ".iphonePreviewContainer #carousel",
                                                                                               "smallPreview": "#threeCol-right-1",
                                                                                               "imagePosition": "0"
                                                                                          }'
                                                                        data-width="1024"
                                                                        data-height="1310"
                                                                > </div>
                                                            </div>
                                                        </div>
                                                        <div class="rightCol pull-right">
                                                            <div class="file-uploader-box">
                                                                <div
                                                                        id="threeLayout-fine-uploader-right-2"
                                                                        class="fine-uploader"
                                                                        data-option='{
                                                                                               "layoutType":"triple",
                                                                                               "viewTemplate":"triple-right-layout.mt",
                                                                                               "iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
                                                                                               "iPhoneView": ".iphonePreviewContainer #carousel",
                                                                                               "smallPreview": "#threeCol-right-2",
                                                                                               "imagePosition": "1"
                                                                                          }'
                                                                        data-width="1024"
                                                                        data-height="704"
                                                                ></div>
                                                            </div>
                                                            <div class="file-uploader-box">
                                                                <div
                                                                        id="threeLayout-fine-uploader-right-3"
                                                                        class="fine-uploader"
                                                                        data-option='{
                                                                                               "layoutType":"triple",
                                                                                               "viewTemplate":"triple-right-layout.mt",
                                                                                               "iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
                                                                                               "iPhoneView": ".iphonePreviewContainer #carousel",
                                                                                               "smallPreview": "#threeCol-right-3",
                                                                                               "imagePosition": "2"
                                                                                          }'
                                                                        data-width="1024"
                                                                        data-height="704"
                                                                ></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="page-6 upload-section">
                                                <div class="layoutItem">
                                                    <div class="grayBlock">
                                                        <div class="blockTitle">1</div>
                                                    </div>
                                                    <div class="file-uploader">
                                                        <div class="file-uploader-box">
                                                            <div
                                                                    id="fine-uploader-bar"
                                                                    class="fine-uploader"
                                                                    data-width="2048"
                                                                    data-height="302"
                                                                    data-option='{
                                                                                          "layoutType":"single",
                                                                                          "viewTemplate":"single-layout.mt",
                                                                                          "iPadView": ".ipadPreviewContainer #carouselOuter #carousel",
                                                                                          "iPhoneView": ".iphonePreviewContainer #carousel",
                                                                                          "smallPreview": "#barLayout",
                                                                                          "imagePosition": "0"
                                                                                     }'
                                                            > </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="done-section">
                                                <button disabled id="btnDoneImageUpload" class="done btn btn-primary btn-block">done</button>
                                            </div>
                                        </div>
                                        <div class="ddPage">
                                            <div class="ddTitle"> <span class="ddTitleText"> CHOOSE TEMPLATE </span> </div>
                                            <ul class="ddList">
                                                <li class="ddItem singleLayoutContainer">
                                                    <div class="templateTitle">one image:</div>
                                                    <div
                                                            class="layoutItemContainer"
                                                            data-option='{
                                                                                "pageHide":".ddPage",
                                                                                "pageShow":".pageSection",
                                                                                "layoutShow":".page-1",
                                                                                "layoutHide":".upload-section",
                                                                                "smallPreviewShow":".previewPayouts#single-layout",
                                                                                "smallPreviewHide":".previewPayouts"
                                                                           }'
                                                    >
                                                        <div class="single-layout">
                                                            <div class="layoutTemplate">
                                                                <div class="singleLayout">
                                                                    <div class="layoutItem">
                                                                        <div class="grayBlock">
                                                                            <div class="blockTitle"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="ddItem twoLayoutContainer">
                                                    <div class="templateTitle">two images:</div>
                                                    <div class="layoutItemContainer short"
                                                         data-option='{
                                                                                "pageHide":".ddPage",
                                                                                "pageShow":".pageSection",
                                                                                "layoutShow":".page-3",
                                                                                "layoutHide":".upload-section",
                                                                                "smallPreviewShow":".previewPayouts#two-col-layout-short",
                                                                                "smallPreviewHide":".previewPayouts"
                                                                           }'
                                                    >
                                                        <div class="two-layout">
                                                            <div class="layoutTemplate">
                                                                <div class="twoLayout clearfix">
                                                                    <div class="leftItem pull-left">
                                                                        <div class="grayBlock">
                                                                            <div class="blockTitle"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="rightItem pull-right">
                                                                        <div class="grayBlock">
                                                                            <div class="blockTitle"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="layoutItemContainer"
                                                         data-option='{
                                                                                "pageHide":".ddPage",
                                                                                "pageShow":".pageSection",
                                                                                "layoutShow":".page-2",
                                                                                "layoutHide":".upload-section",
                                                                                "smallPreviewShow":".previewPayouts#two-col-layout",
                                                                                "smallPreviewHide":".previewPayouts"
                                                                           }'
                                                    >
                                                        <div class="two-layout">
                                                            <div class="layoutTemplate">
                                                                <div class="twoLayout clearfix">
                                                                    <div class="leftItem pull-left">
                                                                        <div class="grayBlock">
                                                                            <div class="blockTitle"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="rightItem pull-right">
                                                                        <div class="grayBlock">
                                                                            <div class="blockTitle"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="ddItem threeLayoutContainer">
                                                    <div class="templateTitle">three images:</div>
                                                    <div class="layoutItemContainer"
                                                         data-option='{
                                                                                "pageHide":".ddPage",
                                                                                "pageShow":".pageSection",
                                                                                "layoutShow":".page-4",
                                                                                "layoutHide":".upload-section",
                                                                                "smallPreviewShow":".previewPayouts#three-col-layout-left",
                                                                                "smallPreviewHide":".previewPayouts"
                                                                           }'
                                                    >
                                                        <div class="three-layout-left">
                                                            <div class="layoutTemplate">
                                                                <div class="threeLayout clearfix">
                                                                    <div class="leftItem pull-left">
                                                                        <div class="grayBlock">
                                                                            <div class="blockTitle"></div>
                                                                        </div>
                                                                        <div class="grayBlock">
                                                                            <div class="blockTitle"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="rightItem pull-right">
                                                                        <div class="grayBlock">
                                                                            <div class="blockTitle"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="layoutItemContainer"
                                                         data-option='{
                                                                                "pageHide":".ddPage",
                                                                                "pageShow":".pageSection",
                                                                                "layoutShow":".page-5",
                                                                                "layoutHide":".upload-section",
                                                                                "smallPreviewShow":".previewPayouts#three-col-layout-right",
                                                                                "smallPreviewHide":".previewPayouts"
                                                                           }'
                                                    >
                                                        <div class="three-layout-right">
                                                            <div class="layoutTemplate">
                                                                <div class="threeLayout clearfix">
                                                                    <div class="leftItem pull-left">
                                                                        <div class="grayBlock">
                                                                            <div class="blockTitle"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="rightItem pull-right">
                                                                        <div class="grayBlock">
                                                                            <div class="blockTitle"></div>
                                                                        </div>
                                                                        <div class="grayBlock">
                                                                            <div class="blockTitle"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="ddItem barLayoutContainer">
                                                    <div class="templateTitle">bar:</div>
                                                    <div
                                                            class="layoutItemContainer"
                                                            data-option='{
                                                                                "pageHide":".ddPage",
                                                                                "pageShow":".pageSection",
                                                                                "layoutShow":".page-6",
                                                                                "layoutHide":".upload-section",
                                                                                "smallPreviewShow":".previewPayouts#bar-layout",
                                                                                "smallPreviewHide":".previewPayouts"
                                                                           }'
                                                    >
                                                        <div class="bar-layout">
                                                            <div class="layoutTemplate">
                                                                <div class="barLayout">
                                                                    <div class="layoutItem">
                                                                        <div class="grayBlock">
                                                                            <div class="blockTitle"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="step step-2">
                                <div class="clearfix"> <span class="step-number">2</span>
                                    <div class="step-detail">
                                        <h6>Add content</h6>
                                        <p> Tap the images below to add a content link to them. Images can link to content like a category, product, website or can be static. </p>
                                    </div>
                                </div>
                                <div class="img-link">
                                    <div class="image-preview-small">
                                        <div class="preview-layout">
                                            <div class="previewPayouts single-layout" id="single-layout">
                                                <div class="img-thumb" id="singleLayout" data-all-hide=".contentDdPopup" data-click-show="#singleContentDdPopup" data-attachmentpos="0"></div>
                                                <div class="contentDdPopup" id="singleContentDdPopup"></div>
                                            </div>
                                            <div class="previewPayouts two-col-layout clearfix" id="two-col-layout">
                                                <div class="img-thumb pull-left" id="twoCol-left" data-all-hide=".contentDdPopup" data-click-show="#twoColLeftContentDdPopup" data-attachmentpos="0"></div>
                                                <div class="contentDdPopup" id="twoColLeftContentDdPopup"></div>
                                                <div class="img-thumb pull-left" id="twoCol-right" data-all-hide=".contentDdPopup" data-click-show="#twoColRightContentDdPopup" data-attachmentpos="1"></div>
                                                <div class="contentDdPopup" id="twoColRightContentDdPopup"></div>
                                            </div>
                                            <div class="previewPayouts two-col-layout-short clearfix" id="two-col-layout-short">
                                                <div class="img-thumb pull-left" id="twoCol-left-short" data-all-hide=".contentDdPopup" data-click-show="#twoColLeftContentDdPopupShort" data-attachmentpos="0"></div>
                                                <div class="contentDdPopup" id="twoColLeftContentDdPopupShort"></div>
                                                <div class="img-thumb pull-left" id="twoCol-right-short" data-all-hide=".contentDdPopup" data-click-show="#twoColRightContentDdPopupShort" data-attachmentpos="1"></div>
                                                <div class="contentDdPopup" id="twoColRightContentDdPopupShort"></div>
                                            </div>
                                            <div class="previewPayouts three-col-layout-left clearfix" id="three-col-layout-left">
                                                <div class="img-thumb pull-left" id="threeCol-left-1" data-all-hide=".contentDdPopup" data-click-show="#threeColLeftContentDdPopupShort-1" data-attachmentpos="0"></div>
                                                <div class="contentDdPopup" id="threeColLeftContentDdPopupShort-1"></div>
                                                <div class="img-thumb pull-left" id="threeCol-left-2" data-all-hide=".contentDdPopup" data-click-show="#threeColLeftContentDdPopupShort-2" data-attachmentpos="1"></div>
                                                <div class="contentDdPopup" id="threeColLeftContentDdPopupShort-2"></div>
                                                <div class="img-thumb pull-left" id="threeCol-left-3" data-all-hide=".contentDdPopup" data-click-show="#threeColLeftContentDdPopupShort-3" data-attachmentpos="2"></div>
                                                <div class="contentDdPopup" id="threeColLeftContentDdPopupShort-3"></div>
                                            </div>
                                            <div class="previewPayouts three-col-layout-right clearfix" id="three-col-layout-right">
                                                <div class="img-thumb pull-left" id="threeCol-right-1" data-all-hide=".contentDdPopup" data-click-show="#threeColRightContentDdPopupShort-1" data-attachmentpos="0"></div>
                                                <div class="contentDdPopup" id="threeColRightContentDdPopupShort-1"></div>
                                                <div class="img-thumb pull-left" id="threeCol-right-2" data-all-hide=".contentDdPopup" data-click-show="#threeColRightContentDdPopupShort-2" data-attachmentpos="1"></div>
                                                <div class="contentDdPopup" id="threeColRightContentDdPopupShort-2"></div>
                                                <div class="img-thumb pull-left" id="threeCol-right-3" data-all-hide=".contentDdPopup" data-click-show="#threeColRightContentDdPopupShort-3" data-attachmentpos="2"></div>
                                                <div class="contentDdPopup" id="threeColRightContentDdPopupShort-3"></div>
                                            </div>
                                            <div class="previewPayouts bar-layout" id="bar-layout">
                                                <div class="img-thumb" id="barLayout" data-all-hide=".contentDdPopup" data-click-show="#barContentDdPopup" data-attachmentpos="0"></div>
                                                <div class="contentDdPopup" id="barContentDdPopup"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="step step-3">
                                <div class="clearfix"> <span class="step-number">3</span>
                                    <div class="step-detail">
                                        <h6>Finalize row</h6>
                                        <p> All changes wil be saved and published </p>
                                    </div>
                                </div>
                                <div class="submit-button">
                                    <button disabled id="saveDesktopLanderRow" class="btn btn-primary">Save changes</button>
                                    <button onClick="window.location.href='desktop-lander-overview.php?app_id=<?php echo $_GET['app_id']; ?>&cat_id=<?php echo $_GET['cat_id']; ?>&type=<?php echo $_GET['type']; ?>';" id="saveRow" class="btn btn-danger">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<?php include 'includes/footer.php'; ?>
</body>
</html>
