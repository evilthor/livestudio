<?php
include('../config.php');
include('../cache/memcache.php');
include('../cache/cacheOutput.php');
error_reporting(1);
// if cache enable & we have valid key
if ($cacheObject->bEnabled && !$cacheRefresh) {
    $data = $cacheObject->getData($cache_key);
    $data_decoded = json_decode($data,true);
    if(!empty($data_decoded['messagesList'])) {
        header("Content-Type: application/json");
        echo $data;
        die;
    }
}
$store = sanitise_string($_REQUEST['store']);
$store_array = explode("_",$store);
$store_alt = $store_array[0].'_global';

$sql = "SELECT id FROM tbl_apps WHERE app_store = '" . $store_alt . "'";
$result = $db->get_row($sql);

$data = array();

$sql = "SELECT * FROM tbl_messages_cat WHERE app_id = '" . $result->id . "'";
$messages_cats = $db->get_results($sql);
$messagesList = array();
if($messages_cats){
    foreach ($messages_cats as  $messages_cat){
        $messagesList[$messages_cat->title]= array();

        $sql = "SELECT * FROM tbl_messages WHERE cat_id = '" . $messages_cat->id . "'";
        $messages = $db->get_results($sql);
        foreach ($messages as  $key => $message) {
            $messagesList[$messages_cat->title][$key]["id"] = $message->id;
            $messagesList[$messages_cat->title][$key]["message"] = $message->message;
        }
    }
}
$response['messagesList'] = $messagesList;
// save response in cache
include('../cache/cacheSave.php');

header("Content-Type: application/json");
echo json_encode($response);
die;
?>