<?php
include('../config.php');
include('../cache/memcache.php');
include('../cache/cacheOutput.php');
error_reporting(0);
$siteUrl = 'https://d3qomc2rvsm9mu.cloudfront.net';
// if cache enable & we have valid key
if ($cacheObject->bEnabled && !$cacheRefresh) {
    $data = $cacheObject->getData($cache_key);
    $data_decoded = json_decode($data,true);
    if(!empty($data_decoded['data']['Men']) && !empty($data_decoded['data']['Women']) && !empty($data_decoded['data']['Kids'])) {
        header("Content-Type: application/json");
        echo $data;
        die;
    }
}
$store = sanitise_string($_REQUEST['store']);
$sql = "SELECT * FROM tbl_apps WHERE app_store = '" . $store . "'";
$result = $db->get_row($sql);

$store_array = explode("_",$store);
$store_alt = $store_array[0].'_global';

 // if no app exists, show data from global apps
 $sql_global = "SELECT * FROM tbl_apps WHERE app_store = '" . $store_alt . "'";
 $res_global = $db->get_row($sql_global);
 $global_id = $res_global->id;
if ($result) {
    $app_id = $result->id;
}else{
    $app_id = '';
}
$sql_cats = "SELECT * FROM tbl_categories WHERE app_id = '" . $global_id . "'";
$res_cats = $db->get_results($sql_cats);

if(!$app_id){
   $alt_app_id = $global_id;
}else{
   $alt_app_id = $app_id;
}

if($app_id==''){
  $global_check = "AND tbl_cat_rows.is_global=1";
  $row_ids_arr='';
  $deleted_check = "";
  $apps_check = " AND tbl_cat_rows.app_id = ".$global_id." ";
}else{
    $sql = "SELECT GROUP_CONCAT(row_id) as row_ids FROM tbl_delete_data WHERE 
                                        app_id ='".$alt_app_id."' AND 
                                        row_type='categories_row'";
    $skip_data = $db->get_row($sql);
    $cat_row_ids_arr = $skip_data->row_ids;
    if($cat_row_ids_arr){
      $row_ids_temp_arr = explode(',', $cat_row_ids_arr);
      $row_ids_arr = "'" . implode ( "', '", $row_ids_temp_arr ) . "'";
      $row_ids_arr = rtrim($row_ids_arr,"'");
      $row_ids_arr = ltrim($row_ids_arr,"'");
    }else{
      $row_ids_arr='';
    }
    $global_check = "";
    $deleted_check = "AND tbl_cat_rows.id NOT IN ('".$row_ids_arr."')";

    $apps_check = " AND tbl_cat_rows.app_id IN(".$app_id.", ".$global_id." ) ";
}

$sql_rows= "SELECT a.title,tbl_cat_rows.* FROM tbl_categories a JOIN tbl_cat_rows ON a.id=tbl_cat_rows.merchandise_id  
                                                            JOIN tbl_sort_rows ON tbl_cat_rows.id=tbl_sort_rows.row_id 
                                                            where tbl_cat_rows.merchandise_id = a.id 
                                                            AND tbl_cat_rows.show_in_app = '1' $global_check $deleted_check $apps_check 
                                                            AND tbl_sort_rows.id IN ( 
                                                                                      SELECT id FROM tbl_sort_rows WHERE tbl_sort_rows.row_type='search_category' 
                                                                                      AND tbl_sort_rows.app_id = '".$alt_app_id."'
                                                                                    ) 
                                                            ORDER BY tbl_sort_rows.sort_order ASC";

$rows = $db->get_results($sql_rows);
if ($rows) {
    $data = array();
    $data['Men'] = array();
    $data['Women'] = array();
    $data['Kids'] = array();
    //$response = array('success'=>1,'message'=>'Record found.');
    $i = 0;
    foreach ($rows as $row) {

        $data[$row->title][$i]['layoutType'] = $row->layout;
        $data[$row->title][$i]['images'] = array();
        $images = json_decode($row->images);
        $attachments = json_decode($row->attachment);
        $c = 0;
        foreach ($images as $image) {
            $data[$row->title][$i]['images'][$c]['url'] = $siteUrl . trim($image, '.');
            $attachment = $attachments[$c];
            if ($attachment[1] == 1) {
                $data[$row->title][$i]['images'][$c]['id'] = $attachment[2];
                $data[$row->title][$i]['images'][$c]['attachmentType'] = 'category';
                $data[$row->title][$i]['images'][$c]['title'] = $attachment[3];
                $data[$row->title][$i]['images'][$c]['extras'] = "";
                if (isset($attachment[4])) {
                    $data[$row->title][$i]['images'][$c]['extras'] = '&manufacturer=' . $attachment[4];
                }
            }elseif ($attachment[1] == 5) {
                $data[$row->title][$i]['images'][$c]['id'] = 2;
                $data[$row->title][$i]['images'][$c]['attachmentType'] = 'category';
                $data[$row->title][$i]['images'][$c]['title'] = $attachment[3];
                $data[$row->title][$i]['images'][$c]['extras'] = '&manufacturer='.$attachment[2];
            }elseif ($attachment[1] == 6) {
                if($attachment[2]=='sk' || $attachment[2]=='sm' || $attachment[2]=='sw'){

                    $data[$row->title][$i]['images'][$c]['id'] = $attachment[4];
                    $data[$row->title][$i]['images'][$c]['attachmentType'] = 'category';

                    if($attachment[2]=='sk'){
                        $data[$row->title][$i]['images'][$c]['title'] = 'Kids';
                    }elseif($attachment[2]=='sm'){
                        $data[$row->title][$i]['images'][$c]['title'] = 'Men';
                    }elseif($attachment[2]=='sw'){
                        $data[$row->title][$i]['images'][$c]['title'] = 'Women';
                    }

                    $data[$row->title][$i]['images'][$c]['extras'] = "";

                }else {
                    $sql_lander = "SELECT title FROM tbl_lander_pages WHERE id='".$attachment[2]."'";
                    $res_lander = $db->get_row($sql_lander);
                    $data[$row->title][$i]['images'][$c]['id'] = $attachment[2];
                    $data[$row->title][$i]['images'][$c]['attachmentType'] = 'lander';
                    $data[$row->title][$i]['images'][$c]['title'] = $res_lander->title;
                    $data[$row->title][$i]['images'][$c]['extras'] = "";
                }
            }
            $c++;
        }
        $i++;
    }
    $data['Men'] = array_values($data['Men']);
    $data['Women'] = array_values($data['Women']);
    $data['Kids'] = array_values($data['Kids']);
    $response['data'] = $data;
}

//fetching the brands
$response['brands'] = array();
$brands = array();
$lang = explode('_',$store);
$lang = $lang[0];
$sql_brands = "SELECT * FROM tbl_selected_brands WHERE brand_lang = '" . $lang . "'";
$res_brands = $db->get_results($sql_brands);
if($res_brands){
    foreach($res_brands as $res_brand_single){
        $btype = ucfirst($res_brand_single->brand_type);
        $brands[$btype][]= array("id"=>$res_brand_single->brand_id, "name"=>$res_brand_single->brand_name);
    }
}

$response['brands'] = $brands;

// save response in cache
include('../cache/cacheSave.php');

header("Content-Type: application/json");
echo json_encode($response);
die;
?>