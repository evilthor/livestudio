<?php
include('../config.php');
include('../cache/memcache.php');
include('../cache/cacheOutput.php');
error_reporting(0);
$siteUrl = 'https://d3qomc2rvsm9mu.cloudfront.net';
// if cache enable & we have valid key
if ($cacheObject->bEnabled && !$cacheRefresh) {
    $data = $cacheObject->getData($cache_key);
    $data_decoded = json_decode($data,true);
    if(!empty($data_decoded['homeData']) && !empty($data_decoded['categoriesTree'])) {
        header("Content-Type: application/json");
        echo $data;
        die;
    }
}
$store = sanitise_string($_REQUEST['store']);
$sql = "SELECT * FROM tbl_apps WHERE app_store = '" . $store . "'";
$result = $db->get_row($sql);

$store_array = explode("_",$store);
$store_alt = $store_array[0].'_global';
if ($result){
      $app_id = $result->id;
 }else{
      $app_id = '';
 }
     
$sql_global = "SELECT id FROM tbl_apps WHERE app_store = '" . $store_alt . "'";
$res_global = $db->get_row($sql_global);
$global_id = $res_global->id;

$data = array();
$i = 0;
$sql_block = "SELECT tbl_block.*, tbl_static_block_status.is_default FROM
                     tbl_block JOIN tbl_static_block_status ON 
                     tbl_block.id=tbl_static_block_status.block_id AND 
                     tbl_block.show_in_app = '1' AND 
                     tbl_static_block_status.is_default='1' AND  
                     tbl_static_block_status.app_id = '".$app_id."'";

 $res_block = $db->get_row($sql_block);
 if(!$res_block){
     $sql_block = "SELECT tbl_block.*, tbl_static_block_status.is_default FROM
                     tbl_block JOIN tbl_static_block_status ON 
                     tbl_block.id=tbl_static_block_status.block_id AND 
                     tbl_block.show_in_app = '1' AND 
                     tbl_static_block_status.is_default='1' AND  
                     tbl_static_block_status.app_id = '".$global_id."'";

      $res_block = $db->get_row($sql_block);
 }
if ($res_block) {
    $data[$i]['layoutType'] = 'block';
    $data[$i]['images'] = array();
    $data[$i]['images'][0]['url'] = $siteUrl . trim($res_block->image_url, '.');
    $data[$i]['images'][0]['id'] = '';
    $data[$i]['images'][0]['attachmentType'] = '';
    $data[$i]['images'][0]['title'] = '';
    $i++;
    if ($res_block->has_text && $res_block->welcome_text != '') {
        $data[$i]['layoutType'] = 'text';
        $data[$i]['images'] = array();
        $data[$i]['images'][0]['url'] = $res_block->welcome_text;
        $data[$i]['images'][0]['id'] = '';
        $data[$i]['images'][0]['attachmentType'] = '';
        $data[$i]['images'][0]['title'] = '';
        $i++;
    }
}

$sql_merch = "SELECT tbl_merchandise.*, tbl_merchandise_status.status FROM
                     tbl_merchandise JOIN tbl_merchandise_status ON 
                     tbl_merchandise.id=tbl_merchandise_status.merchandise_id AND 
                     tbl_merchandise.show_in_app = '1' AND 
                     tbl_merchandise_status.status='1' AND  
                     tbl_merchandise_status.app_id = '".$app_id."'";

 $res_merch = $db->get_row($sql_merch);
 if(!$res_merch){

      $sql_merch = "SELECT tbl_merchandise.*, tbl_merchandise_status.status FROM
                     tbl_merchandise JOIN tbl_merchandise_status ON 
                     tbl_merchandise.id=tbl_merchandise_status.merchandise_id AND 
                     tbl_merchandise.show_in_app = '1' AND 
                     tbl_merchandise_status.status='1' AND  
                     tbl_merchandise_status.app_id = '".$global_id."'";

      $res_merch = $db->get_row($sql_merch);
 }
if($res_merch){
      $sql = "SELECT GROUP_CONCAT(row_id) as row_ids FROM tbl_delete_data 
                                    WHERE app_id ='".$res_merch->app_id."' AND 
                                    row_type='merchandise_row'";
      $skip_data = $db->get_row($sql);
      $row_ids_arr_temp = $skip_data->row_ids;

      if($row_ids_arr_temp){
           $row_ids_temp_arr = explode(',', $row_ids_arr_temp);
           $mer_row_ids_arr = "'" . implode ( "', '", $row_ids_temp_arr ) . "'";
           $mer_row_ids_arr = rtrim($mer_row_ids_arr,"'");
           $mer_row_ids_arr = ltrim($mer_row_ids_arr,"'");
      }else{
           $mer_row_ids_arr='';
      }

      if(!$app_id){
           $alt_app_id = $res_merch->app_id;
      }else{
           $alt_app_id = $app_id;
      }
        $sql_rows = "SELECT tbl_rows.* FROM tbl_rows JOIN tbl_sort_rows ON tbl_rows.id=tbl_sort_rows.row_id 
                                                        where tbl_rows.merchandise_id = '".$res_merch->id."' 
                                                        AND tbl_rows.show_in_app = '1' 
                                                        AND tbl_rows.id NOT IN ('".$mer_row_ids_arr."') 
                                                        AND tbl_rows.app_id IN(".$alt_app_id.", ".$global_id.") 
                                                        AND tbl_sort_rows.id IN ( 
                                                                                    SELECT id FROM tbl_sort_rows WHERE tbl_sort_rows.row_type='merchandise' 
                                                                                    AND tbl_sort_rows.app_id = '".$alt_app_id."'
                                                                                ) 
                                                        ORDER BY tbl_sort_rows.sort_order ASC";

      $rows = $db->get_results($sql_rows);
}
if ($rows) {
    $skip_layouts = array("singleShort","doubleShort");
    foreach ($rows as $row) {
        if(in_array($row->layout, $skip_layouts)){
            continue;
        }
        $data[$i]['layoutType'] = $row->layout;
        $data[$i]['images'] = array();
        $images = json_decode($row->images);
        $attachments = json_decode($row->attachment);
        $c = 0;
        foreach ($images as $image) {
            $data[$i]['images'][$c]['url'] = $siteUrl . trim($image, '.');
            $attachment = $attachments[$c];
            if ($attachment[1] == 1) {
                $data[$i]['images'][$c]['id'] = $attachment[2].',,';
                $data[$i]['images'][$c]['attachmentType'] = 'category';
                $data[$i]['images'][$c]['title'] = $attachment[3];
            } elseif ($attachment[1] == 2) {
                $data[$i]['images'][$c]['id'] = $attachment[2];
                $data[$i]['images'][$c]['attachmentType'] = 'product';
                $data[$i]['images'][$c]['title'] = $attachment[3];
            } elseif ($attachment[1] == 3) {
                $data[$i]['images'][$c]['id'] = $attachment[2];
                $data[$i]['images'][$c]['attachmentType'] = 'url';
                $data[$i]['images'][$c]['title'] = $attachment[3];
            } elseif ($attachment[1] == 4) {
                $data[$i]['images'][$c]['id'] = '';
                $data[$i]['images'][$c]['attachmentType'] = 'static';
                $data[$i]['images'][$c]['title'] = $attachment[3];
            } elseif ($attachment[1] == 5) {
                $data[$i]['images'][$c]['id'] = '2,,'.$attachment[2];
                $data[$i]['images'][$c]['attachmentType'] = 'category';
                $data[$i]['images'][$c]['title'] = $attachment[3];
            }elseif ($attachment[1] == 6) {
                if($attachment[2]=='sk' || $attachment[2]=='sm' || $attachment[2]=='sw'){

                    $data[$i]['images'][$c]['id'] = $attachment[4];
                    $data[$i]['images'][$c]['attachmentType'] = 'category';

                    if($attachment[2]=='sk'){
                        $data[$i]['images'][$c]['title'] = 'Kids';
                    }elseif($attachment[2]=='sm'){
                        $data[$i]['images'][$c]['title'] = 'Men';
                    }elseif($attachment[2]=='sw'){
                        $data[$i]['images'][$c]['title'] = 'Women';
                    }
                    $data[$i]['images'][$c]['extras'] = "";
                }else{
                    $id = $attachment[2];
                    $sqlLander = "SELECT cat_id,cat_name FROM tbl_lander_pages WHERE id = '".$id."'";
                    $resultLander = $db->get_row($sqlLander);
                    $data[$i]['images'][$c]['attachmentType'] = 'category';
                    $data[$i]['images'][$c]['id'] = $resultLander->cat_id;
                    $data[$i]['images'][$c]['title'] = $resultLander->cat_name;
                }
            }
            $c++;
        }
        $i++;
    }
    $response['homeData'] = $data;
}

$store_arr = explode('_', $store);
$locale = trim($store_arr[0]);
$categories_tree = file_get_contents('../tree_' . $locale . '.json');
$response['categoriesTree'] = json_decode($categories_tree, true);

// save response in cache
include('../cache/cacheSave.php');

header("Content-Type: application/json");
echo json_encode($response);
die;
?>