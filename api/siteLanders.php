<?php
include('../config.php');
include('../cache/memcache.php');
include('../cache/cacheOutput.php');
//$siteUrl = 'https://95361-405780-raikfcquaxqncofqfm.stackpathdns.com';
error_reporting(0);
// if cache enable & we have valid key
if ($cacheObject->bEnabled && !$cacheRefresh) {
    $data = $cacheObject->getData($cache_key);
    $data_decoded = json_decode($data,true);
    if(!empty($data_decoded['landerData']['Slider']) && !empty($data_decoded['landerData']['Categories'])) {
        header("Content-Type: application/json");
        echo $data;
        die;
    }
}
$store = sanitise_string($_REQUEST['store']);
$lander = sanitise_string(strtolower($_REQUEST['lander']));

$store_array = explode("_",$store);
$store_alt = $store_array[0].'_global';

$sql = "SELECT * FROM tbl_apps WHERE app_store = '" . $store_alt . "'";
$result = $db->get_row($sql);

$data = array();

if($result) {
    $app_id = $result->id;

    $sql_cat = "SELECT id FROM tbl_categories WHERE app_id = '" . $app_id . "' AND title='" . ucfirst($lander) . "'";
    $res_cat = $db->get_row($sql_cat);
    $cat_id = $res_cat->id;

    $sql_slider_data = "SELECT * FROM tbl_desktop_lander_slider WHERE cat_id = '" . $cat_id . "' AND app_id = '" . $app_id . "' ORDER BY row_order ASC";
    $res_slider_data = $db->get_results($sql_slider_data);

    if ($res_slider_data) {
        $i = 0;
        foreach ($res_slider_data as $res_slider) {
            $attachments = json_decode($res_slider->attachment);
            $image = json_decode($res_slider->images);
            $image_arr = explode(".",$image[0]);
            $data['Slider'][$i]['url'] = $siteUrl . trim($image[0], '.');
            $data['Slider'][$i]['webp_url'] = $siteUrl . $image_arr[1].'.webp';
            if($attachments[0][1]==1){
                $data['Slider'][$i]['attachment_type'] = 'category';
            }
            if($attachments[0][1]==5){
                $data['Slider'][$i]['attachment_type'] = 'brand';
            }
            $data['Slider'][$i]['attachment_url'] = $attachments[0][2];
            $data['Slider'][$i]['attachment_title'] = $attachments[0][3];
            if($attachments[0][4]!=''){
                $data['Slider'][$i]['brand_url'] = $attachments[0][4];
            }
            $i++;
        }

    }

    $sql_cat_data = "SELECT * FROM tbl_desktop_lander_rows WHERE cat_id = '" . $cat_id . "' AND app_id = '" . $app_id . "'  ORDER BY row_order ASC";
    $res_cat_data = $db->get_results($sql_cat_data);

    if ($res_cat_data) {
        $i = 0;
        foreach ($res_cat_data as $res_cat) {
            $attachments = json_decode($res_cat->attachment);
            $image = json_decode($res_cat->images);
            $image_arr = explode(".",$image[0]);
            $data['Categories'][$i]['url'] = $siteUrl . trim($image[0], '.');
            $data['Categories'][$i]['webp_url'] = $siteUrl . $image_arr[1].'.webp';
            if($attachments[0][1]==1){
                $data['Categories'][$i]['attachment_type'] = 'category';
            }
            if($attachments[0][1]==5){
                $data['Categories'][$i]['attachment_type'] = 'brand';
            }
            $data['Categories'][$i]['attachment_url'] = $attachments[0][2];
            $data['Categories'][$i]['attachment_title'] = $attachments[0][3];
            if($attachments[0][4]!=''){
                $data['Categories'][$i]['brand_url'] = $attachments[0][4];
            }
            $i++;
        }

    }
}
$response['landerData'] = $data;
// save response in cache
include('../cache/cacheSave.php');

header("Content-Type: application/json");
echo json_encode($response);
die;
?>