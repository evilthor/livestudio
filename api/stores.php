<?php
include('../config.php');
include('../cache/memcache.php');
include('../cache/cacheOutput.php');
$siteUrl = 'https://d3qomc2rvsm9mu.cloudfront.net';

// if cache enable & we have valid key
if ($cacheObject->bEnabled && !$cacheRefresh) {
    $data = $cacheObject->getData($cache_key);
    $data_decoded = json_decode($data,true);
    if(!empty($data_decoded['Stores'])) {
        header("Content-Type: application/json");
        echo $data;
        die;
    }
}

$sql = "SELECT id,store_lang,country_flag,country_name as store_name,country_name_arabic as store_name_arabic,store_code,status,country_name_analytics,currency_analytics,fb_content_id_en,fb_content_id_ar FROM tbl_stores WHERE status=1";
$result = $db->get_results($sql);
if($result) {
    $data = array();
    foreach ($result as $key => $value) {
         $data[$key]['id'] = $value->id;
         if($value->store_lang == 'en'){
             $data[$key]['store_lang'] = 1;
         }elseif($value->store_lang == 'ar'){
             $data[$key]['store_lang'] = 2;
         }else{
             $data[$key]['store_lang'] = 3;
         }
         $data[$key]['country_flag'] = $siteUrl."/images/countries_flags/".$value->country_flag;
         $data[$key]['store_name'] = $value->store_name;
         $data[$key]['store_name_arabic'] = $value->store_name_arabic;
         $data[$key]['store_code'] = $value->store_code;
         $data[$key]['analytics_country_name'] = $value->country_name_analytics;
         $data[$key]['analytics_currency'] = $value->currency_analytics;
         $data[$key]['fb_content_id_en'] = $value->fb_content_id_en;
         $data[$key]['fb_content_id_ar'] = $value->fb_content_id_ar;
    }
    $response['Stores'] = $data;
}
// save response in cache
include('../cache/cacheSave.php');

header("Content-Type: application/json");
echo json_encode($response);
die;
?>