<?php
include('../config.php');
include('../cache/memcache.php');
include('../cache/cacheOutput.php');

$store = trim($_REQUEST['store']);
$sql = "SELECT * FROM tbl_apps WHERE app_store = '" . $store . "'";
$result = $db->get_row($sql);

if(!$result){
     // if no app exists, show data from global apps
     $store_array = explode("_",$store);
     $store_alt = $store_array[0].'_global';

     $sql = "SELECT * FROM tbl_apps WHERE app_store = '" . $store_alt . "'";
     $result = $db->get_row($sql);
}

$data = array();
if ($result) {
    $app_id = $result->id;
    $i = 0;
    $sql_block = "SELECT * FROM tbl_block WHERE is_default = '1' AND app_id = '" . $app_id . "'";
    $res_block = $db->get_row($sql_block);
    if ($res_block) {
        $data[$i]['layoutType'] = 'block';
        $data[$i]['images'] = array();
        $data[$i]['images'][0]['url'] = $siteUrl . trim($res_block->image_url, '.');
        $data[$i]['images'][0]['id'] = '';
        $data[$i]['images'][0]['attachmentType'] = '';
        $data[$i]['images'][0]['title'] = '';
        $i++;
        if ($res_block->has_text && $res_block->welcome_text != '') {
            $data[$i]['layoutType'] = 'text';
            $data[$i]['images'] = array();
            $data[$i]['images'][0]['url'] = $res_block->welcome_text;
            $data[$i]['images'][0]['id'] = '';
            $data[$i]['images'][0]['attachmentType'] = '';
            $data[$i]['images'][0]['title'] = '';
            $i++;
        }
    }

    $sql_rows = "SELECT a.title,b.* FROM tbl_merchandise a JOIN tbl_rows b ON a.id = b.merchandise_id WHERE a.status='1' AND a.app_id = '" . $app_id . "'  ORDER BY b.row_order ASC";
    $rows = $db->get_results($sql_rows);
    if ($rows) {
        //$response = array('success'=>1,'message'=>'Record found.');
        foreach ($rows as $row) {
            $data[$i]['layoutType'] = $row->layout;
            $data[$i]['images'] = array();
            $images = json_decode($row->images);
            $attachments = json_decode($row->attachment);
            $c = 0;
            foreach ($images as $image) {
                $data[$i]['images'][$c]['url'] = $siteUrl . trim($image, '.');
                $attachment = $attachments[$c];
                if ($attachment[1] == 1) {
                    $data[$i]['images'][$c]['id'] = $attachment[2];
                    $data[$i]['images'][$c]['attachmentType'] = 'category';
                    $data[$i]['images'][$c]['title'] = $attachment[3];
                    $data[$i]['images'][$c]['extras'] = "";
                    if(isset($attachment[4])) {
                        $data[$i]['images'][$c]['extras'] = '&manufacturer='.$attachment[4];
                    }
                } elseif ($attachment[1] == 2) {
                    $data[$i]['images'][$c]['id'] = $attachment[2];
                    $data[$i]['images'][$c]['attachmentType'] = 'product';
                    $data[$i]['images'][$c]['title'] = $attachment[3];
                    $data[$i]['images'][$c]['extras'] = "";
                } elseif ($attachment[1] == 3) {
                    $data[$i]['images'][$c]['id'] = $attachment[2];
                    $data[$i]['images'][$c]['attachmentType'] = 'url';
                    $data[$i]['images'][$c]['title'] = $attachment[3];
                    $data[$i]['images'][$c]['extras'] = "";
                } elseif ($attachment[1] == 4) {
                    $data[$i]['images'][$c]['id'] = '';
                    $data[$i]['images'][$c]['attachmentType'] = 'static';
                    $data[$i]['images'][$c]['title'] = $attachment[3];
                    $data[$i]['images'][$c]['extras'] = "";
                } elseif ($attachment[1] == 5) {
                    $data[$i]['images'][$c]['id'] = '2';
                    $data[$i]['images'][$c]['attachmentType'] = 'category';
                    $data[$i]['images'][$c]['title'] = $attachment[3];
                    $data[$i]['images'][$c]['extras'] = '&manufacturer='.$attachment[2];
                }elseif ($attachment[1] == 6) {
                    $id = $attachment[2];
                    $sqlLander = "SELECT cat_id,cat_name FROM tbl_lander_pages WHERE id = '".$id."'";
                    $resultLander = $db->get_row($sqlLander);
                    $data[$i]['images'][$c]['attachmentType'] = 'category';
                    $data[$i]['images'][$c]['id'] = $resultLander->cat_id;
                    $data[$i]['images'][$c]['title'] = $resultLander->cat_name;
                    $data[$i]['images'][$c]['extras'] = "";
                }

                $c++;
            }
            $i++;
        }
        $response['homeData'] = $data;
    }
}
$store_arr = explode('_', $store);
$locale = trim($store_arr[0]);
$categories_tree = file_get_contents('../tree_' . $locale . '.json');
$response['categoriesTree'] = json_decode($categories_tree, true);

// save response in cache
include('../cache/cacheSave.php');

header("Content-Type: application/json");
echo json_encode($response);
die;
?>