<?php
include('../config.php');
include('../cache/memcache.php');
include('../cache/cacheOutput.php');
error_reporting(0);
$siteUrl = 'https://d3qomc2rvsm9mu.cloudfront.net';
// if cache enable & we have valid key
if ($cacheObject->bEnabled && !$cacheRefresh) {
    $data = $cacheObject->getData($cache_key);
    $data_decoded = json_decode($data,true);
    if(!empty($data_decoded['homeData'])) {
        header("Content-Type: application/json");
        echo $data;
        die;
    }
}
$id = (int)$_REQUEST['id'];
if ($id) {
    $i = 0;
    $sql_rows = "SELECT a.title,a.cat_id,b.* FROM tbl_lander_pages a JOIN tbl_lander_rows b ON a.id = b.lander_id WHERE a.id='".$id."' ORDER BY b.row_order ASC";
    $rows = $db->get_results($sql_rows);
    if ($rows) {
        $data = array();
        //$response = array('success'=>1,'message'=>'Record found.');
        foreach ($rows as $row) {
            $data[$i]['layoutType'] = $row->layout;
            $data[$i]['images'] = array();
            $images = json_decode($row->images);
            $attachments = json_decode($row->attachment);
            $c = 0;
            foreach ($images as $image) {
                $data[$i]['images'][$c]['url'] = $siteUrl . trim($image, '.');
                $attachment = $attachments[$c];
                if ($attachment[1] == 1) {
                    $data[$i]['images'][$c]['id'] = $attachment[2];
                    $data[$i]['images'][$c]['attachmentType'] = 'category';
                    $data[$i]['images'][$c]['title'] = $attachment[3];
                    $data[$i]['images'][$c]['extras'] = "";
                    if(isset($attachment[4])) {
                        $data[$i]['images'][$c]['extras'] = '&manufacturer='.$attachment[4];
                    }
                } elseif ($attachment[1] == 2) {
                    $data[$i]['images'][$c]['id'] = $attachment[2];
                    $data[$i]['images'][$c]['attachmentType'] = 'product';
                    $data[$i]['images'][$c]['title'] = $attachment[3];
                    $data[$i]['images'][$c]['extras'] = "";
                    if(isset($attachment[4])) {
                        $data[$i]['images'][$c]['extras'] = '&manufacturer='.$attachment[4];
                    }
                } elseif ($attachment[1] == 3) {
                    $data[$i]['images'][$c]['id'] = $attachment[2];
                    $data[$i]['images'][$c]['attachmentType'] = 'url';
                    $data[$i]['images'][$c]['title'] = $attachment[3];
                    $data[$i]['images'][$c]['extras'] = "";
                    if(isset($attachment[4])) {
                        $data[$i]['images'][$c]['extras'] = '&manufacturer='.$attachment[4];
                    }
                } elseif ($attachment[1] == 4) {
                    $data[$i]['images'][$c]['id'] = '';
                    $data[$i]['images'][$c]['attachmentType'] = 'static';
                    $data[$i]['images'][$c]['title'] = $attachment[3];
                    $data[$i]['images'][$c]['extras'] = "";
                    if(isset($attachment[4])) {
                        $data[$i]['images'][$c]['extras'] = '&manufacturer='.$attachment[4];
                    }
                } elseif ($attachment[1] == 5) {
                    $data[$i]['images'][$c]['id'] = '2';
                    $data[$i]['images'][$c]['attachmentType'] = 'category';
                    $data[$i]['images'][$c]['title'] = $attachment[3];
                    $data[$i]['images'][$c]['extras'] = '&manufacturer='.$attachment[2];
                }elseif ($attachment[1] == 6) {
                    if($attachment[2]=='sk' || $attachment[2]=='sm' || $attachment[2]=='sw'){

                        if($attachment[2]=='sk'){
                            $data[$i]['images'][$c]['id'] = '3';
                            $data[$i]['images'][$c]['attachmentType'] = 'oldStaticLander';
                            $data[$i]['images'][$c]['title'] = 'Kids';
                        }elseif($attachment[2]=='sm'){
                            $data[$i]['images'][$c]['id'] = '2';
                            $data[$i]['images'][$c]['attachmentType'] = 'oldStaticLander';
                            $data[$i]['images'][$c]['title'] = 'Men';
                        }elseif($attachment[2]=='sw'){
                            $data[$i]['images'][$c]['id'] = '1';
                            $data[$i]['images'][$c]['attachmentType'] = 'oldStaticLander';
                            $data[$i]['images'][$c]['title'] = 'Women';
                        }

                        $data[$i]['images'][$c]['extras'] = "";

                    }else{
                        $sql_lander = "SELECT title FROM tbl_lander_pages WHERE id='".$attachment[2]."'";
                        $res_lander = $db->get_row($sql_lander);
                        $data[$i]['images'][$c]['attachmentType'] = 'lander';
                        $data[$i]['images'][$c]['id'] = $attachment[2];
                        $data[$i]['images'][$c]['title'] = $res_lander->title;
                        $data[$i]['images'][$c]['extras'] = "";

                    }
                    //$data[$i]['images'][$c]['attachmentType'] = 'lander';
                    //$data[$i]['images'][$c]['id'] = $attachment[2];
                    //$data[$i]['images'][$c]['title'] = $attachment[3];
                }
                $c++;
            }
            $i++;
        }
        $response['categoryId'] = $rows[0]->cat_id;
        $response['landerTitle'] = $rows[0]->title;
        $response['homeData'] = $data;
    }
}

// save response in cache
include('../cache/cacheSave.php');

header("Content-Type: application/json");
echo json_encode($response);
die;
?>