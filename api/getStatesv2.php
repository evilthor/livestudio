<?php
include('../config.php');
include('../cache/memcache.php');
include('../cache/cacheOutput.php');
error_reporting(0);
// if cache enable & we have valid key
if ($cacheObject->bEnabled && !$cacheRefresh) {
    $data = $cacheObject->getData($cache_key);
    $data_decoded = json_decode($data,true);
    if(!empty($data_decoded['countries'])) {
        header("Content-Type: application/json");
        echo $data;
        die;
    }
}
$store = sanitise_string($_REQUEST['store']);
$store_arr = explode('_', $store);
/*$locale = trim($store_arr[0]);
$response_raw = file_get_contents('../states_' . $locale . '.json');
$response_raw = json_decode($response_raw,true);
$countries = array();
foreach ($response_raw['countries'] as $key => $country){
    $sql = "SELECT country_code FROM tbl_countries WHERE status=1 AND short_code = '".$country['short_name']."' LIMIT 1";
    $country_code = $db->get_row($sql);
    $countries[$key]['name'] = $country['name'];
    $countries[$key]['short_name'] = $country['short_name'];
    $countries[$key]['country_code'] = $country_code->country_code;
    $countries[$key]['states'] = $country['states'];
}
$response['status']= $response_raw['status'];
$response['countries'] = $countries;
// save response in cache
include('../cache/cacheSave.php');
header("Content-Type: application/json");
echo json_encode($response);
die;*/

$sql = "SELECT id,short_name,country_name,dialing_code FROM tbl_states_countries WHERE store_lang='".$store_arr[0]."'";
$result = $db->get_results($sql);
if($result) {
    $data = array();
    foreach ($result as $key => $value) {
        $data[$key]['name'] = $value->country_name;
        $data[$key]['short_name'] = $value->short_name;
        $data[$key]['country_code'] = $value->dialing_code;

        $sql_states = "SELECT state_id,state_name FROM tbl_states WHERE cid='".$value->id."'";
        $res_states = $db->get_results($sql_states);
        $data[$key]['states']=array();
        if($res_states){
            foreach ($res_states as $sk => $state_value) {
                $data[$key]['states'][$sk]['name'] = $state_value->state_name;
                $data[$key]['states'][$sk]['id'] = $state_value->state_id;
            }
        }
    }
    $response['status'] = "1";
    $response['countries'] = $data;
}
// save response in cache
include('../cache/cacheSave.php');
header("Content-Type: application/json");
echo json_encode($response);
die;
?>