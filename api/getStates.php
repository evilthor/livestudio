<?php
include('../config.php');
include('../cache/memcache.php');
include('../cache/cacheOutput.php');
error_reporting(0);
// if cache enable & we have valid key
if ($cacheObject->bEnabled && !$cacheRefresh) {
    $data = $cacheObject->getData($cache_key);
    $data_decoded = json_decode($data,true);
    if(!empty($data_decoded['countries'])) {
        header("Content-Type: application/json");
        echo $data;
        die;
    }
}
$store = sanitise_string($_REQUEST['store']);
$store_arr = explode('_', $store);
/*$locale = trim($store_arr[0]);
$response = file_get_contents('../states_' . $locale . '.json');
$response = json_decode($response,true);
// save response in cache
include('../cache/cacheSave.php');
header("Content-Type: application/json");
echo json_encode($response);
die;*/

$sql = "SELECT id,short_name,country_name FROM tbl_states_countries WHERE store_lang='".$store_arr[0]."'";
$result = $db->get_results($sql);
if($result) {
    $data = array();
    foreach ($result as $key => $value) {
        $data[$key]['name'] = $value->country_name;
        $data[$key]['short_name'] = $value->short_name;

        $sql_states = "SELECT state_id,state_name FROM tbl_states WHERE cid='".$value->id."'";
        $res_states = $db->get_results($sql_states);
        $data[$key]['states']=array();
        if($res_states){
            foreach ($res_states as $sk => $state_value) {
                $data[$key]['states'][$sk]['name'] = $state_value->state_name;
                $data[$key]['states'][$sk]['id'] = $state_value->state_id;
            }
        }
    }
    $response['status'] = "1";
    $response['countries'] = $data;
}
// save response in cache
include('../cache/cacheSave.php');
header("Content-Type: application/json");
echo json_encode($response);
die;
?>