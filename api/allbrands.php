<?php
include('../cache/memcache.php');
include('../cache/cacheOutput.php');

$store = sanitise_string($_REQUEST['store']);

//fetching the brands
$response['brands'] = array();
$lang = explode('_',$store);
$lang = $lang[0];
$file = '../brands_'.trim($lang).'.json';
$result = file_get_contents($file);
$result = json_decode($result, true);
$response['brands'] = $result;

// save response in cache
include('../cache/cacheSave.php');

header("Content-Type: application/json");
echo json_encode($response);
die;
?>