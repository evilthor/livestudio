-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 02, 2017 at 07:16 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mobilestudio`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notifications`
--

CREATE TABLE `tbl_notifications` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_scheduled` enum('0','1') NOT NULL DEFAULT '0',
  `schedule_date` date NOT NULL,
  `schedule_time` time NOT NULL,
  `apps` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_notifications`
--

INSERT INTO `tbl_notifications` (`id`, `title`, `message`, `date_time`, `is_scheduled`, `schedule_date`, `schedule_time`, `apps`) VALUES
(1, 'test', 'We will wish you Eid Adha Mubarak filled with true happiness, love, and joy. Lots of love from @Elabelz family ', '2017-02-01 05:00:00', '0', '0000-00-00', '00:00:00', '0'),
(2, 'test1', 'We will wish you Eid Adha Mubarak filled with true happiness, love, and joy. Lots of love from @Elabelz family ', '2017-02-04 19:30:00', '0', '0000-00-00', '00:00:00', '0'),
(3, 'test3', 'We will wish you Eid Adha Mubarak filled with true happiness, love, and joy. Lots of love from @Elabelz family ', '2017-02-09 05:43:26', '1', '2017-02-28', '00:00:00', ''),
(4, 'test4', 'We will wish you Eid Adha Mubarak filled with true happiness, love, and joy. Lots of love from @Elabelz family ', '2017-02-09 05:43:26', '1', '2017-03-10', '12:00:00', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_notifications`
--
ALTER TABLE `tbl_notifications`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_notifications`
--
ALTER TABLE `tbl_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
