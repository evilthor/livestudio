-- Click&Go MySQL Manager 4.2.5 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `tbl_cron_status`;
CREATE TABLE `tbl_cron_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cron_running` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_cron_status` (`id`, `cron_running`) VALUES
(1,	'0');

-- 2017-06-14 15:46:31
