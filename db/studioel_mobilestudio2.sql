-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 08, 2017 at 08:23 AM
-- Server version: 5.6.35
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `studioel_mobilestudio2`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_apps`
--

CREATE TABLE `tbl_apps` (
  `id` int(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `app_name` varchar(40) NOT NULL,
  `app_desc` text NOT NULL,
  `app_store` varchar(100) NOT NULL,
  `app_key` varchar(150) NOT NULL,
  `app_and_key` varchar(150) NOT NULL,
  `app_ios_key` varchar(150) NOT NULL,
  `dev_ssl` varchar(100) NOT NULL,
  `dev_passphrase` varchar(5) NOT NULL,
  `pro_ssl` varchar(150) NOT NULL,
  `pro_passphrase` varchar(5) NOT NULL,
  `mode` text,
  `appimage` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_apps`
--

INSERT INTO `tbl_apps` (`id`, `user_id`, `app_name`, `app_desc`, `app_store`, `app_key`, `app_and_key`, `app_ios_key`, `dev_ssl`, `dev_passphrase`, `pro_ssl`, `pro_passphrase`, `mode`, `appimage`) VALUES
(1, 1, 'Elabelz', '                                                                                                                                                                                                                                                                                                                                                                        English UAE                                                                                                                                                                                                                                                                                                                                                    ', 'en_ae', 'testkey', '', '', 'ssl/dev_cer.pem', '', 'ssl/aps_dis.pem', '123', NULL, 'images/100x100.jpg'),
(3, 1, 'Elabelz', 'English Bahrain', 'en_bh', 'testkey', '', '', 'ssl/', '', 'ssl/', '', NULL, 'images/100x100.jpg'),
(4, 1, 'Elabelz', 'English Iraq', 'en_iq', 'asdasdasdas', '', '', 'ssl/', '', 'ssl/', '', NULL, 'images/100x100.jpg'),
(5, 1, 'Elabelz', 'English Kuwait', 'en_kw', 'asdasdasdas', '', '', 'ssl/', '', 'ssl/', '', NULL, 'images/100x100.jpg'),
(6, 1, 'Elabelz', 'English Oman', 'en_om', 'asdasdasdas', '', '', 'ssl/', '', 'ssl/', '', NULL, 'images/100x100.jpg'),
(7, 1, 'Elabelz', 'English Qatar', 'en_qa', 'asdasdasdas', '', '', 'ssl/', '', 'ssl/', '', NULL, 'images/100x100.jpg'),
(8, 1, 'Elabelz', '                                                                        Arabic Saudi Arabia                                                                    ', 'ar_sa', 'asdasdasdas', '', '', 'ssl/', '', 'ssl/', '', NULL, 'images/100x100.jpg'),
(9, 1, 'Elabelz', 'Arabic UAE', 'ar_ae', 'asdasdasdas', '', '', 'ssl/', '', 'ssl/', '', NULL, 'images/100x100.jpg'),
(10, 1, 'Elabelz', 'Arabic Bahrain', 'ar_bh', 'asdasdasdas', 'testandroidkey', '', 'ssl/', '', 'ssl/', '', NULL, 'images/100x100.jpg'),
(11, 1, 'Elabelz', 'Arabic Iraq', 'ar_iq', 'asdasdasdas', '', '', 'ssl/', '', 'ssl/', '', NULL, 'images/100x100.jpg'),
(12, 1, 'Elabelz', 'Arabic Kuwait', 'ar_kw', 'asdasdasdas', '', '', 'ssl/', '', 'ssl/', '', NULL, 'images/100x100.jpg'),
(13, 1, 'Elabelz', 'Arabic Oman', 'ar_om', 'asdasdasdas', '', '', 'ssl/', '', 'ssl/', '', NULL, 'images/100x100.jpg'),
(14, 1, 'Elabelz', 'Arabic Qatar', 'ar_qa', 'asdasdasdas', '', '', 'ssl/', '', 'ssl/', '', NULL, 'images/100x100.jpg'),
(15, 1, 'Elabelz', '                                                                        English Saudi Arabia                                                                    ', 'en_sa', 'asdasdasdas', '', '', 'ssl/', '', 'ssl/', '', NULL, 'images/100x100.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_block`
--

CREATE TABLE `tbl_block` (
  `id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `has_text` enum('0','1') NOT NULL DEFAULT '0',
  `welcome_text` text,
  `is_default` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_block`
--

INSERT INTO `tbl_block` (`id`, `app_id`, `image_url`, `has_text`, `welcome_text`, `is_default`) VALUES
(13, 8, './vendor/fineuploader/php-traditional-server/files/cd4186a4-f465-43f4-9647-6060d40b96c8/App-Home-Page1.png', '0', NULL, '0'),
(14, 9, './vendor/fineuploader/php-traditional-server/files/f22074bc-7bb2-40f4-98d0-f85ec909fe56/App-Home-Page1.png', '0', NULL, '0'),
(15, 10, './vendor/fineuploader/php-traditional-server/files/29983d7a-a652-4800-ac47-cb5489e162b8/App-Home-Page1.png', '0', NULL, '0'),
(16, 11, './vendor/fineuploader/php-traditional-server/files/cab4c2b0-aa58-422e-a6b1-d8322bb370ef/App-Home-Page1.png', '0', NULL, '0'),
(17, 12, './vendor/fineuploader/php-traditional-server/files/fae44a4d-fe35-4e63-873e-0c8b12bd479b/App-Home-Page1.png', '0', NULL, '0'),
(18, 13, './vendor/fineuploader/php-traditional-server/files/374ded49-3bd9-4446-9029-08a17cc36ef0/App-Home-Page1.png', '0', NULL, '0'),
(19, 14, './vendor/fineuploader/php-traditional-server/files/3bebab3e-57e6-47a9-87e4-e6196862aa07/App-Home-Page1.png', '0', NULL, '0'),
(21, 1, './vendor/fineuploader/php-traditional-server/files/a0785731-d495-4693-a856-c7c3f8319408/Main1.jpg', '0', NULL, '0'),
(22, 1, './vendor/fineuploader/php-traditional-server/files/96891df1-4931-4ca7-8c04-61fd84cc03d4/Main2.jpg', '0', NULL, '0'),
(23, 1, './vendor/fineuploader/php-traditional-server/files/10477f6e-7846-46b4-a843-95069015f4aa/Main3.jpg', '0', NULL, '0'),
(26, 3, './vendor/fineuploader/php-traditional-server/files/b3a5a03f-5c57-439e-9e50-b573f7748f5d/Main2.jpg', '0', NULL, '0'),
(27, 3, './vendor/fineuploader/php-traditional-server/files/78686f7e-a8c0-462a-a009-e243bacbc324/Main3.jpg', '0', NULL, '0'),
(28, 3, './vendor/fineuploader/php-traditional-server/files/933f35ca-5728-4129-8185-75efb06eeda0/Main1.jpg', '0', NULL, '0'),
(29, 8, './vendor/fineuploader/php-traditional-server/files/402e5356-3ce5-4c20-ad0e-8b2316fda583/Main1.jpg', '0', NULL, '0'),
(30, 8, './vendor/fineuploader/php-traditional-server/files/582e982f-1e9b-4015-a965-43e9a011b536/Main2.jpg', '0', NULL, '0'),
(31, 8, './vendor/fineuploader/php-traditional-server/files/ceb7fa3a-2a9c-4257-84f6-164818cce421/Main3.jpg', '0', NULL, '0'),
(32, 9, './vendor/fineuploader/php-traditional-server/files/24b1d75d-f168-407d-8f04-5cbe464788b0/Main1.jpg', '0', NULL, '0'),
(33, 9, './vendor/fineuploader/php-traditional-server/files/c32899a1-d2bc-40cd-b54f-f0a7b4fad44e/Main2.jpg', '0', NULL, '0'),
(34, 9, './vendor/fineuploader/php-traditional-server/files/03f0d0e0-5b63-43d5-afd5-b1c45f99db32/Main3.jpg', '0', NULL, '0'),
(35, 10, './vendor/fineuploader/php-traditional-server/files/6f5aeea5-6533-4bba-acbf-b4ddd603a431/Main1.jpg', '0', NULL, '0'),
(36, 10, './vendor/fineuploader/php-traditional-server/files/7498f1b3-c379-47a0-bbe6-3f8fcd438715/Main2.jpg', '0', NULL, '0'),
(37, 10, './vendor/fineuploader/php-traditional-server/files/06a4f486-fe65-4afd-a139-aae2acbc0c0a/Main3.jpg', '0', NULL, '0'),
(38, 11, './vendor/fineuploader/php-traditional-server/files/6524cddf-7125-4af7-b5e9-63a9d1223d34/Main1.jpg', '0', NULL, '0'),
(39, 11, './vendor/fineuploader/php-traditional-server/files/948014e4-7b7e-4830-831d-b250dd169c19/Main2.jpg', '0', NULL, '0'),
(40, 11, './vendor/fineuploader/php-traditional-server/files/69e67779-8c44-4ff5-8985-3e89f70ae55b/Main3.jpg', '0', NULL, '0'),
(41, 12, './vendor/fineuploader/php-traditional-server/files/6430522f-1026-41e7-8180-119935a5801c/Main1.jpg', '0', NULL, '0'),
(42, 12, './vendor/fineuploader/php-traditional-server/files/c9b5be88-f943-49a4-8b3a-2e1cb8579686/Main2.jpg', '0', NULL, '0'),
(43, 12, './vendor/fineuploader/php-traditional-server/files/b4a2a45a-bb6c-4247-b1f5-775fa9a0387c/Main3.jpg', '0', NULL, '0'),
(44, 13, './vendor/fineuploader/php-traditional-server/files/69952f11-6455-4699-9f0e-0dc434a8b645/Main1.jpg', '0', NULL, '0'),
(45, 13, './vendor/fineuploader/php-traditional-server/files/17139013-0f41-4984-8251-e639c22a9402/Main2.jpg', '0', NULL, '0'),
(46, 13, './vendor/fineuploader/php-traditional-server/files/2aec359f-2e56-4073-a6f7-0324aed8a4a5/Main3.jpg', '0', NULL, '0'),
(47, 14, './vendor/fineuploader/php-traditional-server/files/f22c4a97-fde1-4ec2-bcc1-93ef042831d7/Main1.jpg', '0', NULL, '0'),
(48, 14, './vendor/fineuploader/php-traditional-server/files/63bb46c1-7d82-4899-9ea1-e0fe89594c36/Main2.jpg', '0', NULL, '0'),
(49, 14, './vendor/fineuploader/php-traditional-server/files/d388da38-2343-4388-8971-7e855bffd640/Main3.jpg', '0', NULL, '0'),
(50, 4, './vendor/fineuploader/php-traditional-server/files/16d92dee-a6c6-419d-a7d7-2e42b6856315/Main1.jpg', '0', NULL, '0'),
(51, 4, './vendor/fineuploader/php-traditional-server/files/704c01da-bc40-4607-a473-967848fb4e66/Main2.jpg', '0', NULL, '0'),
(52, 4, './vendor/fineuploader/php-traditional-server/files/82d6d15c-4025-4607-afb1-3192e4cbab0c/Main3.jpg', '0', NULL, '0'),
(53, 5, './vendor/fineuploader/php-traditional-server/files/8823b233-0d7c-497a-a3db-570eeab6c37b/Main1.jpg', '0', NULL, '0'),
(54, 5, './vendor/fineuploader/php-traditional-server/files/7663d02a-4afb-4818-b186-dbe5fe718130/Main2.jpg', '0', NULL, '0'),
(55, 5, './vendor/fineuploader/php-traditional-server/files/5b063c4b-bead-431c-82ca-fb9b5580da22/Main3.jpg', '0', NULL, '0'),
(56, 6, './vendor/fineuploader/php-traditional-server/files/7a508973-b8ba-4f78-be6f-63a902add377/Main1.jpg', '0', NULL, '0'),
(57, 6, './vendor/fineuploader/php-traditional-server/files/b2c42c02-91b8-4ba4-91c1-9f29b012e7e6/Main2.jpg', '0', NULL, '0'),
(58, 6, './vendor/fineuploader/php-traditional-server/files/ba056f26-c381-40f6-90ed-38b706e80d3e/Main3.jpg', '0', NULL, '0'),
(59, 7, './vendor/fineuploader/php-traditional-server/files/63f8455b-860d-4765-86cc-42de41c883d7/Main1.jpg', '0', NULL, '0'),
(60, 7, './vendor/fineuploader/php-traditional-server/files/de9d88b7-72ad-4a44-9526-dbbb36dc661a/Main2.jpg', '0', NULL, '0'),
(61, 7, './vendor/fineuploader/php-traditional-server/files/ec4065d9-7983-45a8-8a21-ea99f61dd8e8/Main3.jpg', '0', NULL, '0'),
(62, 15, './vendor/fineuploader/php-traditional-server/files/05b5f419-ab78-4107-a29a-3e6f076b63f6/Main1.jpg', '0', NULL, '0'),
(63, 15, './vendor/fineuploader/php-traditional-server/files/b3d50fdc-8012-449a-946a-73d88eb9a5fd/Main2.jpg', '0', NULL, '0'),
(64, 15, './vendor/fineuploader/php-traditional-server/files/e67fc60a-9a74-45ad-b5cb-a4340662c31d/Main3.jpg', '0', NULL, '0'),
(65, 9, './vendor/fineuploader/php-traditional-server/files/d35fdab7-15bb-4986-83d7-00a823545165/White-1125x810px.jpg', '0', NULL, '0'),
(66, 1, './vendor/fineuploader/php-traditional-server/files/944bb376-d747-4e65-be2a-f4758316b04e/White-1125x810px.jpg', '0', NULL, '0'),
(67, 9, './vendor/fineuploader/php-traditional-server/files/4f0af573-0b0f-42ea-8d95-9797d1808097/White-1125x810px-100pxwthie-border.jpg', '0', NULL, '0'),
(68, 9, './vendor/fineuploader/php-traditional-server/files/033e0842-6e81-4b73-91e3-71f768f159f7/White-1125x810px-50pxwthie-border.jpg', '0', NULL, '0'),
(69, 9, './vendor/fineuploader/php-traditional-server/files/481b193f-2eeb-4e36-80a9-89d07643085c/White-1125x810px-20pxwthie-border.jpg', '0', NULL, '1'),
(70, 1, './vendor/fineuploader/php-traditional-server/files/6fa28ede-58db-48e0-802c-d6f64878cd1d/White-1125x810px-20pxwthie-border.jpg', '0', NULL, '1'),
(71, 3, './vendor/fineuploader/php-traditional-server/files/9f01c596-f7ba-4d20-b309-1aa3499fa0a8/White-1125x810px-20pxwthie-border-1.jpg', '0', NULL, '1'),
(72, 5, './vendor/fineuploader/php-traditional-server/files/314c15b5-b4e0-4cd4-a692-1cc34dd3f7c3/White-1125x810px-20pxwthie-border-1.jpg', '0', NULL, '1'),
(73, 4, './vendor/fineuploader/php-traditional-server/files/7b0c9f8d-f437-4a89-a797-4804e11bae53/White-1125x810px-20pxwthie-border-1.jpg', '0', NULL, '1'),
(74, 6, './vendor/fineuploader/php-traditional-server/files/a1f1c217-915c-4c5a-9c8b-1d2c7890c84a/White-1125x810px-20pxwthie-border-1.jpg', '0', NULL, '1'),
(75, 7, './vendor/fineuploader/php-traditional-server/files/73cefafe-7a23-4f69-859f-8d826b7897a2/White-1125x810px-20pxwthie-border-1.jpg', '0', NULL, '1'),
(76, 8, './vendor/fineuploader/php-traditional-server/files/8cfc4571-4605-4ed1-9dc8-9a4c8e2f31c6/White-1125x810px-20pxwthie-border-1.jpg', '0', NULL, '1'),
(77, 10, './vendor/fineuploader/php-traditional-server/files/e6d6d87d-c0ca-47c7-8bf7-59c6dbb8daf8/White-1125x810px-20pxwthie-border-1.jpg', '0', NULL, '1'),
(78, 12, './vendor/fineuploader/php-traditional-server/files/9ed17e20-4113-4887-9bb3-700c9f9062c8/White-1125x810px-20pxwthie-border-1.jpg', '0', NULL, '1'),
(79, 13, './vendor/fineuploader/php-traditional-server/files/0d7ac84c-337a-46f5-899b-887f27e7cdf5/White-1125x810px-20pxwthie-border-1.jpg', '0', NULL, '1'),
(80, 15, './vendor/fineuploader/php-traditional-server/files/d7644150-2db5-4155-99cc-1d33b9b2351c/White-1125x810px-20pxwthie-border-1.jpg', '0', NULL, '1'),
(81, 14, './vendor/fineuploader/php-traditional-server/files/c3d0454f-1498-4055-8971-7953abeaf859/White-1125x810px-20pxwthie-border-1.jpg', '0', NULL, '1'),
(82, 11, './vendor/fineuploader/php-traditional-server/files/1e62db39-2cc6-4e2d-97d0-ccc776e59466/White-1125x810px-20pxwthie-border-1.jpg', '0', NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_categories`
--

CREATE TABLE `tbl_categories` (
  `id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_categories`
--

INSERT INTO `tbl_categories` (`id`, `app_id`, `title`, `status`) VALUES
(4, 1, 'Men', 1),
(5, 1, 'Women', 1),
(6, 2, 'Men', 1),
(7, 2, 'Women', 1),
(8, 2, 'Men', 1),
(9, 2, 'Women', 1),
(10, 3, 'Men', 1),
(11, 3, 'Women', 1),
(12, 4, 'Men', 1),
(13, 4, 'Women', 1),
(14, 5, 'Men', 1),
(15, 5, 'Women', 1),
(16, 6, 'Men', 1),
(17, 6, 'Women', 1),
(18, 7, 'Men', 1),
(19, 7, 'Women', 1),
(20, 8, 'Men', 1),
(21, 8, 'Women', 1),
(22, 9, 'Men', 1),
(23, 9, 'Women', 1),
(24, 10, 'Men', 1),
(25, 10, 'Women', 1),
(26, 11, 'Men', 1),
(27, 11, 'Women', 1),
(28, 12, 'Men', 1),
(29, 12, 'Women', 1),
(30, 13, 'Men', 1),
(31, 13, 'Women', 1),
(32, 14, 'Men', 1),
(33, 14, 'Women', 1),
(34, 15, 'Men', 1),
(35, 15, 'Women', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cat_rows`
--

CREATE TABLE `tbl_cat_rows` (
  `id` int(11) NOT NULL,
  `merchandise_id` int(11) NOT NULL,
  `layout` varchar(255) NOT NULL,
  `images` text NOT NULL,
  `attachment` text NOT NULL,
  `row_order` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cat_rows`
--

INSERT INTO `tbl_cat_rows` (`id`, `merchandise_id`, `layout`, `images`, `attachment`, `row_order`) VALUES
(11, 6, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/55764e84-beef-409e-b602-34ebe942fffb/1024x1310.png\",\"./vendor/fineuploader/php-traditional-server/files/d3784e7a-7be1-4c6d-a977-8baa23f972d7/1024x1310.png\"]', '[[\"0\",\"1\",\"85\",\"Accessories\"],[\"1\",\"1\",\"628\",\"Accessories\"]]', 0),
(12, 7, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/fe3f1613-6f0b-4592-b2e0-bdf928c6a11d/1024x1310.png\",\"./vendor/fineuploader/php-traditional-server/files/2c4ff1a8-cd9f-434b-b7f0-3c88847aaca8/1024x1310.png\"]', '[[\"0\",\"1\",\"9\",\"Bags\"],[\"1\",\"1\",\"146\",\"Abayas\"]]', 0),
(17, 4, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/618a720b-2147-4e41-93d6-ee0108603399/MenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/0d5075a5-4def-4c08-bec0-6646c38094f9/MenSneakers.jpg\"]', '[[\"0\",\"1\",\"51\",\"\"],[\"1\",\"1\",\"44\",\"\"]]', 0),
(18, 4, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/ea556284-78f4-44d9-97f6-544686a49bd5/MenTshirts.jpg\",\"./vendor/fineuploader/php-traditional-server/files/e461ba0e-6ad7-4d00-b972-378ae371c500/MenSports.jpg\"]', '[[\"0\",\"1\",\"52\",\"\"],[\"1\",\"1\",\"54\",\"\"]]', 0),
(19, 4, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/d8f0f8f3-02dc-4a47-9ddf-df5bce2a25ce/MenDenim.jpg\",\"./vendor/fineuploader/php-traditional-server/files/e7a83730-29f8-436b-8280-d03b8a4a1fb7/MenAccessories.jpg\"]', '[[\"0\",\"1\",\"61\",\"\"],[\"1\",\"1\",\"65\",\"\"]]', 0),
(20, 4, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/fcf6187f-50ab-4ab8-9722-a46bc96f8ae2/MenShoes.jpg\",\"./vendor/fineuploader/php-traditional-server/files/cb632bb6-02f8-41c3-9bd7-1a96a3f9d6da/MenSwimwear.jpg\"]', '[[\"0\",\"1\",\"43\",\"\"],[\"1\",\"1\",\"64\",\"\"]]', 0),
(21, 20, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/37f7cf98-7455-4f49-ab75-9408a67fd2dd/MenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/55f78c34-7dac-49c9-82e6-001adf6bdc93/MenSneakers.jpg\"]', '[[\"0\",\"1\",\"51\",\"\"],[\"1\",\"1\",\"44\",\"\"]]', 0),
(22, 10, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/62fa64f5-71c6-4f40-9e3b-7845824737c2/MenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/311177f9-55ef-4d3a-8c02-c291497ef610/MenSneakers.jpg\"]', '[[\"0\",\"1\",\"51\",\"\"],[\"1\",\"1\",\"44\",\"\"]]', 0),
(23, 20, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/518e89af-6df8-44a9-aa23-b2be04e646b4/MenTshirts.jpg\",\"./vendor/fineuploader/php-traditional-server/files/2f92d309-7dc2-486d-af85-f1505bfed61a/MenSports.jpg\"]', '[[\"0\",\"1\",\"52\",\"\"],[\"1\",\"1\",\"54\",\"\"]]', 0),
(24, 10, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/7801a9f3-8514-4ee6-a43f-c4db6f4ec82c/MenTshirts.jpg\",\"./vendor/fineuploader/php-traditional-server/files/103c45bb-61e7-4c70-a635-b527e3c82e7a/MenSports.jpg\"]', '[[\"0\",\"1\",\"52\",\"\"],[\"1\",\"1\",\"54\",\"\"]]', 0),
(25, 20, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/d9c738ef-e053-4b3a-b0d3-cc8a6cdbc370/MenJeans.jpg\",\"./vendor/fineuploader/php-traditional-server/files/ddc10f47-e2f8-4ef4-8727-a6b4e3d68d53/MenAccessories.jpg\"]', '[[\"0\",\"1\",\"61\",\"\"],[\"1\",\"1\",\"65\",\"\"]]', 0),
(26, 10, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/0d7bd7b5-24db-4105-b130-27ee1a9264d2/MenDenim.jpg\",\"./vendor/fineuploader/php-traditional-server/files/2c119ee1-3b88-4118-9d72-4c38b26b09a6/MenAccessories.jpg\"]', '[[\"0\",\"1\",\"61\",\"\"],[\"1\",\"1\",\"65\",\"\"]]', 0),
(27, 20, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/974731ed-f720-4064-885c-2dfdbad3156a/MenShoes.jpg\",\"./vendor/fineuploader/php-traditional-server/files/3f3a7f70-00d8-44d2-b653-c0ba0ef674bc/MenSwimwear.jpg\"]', '[[\"0\",\"1\",\"43\",\"\"],[\"1\",\"1\",\"64\",\"\"]]', 0),
(28, 10, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/711cc45c-d55d-464e-a447-265bbbe5eed9/MenShoes.jpg\",\"./vendor/fineuploader/php-traditional-server/files/bb6bc4dc-1c8d-4b0b-acbb-a49472e18f3a/MenSwimwear.jpg\"]', '[[\"0\",\"1\",\"43\",\"\"],[\"1\",\"1\",\"64\",\"\"]]', 0),
(29, 11, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/7d58a830-e5be-4703-990d-b95735451443/WomenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/cda838ce-70c3-4462-b668-a23d912bc46d/WomenShoes.jpg\"]', '[[\"0\",\"1\",\"7\",\"\"],[\"1\",\"1\",\"6\",\"\"]]', 0),
(30, 21, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/afa81265-d97b-4d4a-b9c6-9c50474b630e/WomenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/34244544-5755-49fb-9b29-0dafa59994f2/WomenShoes.jpg\"]', '[[\"0\",\"1\",\"7\",\"\"],[\"1\",\"1\",\"6\",\"\"]]', 0),
(31, 11, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/b2b8784c-78f5-4a46-918e-6ce1f37e83f2/WomenDresses.jpg\",\"./vendor/fineuploader/php-traditional-server/files/5b7e2a3f-8245-48b0-8ed4-9af2f680b1ff/WomenAbayas.jpg\"]', '[[\"0\",\"1\",\"19\",\"\"],[\"1\",\"1\",\"146\",\"\"]]', 0),
(32, 21, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/73793ece-1c7a-429d-aff2-eb51df97b28e/WomenDresses.jpg\",\"./vendor/fineuploader/php-traditional-server/files/ba36d401-849c-43ff-ad97-56f7e9b0845f/WomenAbayas.jpg\"]', '[[\"0\",\"1\",\"19\",\"\"],[\"1\",\"1\",\"146\",\"\"]]', 0),
(33, 21, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/9be992c6-191f-4072-8c5c-439b562814b5/WomenJeans.jpg\",\"./vendor/fineuploader/php-traditional-server/files/5ff9b9a7-f467-4a57-ae48-2f486509aa5c/WomenBags.jpg\"]', '[[\"0\",\"1\",\"33\",\"\"],[\"1\",\"1\",\"9\",\"\"]]', 0),
(34, 11, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/9f4ebee6-1fee-4d18-9692-7c1dde81858e/WomenDenim.jpg\",\"./vendor/fineuploader/php-traditional-server/files/84878826-2a7e-4efb-b54a-d2b4197b6701/WomenBags.jpg\"]', '[[\"0\",\"1\",\"33\",\"\"],[\"1\",\"1\",\"9\",\"\"]]', 0),
(35, 11, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/0007deca-2e39-4739-9778-054a478a5091/WomenAccessories.jpg\",\"./vendor/fineuploader/php-traditional-server/files/ae982f48-977f-469e-9551-9e948a9e9a59/WomenSports.jpg\"]', '[[\"0\",\"1\",\"8\",\"\"],[\"1\",\"1\",\"22\",\"\"]]', 0),
(36, 21, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/01ea5bb4-1042-4c39-ac3e-5faf9bb98894/WomenAccessories.jpg\",\"./vendor/fineuploader/php-traditional-server/files/6fa71fee-9dbb-4965-94e4-cca804182476/WomenSports.jpg\"]', '[[\"0\",\"1\",\"8\",\"\"],[\"1\",\"1\",\"22\",\"\"]]', 0),
(37, 5, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/2d53c118-6772-4c82-9bc3-ac463f4f26e1/WomenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/d87d8077-a0e0-41e0-afdd-95d0c0b05d7a/WomenShoes.jpg\"]', '[[\"0\",\"1\",\"7\",\"\"],[\"1\",\"1\",\"6\",\"\"]]', 0),
(38, 5, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/fecad25f-d404-42b7-91c4-9b9e5acaf887/WomenDresses.jpg\",\"./vendor/fineuploader/php-traditional-server/files/e810c18e-5951-4acb-a99d-4ee043a63d9c/WomenAbayas.jpg\"]', '[[\"0\",\"1\",\"19\",\"\"],[\"1\",\"1\",\"146\",\"\"]]', 0),
(39, 5, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/fe621656-554a-4ac7-b4a0-423989595949/WomenDenim.jpg\",\"./vendor/fineuploader/php-traditional-server/files/c2c75da3-7c57-432b-bfa7-781996855967/WomenBags.jpg\"]', '[[\"0\",\"1\",\"33\",\"\"],[\"1\",\"1\",\"9\",\"\"]]', 0),
(40, 5, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/62c9e4da-7ba6-4f6e-ab99-4c7b077e627c/WomenAccessories.jpg\",\"./vendor/fineuploader/php-traditional-server/files/196dc6cb-1cc1-4065-a5d3-11610d8584f6/WomenSports.jpg\"]', '[[\"0\",\"1\",\"8\",\"\"],[\"1\",\"1\",\"22\",\"\"]]', 0),
(41, 12, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/074a8f65-ff58-4079-92d4-1db21324a0b3/MenAccessories.jpg\",\"./vendor/fineuploader/php-traditional-server/files/b15d9819-680c-4fa3-9c13-7c767f5785ad/MenSneakers.jpg\"]', '[[\"0\",\"1\",\"51\",\"\"],[\"1\",\"1\",\"44\",\"\"]]', 0),
(42, 12, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/65c8b371-31c9-4d40-b262-3af01dd5a1e7/MenTshirts.jpg\",\"./vendor/fineuploader/php-traditional-server/files/793a32e2-38fc-44be-b9e3-a2c11e9e3013/MenSports.jpg\"]', '[[\"0\",\"1\",\"171\",\"\"],[\"1\",\"1\",\"54\",\"\"]]', 0),
(43, 12, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/aba369af-5f63-44b8-9339-450bd1a1dee8/MenDenim.jpg\",\"./vendor/fineuploader/php-traditional-server/files/9e9aebd8-5011-4612-8e12-37e97c359aab/MenAccessories.jpg\"]', '[[\"0\",\"1\",\"61\",\"\"],[\"1\",\"1\",\"65\",\"\"]]', 0),
(44, 12, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/e6a01e39-1e88-45d0-833c-0df83825afa9/MenShoes.jpg\",\"./vendor/fineuploader/php-traditional-server/files/e380eba2-804e-4e33-ac7d-e98961c69398/MenSwimwear.jpg\"]', '[[\"0\",\"1\",\"43\",\"\"],[\"1\",\"1\",\"64\",\"\"]]', 0),
(45, 13, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/c97ca2be-61f3-4c88-bed1-7521f716cbe3/WomenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/07420ea6-f410-4178-a569-eb3d74344920/WomenShoes.jpg\"]', '[[\"0\",\"1\",\"7\",\"\"],[\"1\",\"1\",\"6\",\"\"]]', 0),
(46, 13, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/833ebd9d-dfa3-47ca-84a7-863cfab2655b/WomenDresses.jpg\",\"./vendor/fineuploader/php-traditional-server/files/da46d6db-842d-436e-9959-cb293399f618/WomenAbayas.jpg\"]', '[[\"0\",\"1\",\"19\",\"\"],[\"1\",\"1\",\"146\",\"\"]]', 0),
(47, 13, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/3bc29b10-f258-4f62-8ab1-ebeb2fbdc47d/WomenDenim.jpg\",\"./vendor/fineuploader/php-traditional-server/files/45279230-889b-45d9-8f9a-5c43cd71c9f0/WomenBags.jpg\"]', '[[\"0\",\"1\",\"33\",\"\"],[\"1\",\"1\",\"9\",\"\"]]', 0),
(48, 13, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/1699beaa-685d-4ebf-aec3-a22ab6fecc2d/WomenAccessories.jpg\",\"./vendor/fineuploader/php-traditional-server/files/a7957bef-5b45-4b27-b458-729d27f28fab/WomenSports.jpg\"]', '[[\"0\",\"1\",\"8\",\"\"],[\"1\",\"1\",\"22\",\"\"]]', 0),
(49, 14, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/3ac522b0-1781-4844-a4ab-1d92c1f6ae8a/MenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/ad321c55-5cbc-4ece-8624-3b86882ce9c4/MenSneakers.jpg\"]', '[[\"0\",\"1\",\"51\",\"\"],[\"1\",\"1\",\"44\",\"\"]]', 0),
(50, 14, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/b39adb37-15e4-447f-ae2f-4966868e3d39/MenTshirts.jpg\",\"./vendor/fineuploader/php-traditional-server/files/50d5f4d6-fcac-49f2-8693-2c4bcd0c9e04/MenSports.jpg\"]', '[[\"0\",\"1\",\"171\",\"\"],[\"1\",\"1\",\"54\",\"\"]]', 0),
(51, 14, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/4ac50517-2b04-4011-b6f0-65382f39435f/MenDenim.jpg\",\"./vendor/fineuploader/php-traditional-server/files/d07463f8-49bc-42a0-affa-0b71147631cc/MenAccessories.jpg\"]', '[[\"0\",\"1\",\"61\",\"\"],[\"1\",\"1\",\"65\",\"\"]]', 0),
(52, 14, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/524408fb-ff10-4b0d-bf36-563437a66575/MenShoes.jpg\",\"./vendor/fineuploader/php-traditional-server/files/db3c9098-aa4e-4d54-bae2-aa0bb55dcc59/MenSwimwear.jpg\"]', '[[\"0\",\"1\",\"43\",\"\"],[\"1\",\"1\",\"64\",\"\"]]', 0),
(53, 15, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/34f0ae9e-673c-4868-b07c-b856842898b4/WomenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/fad073e7-1529-4dda-8a62-643e2b9b9a96/WomenShoes.jpg\"]', '[[\"0\",\"1\",\"7\",\"\"],[\"1\",\"1\",\"6\",\"\"]]', 0),
(54, 15, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/43b79b12-04fe-4d1d-bdc0-94a47ccc4c97/WomenDresses.jpg\",\"./vendor/fineuploader/php-traditional-server/files/584f60e1-11c6-4286-acc1-cd3dd1fbfed6/WomenAbayas.jpg\"]', '[[\"0\",\"1\",\"19\",\"\"],[\"1\",\"1\",\"146\",\"\"]]', 0),
(55, 15, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/bddd6ec2-3099-452e-8f16-7e5f486eb79d/WomenDenim.jpg\",\"./vendor/fineuploader/php-traditional-server/files/e177441a-6aec-4fd3-93c7-522f7fa009a7/WomenBags.jpg\"]', '[[\"0\",\"1\",\"33\",\"\"],[\"1\",\"1\",\"9\",\"\"]]', 0),
(56, 15, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/3da7493d-e988-4420-b7db-3b494451a8b0/WomenAccessories.jpg\",\"./vendor/fineuploader/php-traditional-server/files/4a0fd89d-16f9-487a-9376-5deb24f17436/WomenSports.jpg\"]', '[[\"0\",\"1\",\"8\",\"\"],[\"1\",\"1\",\"22\",\"\"]]', 0),
(57, 22, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/b3adb10b-8855-401a-ba31-544acdca5221/MenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/d91b364d-2da5-47e2-bc91-a42cdab2adfd/MenSneakers.jpg\"]', '[[\"0\",\"1\",\"51\",\"\"],[\"1\",\"1\",\"44\",\"\"]]', 0),
(58, 22, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/f474b215-fc9f-43c9-b17b-04a7614a6d36/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/a755ab0f-f5e7-4212-90db-679e87262120/MenSports.jpg\"]', '[[\"0\",\"1\",\"52\",\"\"],[\"1\",\"1\",\"54\",\"\"]]', 0),
(59, 16, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/64e3de8c-ed12-4dea-b50a-1fa7f6511e5b/MenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/3e92afc2-e0a9-4f48-bae8-2dbc5cf45a37/MenSneakers.jpg\"]', '[[\"0\",\"1\",\"51\",\"\"],[\"1\",\"1\",\"44\",\"\"]]', 0),
(60, 22, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/34b1f2e1-0e84-43bf-ae33-fcdb9ce10ee1/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/e3fa25ee-5a18-4ca7-9cbe-93a928b559f2/MenAccessories.jpg\"]', '[[\"0\",\"1\",\"61\",\"\"],[\"1\",\"1\",\"65\",\"\"]]', 0),
(61, 16, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/e5c77962-799a-455e-bc31-517a261c152e/MenTshirts.jpg\",\"./vendor/fineuploader/php-traditional-server/files/a716e046-508d-4fc1-bb89-72587c8daca1/MenSports.jpg\"]', '[[\"0\",\"1\",\"171\",\"\"],[\"1\",\"1\",\"54\",\"\"]]', 0),
(62, 22, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/9a6e6b92-b7e1-4f19-bdd7-ddb4d74cab3b/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/e64b7379-c8f5-4593-8753-b68469142583/MenSwimwear.jpg\"]', '[[\"0\",\"1\",\"43\",\"\"],[\"1\",\"1\",\"64\",\"\"]]', 0),
(63, 16, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/6b06ff7c-f238-4686-bc1c-3b8ae764b5e3/MenDenim.jpg\",\"./vendor/fineuploader/php-traditional-server/files/81fd26e5-7d13-4a59-bada-65ca98e82e63/MenAccessories.jpg\"]', '[[\"0\",\"1\",\"61\",\"\"],[\"1\",\"1\",\"65\",\"\"]]', 0),
(64, 16, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/ac16f55d-defb-485b-aeba-41d3e8196fe5/MenShoes.jpg\",\"./vendor/fineuploader/php-traditional-server/files/cb7ccd6b-e587-4fc7-ba73-2c54063718ee/MenSwimwear.jpg\"]', '[[\"0\",\"1\",\"43\",\"\"],[\"1\",\"1\",\"64\",\"\"]]', 0),
(65, 24, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/a09fc69a-c320-4380-9f6e-1290da90fad6/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/0fe9d4bb-5166-406b-93a2-77f8754128b0/MenSneakers.jpg\"]', '[[\"0\",\"1\",\"51\",\"\"],[\"1\",\"1\",\"44\",\"\"]]', 0),
(66, 17, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/24d1af12-9f27-46e4-8282-bb8629ad58c6/WomenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/04d58056-32ac-4990-a216-6c7ae22008e4/WomenShoes.jpg\"]', '[[\"0\",\"1\",\"7\",\"\"],[\"1\",\"1\",\"6\",\"\"]]', 0),
(67, 24, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/78bd29ee-fb6f-4ef5-bf84-b823b2a6cf5c/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/3ad761c5-a83a-4d29-b1b4-0fcfe96dcb8d/MenSports.jpg\"]', '[[\"0\",\"1\",\"52\",\"\"],[\"1\",\"1\",\"54\",\"\"]]', 0),
(68, 17, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/62906166-2c65-4b8a-b0d5-c69db9997286/WomenDresses.jpg\",\"./vendor/fineuploader/php-traditional-server/files/c04f84ac-4344-4c12-8d0b-e659f521eddc/WomenAbayas.jpg\"]', '[[\"0\",\"1\",\"19\",\"\"],[\"1\",\"1\",\"146\",\"\"]]', 0),
(69, 17, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/040f037d-e2a3-42df-a977-3514fdca3678/WomenDenim.jpg\",\"./vendor/fineuploader/php-traditional-server/files/38a61179-1b51-455c-b049-5b2b370b5a9f/WomenBags.jpg\"]', '[[\"0\",\"1\",\"33\",\"\"],[\"1\",\"1\",\"9\",\"\"]]', 0),
(70, 24, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/91293491-1017-4d05-bf1e-31d8e24d88b5/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/4cccf463-171c-4fdc-9349-d63621d6c8c4/MenAccessories.jpg\"]', '[[\"0\",\"1\",\"61\",\"\"],[\"1\",\"1\",\"65\",\"\"]]', 0),
(71, 17, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/456f64e7-c5e1-40a6-ae44-3f7040a5a619/WomenAccessories.jpg\",\"./vendor/fineuploader/php-traditional-server/files/d46bd1b1-24cb-4aa7-b09f-65792c3a43ac/WomenSports.jpg\"]', '[[\"0\",\"1\",\"8\",\"\"],[\"1\",\"1\",\"22\",\"\"]]', 0),
(72, 24, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/bef679d1-e381-4ad8-8f14-8e028677919f/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/e135540f-4edb-456e-bea5-83849651cb6c/MenSwimwear.jpg\"]', '[[\"0\",\"1\",\"43\",\"\"],[\"1\",\"1\",\"64\",\"\"]]', 0),
(73, 26, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/260c139d-c363-4db0-a923-2652364e2ddd/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/0e59563b-a45d-485d-9f2c-39851ea5b535/MenSneakers.jpg\"]', '[[\"0\",\"1\",\"51\",\"\"],[\"1\",\"1\",\"44\",\"\"]]', 0),
(74, 26, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/0e151ce9-2afb-48c2-a0f0-47c34c663e41/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/c04f07f3-2ffe-4f00-aad0-ab65ffb86c0d/MenSports.jpg\"]', '[[\"0\",\"1\",\"52\",\"\"],[\"1\",\"1\",\"54\",\"\"]]', 0),
(75, 18, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/d5135fad-6b12-4691-8bd6-614ae03e2bde/MenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/1fdce256-fcef-446f-9e57-8d53b40ef9ba/MenSneakers.jpg\"]', '[[\"0\",\"1\",\"51\",\"\"],[\"1\",\"1\",\"44\",\"\"]]', 0),
(76, 26, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/ffa84b53-5ca3-4e50-ae93-1e8abe68ac35/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/838c5f9f-8d72-4059-b3b1-4ceb46068f81/MenAccessories.jpg\"]', '[[\"0\",\"1\",\"61\",\"\"],[\"1\",\"1\",\"65\",\"\"]]', 0),
(77, 18, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/5710c94f-dbcb-4050-a7b2-174cc814e38d/MenTshirts.jpg\",\"./vendor/fineuploader/php-traditional-server/files/12f2e60a-9fba-4f52-ad61-165524ac55cf/MenSports.jpg\"]', '[[\"0\",\"1\",\"171\",\"\"],[\"1\",\"1\",\"54\",\"\"]]', 0),
(78, 26, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/fefd976c-93c8-485c-a01c-21dee16a46bc/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/8aa459c1-9664-41a1-b6cc-b60be4151424/MenSwimwear.jpg\"]', '[[\"0\",\"1\",\"43\",\"\"],[\"1\",\"1\",\"64\",\"\"]]', 0),
(79, 18, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/cc03d5d0-ad7a-4944-835e-0d7c4cf90f72/MenDenim.jpg\",\"./vendor/fineuploader/php-traditional-server/files/9cf3b3fc-4929-4efc-9802-18e994ae213a/MenAccessories.jpg\"]', '[[\"0\",\"1\",\"61\",\"\"],[\"1\",\"1\",\"65\",\"\"]]', 0),
(80, 18, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/7b5ec7b2-d12e-46ee-8774-ed094af4e5fa/MenShoes.jpg\",\"./vendor/fineuploader/php-traditional-server/files/19cfb688-57d3-4427-b209-a0dbb5e53460/MenSwimwear.jpg\"]', '[[\"0\",\"1\",\"43\",\"\"],[\"1\",\"1\",\"64\",\"\"]]', 0),
(81, 28, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/d672cc87-eb41-41be-9c1b-041e8785b89f/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/e6034c2e-7662-4ca7-9ab6-3ec079db30af/MenSneakers.jpg\"]', '[[\"0\",\"1\",\"51\",\"\"],[\"1\",\"1\",\"44\",\"\"]]', 0),
(82, 28, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/2893f931-d830-43ee-ac9d-553627f1e57f/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/b4ae433a-23d6-4135-8023-6b435249144a/MenSports.jpg\"]', '[[\"0\",\"1\",\"52\",\"\"],[\"1\",\"1\",\"54\",\"\"]]', 0),
(83, 19, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/1d11a62e-b161-4e68-855b-965c454df55f/WomenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/a4e047c0-94da-41cc-863f-b11fbd163e48/WomenShoes.jpg\"]', '[[\"0\",\"1\",\"7\",\"\"],[\"1\",\"1\",\"6\",\"\"]]', 0),
(84, 19, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/a154e982-aaec-4ee8-ab03-9ea54da07502/WomenDresses.jpg\",\"./vendor/fineuploader/php-traditional-server/files/01209526-c7e8-40ea-8ffb-9f183fe78016/WomenAbayas.jpg\"]', '[[\"0\",\"1\",\"19\",\"\"],[\"1\",\"1\",\"146\",\"\"]]', 0),
(85, 28, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/6ed2fde8-5e39-41ad-85b5-421fa08cd6b4/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/95a02340-50e8-4453-a9a3-dcfeaa589bda/MenAccessories.jpg\"]', '[[\"0\",\"1\",\"61\",\"\"],[\"1\",\"1\",\"65\",\"\"]]', 0),
(86, 19, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/bf808570-f9db-41ab-b03c-d613b481b752/WomenDenim.jpg\",\"./vendor/fineuploader/php-traditional-server/files/189312fd-39a7-48dd-a516-dec37101e479/WomenBags.jpg\"]', '[[\"0\",\"1\",\"33\",\"\"],[\"1\",\"1\",\"9\",\"\"]]', 0),
(87, 28, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/87336169-c2bc-4955-b258-a69cdddb4e46/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/fe57bc70-882c-4566-9d14-8d574f7fd592/MenSwimwear.jpg\"]', '[[\"0\",\"1\",\"43\",\"\"],[\"1\",\"1\",\"64\",\"\"]]', 0),
(88, 19, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/e68312cf-8fa2-4c4e-b517-87f76bca5cf1/WomenAccessories.jpg\",\"./vendor/fineuploader/php-traditional-server/files/9293c73b-33f1-4f40-b049-3fb5a4e9bc87/WomenSports.jpg\"]', '[[\"0\",\"1\",\"8\",\"\"],[\"1\",\"1\",\"22\",\"\"]]', 0),
(89, 30, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/7f70a652-e9d5-4de5-8a88-1a2914749fe6/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/6ce7ca96-ff0b-418c-9e75-4c2d60a33b36/MenSneakers.jpg\"]', '[[\"0\",\"1\",\"51\",\"\"],[\"1\",\"1\",\"44\",\"\"]]', 0),
(90, 30, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/409a0106-1208-47c8-8855-a2cfc8ac3c96/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/94fafd7c-3435-4d9e-9a91-3710276c59c1/MenSports.jpg\"]', '[[\"0\",\"1\",\"52\",\"\"],[\"1\",\"1\",\"54\",\"\"]]', 0),
(91, 30, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/77abc987-7602-4201-8c93-23185726fb5c/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/0f15500b-c43a-41db-80b1-5ab38c02cba8/MenAccessories.jpg\"]', '[[\"0\",\"1\",\"61\",\"\"],[\"1\",\"1\",\"65\",\"\"]]', 0),
(92, 30, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/f4380f1a-ff67-4d2c-9e0c-ec9ff4c40906/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/b7a5c30e-4b15-44d3-bc60-01cbfd1c7209/MenSwimwear.jpg\"]', '[[\"0\",\"1\",\"43\",\"\"],[\"1\",\"1\",\"64\",\"\"]]', 0),
(93, 32, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/b37720c3-315b-4d70-aeab-6e6f884d066d/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/b6e60504-5c2b-4eff-86be-9c6b54272e77/MenSneakers.jpg\"]', '[[\"0\",\"1\",\"51\",\"\"],[\"1\",\"1\",\"44\",\"\"]]', 0),
(94, 34, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/60947049-057f-4bc8-b5ba-aa0cc95e7982/MenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/6d7b6734-ac48-478a-811c-8078518c8bce/MenSneakers.jpg\"]', '[[\"0\",\"1\",\"51\",\"\"],[\"1\",\"1\",\"44\",\"\"]]', 0),
(95, 32, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/bb90f6f5-4602-46f8-a99e-a77d178818a7/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/db0d7d15-2c04-4c48-bccf-cb67cad2e3af/MenSports.jpg\"]', '[[\"0\",\"1\",\"52\",\"\"],[\"1\",\"1\",\"54\",\"\"]]', 0),
(96, 34, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/fc832c05-c555-44dd-8ddd-6a255256c5c1/MenTshirts.jpg\",\"./vendor/fineuploader/php-traditional-server/files/1829f351-79b0-48a2-8e8a-126774435efc/MenSports.jpg\"]', '[[\"0\",\"1\",\"171\",\"\"],[\"1\",\"1\",\"54\",\"\"]]', 0),
(97, 32, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/47c9db03-200d-4609-8907-4fbce7b0f4e1/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/52e3202e-2a9e-4b87-8711-00a21e233028/MenAccessories.jpg\"]', '[[\"0\",\"1\",\"61\",\"\"],[\"1\",\"1\",\"65\",\"\"]]', 0),
(98, 34, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/39250e91-69e9-4944-b9ce-470cd05a8a59/MenDenim.jpg\",\"./vendor/fineuploader/php-traditional-server/files/20f98293-deba-4ca3-9453-170745513c3f/MenAccessories.jpg\"]', '[[\"0\",\"1\",\"61\",\"\"],[\"1\",\"1\",\"65\",\"\"]]', 0),
(99, 32, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/4e831bf8-cb7c-41f3-92f7-b45f2f402d91/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/11421d45-19c1-403c-a84b-576c63caed73/MenSwimwear.jpg\"]', '[[\"0\",\"1\",\"43\",\"\"],[\"1\",\"1\",\"64\",\"\"]]', 0),
(100, 34, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/65568a37-cc06-4ab8-838c-318d521c4e9a/MenShoes.jpg\",\"./vendor/fineuploader/php-traditional-server/files/2458c36b-9d94-4a1c-b668-38f4c8f50541/MenSwimwear.jpg\"]', '[[\"0\",\"1\",\"43\",\"\"],[\"1\",\"1\",\"64\",\"\"]]', 0),
(101, 35, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/6e646903-47aa-4236-b53a-c7497970b46d/WomenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/18974111-12ee-4d13-ac30-d3fbed596a07/WomenShoes.jpg\"]', '[[\"0\",\"1\",\"7\",\"\"],[\"1\",\"1\",\"6\",\"\"]]', 0),
(102, 23, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/be82a0b7-386f-4a54-86c4-8abee2164643/WomenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/5020367d-aa59-4327-a826-d58ea406326d/WomenShoes.jpg\"]', '[[\"0\",\"1\",\"7\",\"\"],[\"1\",\"1\",\"6\",\"\"]]', 0),
(103, 35, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/de77be69-22f9-4f39-b031-31257408518a/WomenDresses.jpg\",\"./vendor/fineuploader/php-traditional-server/files/898258d5-100c-400f-b636-cda9617ca94e/WomenAbayas.jpg\"]', '[[\"0\",\"1\",\"19\",\"\"],[\"1\",\"1\",\"146\",\"\"]]', 0),
(104, 23, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/31ae5801-cc17-4068-bd8d-8df2a7f73300/WomenDresses.jpg\",\"./vendor/fineuploader/php-traditional-server/files/ac4965d7-cce2-4735-b7bf-f2c9db9b002d/WomenAbayas.jpg\"]', '[[\"0\",\"1\",\"19\",\"\"],[\"1\",\"1\",\"146\",\"\"]]', 0),
(105, 35, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/1b4436d1-010e-434e-82b1-19bf2915acc6/WomenDenim.jpg\",\"./vendor/fineuploader/php-traditional-server/files/843a0df2-a938-4469-955c-b706d8d58e9c/WomenBags.jpg\"]', '[[\"0\",\"1\",\"33\",\"\"],[\"1\",\"1\",\"9\",\"\"]]', 0),
(106, 35, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/7b87dbef-01ab-4153-9185-9738623522fd/WomenAccessories.jpg\",\"./vendor/fineuploader/php-traditional-server/files/1dcc4977-15e5-4185-8d01-cd3f40404060/WomenSports.jpg\"]', '[[\"0\",\"1\",\"8\",\"\"],[\"1\",\"1\",\"22\",\"\"]]', 0),
(107, 23, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/8054d7d4-485d-4844-baca-dfdb5dde5568/WomenJeans.jpg\",\"./vendor/fineuploader/php-traditional-server/files/b67b3ea5-362f-4ece-8cfc-b1d457f13a86/WomenBags.jpg\"]', '[[\"0\",\"1\",\"33\",\"\"],[\"1\",\"1\",\"9\",\"\"]]', 0),
(108, 23, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/a7b140de-0a80-4436-b5dc-eb8fb6fd7fb0/WomenAccessories.jpg\",\"./vendor/fineuploader/php-traditional-server/files/2405941f-b2a8-429b-a6d7-44a1de4f2e3a/WomenSports.jpg\"]', '[[\"0\",\"1\",\"8\",\"\"],[\"1\",\"1\",\"22\",\"\"]]', 0),
(109, 25, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/e170f354-96b1-40e3-a873-25342bba891c/WomenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/4f1251be-b18d-4240-8375-07a4e20d898e/WomenShoes.jpg\"]', '[[\"0\",\"1\",\"7\",\"\"],[\"1\",\"1\",\"6\",\"\"]]', 0),
(110, 25, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/23c88f81-50e0-4c8a-8eab-0f30af3bc37b/WomenDresses.jpg\",\"./vendor/fineuploader/php-traditional-server/files/dad2d4e8-0d9c-48c9-9f58-30a41acfc887/WomenAbayas.jpg\"]', '[[\"0\",\"1\",\"19\",\"\"],[\"1\",\"1\",\"146\",\"\"]]', 0),
(111, 25, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/640607ee-4db6-44c4-99af-040661e8d443/WomenJeans.jpg\",\"./vendor/fineuploader/php-traditional-server/files/8fb23b18-71bd-4488-9025-a787577f2956/WomenBags.jpg\"]', '[[\"0\",\"1\",\"33\",\"\"],[\"1\",\"1\",\"9\",\"\"]]', 0),
(112, 25, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/8a61eee7-8e07-4dab-a934-c8a12da1271a/WomenAccessories.jpg\",\"./vendor/fineuploader/php-traditional-server/files/d2c8f3a0-dc5d-4314-ab2d-2117f02051ce/WomenSports.jpg\"]', '[[\"0\",\"1\",\"8\",\"\"],[\"1\",\"1\",\"22\",\"\"]]', 0),
(113, 27, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/b1cd730e-e660-4d77-a552-325c540e2ad6/WomenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/45a8bfcf-098b-49b5-874d-7b6c5e4a2d7c/WomenShoes.jpg\"]', '[[\"0\",\"1\",\"7\",\"\"],[\"1\",\"1\",\"6\",\"\"]]', 0),
(114, 27, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/21e32cd1-c7d6-46a6-8e79-5b5da2ae54f6/WomenDresses.jpg\",\"./vendor/fineuploader/php-traditional-server/files/a0a2a497-b582-4ac3-8d5d-8f3d17ed0866/WomenAbayas.jpg\"]', '[[\"0\",\"1\",\"19\",\"\"],[\"1\",\"1\",\"146\",\"\"]]', 0),
(115, 27, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/1028af23-974b-438d-97a6-5e288fd1a517/WomenJeans.jpg\",\"./vendor/fineuploader/php-traditional-server/files/9cee3c9f-5873-4d24-9aa0-5a3b07e46a78/WomenBags.jpg\"]', '[[\"0\",\"1\",\"33\",\"\"],[\"1\",\"1\",\"9\",\"\"]]', 0),
(116, 27, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/e8687a8d-96bf-4a7c-9cf9-fb82d41dab06/WomenAccessories.jpg\",\"./vendor/fineuploader/php-traditional-server/files/e4433317-dd58-4814-afc7-6e7f321c5095/WomenSports.jpg\"]', '[[\"0\",\"1\",\"8\",\"\"],[\"1\",\"1\",\"22\",\"\"]]', 0),
(117, 29, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/92e5d832-bce6-4198-a061-cd037663f6d8/WomenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/f1ba90db-df92-45a9-be80-e7f53ccaa089/WomenShoes.jpg\"]', '[[\"0\",\"1\",\"7\",\"\"],[\"1\",\"1\",\"6\",\"\"]]', 0),
(118, 29, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/ca6724f2-c607-4120-874a-fac440b37f23/WomenDresses.jpg\",\"./vendor/fineuploader/php-traditional-server/files/34e81313-6132-4d10-9c99-24905f7d97f9/WomenAbayas.jpg\"]', '[[\"0\",\"1\",\"19\",\"\"],[\"1\",\"1\",\"146\",\"\"]]', 0),
(119, 29, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/a97c457f-091a-407e-80b5-8bf1fc0b8dec/WomenJeans.jpg\",\"./vendor/fineuploader/php-traditional-server/files/de088a1b-5fa3-4e31-8607-1a7a21e14a98/WomenBags.jpg\"]', '[[\"0\",\"1\",\"33\",\"\"],[\"1\",\"1\",\"9\",\"\"]]', 0),
(120, 29, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/f9a932a6-99db-4e01-80e9-3a7c5c494f3d/WomenAccessories.jpg\",\"./vendor/fineuploader/php-traditional-server/files/feaedf9d-9137-4f1c-ab88-00e0b35b35fb/WomenSports.jpg\"]', '[[\"0\",\"1\",\"8\",\"\"],[\"1\",\"1\",\"22\",\"\"]]', 0),
(121, 31, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/9ea0fb80-1e01-4dd5-afac-7f90d2a32260/WomenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/6bf6dfbf-ebee-493e-8fe0-a2a86d997d70/WomenShoes.jpg\"]', '[[\"0\",\"1\",\"7\",\"\"],[\"1\",\"1\",\"6\",\"\"]]', 0),
(122, 31, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/6b4c965e-4321-4539-bb8f-d4fa060c594f/WomenDresses.jpg\",\"./vendor/fineuploader/php-traditional-server/files/38453b1a-f99d-4a5b-b09d-8cdd035c4a31/WomenAbayas.jpg\"]', '[[\"0\",\"1\",\"19\",\"\"],[\"1\",\"1\",\"146\",\"\"]]', 0),
(123, 31, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/0cb2021f-850d-42c2-9c4b-0f5dcca80920/WomenJeans.jpg\",\"./vendor/fineuploader/php-traditional-server/files/c9e18f18-b740-453e-bb24-93b51cd6433c/WomenBags.jpg\"]', '[[\"0\",\"1\",\"33\",\"\"],[\"1\",\"1\",\"9\",\"\"]]', 0),
(124, 31, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/73257a5b-1649-4b3a-a1df-30ed89df95cf/WomenAccessories.jpg\",\"./vendor/fineuploader/php-traditional-server/files/2035a9b5-60d5-4a2f-b38f-191109a34626/WomenSports.jpg\"]', '[[\"0\",\"1\",\"8\",\"\"],[\"1\",\"1\",\"22\",\"\"]]', 0),
(125, 33, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/5cf4fb2f-f277-4c6a-94f8-d43e97d0e44d/WomenClothing.jpg\",\"./vendor/fineuploader/php-traditional-server/files/0aeb7a82-da43-48a6-8c8a-6490e08e344e/WomenShoes.jpg\"]', '[[\"0\",\"1\",\"7\",\"\"],[\"1\",\"1\",\"6\",\"\"]]', 0),
(126, 33, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/951a8009-51ea-433f-bf98-efa3416cb193/WomenDresses.jpg\",\"./vendor/fineuploader/php-traditional-server/files/472196e1-1e5a-4ef2-88c1-7ee2aa3d525d/WomenAbayas.jpg\"]', '[[\"0\",\"1\",\"19\",\"\"],[\"1\",\"1\",\"146\",\"\"]]', 0),
(127, 33, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/ebb2ddcb-8d8b-4a5e-96a1-ea3425d76791/WomenJeans.jpg\",\"./vendor/fineuploader/php-traditional-server/files/226072ed-3709-4c12-902b-9224460bc8b7/WomenBags.jpg\"]', '[[\"0\",\"1\",\"33\",\"\"],[\"1\",\"1\",\"9\",\"\"]]', 0),
(128, 33, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/0048d42b-4142-4204-85fb-b5e9127c1baa/WomenAccessories.jpg\",\"./vendor/fineuploader/php-traditional-server/files/63d69df5-9113-4111-9e0b-6b34aa101950/WomenSports.jpg\"]', '[[\"0\",\"1\",\"8\",\"\"],[\"1\",\"1\",\"22\",\"\"]]', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_devices`
--

CREATE TABLE `tbl_devices` (
  `id` int(11) NOT NULL,
  `token` text,
  `type` text,
  `deviceuid` varchar(100) DEFAULT NULL,
  `devicename` varchar(100) DEFAULT NULL,
  `devicemodel` varchar(100) DEFAULT NULL,
  `deviceversion` varchar(100) DEFAULT NULL,
  `appname` varchar(100) DEFAULT NULL,
  `appversion` varchar(100) DEFAULT NULL,
  `mode` varchar(20) DEFAULT NULL,
  `app_id` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_devices`
--

INSERT INTO `tbl_devices` (`id`, `token`, `type`, `deviceuid`, `devicename`, `devicemodel`, `deviceversion`, `appname`, `appversion`, `mode`, `app_id`) VALUES
(1, 'dtsHUXjag8Q:APA91bEe0lSDvjcMSRvic-rNOBCcjRUTNNGjxmBLn2LIgWgsdo6QHBcr4ywzpHvptW8AuHtPOcczPy23POMnWOWEi-RJlRqT_xHx9uTzOBm2TwGIMpyEoMr6AEBYh7a3uL0D-6I-hl5U', 'android', NULL, 'samsung', 'GT-N7100', '4.4.2', 'ELABELZ', '1.0.3', 'production', '1'),
(2, '37FA0873903F6A713F3FC5AB7A3FC0BC65E78CE066D59E4AF62F88B5D6677AC4', 'ios', NULL, NULL, NULL, NULL, NULL, NULL, 'development', '15'),
(3, 'd6McU6I2baQ:APA91bH6j3RQCwD_9VQXXFo_-aiVrF0YJ6SQTB_J-9zQut8bXF5OMMhvejMsIIAt-esivVon_GdqE_qI92KsBQfcASDEfBu7E_Sgd1vgPCG6Oi5ANnp5AqUlXBJcMjEAbFyOBflJwYaI', 'android', '', 'samsung', 'SM-A710F', '7.0', 'ELABELZ', '1.0.5.2', NULL, '15'),
(4, 'cfEajBafLiM:APA91bFMKDP4pNPE9XYfCT6hRQW_4ZVcypwuSgsb1i3uHs3Rj1r-uN6JU9OCZz3CBgsJ3BQqPR0Ufe44VMb2wDm1SImCaIhPdxxp3iyl37h1ciE1CB9mZqu-ehOYyS3-PBllN4Dt8qdg', 'android', '', 'samsung', 'SM-A710F', '7.0', 'ELABELZ', '1.0.5.2', NULL, '1'),
(5, 'dQx8nOQwMVo:APA91bHC46hYtZTY0Dqsd-slKSD3p3jsO8xl6miz-0oCnKkCwIE3_PDdCbpvt0WxDB9BekmEJqxd6sl2oI4JkW17QUhuzFnYnk30Aho71EwlY7VOJfale0ZfFCr2ffVtiHy6lUldQfTg', 'android', '', 'samsung', 'SM-A710F', '7.0', 'ELABELZ', '1.0.5.3', NULL, '1'),
(6, 'fIig7nfq7e8:APA91bHy8tqWusYfRYhH-8qi0fZ_rXqmDjN2wJ-cz5_hyE_ZTSnjZglwj2vxvodtd-QG7Oq2WomDgoPUOMgzcavfRyQnJqYXy8hPU7XMAFjwayGFP8U-IVc8bcKK2kfIfcqRA51MFk_x', 'android', '', 'samsung', 'SM-A710F', '7.0', 'ELABELZ', '1.0.5.3', NULL, '1'),
(7, 'c8FAWcfwz28:APA91bE45kvRZbO5Ug9E5pPKRp9xLK3qiY1bhEuvOA2O352nkIgzF5Qktui2SftDblHXLB7D1ClDo75OpcOCQXpRZ-b4UL8gfopiOnqIDtZ-P6Fc-FhQ8QAzlZF7qdQSPfwa7h-O5FoY', 'android', '', 'samsung', 'SM-A710F', '7.0', 'ELABELZ', '1.0.5.3', NULL, '1'),
(8, 'emuOVD-cmNY:APA91bGl_6DGIeNW2m38JixIDHZeBWTjmsxw4jZm_SlIozcEIy5HncPhiZKvryung_yu49EWyvGhk7o8ULGKqSas3BH9-LdoX9NyQzUHFMIDz4I0gJvmmhlspw7RZtW4k83SI0GNphnv', 'android', '', 'samsung', 'SM-A710F', '7.0', 'ELABELZ', '1.0.5.3', NULL, '1'),
(9, 'cT79Oqm2tNY:APA91bG70KsUkfWJFFtagLvbg_k60TP_-YVRy4DVPp_2G-Dy0d_JuRqeDNyjfxgFDZC2FzEtEwQFh14V996uTtf4mF_Fr9iuUUFr7UI2iAew1OFtD_R-AK9akH8n5wQ_c_L7Pu0wYxTn', 'android', '', 'samsung', 'SM-A710F', '7.0', 'ELABELZ', '1.0.5.3', NULL, '15'),
(10, 'e2Sb9S4bAuE:APA91bGWdOpED2BZjuYGS5hRE5CKuIkhVhY6AN-uLWXReB1Uw1SgEa2EQ5MbH1xmHKwog_1ajCZsvgaFlo42jehh6VV5O7gy4351b1tkg7EKeDklyUSngOInKy4SVGAWVI5yxO5qWj7p', 'android', '', 'samsung', 'SM-G950F', '7.0', 'ELABELZ', '1.0.5.3', NULL, '1'),
(11, 'BE7A8EAC5DD128DBC2B2689CA22002367A3B49E1C8D5487EE77B9D176390BF76', 'ios', NULL, NULL, NULL, NULL, NULL, NULL, 'development', '1'),
(12, 'cYLYzlR0tC4:APA91bH1LVNwTYF7SYk1yKyT13zraSqrh2x34kGKd3mToOkogmjIfyi8ngjOTymvYJANY-L2CVo4XAZNSDH8oi1so4_Q_3Pn4qslkeau52A2ru7t_wkfrrXAWmbeIBG8MXtwQjjZ8dPe', 'android', '', 'Huawei', 'ALE-L21', '6.0', 'ELABELZ', '1.0.6', NULL, '1'),
(13, 'dxQhyfRGypc:APA91bG7nSMb5a__mvvFM0bWFDo5J_dwuE-o8z77LHbdzVxa9sMSYQ-qc_m_u520y2gN2jM6JQJZkLpw6ANDu6xpgUgqkSkY7ie61BUocXhrQRNaWyZbqR5FaCxUpDPttgN1dS7IocW9', 'android', '', 'Huawei', 'ALE-L21', '6.0', 'ELABELZ', '1.0.6', NULL, '1'),
(14, 'emJeWqbr9j8:APA91bHF1HtlqGMYLp9YXaLexWF8sy7crMhWZGR7n8hmrZ-HoYLVpmsJdWAUP5UDEVbkmDerDu2WCOzEe6ZTW_78p8oiKNzGy_8DxccwL0YfZw-YaClaVexbRXglOMH6aMgRDHMao7WA', 'android', '', 'Huawei', 'ALE-L21', '6.0', 'ELABELZ', '1.0.7', NULL, '1'),
(15, 'frQhQmd6YKo:APA91bGD7Ap1VHuuYqALwVEz576QtnjFYsjy5mNtUCIPV91qjkGDc3TEqOExAjshDRq8UbsTUkTVjgwTgCRkW6j4o4EGnxgPcO8MTTsaxyxjwa2GinsdQLvldqzXevsLCWHkuZ9UaW60', 'android', '', 'Huawei', 'ALE-L21', '6.0', 'ELABELZ', '1.0.7', NULL, '1'),
(16, 'fDNkOJeBgZc:APA91bEYgDxxqw9I35Fsi7R1hmBL0fS8f3mNpHdQT2DpxluZqouvBVU98TtCT6a4oTI20Q0nHQZ0-jZjAIoz5WkWNnbBiGBCwqwSTJ33CX8-3qHLZ_PltYDPCQ0lCPCyu26NeiWp_Q_7', 'android', '', 'Huawei', 'ALE-L21', '6.0', 'ELABELZ', '1.0.7', NULL, '1'),
(17, 'c8-rHaBQwP0:APA91bEz4PbinPg6oII3wVQITdHfbk3HgP04B7GC33aAHd1QPST3joVjT_ctX9c2x1TYcHojTEBWu1Qoi1S2CQueh6gsIJWHr5OYs1MR3Hgd28NvrlO5PMzngBS5GMAnb5x8BmzKQQiT', 'android', '', 'Huawei', 'ALE-L21', '6.0', 'ELABELZ', '1.0.7', NULL, '1'),
(18, 'fy6AfIb5G3o:APA91bHp9MJoafckHHAf7Fg-1KtA4BoDL5oEhb-GuyqCGH7-9L2Fhwgz_FGbaeuPcRFq5XWto0bfSH6vN2SmqIh4SqBckMXmIJyiamIGwE0TeBs1gOU3jEzy6K78m3cM12dzz-vJHrTo', 'android', '', 'samsung', 'SM-A710F', '7.0', 'ELABELZ', '1.0.7', NULL, '15'),
(19, 'eA3EOKqpAkw:APA91bHqO3fTm24EH9xiZncLhYh01GXmP-r0aFsyu7HOlTXvoRLLTsF_12TlgVpKXeDMA176AM8vYo3dSwTXRjJgPQCIqft6OYISmzvt8GuKZAuUeqI8Dj5BJT0nrveRgxk3Anm6gnLz', 'android', '', 'Huawei', 'ALE-L21', '6.0', 'ELABELZ', '1.0.7', NULL, '1'),
(20, 'ftp6EK7KW2Y:APA91bGCvPJwzFzEkn9lGy17_BzcbN6QW0MJZfCp-MLerVU3POZQrjvutuvQPNyXRBeOOraAG3QD7DjSm1qfvBWepwwxKGe5NXrGDRwOGFI6NJMBxa-_toIqAvmvfrQ0P8LnbM0g2ZVw', 'android', '', 'Huawei', 'ALE-L21', '6.0', 'ELABELZ', '1.0.7', NULL, '1'),
(21, 'cIrWElHnOv0:APA91bGAc2xslOJYJyDQrw12N0ouSzEM7DlKDAq4S6dwmUmNrEqZ5dIu2KZ6c65BWbQKGeCjDG_09M2PBAh4Aq-vXYrtK_grDbem47fKZ8_Avq8l5JnYkvFcdv-DmNUq0DhIUBFM_p9l', 'android', '', 'Huawei', 'ALE-L21', '6.0', 'ELABELZ', '1.0.7', NULL, '1'),
(22, 'dplXv8f6Bgw:APA91bFIwZWDqWuqd34YNbG020G0mB-rm0oFyD_u1ACAC-Ftwsln0Re_2_wHZLkDxlE2gUMOJB_SolVkO9SHGahyw23uVOkTYjKhSU81bd6BlHYs5MOxe2m23qEAsTvwdJaruFfglr9r', 'android', '', 'Huawei', 'ALE-L21', '6.0', 'ELABELZ', '1.0.7', NULL, '1'),
(23, 'fjIpUwEwLYg:APA91bEga-RiY8LCp8KIwcLTiRJgC8i5ehZtvIXClEIa-TBKeJDmoZb_hKCtwYVg9TG3e_pt0JemuDJwUrBUjVMulGIvSsCD4NBnjRmhlfKjgvldABMy6mVJjsMKSLIka1tzsFTd-VSC', 'android', '', 'Huawei', 'ALE-L21', '6.0', 'ELABELZ', '1.0.7', NULL, '1'),
(24, 'cYImhPYqegY:APA91bFkZ4Yk2jEADGEDtEGWn4ts5UkumFTxzVwitLoNbz_hCF0t_8V1e30odZx42F4dIxpbNy3KqDYmIERUF0sp9nwkJnBVb5O2ia_1Ud5CrNrtBJ8T8WjtAA16CaYl81VT444ygIRl', 'android', '', 'Huawei', 'ALE-L21', '6.0', 'ELABELZ', '1.0.7', NULL, '1'),
(25, 'dvCZakUXaAw:APA91bHNIHum1QjISRSKTAfzpnsz5hzarKsRcKUGn9sxBIgVe1IKBATBfKCcRUULN57eufDO6qTLTlMlBloffNa8HEZJ08JaORxORD3rFOMlcW8J_qvhFIvM5V7wNRxCawp5rWf4VDl1', 'android', '', 'Huawei', 'ALE-L21', '6.0', 'ELABELZ', '1.0.7', NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_merchandise`
--

CREATE TABLE `tbl_merchandise` (
  `id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_merchandise`
--

INSERT INTO `tbl_merchandise` (`id`, `app_id`, `title`, `status`) VALUES
(23, 1, '', 0),
(25, 3, '', 1),
(28, 8, '', 1),
(29, 4, '', 1),
(30, 9, '', 1),
(31, 10, '', 1),
(33, 11, '', 1),
(34, 5, '', 1),
(35, 12, '', 1),
(36, 13, '', 1),
(37, 14, '', 1),
(38, 6, '', 1),
(39, 7, '', 1),
(40, 15, '', 1),
(41, 1, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notifications`
--

CREATE TABLE `tbl_notifications` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_scheduled` enum('0','1') NOT NULL DEFAULT '0',
  `schedule_date` varchar(255) DEFAULT NULL,
  `schedule_time` varchar(255) DEFAULT NULL,
  `attachment` varchar(255) NOT NULL,
  `apps` varchar(255) NOT NULL,
  `is_sent` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_notifications`
--

INSERT INTO `tbl_notifications` (`id`, `title`, `message`, `date_time`, `is_scheduled`, `schedule_date`, `schedule_time`, `attachment`, `apps`, `is_sent`) VALUES
(1, 'Test', 'Test Product', '2017-05-26 15:38:42', '0', NULL, NULL, '[\"2\",\"113085\",\"Mid Length Shorts\"]', 'all,and-1,and-3,and-4,and-5,and-6,and-7,and-8,and-9,and-10,and-11,and-12,and-13,and-14,and-15,ios-1,ios-3,ios-4,ios-5,ios-6,ios-7,ios-8,ios-9,ios-10,ios-11,ios-12,ios-13,ios-14,ios-15,android,ios', '1'),
(2, 'Test', 'Test product', '2017-05-26 15:39:38', '0', NULL, NULL, '[\"2\",\"61308\",\"Wide Leg Pants\"]', 'ios,ios-1,ios-3,ios-4,ios-5,ios-6,ios-7,ios-8,ios-9,ios-10,ios-11,ios-12,ios-13,ios-14,ios-15', '1'),
(3, 'test', 'Test product', '2017-05-26 15:45:10', '0', NULL, NULL, '[\"2\",\"113085\",\"Mid Length Shorts\"]', 'ios,ios-1,ios-3,ios-4,ios-5,ios-6,ios-7,ios-8,ios-9,ios-10,ios-11,ios-12,ios-13,ios-14,ios-15', '1'),
(4, 'test', 'Test product', '2017-05-26 15:53:04', '0', NULL, NULL, '[\"2\",\"113085\",\"Mid Length Shorts\"]', 'ios,ios-1,ios-3,ios-4,ios-5,ios-6,ios-7,ios-8,ios-9,ios-10,ios-11,ios-12,ios-13,ios-14,ios-15', '1'),
(5, 'Test', 'Test message', '2017-05-26 15:54:18', '0', NULL, NULL, '[\"2\",\"113085\",\"Mid Length Shorts\"]', 'ios,ios-1,ios-3,ios-4,ios-5,ios-6,ios-7,ios-8,ios-9,ios-10,ios-11,ios-12,ios-13,ios-14,ios-15,android,and-1,and-3,and-4,and-5,and-6,and-7,and-8,and-9,and-10,and-11,and-12,and-13,and-14,and-15,all', '1'),
(6, 'test', 'Elabelz', '2017-05-29 08:02:12', '0', NULL, NULL, '[\"1\",\"288\",\"Abayas\"]', 'all,and-1,and-3,and-4,and-5,and-6,and-7,and-8,and-9,and-10,and-11,and-12,and-13,and-14,and-15,ios-1,ios-3,ios-4,ios-5,ios-6,ios-7,ios-8,ios-9,ios-10,ios-11,ios-12,ios-13,ios-14,ios-15,android,ios', '1'),
(7, 'Hello', 'Test message Android', '2017-05-29 09:30:57', '0', NULL, NULL, '[\"1\",\"288\",\"Abayas\"]', 'android,and-1,and-3,and-4,and-5,and-6,and-7,and-8,and-9,and-10,and-11,and-12,and-13,and-14,and-15', '1'),
(8, 'test', 'test message', '2017-05-29 09:33:24', '0', NULL, NULL, '[\"1\",\"288\",\"Abayas\"]', 'android,and-1,and-3,and-4,and-5,and-6,and-7,and-8,and-9,and-10,and-11,and-12,and-13,and-14,and-15', '1'),
(9, 'test', 'test static', '2017-05-29 09:36:35', '0', NULL, NULL, '[\"4\",\"Static Image\"]', 'and-1,and-3,and-4,and-5,and-6,and-7,and-8,and-9,and-10,and-11,and-12,and-13,and-14,and-15,android', '1'),
(10, 'test', 'test category', '2017-05-29 09:37:22', '0', NULL, NULL, '[\"1\",\"288\",\"Abayas\"]', 'android,and-1,and-3,and-4,and-5,and-6,and-7,and-8,and-9,and-10,and-11,and-12,and-13,and-14,and-15', '1'),
(11, 'test', 'test product', '2017-05-29 09:39:07', '0', NULL, NULL, '[\"2\",\"12038\",\" Analog Leather Watch\"]', 'android,and-1,and-3,and-4,and-5,and-6,and-7,and-8,and-9,and-10,and-11,and-12,and-13,and-14,and-15', '1'),
(12, 'tet', 'test release', '2017-05-29 10:04:59', '0', NULL, NULL, '[\"1\",\"288\",\"Abayas\"]', 'android,and-1,and-3,and-4,and-5,and-6,and-7,and-8,and-9,and-10,and-11,and-12,and-13,and-14,and-15', '1'),
(13, 'TestTitle', 'Message', '2017-05-29 12:28:14', '0', NULL, NULL, '[\"1\",\"288\",\"Abayas\"]', 'android,and-1,and-3,and-4,and-5,and-6,and-7,and-8,and-9,and-10,and-11,and-12,and-13,and-14,and-15', '1'),
(14, 'TEST', 'Message', '2017-05-29 12:29:19', '0', NULL, NULL, '[\"4\",\"Static Image\"]', 'android,and-1,and-3,and-4,and-5,and-6,and-7,and-8,and-9,and-10,and-11,and-12,and-13,and-14,and-15,ios,ios-1,ios-3,ios-4,ios-5,ios-6,ios-7,ios-8,ios-9,ios-10,ios-11,ios-12,ios-13,ios-14,ios-15,all', '1'),
(15, 'TEST', 'send notification', '2017-05-29 12:38:24', '0', NULL, NULL, '[\"1\",\"288\",\"Abayas\"]', 'android,and-1,and-3,and-4,and-5,and-6,and-7,and-8,and-9,and-10,and-11,and-12,and-13,and-14,and-15', '1'),
(16, 'Title', 'message', '2017-05-29 12:54:15', '0', NULL, NULL, '[\"1\",\"562\",\"Cardigans\"]', 'android,and-1,and-3,and-4,and-5,and-6,and-7,and-8,and-9,and-10,and-11,and-12,and-13,and-14,and-15', '1'),
(17, 'test', 'test', '2017-05-29 12:55:07', '0', NULL, NULL, '[\"4\",\"Static Image\"]', 'android,and-1,and-3,and-4,and-5,and-6,and-7,and-8,and-9,and-10,and-11,and-12,and-13,and-14,and-15', '1'),
(18, 'test noor', 'test noor body', '2017-05-29 14:55:13', '0', NULL, NULL, '[\"1\",\"423\",\"Sales\"]', 'all,and-1,and-3,and-4,and-5,and-6,and-7,and-8,and-9,and-10,and-11,and-12,and-13,and-14,and-15,ios-1,ios-3,ios-4,ios-5,ios-6,ios-7,ios-8,ios-9,ios-10,ios-11,ios-12,ios-13,ios-14,ios-15,android,ios', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rows`
--

CREATE TABLE `tbl_rows` (
  `id` int(11) NOT NULL,
  `merchandise_id` int(11) NOT NULL,
  `layout` varchar(255) NOT NULL,
  `images` text NOT NULL,
  `attachment` text NOT NULL,
  `row_order` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_rows`
--

INSERT INTO `tbl_rows` (`id`, `merchandise_id`, `layout`, `images`, `attachment`, `row_order`) VALUES
(45, 23, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/388ec372-896e-49d1-a422-0e292437746f/woMen-Sale.jpg\",\"./vendor/fineuploader/php-traditional-server/files/0966be4d-6b0b-40f5-bab3-eb5821fd030d/Men-Sale.jpg\"]', '[[\"0\",\"1\",\"428\",\"Clothing\"],[\"1\",\"1\",\"432\",\"Clothing\"]]', 1),
(46, 23, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/2f8070a6-1dd0-4b85-9d6f-bdb1be51b993/Menns-Activewear.jpg\",\"./vendor/fineuploader/php-traditional-server/files/4ea3ac92-0b4e-447d-a31b-58b25c90f59c/Womens-Activewear.jpg\"]', '[[\"0\",\"1\",\"54\",\"Sportswear\"],[\"1\",\"1\",\"22\",\"Sportswear\"]]', 0),
(51, 28, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/09d07ebc-92a7-4c43-947b-875aeb78c4ea/woMen-Sale-arab.jpg\",\"./vendor/fineuploader/php-traditional-server/files/6c9dd8e8-48b6-487a-8a37-ee8cce148e21/Men-Sale-rab.jpg\"]', '[[\"0\",\"1\",\"424\",\"Ø§Ù„Ù†Ø³Ø§Ø¡\"],[\"1\",\"1\",\"425\",\"Ø§Ù„Ø±Ø¬Ø§Ù„\"]]', 1),
(52, 28, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/89f2e53a-9fd4-4a7d-9413-3505ccf44ce1/Womens-Activewear-arab.jpg\",\"./vendor/fineuploader/php-traditional-server/files/78e82942-9b95-49ae-be51-ef407c657404/Mens-Activewear-arab.jpg\"]', '[[\"0\",\"1\",\"22\",\"Ù…Ù„Ø§Ø¨Ø³ Ø±ÙŠØ§Ø¶ÙŠØ©\"],[\"1\",\"1\",\"54\",\"Ù…Ù„Ø§Ø¨Ø³ Ø±ÙŠØ§Ø¶ÙŠØ©\"]]', 0),
(53, 25, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/c6ee29b4-1703-4b75-b8ab-a81eaba6f24c/WomensSale.jpg\",\"./vendor/fineuploader/php-traditional-server/files/0ffd6c84-9206-4714-97d4-aa9746a0fa5a/MensSale.jpg\"]', '[[\"0\",\"1\",\"424\",\"Women\"],[\"1\",\"1\",\"425\",\"Men\"]]', 1),
(54, 25, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/20f4e6e3-d662-4104-8444-181db7488264/MensActive.jpg\",\"./vendor/fineuploader/php-traditional-server/files/587e58ea-36d4-4082-8413-1178d9979dbc/WomensActive.jpg\"]', '[[\"0\",\"1\",\"54\",\"Sportswear\"],[\"1\",\"1\",\"22\",\"Sportswear\"]]', 0),
(56, 29, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/e55a4a54-6a5d-4274-8fb3-dcbad0040c2d/WomensSale.jpg\",\"./vendor/fineuploader/php-traditional-server/files/0b1368e3-6421-440d-97cb-5b4df581bf8c/MensSale.jpg\"]', '[[\"0\",\"1\",\"424\",\"Women\"],[\"1\",\"1\",\"425\",\"Men\"]]', 1),
(58, 29, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/3e3d3e18-6120-43bc-9511-767cc8c4c054/MensActive.jpg\",\"./vendor/fineuploader/php-traditional-server/files/f445de83-bb2e-49cc-b5aa-f705c52bb8a5/WomensActive.jpg\"]', '[[\"0\",\"1\",\"54\",\"Sportswear\"],[\"1\",\"1\",\"22\",\"Sportswear\"]]', 0),
(60, 30, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/980debc0-28c1-4d7c-bc42-a335e72dd292/Men-Sale-rab.jpg\",\"./vendor/fineuploader/php-traditional-server/files/d7264ab3-1889-4906-bd16-f7476463932c/woMen-Sale-arab.jpg\"]', '[[\"0\",\"1\",\"432\",\"Ù…Ù„Ø§Ø¨Ø³\"],[\"1\",\"1\",\"428\",\"Ù…Ù„Ø§Ø¨Ø³\"]]', 2),
(61, 30, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/c65292e1-dd4e-467c-9599-3d965353a6b3/Mens-Activewear-arab.jpg\",\"./vendor/fineuploader/php-traditional-server/files/a00c8109-fbb9-4ed9-81a3-3143a3a1d011/Womens-Activewear-arab.jpg\"]', '[[\"0\",\"1\",\"54\",\"Ù…Ù„Ø§Ø¨Ø³ Ø±ÙŠØ§Ø¶ÙŠØ©\"],[\"1\",\"1\",\"22\",\"Ù…Ù„Ø§Ø¨Ø³ Ø±ÙŠØ§Ø¶ÙŠØ©\"]]', 0),
(63, 31, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/d67faf5c-d23f-4415-9eb8-78dac9795d76/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/7e09ba25-5c5b-4893-bac6-c3d1c0867069/sec.jpg\"]', '[[\"0\",\"1\",\"424\",\"Ø§Ù„Ù†Ø³Ø§Ø¡\"],[\"1\",\"1\",\"425\",\"Ø§Ù„Ø±Ø¬Ø§Ù„\"]]', 1),
(64, 31, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/20f2926f-fed0-4b78-b3ac-44d65a549854/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/4a0cc974-1344-42fe-9018-a500b9c1c020/WomensActivewear.jpg\"]', '[[\"0\",\"1\",\"54\",\"Ù…Ù„Ø§Ø¨Ø³ Ø±ÙŠØ§Ø¶ÙŠØ©\"],[\"1\",\"1\",\"22\",\"Ù…Ù„Ø§Ø¨Ø³ Ø±ÙŠØ§Ø¶ÙŠØ©\"]]', 0),
(67, 33, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/aaaa7e35-6dfb-4975-9f9c-542b4bbd1df9/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/0a455e71-e5ae-478e-9570-68f7c9ad025d/sec.jpg\"]', '[[\"0\",\"1\",\"424\",\"Ø§Ù„Ù†Ø³Ø§Ø¡\"],[\"1\",\"1\",\"425\",\"Ø§Ù„Ø±Ø¬Ø§Ù„\"]]', 1),
(68, 33, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/7c9e4c65-a24d-4abd-b0df-2256403ad736/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/41818ac0-6528-4572-83c6-c07200fd07a4/WomensActivewear.jpg\"]', '[[\"0\",\"1\",\"54\",\"Ù…Ù„Ø§Ø¨Ø³ Ø±ÙŠØ§Ø¶ÙŠØ©\"],[\"1\",\"1\",\"22\",\"Ù…Ù„Ø§Ø¨Ø³ Ø±ÙŠØ§Ø¶ÙŠØ©\"]]', 0),
(69, 34, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/c0cd7fdf-1329-4f2c-bf4b-f71fc1e4cb19/WomensSale.jpg\",\"./vendor/fineuploader/php-traditional-server/files/d62c35de-8845-4710-87a5-4c9ce7b5a89f/MensSale.jpg\"]', '[[\"0\",\"1\",\"424\",\"Women\"],[\"1\",\"1\",\"425\",\"Men\"]]', 1),
(71, 34, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/6eab3887-d018-4991-92b5-21c622bd58a8/MensActive.jpg\",\"./vendor/fineuploader/php-traditional-server/files/4bd94998-a393-4456-9441-e8645dea37b0/WomensActive.jpg\"]', '[[\"0\",\"1\",\"54\",\"Sportswear\"],[\"1\",\"1\",\"22\",\"Sportswear\"]]', 2),
(72, 35, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/d4fb28a4-4d7c-430d-b452-46797b6d230e/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/122d5f79-99bb-4c14-ade6-4887a147438a/sec.jpg\"]', '[[\"0\",\"1\",\"424\",\"Ø§Ù„Ù†Ø³Ø§Ø¡\"],[\"1\",\"1\",\"425\",\"Ø§Ù„Ø±Ø¬Ø§Ù„\"]]', 1),
(73, 35, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/2d39f0f4-808c-4800-b2ec-69e1239c5152/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/38035fa9-3977-44d8-9341-dee7e618b817/WomensActivewear.jpg\"]', '[[\"0\",\"1\",\"54\",\"Ù…Ù„Ø§Ø¨Ø³ Ø±ÙŠØ§Ø¶ÙŠØ©\"],[\"1\",\"1\",\"22\",\"Ù…Ù„Ø§Ø¨Ø³ Ø±ÙŠØ§Ø¶ÙŠØ©\"]]', 0),
(75, 36, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/e10a15e6-b8f3-4be1-a898-89425ca44971/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/ab5a90d6-6f93-4ffb-91b2-e98e8f4bd59b/sec.jpg\"]', '[[\"0\",\"1\",\"424\",\"Ø§Ù„Ù†Ø³Ø§Ø¡\"],[\"1\",\"1\",\"425\",\"Ø§Ù„Ø±Ø¬Ø§Ù„\"]]', 1),
(76, 36, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/5b3c7958-0e6d-42d1-829f-855f6cebbade/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/e019bb9c-11ca-4994-9d7b-f5c675a13e21/WomensActivewear.jpg\"]', '[[\"0\",\"1\",\"54\",\"Ù…Ù„Ø§Ø¨Ø³ Ø±ÙŠØ§Ø¶ÙŠØ©\"],[\"1\",\"1\",\"22\",\"Ù…Ù„Ø§Ø¨Ø³ Ø±ÙŠØ§Ø¶ÙŠØ©\"]]', 0),
(78, 37, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/eae866e2-bf71-4bad-8523-a0178ac4764e/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/35907363-21bd-460e-b228-1f56270bda5f/sec.jpg\"]', '[[\"0\",\"1\",\"424\",\"Ø§Ù„Ù†Ø³Ø§Ø¡\"],[\"1\",\"1\",\"425\",\"Ø§Ù„Ø±Ø¬Ø§Ù„\"]]', 1),
(79, 37, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/bfbe32ed-b3ab-4946-8881-2cdc3da9e60c/ist.jpg\",\"./vendor/fineuploader/php-traditional-server/files/5c99edb4-8285-4e1f-b33f-246790c60126/WomensActivewear.jpg\"]', '[[\"0\",\"1\",\"54\",\"Ù…Ù„Ø§Ø¨Ø³ Ø±ÙŠØ§Ø¶ÙŠØ©\"],[\"1\",\"1\",\"22\",\"Ù…Ù„Ø§Ø¨Ø³ Ø±ÙŠØ§Ø¶ÙŠØ©\"]]', 0),
(81, 38, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/5e6a9030-efda-4bec-836e-0021f6191aab/WomensSale.jpg\",\"./vendor/fineuploader/php-traditional-server/files/b066512b-f05f-410b-8068-37db29b7def4/MensSale.jpg\"]', '[[\"0\",\"1\",\"424\",\"Women\"],[\"1\",\"1\",\"425\",\"Men\"]]', 1),
(82, 38, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/5cf4e878-f0d5-4ced-8bdd-50c7261b1c2d/MensActive.jpg\",\"./vendor/fineuploader/php-traditional-server/files/0fd9233a-7599-4c62-b109-4bb07343c56e/WomensActive.jpg\"]', '[[\"0\",\"1\",\"54\",\"Sportswear\"],[\"1\",\"1\",\"22\",\"Sportswear\"]]', 2),
(84, 39, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/8b3675e6-d639-4e7d-bd9e-0540111dbaa3/WomensSale.jpg\",\"./vendor/fineuploader/php-traditional-server/files/91470d5e-8647-437f-ae31-76b30215b721/MensSale.jpg\"]', '[[\"0\",\"1\",\"424\",\"Women\"],[\"1\",\"1\",\"425\",\"Men\"]]', 1),
(85, 39, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/ce9df078-3485-4289-b2ac-22bc4fc4dde6/MensActive.jpg\",\"./vendor/fineuploader/php-traditional-server/files/ff1c3a60-d198-4ba1-9bb6-e47c2c0c87a2/WomensActive.jpg\"]', '[[\"0\",\"1\",\"54\",\"Sportswear\"],[\"1\",\"1\",\"22\",\"Sportswear\"]]', 0),
(87, 40, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/8e2a73c7-4b44-4d9c-9995-a401db2e0fd2/woMen-Sale.jpg\",\"./vendor/fineuploader/php-traditional-server/files/7a5a7e59-bcb4-48fd-a4d9-a80ac7afbb9b/Men-Sale.jpg\"]', '[[\"0\",\"1\",\"424\",\"Women\"],[\"1\",\"1\",\"425\",\"Men\"]]', 1),
(88, 40, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/d40a317e-d993-4fdd-84b7-31b5e93cb7ac/Menns-Activewear.jpg\",\"./vendor/fineuploader/php-traditional-server/files/0ab6766e-05ce-418d-91d0-dfe9f25068a6/Womens-Activewear.jpg\"]', '[[\"0\",\"1\",\"54\",\"Sportswear\"],[\"1\",\"1\",\"22\",\"Sportswear\"]]', 0),
(89, 30, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/9e463af6-2311-4029-8f5f-ee41371e7100/Women-New-arival-Arabic.jpg\",\"./vendor/fineuploader/php-traditional-server/files/ff8052d7-75ad-4e0b-9263-6780e05e03f1/men-New-arival-Arabic.jpg\"]', '[[\"0\",\"1\",\"14\",\"Ø§Ø­Ø°ÙŠØ© Ø³Ù†ÙŠÙƒØ±Ø³\"],[\"1\",\"1\",\"215\",\"Ø§Ù„Ø±Ø¬Ø§Ù„\"]]', 1),
(90, 28, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/d0aee602-5a4b-40af-b1e3-4c9faee8cac5/Women-New-arival-Arabic.jpg\",\"./vendor/fineuploader/php-traditional-server/files/cc15788d-61ad-4bcf-ab4f-1c29918e9965/men-New-arival-Arabic.jpg\"]', '[[\"0\",\"1\",\"14\",\"Ø§Ø­Ø°ÙŠØ© Ø³Ù†ÙŠÙƒØ±Ø³\"],[\"1\",\"1\",\"215\",\"Ø§Ù„Ø±Ø¬Ø§Ù„\"]]', 0),
(91, 40, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/eb3a8804-7725-49d7-b183-fd29ec6efa95/Women-New-arival-eng.jpg\",\"./vendor/fineuploader/php-traditional-server/files/1d67deda-81df-46ca-930b-c380ce7de9de/men-New-arival-eng.jpg\"]', '[[\"0\",\"1\",\"14\",\"Sneakers\"],[\"1\",\"1\",\"215\",\"Men\"]]', 0),
(92, 23, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/903ed106-b215-4c9f-8534-b47c17ad09ca/Women-New-arival-eng.jpg\",\"./vendor/fineuploader/php-traditional-server/files/8bba2fb5-f3f4-4b00-96da-7016f4d6e07a/men-New-arival-eng.jpg\"]', '[[\"0\",\"1\",\"14\",\"Sneakers\"],[\"1\",\"1\",\"215\",\"Men\"]]', 0),
(93, 41, 'single', '[\"./vendor/fineuploader/php-traditional-server/files/9ed62ab5-2c11-4550-8c07-de68b3a0219c/1496222274.jpg\"]', '[[\"0\",\"1\",\"828\",\"The Ramadan Edit\"]]', 0),
(94, 41, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/ab744b3f-6806-4198-918c-66b2e7db67b0/1496222304.jpg\",\"./vendor/fineuploader/php-traditional-server/files/75fdbeaf-a687-47ab-a5e6-85d41d5e8bf9/1496222311.jpg\"]', '[[\"0\",\"1\",\"424\",\"Women\"],[\"1\",\"1\",\"425\",\"Men\"]]', 2),
(95, 41, 'doubleShort', '[\"./vendor/fineuploader/php-traditional-server/files/2ccc6d22-a8e8-40f1-b271-259f2969864f/1496222455.png\",\"./vendor/fineuploader/php-traditional-server/files/abc01bc5-7dbb-4bd4-9e4f-0bdafe8a2853/1496222457.png\"]', '[[\"0\",\"1\",\"288\",\"Abayas\"],[\"1\",\"1\",\"65\",\"Accessories\"]]', 1),
(96, 41, 'tripleLeft', '[\"./vendor/fineuploader/php-traditional-server/files/d5f8714b-970e-44a3-af0b-803c93fb6feb/1496222497.png\",\"./vendor/fineuploader/php-traditional-server/files/81c9d42c-04d9-41b9-9378-3af4d03174c8/1496222500.png\",\"./vendor/fineuploader/php-traditional-server/files/fc62a185-db90-41c5-b135-1ad2e9eef7de/1496222502.png\"]', '[[\"0\",\"1\",\"288\",\"Abayas\"],[\"1\",\"1\",\"429\",\"Accessories\"],[\"2\",\"1\",\"70\",\"Belts\"]]', 4),
(97, 41, 'tripleRight', '[\"./vendor/fineuploader/php-traditional-server/files/01b87e0d-e1fa-4b48-b88b-b7c11bad7884/1496222529.png\",\"./vendor/fineuploader/php-traditional-server/files/b2d28b0d-1041-43d6-9dd2-a2762da799d1/1496222530.png\",\"./vendor/fineuploader/php-traditional-server/files/7b44f222-7595-44a2-9387-eb66047c0200/1496222532.png\"]', '[[\"0\",\"1\",\"288\",\"Abayas\"],[\"1\",\"1\",\"429\",\"Accessories\"],[\"2\",\"1\",\"420\",\"Boots\"]]', 3),
(98, 41, 'singleBar', '[\"./vendor/fineuploader/php-traditional-server/files/7fbc894c-0fcc-455a-8d3f-6ddfbd020383/1496222612.png\"]', '[[\"0\",\"1\",\"146\",\"Abayas\"]]', 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings`
--

CREATE TABLE `tbl_settings` (
  `id` int(20) NOT NULL,
  `app_and_key` varchar(255) NOT NULL,
  `dev_ssl` varchar(100) NOT NULL,
  `dev_passphrase` varchar(5) NOT NULL,
  `pro_ssl` varchar(150) NOT NULL,
  `pro_passphrase` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_settings`
--

INSERT INTO `tbl_settings` (`id`, `app_and_key`, `dev_ssl`, `dev_passphrase`, `pro_ssl`, `pro_passphrase`) VALUES
(1, 'AIzaSyBFdU_c-Qti2Dh9atrtTCUZ-iCZU7u4CcE', 'ssl/dev_cer.pem', '123', 'ssl/aps_dis.pem', '123');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `username`, `password`, `date_time`, `role`) VALUES
(1, 'admin@elabelz.com', 'pT9RHCZ2KU2x', '2017-01-03 04:56:35', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_apps`
--
ALTER TABLE `tbl_apps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_block`
--
ALTER TABLE `tbl_block`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cat_rows`
--
ALTER TABLE `tbl_cat_rows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_devices`
--
ALTER TABLE `tbl_devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_merchandise`
--
ALTER TABLE `tbl_merchandise`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notifications`
--
ALTER TABLE `tbl_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_rows`
--
ALTER TABLE `tbl_rows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_apps`
--
ALTER TABLE `tbl_apps`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tbl_block`
--
ALTER TABLE `tbl_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tbl_cat_rows`
--
ALTER TABLE `tbl_cat_rows`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT for table `tbl_devices`
--
ALTER TABLE `tbl_devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `tbl_merchandise`
--
ALTER TABLE `tbl_merchandise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `tbl_notifications`
--
ALTER TABLE `tbl_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tbl_rows`
--
ALTER TABLE `tbl_rows`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
