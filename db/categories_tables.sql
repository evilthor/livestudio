-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2017 at 07:35 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mobilestudio`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_categories`
--

CREATE TABLE `tbl_categories` (
  `id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_categories`
--

INSERT INTO `tbl_categories` (`id`, `app_id`, `title`, `status`) VALUES
(4, 1, 'Men', 1),
(5, 1, 'Women', 1),
(6, 2, 'Men', 1),
(7, 2, 'Women', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cat_rows`
--

CREATE TABLE `tbl_cat_rows` (
  `id` int(11) NOT NULL,
  `merchandise_id` int(11) NOT NULL,
  `layout` varchar(255) NOT NULL,
  `images` text NOT NULL,
  `attachment` text NOT NULL,
  `row_order` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cat_rows`
--

INSERT INTO `tbl_cat_rows` (`id`, `merchandise_id`, `layout`, `images`, `attachment`, `row_order`) VALUES
(7, 4, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/ceea3083-10fe-4c16-be1a-f8238a805add/1024x1310.png\",\"./vendor/fineuploader/php-traditional-server/files/2fd3b4b0-b996-4bb8-bdf5-58087421c5ea/1024x1310.png\"]', '[[\"0\",\"1\",\"146\",\"Abayas\"],[\"1\",\"1\",\"146\",\"Abayas\"]]', 0),
(8, 4, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/191af857-093c-4ecc-9340-50f988a9d1aa/1024x1310.png\",\"./vendor/fineuploader/php-traditional-server/files/b8a90f57-7200-450a-9316-5476da4e4412/1024x1310.png\"]', '[[\"0\",\"1\",\"640\",\"Jeans &amp; Pants\"],[\"1\",\"1\",\"146\",\"Abayas\"]]', 1),
(10, 5, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/f5b02e39-b230-4aca-b131-df258648c91b/1024x1310.png\",\"./vendor/fineuploader/php-traditional-server/files/e3fe4761-513e-4ca1-af4e-7b59d5efcdc7/1024x1310.png\"]', '[[\"0\",\"1\",\"429\",\"Accessories\"],[\"1\",\"1\",\"223\",\"Accessories\"]]', 0),
(11, 6, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/55764e84-beef-409e-b602-34ebe942fffb/1024x1310.png\",\"./vendor/fineuploader/php-traditional-server/files/d3784e7a-7be1-4c6d-a977-8baa23f972d7/1024x1310.png\"]', '[[\"0\",\"1\",\"85\",\"Accessories\"],[\"1\",\"1\",\"628\",\"Accessories\"]]', 0),
(12, 7, 'double', '[\"./vendor/fineuploader/php-traditional-server/files/fe3f1613-6f0b-4592-b2e0-bdf928c6a11d/1024x1310.png\",\"./vendor/fineuploader/php-traditional-server/files/2c4ff1a8-cd9f-434b-b7f0-3c88847aaca8/1024x1310.png\"]', '[[\"0\",\"1\",\"9\",\"Bags\"],[\"1\",\"1\",\"146\",\"Abayas\"]]', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cat_rows`
--
ALTER TABLE `tbl_cat_rows`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_cat_rows`
--
ALTER TABLE `tbl_cat_rows`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
