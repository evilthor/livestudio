<?php
ini_set("display_errors", "0");
ini_set('max_execution_time', 300);
include("../config.php");
include('../cache/memcache.php');
if(isset($_GET['type'])){
    $type = trim($_GET['type']);
    $url = 'https://www.elabelz.com/en_ae/emapi/index/getSelectedBrands?type='.$type;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    $result = curl_exec($ch);
    curl_close($ch);
    $results = json_decode($result, true);
}
if( $results){
    $sql = "DELETE FROM tbl_selected_brands WHERE brand_type='".$type."'";
    $db->query($sql);
    foreach($results as $key => $values){
        foreach($values as $val){
            if($val['name']!='' && $val['id']!=''){
                $sql = "INSERT INTO tbl_selected_brands SET brand_lang = '".$key."',
		                                              brand_type = '".$type."',
		                                              brand_name	 = '".htmlspecialchars_decode($val['name'])."',
		                                              brand_id = '".$val['id']."'";
                $db->query($sql);
            }
        }
    }
}

//take a breath and clear services cache
sleep(2);
// get all apis files of categories
$list = glob("../api/categories*.php");
foreach ($list as $file) {
    $single_file = str_replace('..','',$file);
    $files[] = $single_file;
}
$domain_name = $_SERVER['SERVER_NAME'];
foreach ($files as $file_name) {

    //repeat process for all stores
    $sql_stores = "SELECT * FROM tbl_stores";
    $res_stores = $db->get_results($sql_stores);
    if($res_stores){
        foreach ($res_stores as  $store) {
            if($store->store_lang=='both'){
                // generate keys for both en and ar
                $del_key_en = sha1($domain_name.$file_name."?store=en_".$store->store_code);
                $del_key_ar = sha1($domain_name.$file_name."?store=ar_".$store->store_code);
                //delete cache based on keys
                $cacheObject->delData($del_key_en);
                $cacheObject->delData($del_key_ar);
            }else{
                // generate key
                $del_key = sha1($domain_name.$file_name."?store=".$store->store_lang."_".$store->store_code);
                //delete cache based on key
                $cacheObject->delData($del_key);
            }
        }//foreach ($res_stores as  $store)
    }//if($res_stores)
}//foreach ($files as $cat_file)
