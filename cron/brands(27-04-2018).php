<?php
ini_set("display_errors","0");
include('../config.php');
include('../cache/memcache.php');
$categories = array(3,4,5);
$langs = array('en','ar');
foreach($langs as $lang){
    $finalJSON = array();
    foreach($categories as $category){
        //$url = 'https://www.elabelz.com/' . $lang . '_ae/emapi/index/brandsforstudio?cid='.$category;
        $url = 'https://www.elabelz.com/' . $lang . '_ae/emapi/index/brandsforstudio?cid='.$category;
        $result = sendCurl($url);
        if($category == 3){
            $finalJSON['Women'] = json_decode($result);
        }elseif($category == 4){
            $finalJSON['Men'] = json_decode($result);
        }elseif($category == 5){
            $finalJSON['Kids'] = json_decode($result);
        }
    }
    $f = fopen('../brands_'.$lang.'.json','w+');
    fwrite($f,json_encode($finalJSON));
    fclose($f);
    //$url = 'https://www.elabelz.com/' . $lang . '_ae/emapi/index/brandsforstudio';
    $url = 'https://www.elabelz.com/' . $lang . '_ae/emapi/index/brandsforstudio';
    $result = sendCurl($url);
    $f = fopen('../brands_main_'.$lang.'.json','w+');
    fwrite($f,json_encode($result));
    fclose($f);
}

function sendCurl($url){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

//take a breath and clear services cache
sleep(5);
// get all apis files of categories
$list = glob("../api/categories*.php"); 
foreach ($list as $file) { 
  $single_file = ltrim($file,'..');
  $files[] = $single_file; 
}
$domain_name = $_SERVER['SERVER_NAME'];
foreach ($files as $file_name) {

    //repeat process for all stores
    $sql_stores = "SELECT * FROM tbl_stores";
    $res_stores = $db->get_results($sql_stores);
    if($res_stores){
        foreach ($res_stores as  $store) {
            if($store->store_lang=='both'){
                // generate keys for both en and ar
                $del_key_en = sha1($domain_name.$file_name."?store=en_".$store->store_code);
                $del_key_ar = sha1($domain_name.$file_name."?store=ar_".$store->store_code);
                //delete cache based on keys
                $cacheObject->delData($del_key_en);
                $cacheObject->delData($del_key_ar);
            }else{
                // generate key
                $del_key = sha1($domain_name.$file_name."?store=".$store->store_lang."_".$store->store_code);
                //delete cache based on key
                $cacheObject->delData($del_key);
            }
        }//foreach ($res_stores as  $store)
    }//if($res_stores)
}//foreach ($files as $cat_file)
?>