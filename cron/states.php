<?php
ini_set("display_errors","0");
include('../config.php');
include('../cache/memcache.php');

//For english
$url = 'https://www.elabelz.com/en_ae/emapi/index/allowedCountriesAndRegions';
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
$result = curl_exec($ch);
curl_close($ch);

//write json file for categories tree
$f = fopen('../states_en.json','w+');
fwrite($f,$result);
fclose($f);


//For arabic
$url = 'https://www.elabelz.com/ar_ae/emapi/index/allowedCountriesAndRegions';
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
$result = curl_exec($ch);
curl_close($ch);
//write json file for categories tree
$f = fopen('../states_ar.json','w+');
fwrite($f,$result);
fclose($f);
?>