<?php
$url = 'https://www-stage.elabelz.com/en_ae/restmob/index/allproductslist';
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
//curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
$result = curl_exec($ch);
curl_close($ch);  
$result = json_decode($result);
$finalJSON = array();
$i=0;
foreach($result as $r){
	$finalJSON[$i]['id'] = $r->id;
	$finalJSON[$i]['name'] = $r->name;
	$finalJSON[$i]['sku'] = $r->sku;
	$i++;
	}
$f = fopen('../products.json','w+');
usort($finalJSON, function($a, $b) {
    return strcmp($a['name'],$b['name']);
});
fwrite($f,json_encode($finalJSON));
fclose($f);
?>