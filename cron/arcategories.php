<?php
ini_set("display_errors","0");
include('../config.php');
include('../cache/memcache.php');
$url = 'https://www.elabelz.com/mobile_json/categories_ar.json';
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
$result = curl_exec($ch);
curl_close($ch);
//write json file for categories tree
$f = fopen('../tree_ar.json','w+');
fwrite($f,$result);
fclose($f);
$result = json_decode($result);
$finalJSON = array();
$i=0;
foreach($result as $r){
	$finalJSON[$i]['id'] = $r->id;
	$finalJSON[$i]['name'] = $r->title;
	$finalJSON[$i]['relation'] = @$r->relation_tree;
	$i++;
	if(!empty($r->children)){
		getChildIdAndName($r->children,$i,$finalJSON);
		}
	}
$f = fopen('../arcategories.json','w+');
usort($finalJSON, function($a, $b) {
    return strcmp($a['name'],$b['name']);
});
echo json_encode($finalJSON);
fwrite($f,json_encode($finalJSON));
fclose($f);
//echo"<pre>";print_r($finalJSON);
function getChildIdAndName($children,&$i,&$finalJSON){
	foreach($children as $ch){
		$finalJSON[$i]['id'] = $ch->id;
		$finalJSON[$i]['name'] = $ch->title;
		$finalJSON[$i]['relation'] = @$ch->relation_tree;
		$i++;
		if(!empty($ch->children)){
			getChildIdAndName($ch->children,$i,$finalJSON);
			}
		}
	}


//take a breath and clear services cache
sleep(5);
// get all apis files of home
$list = glob("../api/home*.php"); 
foreach ($list as $file) { 
  $single_file = ltrim($file,'..');
  $files[] = $single_file; 
}
$domain_name = $_SERVER['SERVER_NAME'];
foreach ($files as $file_name) {

	//repeat process for all stores
	$sql_stores = "SELECT * FROM tbl_stores";
	$res_stores = $db->get_results($sql_stores);
	if($res_stores){
		foreach ($res_stores as  $store) {
			if($store->store_lang=='both'){
				// generate keys for both en and ar
				$del_key_en = sha1($domain_name.$file_name."?store=en_".$store->store_code);
				$del_key_ar = sha1($domain_name.$file_name."?store=ar_".$store->store_code);
				//delete cache based on keys
				$cacheObject->delData($del_key_en);
				$cacheObject->delData($del_key_ar);
			}else{
				// generate key
				$del_key = sha1($domain_name.$file_name."?store=".$store->store_lang."_".$store->store_code);
				//delete cache based on key
				$cacheObject->delData($del_key);
			}
		}//foreach ($res_stores as  $store)
	}//if($res_stores)
}//foreach ($files as $cat_file)	
?>