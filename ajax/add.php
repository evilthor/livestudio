<?php
include "../config.php";
//print_r($_FILES);
//print_r($_POST);
//die();

function addAppToNewTable($app_lang){
  global $db;
  $sql_new_app = "SELECT id FROM tbl_apps_new WHERE app_store = '".$app_lang."'";
  $res_new_app = $db->get_row($sql_new_app);
  if (!$res_new_app) {

     $sql = "INSERT INTO tbl_apps_new SET app_store = '".$app_lang."'";
     $db->query($sql);

  }
}

if (isset($_POST['command']) && $_POST['command'] == 'add-new-store') {
//echo "<pre>";print_r($_POST);exit;
     //$country_flag = trim($_POST['country-flag']);
     $country_code = trim($_POST['country-code']);
     $country_name = trim($_POST['country-name']);
     $country_name_arabic = trim($_POST['country-name-arabic']);
     $country_name_analytics = trim($_POST['country-name-analytics']);
     $currency_analytics = trim($_POST['currency-analytics']);
     $store_language = trim($_POST['store-language']);
     $fb_content_id_en = trim($_POST['fb-content-id-en']);
     $fb_content_id_ar = trim($_POST['fb-content-id-ar']);
     $store_code = strtolower($country_code);
     $store_status = trim($_POST['status']);

     $destination_path = dirname(getcwd()) ."/images/countries_flags/";
     if( !file_exists($destination_path) ){
          mkdir($destination_path);
     }
     $filename = date('ymdhis').basename( $_FILES["country-flag"]["name"]);
     $target_path = $destination_path .$filename;
     move_uploaded_file($_FILES['country-flag']['tmp_name'], $target_path);


     $sql = "INSERT INTO tbl_stores SET
                         store_lang = '" . $store_language . "',
                         country_flag = '" . $filename . "',
                         country_code = '" . $country_code . "',
                         country_name = '" . $country_name . "',
                         country_name_arabic = '" . $country_name_arabic . "',
                         country_name_analytics = '" . $country_name_analytics . "',
                         currency_analytics = '" . $currency_analytics . "',
                         fb_content_id_en = '" . $fb_content_id_en . "',
                         fb_content_id_ar = '" . $fb_content_id_ar . "',
                         store_code = '" . $store_code . "',
                         status = '" . $store_status . "'";

     if ($db->query($sql)) {
          $store_id = $db->insert_id;
          if($store_language=='both'){
               $ap_en = "en_".strtolower($country_code);
               $ap_ar = "ar_".strtolower($country_code);
               addAppToNewTable($ap_en);
               addAppToNewTable($ap_ar);
          }else{
               $ap_lng = $store_language."_".strtolower($country_code);
               addAppToNewTable($ap_lng);
          }
          
     } else {
          $error = true;
     }

}



//cache management

if (isset($_POST['command']) && $_POST['command'] == 'add-cache-category') {
     $message = '';

     $category = trim($_POST['category']);

     $sql = "SELECT * FROM `tbl_cache_categories` WHERE `title` = '".$category."'";
     $cate = $db->get_results($sql);

     if( count($cate) == 0 ){
          $sql = "INSERT INTO tbl_cache_categories SET title = '" . $category . "'";
          $db->query($sql);
          $message = array(
               'type' => 'success',
               'message' => 'cache category added successfully.'
          );
     }else{
          $message = array(
               'type' => 'warn',
               'message' => 'cache category already exists.'
          );
     }
     echo json_encode($message);
}


// cache service

if (isset($_POST['command']) && $_POST['command'] == 'add-api-service') {
     $message = '';
     $category = trim($_POST['cache-category']);
     $service = trim($_POST['service']);

     $sql = "SELECT * FROM `tbl_cache_services` WHERE `cat_id` = '$category' AND  `title` = '$service'";
     $cate = $db->get_results($sql);

     if( count($cate) == 0 ){
          $sql = "INSERT INTO tbl_cache_services SET cat_id = '".$category."', title = '" . $service . "'";
          $db->query($sql);
          $message = array(
               'type' => 'success',
               'message' => 'cache category added successfully.'
          );
     }else{
          $message = array(
               'type' => 'warn',
               'message' => 'cache category already exists.'
          );
     }
     echo json_encode($message);
}

if (isset($_POST['command']) && $_POST['command'] == 'add-scheme') {

     $name = trim($_POST['name']);

     $destination_path = dirname(getcwd()) ."/color-schemes/";
     if( !file_exists($destination_path) ){
          mkdir($destination_path);
     }
     $filename = date('ymdhis')."-".basename( $_FILES["style"]["name"]);
     $target_path = $destination_path .$filename;
     move_uploaded_file($_FILES['style']['tmp_name'], $target_path);

     $sql = "INSERT INTO tbl_color_schemes SET title = '" . $name . "', slug = '" . $filename . "'";

     if ($db->query($sql)) {
          $store_id = $db->insert_id;
     } else {
          $error = true;
     }
}


if (isset($_POST['command']) && $_POST['command'] == 'add-app-screen') {
    $message = '';

    $category = trim($_POST['screen-name']);

    $sql = "SELECT * FROM `tbl_messages_cat` WHERE `title` = '".$category."'";
    $cate = $db->get_results($sql);

    if( count($cate) == 0 ){
        $sql = "INSERT INTO tbl_messages_cat SET title = '" . $category . "'";
        $db->query($sql);
        $message = array(
            'type' => 'success',
            'message' => 'App screen added successfully.'
        );
    }else{
        $message = array(
            'type' => 'warn',
            'message' => 'App screen already exists.'
        );
    }
    echo json_encode($message);
}

if (isset($_POST['command']) && $_POST['command'] == 'add-marketing-message') {
    $message = '';
    $app_screen = trim($_POST['app-screen']);
    $message = trim($_POST['message']);

    $sql = "SELECT * FROM `tbl_messages` WHERE `cat_id` = '$app_screen' AND  `message` = '$message'";
    $cate = $db->get_results($sql);

    if( count($cate) == 0 ){
        $sql = "INSERT INTO tbl_messages SET cat_id = '".$app_screen."', message = '" . $message . "'";
        $db->query($sql);
        $message = array(
            'type' => 'success',
            'message' => 'Message added successfully.'
        );
    }else{
        $message = array(
            'type' => 'warn',
            'message' => 'Message already exists.'
        );
    }
    echo json_encode($message);
}