<?php
$lang = $_GET['lang'];
$cat = $_GET['cat'];
$url = "https://dev.elabelz.com/" . $lang . "_ae/emapi/index/brandsforstudio";
if ($cat) {
    $url .= "?cid=" + $cat;
}
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
$result = curl_exec($ch);
curl_close($ch);
echo $result;
?>