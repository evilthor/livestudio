<?php
	include '../config.php';

	$_REQUEST['command'] = (isset($_REQUEST['command']) && !empty($_REQUEST['command'])) ? $_REQUEST['command'] : '';
	
	// for cache-management section;
	include('./includes/cache-m/index.php');
	// for service in cache-management section;
	include('./includes/cache-m/service.php');
	// list for cache-management section;
	include('./includes/cache-m/list.php');

	// for marketing-messages-management section;
	include('./includes/marketing-m/index.php');
	// list for marketing-messages-management section;
	include('./includes/marketing-m/list.php');

	// list for scheme section;
	include('./includes/scheme/index.php');

	if( $_REQUEST['command'] == 'add-new-store-form' ){
?>
<div id="add-new-store-form-popup" class="white-popup zoom-anim-dialog">
	<h3>Add new store</h3>
	<hr>
	<form name="add-new-store-form" id="add-new-store-form" action="./ajax/add.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="command" value="add-new-store">
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="country-flag">Store Language</label>
					<select name="store-language" data-ele-disabled=".store-management [name='country-name-arabic']" class="form-control" id="store-language" required>
						<option value="" selected="selected">select</option>
						<option value="en">English</option>
						<option value="ar">Arabic</option>
                        		<option value="both">Both English & Arabic</option>
				    	</select>
				</div>
			</div>
			<div class="col-lg-6">
            	<label for="country-flag">Country Flag</label>
            	<input type="file" name="country-flag" id="country-flag" required accept="image/*">
				<div class="progress">
				  	<div 
						class="progress-bar" 
						role="progressbar" 
						aria-valuenow="60" 
						aria-valuemin="0" 
						aria-valuemax="100" 
						style="width: 0%;"
				  	>
				  	</div>
				</div>
        	</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="country-code">Country Code</label>
					<select name="country-code" class="form-control" id="country-code" required>

					</select>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="country-name">Country Name</label>
					<select name="country-name" class="form-control" id="country-name" required>
					
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="country-name-arabic">Country Name Arabic</label>
				    	<input type="text" name="country-name-arabic" dir="rtl" class="form-control" id="country-name-arabic" required>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="country-name-analytics">Country Name (Analytics)</label>
					<select name="country-name-analytics" class="form-control" id="country-name-analytics" required>
					
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="currency-analytics">Currency (Analytics)</label>
					<select name="currency-analytics" class="form-control" id="currency-analytics" required>
					
					</select>
				</div>
			</div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="status">Status</label>
                    <select name="status" class="form-control" id="status" required>
                        <option value="" selected="selected">select</option>
                        <option value="1">Publish</option>
                        <option value="0">Draft</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 en-field">
                <div class="form-group">
                    <label for="fb-content-id-en">FB Content ID (en)</label>
                    <input type="text" name="fb-content-id-en" class="form-control" id="fb-content-id-en" required>
                </div>
            </div>
            <div class="col-lg-6 ar-field">
                <div class="form-group">
                    <label for="fb-content-id-ar">FB Content ID (ar)</label>
                    <input type="text" name="fb-content-id-ar" class="form-control" id="fb-content-id-ar" required>
                </div>
            </div>
		</div>
		<hr>
		<button type="submit" id="submit-1" class="btn btn-primary">Save <span id="loader-1" style="display:none"><i class="fa fa-spinner fa-spin"></i></span></button>
	</form>
	<script>
		jQuery(document).ready(function($) {
		    $("#add-new-store-form").validate({
		          errorPlacement: function(error, element) {
		               $(element).parents().eq(0).append(error);
		          },
		        	submitHandler: function(form) {
					var progress = $('.progress');
					var bar = $('.progress-bar');
					var percent = $('.percent');
					$(form).ajaxSubmit({
					    	beforeSend: function() {
			                    $("#loader-1").show();
			                    $('#submit-1').attr('disabled', 'disabled');
					    		progress.show();
					        	var percentVal = '0%';
					        	bar.width(percentVal)
					        	percent.html(percentVal);
					    	},
					    	uploadProgress: function(event, position, total, percentComplete) {
					        	var percentVal = percentComplete + '%';
					        	bar.width(percentVal).attr('aria-valuenow',percentComplete);
					        	percent.html(percentVal);
					    	},
					    	success: function(data) {
					        	var percentVal = '100%';
					        	bar.width(percentVal)
					        	percent.html(percentVal);
					        	progress.hide();
				     		var message = 'Store updated successfully';
				     	 	if (data.trim() == "error") {
			                   		message = 'Error occured, please try again later.';
			                	}
					     	$.alert({
					     		title: '',
					     		content: message,
								onClose: function () {
			                			window.location.href = 'store-management.php';
								},					     		
					     	});
					     	$("#loader-1").hide();
					    	},
						complete: function(xhr) {
					        	bar.width('0%').attr('aria-valuenow',0);
						}
					});
	            		return false;
		        	}
		    	});
		});
	</script>
<?php
$sql = "SELECT * FROM tbl_magento_stores";
$res = $db->get_results($sql);
$all_stores = json_encode($res);
?>
	<script>
		jQuery(document).ready(function($) {
			var codeHtml = ''
			var res = $.parseJSON('<?php echo $all_stores;?>');

			$.each(res, function(i, v){
				codeHtml += '<option value="'+v.country_code+'">'+v.country_code+'</option>';
			});
			$('#country-code, #country-name-analytics').html(codeHtml);

			var nameHtml = ''
			$.each(res, function(i, v){
				nameHtml += '<option value="'+v.name+'">'+v.name+'</option>';
			});
			$('#country-name').html(nameHtml);

			var currencyHtml = ''
			$.each(window.currencies, function(i, v){
				currencyHtml += '<option value="'+v.code+'">'+v.code+'</option>';
			});
			$('#currency-analytics').html(currencyHtml);

			$("#country-code, #country-name, #currency-analytics, #country-name-analytics").select2({
			  	dropdownParent: $('.white-popup'),
			});
		});
	</script>
</div>
<?php
	}

	if( $_REQUEST['command'] == 'edit-store-form' ){
		$id = $_POST['ID'];
		$sql = "SELECT * FROM tbl_stores WHERE id='".$id."'";
    	$res_edit_store = $db->get_row($sql);

		$sql = "SELECT * FROM tbl_magento_stores";
		$res = $db->get_results($sql);
		$all_stores = json_encode($res);
?>
<div id="edit-store-form-popup" class="white-popup zoom-anim-dialog">
	<h3>Edit store</h3>
	<hr>
	<form name="edit-store-form" id="edit-store-form" action="./ajax/edit.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="command" value="edit-store">
		<input type="hidden" name="store_id" value="<?php echo $res_edit_store->id;?>">
		<input type="hidden" name="hid_image" value="<?php echo $res_edit_store->country_flag;?>">
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="country-flag">Store Language</label>
					<select name="store-language" data-ele-disabled=".store-management [name='country-name-arabic']" class="form-control" id="store-language" required>
						<option value="" selected="selected">select</option>
						<option value="en" <?php if($res_edit_store->store_lang =='en'){ echo "selected";}?>>English</option>
						<option value="ar" <?php if($res_edit_store->store_lang =='ar'){ echo "selected";}?>>Arabic</option>
                        <option value="both" <?php if($res_edit_store->store_lang =='both'){ echo "selected";}?>>Both English & Arabic</option>
				    </select>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-2">
            				<img style="max-width: 50px;" src="<?php echo $siteUrl?>/images/countries_flags/<?php echo $res_edit_store->country_flag;?>">
					</div>
					<div class="col-lg-10">
			            	<label for="country-flag">Country Flag</label>
			            	<input type="file" name="country-flag" id="country-flag" accept="image/*">
					</div>
				</div>
				<div class="progress">
				  	<div 
						class="progress-bar" 
						role="progressbar" 
						aria-valuenow="60" 
						aria-valuemin="0" 
						aria-valuemax="100" 
						style="width: 0%;"
				  	>
				  	</div>

				</div>
        		</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="country-code">Country Code</label>
					<select name="country-code" class="form-control" id="country-code" required>

					</select>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="country-name">Country Name</label>
					<select name="country-name" class="form-control" id="country-name" required>
					
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="country-name-arabic">Country Name Arabic</label>
				    	<input type="text" <?php //if($res_edit_store->store_lang =='en'){ echo "disabled";}?> name="country-name-arabic" dir="rtl" class="form-control" id="country-name-arabic" required value="<?php echo $res_edit_store->country_name_arabic;?>">
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="country-name-analytics">Country Name (Analytics)</label>
					<select name="country-name-analytics" class="form-control" id="country-name-analytics" required>
					
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="currency-analytics">Currency (Analytics)</label>
					<select name="currency-analytics" class="form-control" id="currency-analytics" required>
					
					</select>
				</div>
			</div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="status">Status</label>
                    <select name="status" class="form-control" id="status" required>
                        <option value="">select</option>
                        <option value="1" <?php if($res_edit_store->status=='1'){ echo "selected";}?>>Publish</option>
                        <option value="0" <?php if($res_edit_store->status=='0'){ echo "selected";}?>>Draft</option>
                    </select>
                </div>
            </div>

            <!--<div class="col-lg-6">
                <div class="form-group">
                    <label for="fb-content-id">FB Content ID</label>
                    <input type="text" name="fb-content-id" class="form-control" value="<?php /*echo $res_edit_store->fb_content_id;*/?>" id="fb-content-id" required>
                </div>
            </div>-->
        </div>
        <div class="row">
            <div class="col-lg-6 en-field" <?php if($res_edit_store->store_lang =='ar'){ echo 'style="display:none;"';}?>>
                <div class="form-group">
                    <label for="fb-content-id-en">FB Content ID (en)</label>
                    <input type="text" name="fb-content-id-en" class="form-control" id="fb-content-id-en" required value="<?php echo $res_edit_store->fb_content_id_en;?>" >
                </div>
            </div>
            <div class="col-lg-6 ar-field" <?php if($res_edit_store->store_lang =='en'){ echo 'style="display:none;"';}?>>
                <div class="form-group">
                    <label for="fb-content-id-ar">FB Content ID (ar)</label>
                    <input type="text" name="fb-content-id-ar" class="form-control" id="fb-content-id-ar" required value="<?php echo $res_edit_store->fb_content_id_ar;?>" >
                </div>
            </div>
		</div>
		<hr>
		<button type="submit" id="submit-1" class="btn btn-primary">Save <span id="loader-1" style="display:none"><i class="fa fa-spinner fa-spin"></i></span></button>
	</form>
	<script>
		jQuery(document).ready(function($) {
		    $("#edit-store-form").validate({
		          errorPlacement: function(error, element) {
		               $(element).parents().eq(0).append(error);
		          },
		        	submitHandler: function(form) {
		            	var progress = $('.progress');
					var bar = $('.progress-bar');
					var percent = $('.percent');
					$(form).ajaxSubmit({
					    	beforeSend: function() {
			                    $("#loader-1").show();
			                    $('#submit-1').attr('disabled', 'disabled');
					    		progress.show();
					        	var percentVal = '0%';
					        	bar.width(percentVal)
					        	percent.html(percentVal);
					    	},
					    	uploadProgress: function(event, position, total, percentComplete) {
					        	var percentVal = percentComplete + '%';
					        	bar.width(percentVal).attr('aria-valuenow',percentComplete);
					        	percent.html(percentVal);
					    	},
					    	success: function(data) {
					        	var percentVal = '100%';
					        	bar.width(percentVal)
					        	percent.html(percentVal);
					        	progress.hide();
				     		var message = 'Store updated successfully';
				     	 	if (data.trim() == "error") {
			                   		message = 'Error occured, please try again later.';
			                	}
					     	$.alert({
					     		title: '',
					     		content: message,
								onClose: function () {
			                			window.location.href = 'store-management.php';
								},					     		
					     	});
					     	$("#loader-1").hide();
					    	},
						complete: function(xhr) {
					        	bar.width('0%').attr('aria-valuenow',0);
						}
					});
		            	return false;
		        	}
		    	});
		});
	</script>
	<script>
		jQuery(document).ready(function($) {
			var codeHtml = '';
			var res = $.parseJSON('<?php echo $all_stores;?>');
			$.each(res, function(i, v){
				if( v.country_code == '<?php echo $res_edit_store->country_code;?>' ){
					codeHtml += '<option value="'+v.country_code+'" selected>'+v.country_code+'</option>';
				}else{
					codeHtml += '<option value="'+v.country_code+'">'+v.country_code+'</option>';
				}

			});
			$('#country-code').html(codeHtml);
			var nameHtml = ''
			$.each(res, function(i, v){
				if( v.name == '<?php echo $res_edit_store->country_name;?>' ){
					nameHtml += '<option value="'+v.name+'" selected>'+v.name+'</option>';
				}else{
					nameHtml += '<option value="'+v.name+'">'+v.name+'</option>';
				}
			});
			$('#country-name').html(nameHtml);

			var codeHtml = '';
			$.each(window.flags, function(i, v){
				if( v.id == '<?php echo $res_edit_store->country_name_analytics;?>' ){
					codeHtml += '<option value="'+v.id+'" selected>'+v.id+'</option>';
				}else{
					codeHtml += '<option value="'+v.id+'">'+v.id+'</option>';
				}
			});
			$('#country-name-analytics').html(codeHtml);

			var currencyHtml = ''
			$.each(window.currencies, function(i, v){
				if( v.code == '<?php echo $res_edit_store->currency_analytics;?>' ){
					currencyHtml += '<option value="'+v.code+'" selected>'+v.code+'</option>';
				}else{
					currencyHtml += '<option value="'+v.code+'">'+v.code+'</option>';
				}
			});
			$('#currency-analytics').html(currencyHtml);

			$("#country-code, #country-name, #currency-analytics, #country-name-analytics").select2({
			  	dropdownParent: $('.white-popup'),
			});
		});
	</script>
</div>
<?php
	}
	if( $_REQUEST['command'] == 'getMagentoStores' ){
		$lang = $_REQUEST['lang'];
		if($lang=='en' || $lang=='ar'){
			$sql = "SELECT * FROM tbl_magento_stores WHERE lang = '".$lang."'";
			$res = $db->get_results($sql);
		}else{
			$sql = "SELECT * FROM tbl_magento_stores";
			$res = $db->get_results($sql);
		}
		echo json_encode($res);
	}

if( $_REQUEST['command'] == 'edit-country-form' ){
    $id = $_POST['ID'];
    $sql = "SELECT * FROM tbl_countries WHERE id='" . $id . "'";
    $res_edit_country = $db->get_row($sql);
?>
<div id="edit-store-form-popup" class="white-popup zoom-anim-dialog">
    <h3>Edit country</h3>
    <hr>
    <form name="edit-country-form" id="edit-country-form" action="./ajax/edit.php" method="post"
          enctype="multipart/form-data">
        <input type="hidden" name="command" value="edit-country">
        <input type="hidden" name="country_id" value="<?php echo $res_edit_country->id; ?>">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="country-name">Country Name</label>
                    <input type="text" name="country-name" class="form-control" id="country-name" required
                           value="<?php echo $res_edit_country->country_name; ?>">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="short-code">Short Code</label>
                    <input type="text" name="short-code" class="form-control" id="short-code" required
                           value="<?php echo $res_edit_country->short_code; ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="country-code">Country Code</label>
                    <input type="text" name="country-code" class="form-control" id="country-code" required
                           value="<?php echo $res_edit_country->country_code; ?>">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="status">Status</label>
                    <select name="status" class="form-control" id="status" required>
                        <option value="1" <?php if($res_edit_country->status=='1'){ echo "selected";}?>>Published</option>
                        <option value="0" <?php if($res_edit_country->status=='0'){ echo "selected";}?>>Disabled</option>
                    </select>
                </div>
            </div>
        </div>
        <hr>
        <button type="submit" id="submit-1" class="btn btn-primary">Save <span id="loader-1" style="display:none"><i
                        class="fa fa-spinner fa-spin"></i></span></button>
    </form>
    <script>
        jQuery(document).ready(function ($) {
            $("#edit-country-form").validate({
                errorPlacement: function (error, element) {
                    $(element).parents().eq(0).append(error);
                },
                submitHandler: function (form) {
                    $(form).ajaxSubmit({
                        beforeSend: function () {
                            $("#loader-1").show();
                            $('#submit-1').attr('disabled', 'disabled');
                        },

                        success: function (data) {
                            var message = 'Country updated successfully';
                            if (data.trim() == "error") {
                                message = 'Error occured, please try again later.';
                            }
                            $.alert({
                                title: '',
                                content: message,
                                onClose: function () {
                                    window.location.href = 'countries-management.php?store=<?php echo $_POST['store'];?>';
                                },
                            });
                            $("#loader-1").hide();
                        },
                        complete: function (xhr) {
                            bar.width('0%').attr('aria-valuenow', 0);
                        }
                    });
                    return false;
                }
            });
        });
    </script>
    <?php
    }
    ?>
