<?php
include "../config.php";
ini_set('display_errors', 0);

$image = $_POST['image'];
// $buttons = json_encode($_POST['buttons'], JSON_UNESCAPED_UNICODE);
$buttons = $_POST['buttons'];
$app_id = $_POST['appid'];
$row_id = $_POST['_id'];

if($_POST['appid'] == GLOBAL_EN || $_POST['appid'] == GLOBAL_AR){
     $is_global = '1';
     }else{
          $is_global = '0';
     }

$sql = "SELECT count(*) FROM tbl_top_sections WHERE app_id = '" . $app_id . "'";
$totalrows = $db->get_var($sql);
$status = 0;
if ($totalrows == 0) {
     $status = 1;
}

$error = false;
if ($row_id == '') {
     $sql = "INSERT INTO tbl_top_sections SET
               app_id = '" . $app_id . "',
               image_url = '" . $image[0] . "',
               show_in_app='1', 
               is_global='".$is_global."',
               status = '".$status."'";
     if ($db->query($sql) !== false) {

          $tid = $db->insert_id;

          $sql_ins = "INSERT INTO tbl_top_sections_status SET
               app_id = '".$app_id."',
               top_section_id  = '".$tid."',
               status = '0'
               ";
               $db->query($sql_ins);

          $check_app = "SELECT id FROM tbl_apps WHERE parent_app = '".$app_id."'";
          $res_app = $db->get_results($check_app);
          if($res_app){
               foreach ($res_app  as $res_app_single) {
                    $sql_ins = "INSERT INTO tbl_top_sections_status SET
                    app_id = '".$res_app_single->id."',
                    top_section_id  = '".$tid."',
                    status = '0'
                    ";
                    $db->query($sql_ins);
               }
          }

          $sql = '';
          foreach ($buttons as $key => $button) {
               $sql = "INSERT INTO tbl_attachments SET
                    app_id = '" . $app_id . "',
                    p_id = '" . $tid . "',
                    section_type = 'top_section',
                    attachment = '" . json_encode($button['attachment'], JSON_UNESCAPED_UNICODE) . "',
                    btn_text = '".$button['value']."',
                    show_in_app='1', 
                    is_global='".$is_global."',
                    sort_order = '".$key."'";
               if($db->query($sql)){
                    $rowId = $db->insert_id;

                    $sql = "INSERT INTO tbl_sort_rows SET
                         app_id = '" . $app_id . "',
                         row_id = '" . $rowId . "',
                         row_type = 'top_section',
                         row_type_id = '" . $tid . "',
                         sort_order = '" . $key . "'";
                    $db->query($sql);
               }

          }

          $check_app = "SELECT id FROM tbl_apps WHERE parent_app = '".$app_id."'";
               $res_app = $db->get_results($check_app);
               if($res_app){
                    foreach ($res_app  as $res_app_single) {
                              $sql_ts = "SELECT * FROM tbl_sort_rows WHERE app_id ='".$app_id."' AND row_type='top_section' AND row_type_id='".$tid."'";
                         $res_ts = $db->get_results($sql_ts);
                         if($res_ts){
                              foreach($res_ts as $res_ts_single){
                                   //print_r($res_ts_single);
                                   $sql_tse = "SELECT id FROM tbl_sort_rows WHERE app_id ='".$res_app_single->id."' AND row_type='top_section' AND row_type_id='".$tid."' AND row_id='".$res_ts_single->row_id."'";
                                   $res_tse = $db->get_row($sql_tse);
                                   //print_r($res_tse);
                                   if(!$res_tse){
                                        $sql = "INSERT INTO tbl_sort_rows SET
                                             app_id = '" . $res_app_single->id. "',
                                             row_id = '" . $res_ts_single->row_id . "',
                                             row_type = '" . $res_ts_single->row_type . "',
                                             row_type_id = '" . $res_ts_single->row_type_id. "',
                                             sort_order = '" . $res_ts_single->sort_order."'";
                                        $res = $db->query($sql);
                                   }
                              }
                    }    }
               }

     } else {
          $error = true;
     }
} else {
     $sql = "UPDATE tbl_top_sections SET
               image_url = '" . $image[0] . "'
               WHERE id = '" . $row_id . "'";
     if ($db->query($sql) !== false) {
          $sql_del = "DELETE FROM tbl_sort_rows WHERE 
               app_id = '$app_id'
               AND row_type_id = '$row_id'
               AND row_type = 'top_section'";
          $db->query($sql_del);
          
          $sql_del = $sql = '';
          foreach ($buttons as $key => $button) {
               if($button['appid'] == $app_id){
                    if($button['btnid']){

                         $sql_update = "UPDATE tbl_attachments SET
                              attachment = '" . json_encode($button['attachment'], JSON_UNESCAPED_UNICODE) . "',
                              sort_order = '".$key."' WHERE id='".$button['btnid']."'";
                         $db->query($sql_update);

                    }else{
                         $sql_insert = "INSERT INTO tbl_attachments SET
                              app_id = '" . $app_id . "',
                              p_id = '" . $row_id . "',
                              section_type = 'top_section',
                              attachment = '" . json_encode($button['attachment'], JSON_UNESCAPED_UNICODE) . "',
                              btn_text = '".$button['value']."',
                              show_in_app='1', 
                              is_global='".$is_global."',
                              sort_order = '".$key."'";
                         $db->query($sql_insert);
                         $insert_id = $db->insert_id;
                    }
               }

               $rID = (isset($button['btnid'])) ? $button['btnid'] : $insert_id;
               $sql = "INSERT INTO tbl_sort_rows SET
                    app_id = '" . $app_id . "',
                    row_id = '" . $rID . "',
                    row_type = 'top_section',
                    row_type_id = '" . $row_id . "',
                    sort_order = '" . $key . "'";
               $db->query($sql);  
          }
          $check_app = "SELECT id FROM tbl_apps WHERE parent_app = '".$app_id."'";
               $res_app = $db->get_results($check_app);
               if($res_app){
                    foreach ($res_app  as $res_app_single) {
                              $sql_ts = "SELECT * FROM tbl_sort_rows WHERE app_id ='".$app_id."' AND row_type='top_section' AND row_type_id='".$row_id."'";
                         $res_ts = $db->get_results($sql_ts);
                         if($res_ts){
                              foreach($res_ts as $res_ts_single){
                                   //print_r($res_ts_single);
                                   $sql_tse = "SELECT id FROM tbl_sort_rows WHERE app_id ='".$res_app_single->id."' AND row_type='top_section' AND row_type_id='".$row_id."' AND row_id='".$res_ts_single->row_id."'";
                                   $res_tse = $db->get_row($sql_tse);
                                   //print_r($res_tse);
                                   if(!$res_tse){
                                        $sql = "INSERT INTO tbl_sort_rows SET
                                             app_id = '" . $res_app_single->id. "',
                                             row_id = '" . $res_ts_single->row_id . "',
                                             row_type = '" . $res_ts_single->row_type . "',
                                             row_type_id = '" . $res_ts_single->row_type_id. "',
                                             sort_order = '" . $res_ts_single->sort_order."'";
                                        $res = $db->query($sql);
                                   }
                              }
                    }    }
               }
         
     } else {
          $error = true;
     }
}
if ($error) {
     echo "error";
} else {
     echo $row_id;
}