<?php
    include '../config.php'; 
    $_GET['img_index'] = isset($_GET['img_index']) ? $_GET['img_index'] : '';
    $_GET['page_global'] = isset($_GET['page_global']) ? $_GET['page_global'] : '';
    $_GET['appid_global'] = isset($_GET['appid_global']) ? $_GET['appid_global'] : '';
    $_GET['optionVal'] = isset($_GET['optionVal']) ? $_GET['optionVal'] : '';
    $_GET['optionSel'] = isset($_GET['optionSel']) ? $_GET['optionSel'] : '';
?>
<a href="#" data-hide=".contentDdPopup" class="close-popup"><i class="fa fa-times"></i></a>
<div class="ddPopupPageContainer">
    <div class="ptPage">
        <div class="ptContentType">
            <div class="ddTitle">
                <span class="ddTitleText"> CHOOSE CONTENT </span>
                <p>Where do you want your user to go when he taps on this image?</p>
            </div>
            <?php require './includes/content/list.php'; ?>
        </div>
        <div class="ptContentPageSection">
            <?php
                require './includes/content/brand.php'; 
                require './includes/content/category.php'; 
                require './includes/content/lander.php'; 
                require './includes/content/no-link.php'; 
                require './includes/content/product.php'; 
                require './includes/content/suggest-brand.php'; 
                require './includes/content/url.php'; 
                require './includes/content/static-lander-category.php'; 
            ?>
        </div>
    </div>
    <script>
        if ('2' == '<?php echo $_GET['optionSel']; ?>') {
            $('#product-search').trigger('keyup');
        }
        $('[data-val="<?php echo $_GET['optionSel']; ?>"]').trigger('click');
    </script>
</div>