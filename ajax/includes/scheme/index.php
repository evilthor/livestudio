<?php if( $_REQUEST['command'] == 'add-scheme-form' ){ ?>
<div id="add-scheme-form-popup" class="white-popup zoom-anim-dialog">
	<h3>Add scheme</h3>
	<hr>
	<form name="add-scheme-form" id="add-scheme-form" action="./ajax/add.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="command" value="add-scheme">
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="name">Name</label>
				    	<input type="text" name="name" class="form-control" id="name" required>
				</div>
			</div>
			<div class="col-lg-6">
            		<label for="style">Style</label>
            		<input type="file" name="style" id="style" required accept="text/css">
				<div class="progress">
				  	<div 
						class="progress-bar" 
						role="progressbar" 
						aria-valuenow="60" 
						aria-valuemin="0" 
						aria-valuemax="100" 
						style="width: 0%;"
				  	>
				  	</div>
				</div>
        		</div>
		</div>
		<hr>
		<button type="submit" id="submit-1" class="btn btn-primary">Save</button>
	</form>
	<script>
		jQuery(document).ready(function($) {
		    $("#add-scheme-form").validate({
		          errorPlacement: function(error, element) {
		               $(element).parents().eq(0).append(error);
		          },
		        	submitHandler: function(form) {
					var progress = $('.progress');
					var bar = $('.progress-bar');
					var percent = $('.percent');
					$(form).ajaxSubmit({
					    	beforeSend: function() {
			                    $('#submit-1').button('loading');
					    		progress.show();
					        	var percentVal = '0%';
					        	bar.width(percentVal)
					        	percent.html(percentVal);
					    	},
					    	uploadProgress: function(event, position, total, percentComplete) {
					        	var percentVal = percentComplete + '%';
					        	bar.width(percentVal).attr('aria-valuenow',percentComplete);
					        	percent.html(percentVal);
					    	},
					    	success: function(data) {
			                    $('#submit-1').button('rest');
					        	var percentVal = '100%';
					        	bar.width(percentVal)
					        	percent.html(percentVal);
					        	progress.hide();
				            	GET([{
				                	type: 'link',
				                	data: {
				                    	command: 'schemes-list',
				                	},
				                	callback: {
				                    	complete: function(XHR, textStatus) {
				                        	$('.theme-schemes #list').html(XHR.responseText);
				                    	}
				                	}
				            	}]);  
				     		var message = 'Color scheme add successfully';
				     	 	if (data.trim() == "error") {
			                   		message = 'Error occured, please try again later.';
			                	}
					     	$.alert({
					     		title: '',
					     		content: message,
								onClose: function () {
			                			if( $.magnificPopup ){
			                				$.magnificPopup.close()
			                			}
								},					     		
					     	}); 
					    	},
						complete: function(xhr) {
					        	bar.width('0%').attr('aria-valuenow',0);
						}
					});
	            		return false;
		        	}
		    	});
		});
	</script>
</div>
<?php
	}
	if( $_REQUEST['command'] == 'schemes-list' ){
?>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  	<?php
         	$sql_schemes = "SELECT * FROM tbl_color_schemes WHERE 1=1";
         	$res_schemes = $db->get_results($sql_schemes);
  	?>
 	<ul class="list-group">
          <?php
              	if($res_schemes){
                  	foreach ($res_schemes as $res_scheme) {
                      
          ?>
          <li class="list-group-item">
          	<code><?php echo $res_scheme->title;?></code>
          	<?php if( $res_scheme->active != '1' ){ ?> 
          	<script>
          		var del_option_<?php echo $res_scheme->id;?> = {
          			type: 'link',
          			data :{
          				"command" : "delete-scheme", 
          				"id" : "<?php echo $res_scheme->id;?>"
          			},
					callback: {
						beforeSend: function(xhr) {
							$('#delete-scheme-<?php echo $res_scheme->id;?>').fadeIn();
						},
						complete: function(XHR, textStatus) {
						    GET([{
						        type: 'link',
						        data: {
						            command: 'schemes-list',
						        },
						        callback: {
						            complete: function(XHR, textStatus) {
						                $('.theme-schemes #list').html(XHR.responseText);
						            }
						        }
						    }]);
						},
						success: function(data, textStatus, XHR) {
							$('#delete-scheme-<?php echo $res_scheme->id;?>').fadeOut();
						},
					}
          		}
          	</script>
          	<a 
          		style="margin-left: 6px;"
          		href="#" 
          		onClick="Delete(del_option_<?php echo $res_scheme->id;?>); return false;"
          		class="btn btn-xs btn-danger pull-right"
          	>Delete <span style="display:none" id="delete-scheme-<?php echo $res_scheme->id;?>"><i class="fa fa-spinner fa-spin"></i></span></a>
               <?php } ?>
               <script>
                    var active_scheme_<?php echo $res_scheme->id;?> = {
                         type: 'link',
                         data :{
                              "command" : "activate-scheme", 
                              "id" : "<?php echo $res_scheme->id;?>"
                         },
                         callback: {
                              beforeSend: function(xhr) {
                                   $('#active-scheme-<?php echo $res_scheme->id;?>').button('loading');
                              },
                              complete: function(XHR, textStatus) {
                              	var data = $.parseJSON(XHR.responseText);
                              	// console.log($.parseJSON(XHR.responseText));
                              	$('link#color-scheme').attr('href','./color-schemes/'+data.slug)
					            GET([{
					                type: 'link',
					                data: {
					                    command: 'schemes-list',
					                },
					                callback: {
					                    complete: function(XHR, textStatus) {
					                        $('.theme-schemes #list').html(XHR.responseText);
					                    }
					                }
					            }]);
                              },
                              success: function(data, textStatus, XHR) {
                                   $('#active-scheme-<?php echo $res_scheme->id;?>').button('reset');
                              },
                         }
                    }
               </script>
               <button
                    type="button" 
                    data-loading-text="Loading..."
                    id="active-scheme-<?php echo $res_scheme->id;?>"
                    <?php echo ( $res_scheme->active == '1' ) ? 'disabled': ''; ?>
                    class="btn btn-xs pull-right <?php echo ( $res_scheme->active == '1' ) ? 'btn-success': 'btn-warning'; ?>"
                    onClick="Edit(active_scheme_<?php echo $res_scheme->id;?>); return false;"
               >Activate</button>
          </li>
     	<?php
         			}
     		}
     	?>
 	</ul>
</div>
<?php } ?>