<?php
	$categoryManager = array(
	   'add-category',
	   'edit-category',
        'desktop-edit-lander',
        'desktop-add-lander',
	);
	$landerShow = array(
        'add-merchandise',
        'edit-merchandise',
        'add-category',
        'edit-category',
        'add-search-category',
        'edit-search-category',
        'new-top-section',
        'edit-top-section',
        'add-lander',
        'edit-lander',
	);
?>
<ul class="ddList">
	<li> <a href="javascript:;" data-imgIndex="<?php echo $_GET['img_index'] ?>" data-val="1" data-page-show=".ptContentPageSection" data-page-current="#category" data-page=".contentPage" data-list-hide=".ptContentType">category <i class="fa fa-angle-right" aria-hidden="true"></i></a> </li>
 	<?php if(!in_array($_GET['page_global'], $categoryManager)){ ?>
	<!-- <li> <a href="javascript:;" data-imgIndex="<?php echo $_GET['img_index'] ?>" data-val="2" data-page-show=".ptContentPageSection" data-page-current="#product" data-page=".contentPage" data-list-hide=".ptContentType">product <i class="fa fa-angle-right" aria-hidden="true"></i></a> </li> -->
        <li> <a href="javascript:;" data-imgIndex="<?php echo $_GET['img_index'] ?>" data-val="3" data-page-show=".ptContentPageSection" data-page-current="#url" data-page=".contentPage" data-list-hide=".ptContentType">url <i class="fa fa-angle-right" aria-hidden="true"></i></a> </li>
	<li> <a href="javascript:;" data-imgIndex="<?php echo $_GET['img_index'] ?>" data-val="4" class="no-link <?php echo $_GET['optionSel'] == '4' ? 'tick-active' : ''; ?>">keep static (no link)</a> </li>
	<?php } ?>
	<li> <a href="javascript:;" data-imgIndex="<?php echo $_GET['img_index'] ?>" data-val="5" data-page-show=".ptContentPageSection" data-page-current="#brand" data-page=".contentPage" data-list-hide=".ptContentType">Brand <i class="fa fa-angle-right" aria-hidden="true"></i></a> </li>
 	<?php if(in_array($_GET['page_global'], $landerShow)){ ?>
	<li> <a href="javascript:;" data-imgIndex="<?php echo $_GET['img_index'] ?>" data-val="6" data-page-show=".ptContentPageSection" data-page-current="#lander" data-page=".contentPage" data-list-hide=".ptContentType">Lander <i class="fa fa-angle-right" aria-hidden="true"></i></a> </li>
	<?php } ?>
</ul>