<?php
    $landerShow = array(
        'add-merchandise',
        'edit-merchandise',
        'add-category',
        'edit-category',
        'add-search-category',
        'edit-search-category',
        'new-top-section',
        'edit-top-section',
        'add-lander',
        'edit-lander',
    );

    $landerShowStatic = array(
        'new-top-section',
        'edit-top-section',
    );
    if(in_array($_GET['page_global'], $landerShow)){

?>
<div class="contentPage lander" id="lander">
    <div class="page-title clearfix">
        <a href="#" class="back" data-backTo=".ptContentType" data-hide=".ptContentPageSection"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
        <h6>choose lander</h6>
    </div>
    <div class="ptDetailSection">
        <div id="landers">
            <div class="form-group">
                <input type="text" class="form-control search" placeholder="Search lander">
            </div>
            <ul class="lander-list list">
                <?php
                $sql_apps = "SELECT * FROM tbl_apps WHERE id = '".$_GET['appid_global']."'";
                $res_apps = $db->get_row($sql_apps);
                $app_store_temp = $res_apps->app_store;
                $app_store_temp_arr = explode("_", $app_store_temp);
                $parent_app = $app_store_temp_arr[0].'_global';
                $sql_parent = "SELECT id FROM tbl_apps WHERE app_store = '".$parent_app."'";
                $res_parent = $db->get_row($sql_parent);
                    $and = isset($_GET['pid']) && !empty($_GET['pid']) ? "AND id NOT IN('".$_GET['pid']."')" : '';
                    $sql = "SELECT * FROM tbl_lander_pages WHERE app_id = '".$res_parent->id."' ". $and;
                    $landers = $db->get_results($sql);
                    if($landers){
                        foreach($landers as $lander){
                ?>
                <li data-toggle="tooltip" data-placement="right" title="">
                    <label for="lander-radio-<?php echo $lander->id;?>" class="clearfix relation">
                        <div class="ptText name">
                            <span class="text text-<?php echo $lander->id;?>"><?php echo $lander->title;?></span>
                            <span class="hide"></span>
                        </div>
                        <div class="ptCheckbox">
                            <input 
                                data-text=".lander-list .text-<?php echo $lander->id;?>" 
                                data-imgIndex="<?php echo $_GET['img_index'] ?>" 
                                type="radio" 
                                name="lander" 
                                id="lander-radio-<?php echo $lander->id;?>" 
                                value="<?php echo $lander->id;?>" 
                                <?php if( $lander->id == $_GET['optionVal'] ){ echo "checked=checked"; } ?>
                            >
                        </div>
                    </label>
                </li>
                <?php
                        }
                    }

                    if(in_array($_GET['page_global'], $landerShow)){
                ?>

                <li data-toggle="tooltip" data-placement="right" title="">
                    <label for="lander-radio-sm" class="clearfix relation">
                        <div class="ptText name">
                            <span class="text text-static-lander-men">Static Lander (Men)</span>
                            <span class="hide"></span>
                        </div>
                        <div class="ptCheckbox">
                            <input 
                                data-text=".lander-list .text-static-lander-men" 
                                data-imgIndex="sm" 
                                type="radio" 
                                name="lander" 
                                id="lander-radio-sm" 
                                value="sm" 
                                <?php if($_GET['optionVal']=='sm'){ echo "checked=checked"; } ?>
                            >
                        </div>
                    </label>
                </li>

                <li data-toggle="tooltip" data-placement="right" title="">
                    <label for="lander-radio-sw" class="clearfix relation">
                        <div class="ptText name">
                            <span class="text text-static-lander-women">Static Lander (Women)</span>
                            <span class="hide"></span>
                        </div>
                        <div class="ptCheckbox">
                            <input 
                                data-text=".lander-list .text-static-lander-women" 
                                data-imgIndex="sw" 
                                type="radio" 
                                name="lander" 
                                id="lander-radio-sw" 
                                value="sw" 
                                <?php if($_GET['optionVal']=='sw'){ echo "checked=checked"; } ?>
                            >
                        </div>
                    </label>
                </li>

                <li data-toggle="tooltip" data-placement="right" title="">
                    <label for="lander-radio-sk" class="clearfix relation">
                        <div class="ptText name">
                            <span class="text text-static-lander-kids">Static Lander (Kids)</span>
                            <span class="hide"></span>
                        </div>
                        <div class="ptCheckbox">
                            <input 
                                data-text=".lander-list .text-static-lander-kids" 
                                data-imgIndex="sk" 
                                type="radio" 
                                name="lander" 
                                id="lander-radio-sk" 
                                value="sk" 
                                <?php if($_GET['optionVal']=='sk'){ echo "checked=checked"; } ?>
                            >
                        </div>
                    </label>
                </li>
                <?php
            }
                ?>
            </ul>
        </div>
    </div>
    <script type="text/javascript">
        var userList, options = {
            valueNames: [
                'name'
            ],
            fuzzySearch: {
                searchClass: "search",
                location: 0,
                distance: 1000,
                threshold: 0.4,
                multiSearch: true
            }
        };
        userList = new List('landers', options);
        // var selected = $('#categories .category-list .selected'),
        // _backup = selected.clone();
        // selected.remove();
        // $('#categories .category-list').prepend(_backup)        
    </script>
</div>
<?php } ?>