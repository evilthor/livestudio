<div class="contentPage static-lander-category" id="static-lander-category">
    <div class="page-title clearfix">
        <a href="#" class="back" data-backTo=".ptContentPageSection .lander" data-hide=".ptContentPageSection .static-lander-category"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
        <h6>choose category</h6>
    </div>
    <div class="ptDetailSection">
        <div id="static-lander-categories">
            <div class="form-group">
                <input type="text" class="form-control search" placeholder="Search category">
            </div>
            <ul class="static-lander-categories-list list"></ul>
        </div>
    </div>
</div>