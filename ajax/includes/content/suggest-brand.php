<div class="contentPage suggest-brand" id="suggest-brand">
    <div class="page-title clearfix">
        <a href="#" class="back" data-backTo=".ptContentPageSection .category" data-hide=".ptContentPageSection .suggest-brand"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
        <h6>choose brand</h6>
    </div>
    <div class="ptDetailSection">
        <div id="suggest-brands">
            <div class="form-group">
                <input type="text" class="form-control search" placeholder="Search brand">
            </div>
            <ul class="suggest-brand-list list"></ul>
            <div class="done-section">
                <button id="btnDoneSuggestBrands" data-hide=".contentDdPopup, .ddPopup" class="done-btn done btn btn-primary btn-block">skip &amp; save</button>
            </div>
        </div>
    </div>
</div>