<div class="contentPage brand" id="brand">
    <div class="page-title clearfix">
        <a href="#" class="back" data-backTo=".ptContentType" data-hide=".ptContentPageSection"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
        <h6>choose brand</h6>
    </div>
    <div class="ptDetailSection">
        <div id="brands">
            <div class="form-group">
                <input type="text" class="form-control search" placeholder="Search brand">
            </div>
            <ul class="brand-list list"></ul>
        </div>
    </div>
    <script>
        //code added for handling brands
        var markup = '';
        var i = 0;
        $.each(window.brands, function(index, val) {
            var url_key = val.hasOwnProperty('url_key') ? val.url_key : '';
            if ((val.id == '<?php echo $_GET['optionVal']; ?>' || url_key == '<?php echo $_GET['optionVal']; ?>') && '<?php echo $_GET['optionSel']; ?>' == '5') {
                markup += '<li class="selected">';
                markup += '<label for="brand-list-' + val.id + '" class="clearfix relation">';
                markup += '<div class="ptText name"><span class="text text-' + val.id + '">' + val.name + '</span><span class="hide">' + val.id + '</span></div>';
                markup += '<div class="ptCheckbox"><input data-url="' + url_key + '" data-text=".brand-list .text-' + val.id + '" data-imgIndex="<?php echo $_GET['img_index'] ?>" type="radio" name="brand" id="brand-list-' + val.id + '" value="' + val.id + '" checked="checked"></div>';
                markup += '</label>';
                markup += '</li>';
            } else {
                markup += '<li>';
                markup += '<label for="brand-list-' + val.id + '" class="clearfix relation">';
                markup += '<div class="ptText name"><span class="text text-' + val.id + '">' + val.name + '</span><span class="hide">' + val.id + '</span></div>';
                markup += '<div class="ptCheckbox"><input data-url="' + url_key + '" data-text=".brand-list .text-' + val.id + '" data-imgIndex="<?php echo $_GET['img_index'] ?>" type="radio" name="brand" id="brand-list-' + val.id + '" value="' + val.id + '"></div>';
                markup += '</label>';
                markup += '</li>';
            }
        });

        $('.brand-list').html(markup);
        
        var userList, options = {
            valueNames: [
                'name'
            ],
            fuzzySearch: {
                searchClass: "search",
                location: 2,
                distance: 100,
                threshold: 0.4,
                multiSearch: true
            }
        };
        userList = new List('brands', options);
        var selected = $('#brands .brand-list .selected');
        _backup = selected.clone();
        selected.remove();
        $('#brands .brand-list').prepend(_backup)
    </script>
</div>