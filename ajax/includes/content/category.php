<div class="contentPage category" id="category">
    <div class="page-title clearfix">
        <a href="#" class="back" data-backTo=".ptContentType" data-hide=".ptContentPageSection"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
        <h6>choose category</h6>
    </div>
    <div class="ptDetailSection">
        <div id="categories">
            <div class="form-group">
                <input type="text" class="form-control cate-search" placeholder="Search category">
            </div>
            <ul class="category-list list"></ul>
            <div id="categories-loader" class="loader" style="display: none"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
        </div>
    </div>
    <script>
        var markup = '';
        var i = 0;
        $.each(window.categories, function(index, val) {
            var relation = val.relation, tip = val.id + ": " + val.name;
            rel = '';
            var count = relation.length;
            var i=0;
            $.each(relation, function(index, el) {
                i++;
                var classA = '';
                if( count == i ){
                    classA = "class='last'";
                }
                rel += '<em '+classA+'>'+el.toString()+'</em>';
            });
            relation = rel;
            var url_key = val.hasOwnProperty('url_key') ? val.url_key : '';
            if ((val.id == '<?php echo $_GET['optionVal']; ?>' || url_key == '<?php echo $_GET['optionVal']; ?>') && '<?php echo $_GET['optionSel']; ?>' == '1') {
                markup += '<li class="selected" data-toggle="tooltip" data-placement="right" title="' + tip + '">';
                markup += '<label for="category-list-' + val.id + '" class="clearfix relation">';
                markup += '<div class="ptText name text text-' + val.id + '" data-title="' + val.name + '">' + relation + '</div>';
                markup += '<div class="id hide">' + val.id + '</div>';
                markup += '<div class="ptCheckbox"><input data-url="' + url_key + '" data-text=".category-list .text-' + val.id + '" data-imgIndex="<?php echo $_GET['img_index'] ?>" type="radio" name="category" id="category-list-' + val.id + '" value="' + val.id + '" checked="checked"></div>';
                markup += '</label>';
                markup += '</li>';
            } else {
                markup += '<li data-toggle="tooltip" data-placement="right" title="' + tip + '">';
                markup += '<label for="category-list-' + val.id + '" class="clearfix relation">';
                markup += '<div class="ptText name text text-' + val.id + '" data-title="' + val.name + '">' + relation + '</div>';
                markup += '<div class="id hide">' + val.id + '</div>';
                markup += '<div class="ptCheckbox"><input data-url="' + url_key + '" data-text=".category-list .text-' + val.id + '" data-imgIndex="<?php echo $_GET['img_index'] ?>" type="radio" name="category" id="category-list-' + val.id + '" value="' + val.id + '"></div>';
                markup += '</label>';
                markup += '</li>';
            }
        });

        $('.category-list').html(markup);
        $('[data-toggle="tooltip"]').tooltip();

        var userList, options = {
            valueNames: [
                'name', 'id'
            ],
            fuzzySearch: {
                searchClass: "search",
                location: 0,
                distance: 1000,
                threshold: 0.4,
                multiSearch: true
            }
        };
        userList = new List('categories', options);
        var selected = $('#categories .category-list .selected'),
        _backup = selected.clone();
        selected.remove();
        $('#categories .category-list').prepend(_backup);
        jQuery(".cate-search").keyup(function () {
            var filter = jQuery(this).val();
            filter = filter.toLowerCase();
            jQuery("#categories .category-list li").each(function () {
                var text = jQuery(this).find('.name em');
                var str = '';
                jQuery.each(text, function (i, v) {
                    str += jQuery(v).text() + ' ';
                });
                str = str.toLowerCase();
                if (str.search(new RegExp(filter, "i")) < 0) {
                    jQuery(this).hide();
                } else {
                    jQuery(this).show()
                }
            });
        });
    </script>
</div>