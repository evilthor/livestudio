<?php
    $categoryManager = array(
       'add-category',
       'edit-category',
    );
?>
<?php if(!in_array($_GET['page_global'], $categoryManager)){ ?>
<div class="contentPage product" id="product">
    <div class="page-title clearfix">
        <a href="#" class="back" data-backTo=".ptContentType" data-hide=".ptContentPageSection"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
        <h6>choose product</h6>
    </div>
    <div class="ptDetailSection">
        <div class="form-group">
            <input type="text" class="form-control" id="product-search" placeholder="Enter product ID or SKU" data-imgIndex="<?php echo $_GET['img_index'] ?>" value="<?php if($_GET['optionSel'] == '2' and $_GET['optionVal'] != ''){echo $_GET['optionVal'];} ?>">
        </div>
        <ul class="product-list" style="height: 200px; overflow: hidden; overflow-y: auto;">
        </ul>
    </div>
</div>
<?php } ?>