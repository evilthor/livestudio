<?php
    $categoryManager = array(
       'add-category',
       'edit-category',
        'desktop-edit-lander',
        'desktop-add-lander',
    );
?>
<?php if(!in_array($_GET['page_global'], $categoryManager)){ ?>
<div class="contentPage url" id="url">
    <div class="page-title clearfix">
        <a href="#" class="back" data-backTo=".ptContentType" data-hide=".ptContentPageSection"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
        <h6>choose url</h6>
    </div>
    <div class="ptDetailSection">
        <div class="form-group">
            <input type="text" name="customurl" value="<?php if($_GET['optionSel'] == '3' and $_GET['optionVal'] != ''){echo $_GET['optionVal'];} ?>" id="customurl" class="form-control" placeholder="Enter your url, including http://" data-imgIndex="<?php echo $_GET['img_index'] ?>" data-button="#url .done">
        </div>
        <div class="done-btn">
             <button data-hide=".contentDdPopup" class="done btn btn-primary btn-xs" disabled="">done</button>
        </div>
    </div>
</div>
<?php } ?>