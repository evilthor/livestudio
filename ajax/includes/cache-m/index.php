<?php
	if( $_REQUEST['command'] == 'add-cache-category-form' ){
?>
<div id="add-cache-category-form-popup" class="white-popup zoom-anim-dialog">
	<h3>Add cache category</h3>
	<hr>
	<form name="add-cache-category-form" id="add-cache-category-form" action="#" method="post">
		<input type="hidden" name="command" value="add-cache-category">
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="category">Category</label>
					<input type="text" name="category" class="form-control" id="category" required>
				</div>
			</div>
			<div class="col-lg-6"></div>
		</div>
		<hr>
		<button 
			type="submit" 
			id="submit" 
			class="btn btn-primary"
			data-loading-text="Loading..."
		>Save</button>
		<a 
			href="#" 
			class="btn btn-xs btn-link __get_popup"
			data-data='{ "command" : "cache-category" }' 
			data-loader="#cache-category"
		>Back <span style="display:none" id="cache-category"><i class="fa fa-spinner fa-spin"></i></span></a>

	</form>
	<script>
		jQuery(document).ready(function($) {
			$.validator.addMethod('requiredNotBlank', function(value, element) {
				return $.validator.methods.required.call(this, $.trim(value), element);
			}, $.validator.messages.required);
			$("#add-cache-category-form").validate({
				rules:{
					category:{
						required: true,
						minlength: 3,
						requiredNotBlank: true
					}
				},
				submitHandler: function(form) {
					var options = {
						type: "form",
						formId: "#add-cache-category-form",
						callback: {
							beforeSend: function(xhr) {
								$('#submit').button('loading');
							},
							complete: function(XHR, textStatus) {
								var response = $.parseJSON(XHR.responseText);
								if( response.type != 'warn' ){
									GET([{
										type: 'link',
										data:{
											command: 'cache-category',
										},
										callback: {
											complete: function(XHR, textStatus) {
											    var el = $(XHR.responseText);
											    callPopup(el);
											}
										 }
									},{
									   type: 'link',
									   data:{
										  command: 'cache-management-list',
									   },
									   callback: {
											 complete: function(XHR, textStatus) {
												$('.cache-management #list').html(XHR.responseText);
											 }
										  }
								    }]);
								}else{
									$.notify(response.message,{
										position: 'top right',
										className: response.type,
										showAnimation: 'fadeIn',
										hideAnimation: 'fadeOut',
									});									
								}
							},
							success: function(data, textStatus, XHR) {
								$('#submit').button('reset');
							},
						}
					};
					Add(options);
				} 
			});
		});
	</script>
</div>
<?php
	}
	if( $_REQUEST['command'] == 'edit-cache-category-form' ){
		$sql_cache_cat_edit = "SELECT * FROM tbl_cache_categories WHERE id='".$_POST['id']."'";
		$res_cache_cat_edit = $db->get_row($sql_cache_cat_edit);
?>
<div id="edit-cache-category-form-popup" class="white-popup zoom-anim-dialog">
	<h3>Edit cache category</h3>
	<hr>
	<form name="edit-cache-category-form" id="edit-cache-category-form" action="#" method="post">
		<input type="hidden" name="command" value="edit-cache-category">
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="category">Category</label>
					<input type="text" name="category" class="form-control" id="category" required value="<?php echo $res_cache_cat_edit->title;?>">
				</div>
			</div>
			<div class="col-lg-6"></div>
		</div>
		<hr>
		<input type="hidden" name="catid" value="<?php echo $res_cache_cat_edit->id;?>">
		<button
			type="submit" 
			id="submit" 
			class="btn btn-primary"
			data-loading-text="Loading..."
		>Save <span id="loader-1" style="display:none"><i class="fa fa-spinner fa-spin"></i></span></button>
		<a 
			href="#" 
			class="btn btn-xs btn-link __get_popup"
			data-data='{ "command" : "cache-category" }' 
			data-loader="#cache-category"
		>Back <span style="display:none" id="cache-category"><i class="fa fa-spinner fa-spin"></i></span></a>	
	</form>
	<script>
		jQuery(document).ready(function($) {
			$.validator.addMethod('requiredNotBlank', function(value, element) {
				return $.validator.methods.required.call(this, $.trim(value), element);
			}, $.validator.messages.required);
			$("#edit-cache-category-form").validate({
				rules:{
					category:{
						required: true,
						minlength: 3,
						requiredNotBlank: true,
					}
				},
				submitHandler: function(form) {
					var options = {
						type: "form",
						formId: "#edit-cache-category-form",
						callback: {
							beforeSend: function(xhr) {
								$('#submit').button('loading');
							},
							complete: function(XHR, textStatus) {
								var response = $.parseJSON(XHR.responseText);
								if( response.type != 'warn' ){
									GET([{
										type: 'link',
										data:{
											command: 'cache-category',
										},
										callback: {
											complete: function(XHR, textStatus) {
											    var el = $(XHR.responseText);
											    callPopup(el);
											}
										 }
									},{
									   type: 'link',
									   data:{
										  command: 'cache-management-list',
									   },
									   callback: {
											 complete: function(XHR, textStatus) {
												$('.cache-management #list').html(XHR.responseText);
											 }
										  }
								    }]);
								}else{
									$.notify(response.message,{
										position: 'top right',
										className: response.type,
										showAnimation: 'fadeIn',
										hideAnimation: 'fadeOut',
									});									
								}
							},
							success: function(data, textStatus, XHR) {
								$('#submit').button('reset');
							},
						}
					};
					Edit(options);
				} 
			});
		});
	</script>
</div>
<?php
	}
	if( $_REQUEST['command'] == 'cache-category' ){
		$sql_cache_cat = "SELECT * FROM tbl_cache_categories";
		$res_cache_cat = $db->get_results($sql_cache_cat);
?>
<div id="cache-category" class="white-popup zoom-anim-dialog">
	<div class="row">
		<div class="col-lg-6">
			<h3 class="pull-left">Cache category</h3> 
			<a 
				style="margin-left: 12px; margin-top: 5px;"				
				href="#" 
				class="btn btn-xs btn-primary pull-left __get_popup"
				data-data='{ "command" : "add-cache-category-form" }' 
				data-loader="#add-cache-category-form"
			>Add category <span style="display:none" id="add-cache-category-form"><i class="fa fa-spinner fa-spin"></i></span></a>
			<?php if( count($res_cache_cat) > 0 ){ ?>
			<a 
				style="margin-left: 12px; margin-top: 5px;"				
				data-data='{ "command" : "add-api-service-form" }' 
				data-loader="#add-api-service-form"
				href="#" 
				class="btn btn-primary btn-xs pull-left __get_popup"
			>Add service <span style="display:none" id="add-api-service-form"><i class="fa fa-spinner fa-spin"></i></span></a>
			<?php } ?>
		</div>
	</div>
	<hr>
	<div style="max-height: 300px; min-height: 300px; overflow: hidden; overflow-y: auto;">
		<?php 
			if($res_cache_cat){
				$res_cache_cat = array_chunk($res_cache_cat, 2);
				foreach($res_cache_cat as $res_cache_cat_single){
		?>
		<div class="row">
			<?php
					foreach ($res_cache_cat_single as $key => $value) {
			?>
			<div class="col-lg-6">
				<div>
					<h4><?php echo $value->title;?></h4>
				</div>
				<div>
					<a 
						href="#" 
						class="btn btn-xs btn-primary __get_popup"
						data-data='{ "command" : "edit-cache-category-form", "id" : "<?php echo $value->id;?>"}'
						data-loader="#edit-cache-category-loader-<?php echo $value->id;?>"
					>Edit <span style="display:none" id="edit-cache-category-loader-<?php echo $value->id;?>"><i class="fa fa-spinner fa-spin"></i></span></a>
					<script>
						var del_option_<?php echo $value->id;?> = {
							type: 'link',
							data :{
								"command" : "delete-cache-category", 
								"id" : "<?php echo $value->id;?>"
							},
							callback : {
								beforeSend: function(xhr) {
									$('#delete-cache-category-<?php echo $value->id;?>').fadeIn();
								},
								complete: function(XHR, textStatus) {
									GET([{
										type: 'link',
										data:{
											command: 'cache-category',
										},
										callback: {
											complete: function(XHR, textStatus) {
											    var el = $(XHR.responseText);
											    callPopup(el);
											}
										 }
									},{
									   type: 'link',
									   data:{
										  command: 'cache-management-list',
									   },
									   callback: {
											 complete: function(XHR, textStatus) {
												$('.cache-management #list').html(XHR.responseText);
											 }
										  }
								    }]);
								},
								success: function(data, textStatus, XHR) {
									$('#delete-cache-category-<?php echo $value->id;?>').fadeOut();
								}
							}
						}
					</script>					
					<a 
						href="#" 
						class="btn btn-xs btn-danger"
						onClick="Delete(del_option_<?php echo $value->id;?>)"
					>Delete <span style="display:none" id="delete-cache-category-<?php echo $value->id;?>"><i class="fa fa-spinner fa-spin"></i></span></a>
				</div>
				<hr>
			</div>
			<?php
					}
			?>
		</div>
		<?php 
		}	
	}
		?>
	</div>
</div>
<?php
	}