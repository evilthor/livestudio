<?php
	if( $_REQUEST['command'] == 'add-api-service-form' ){

		$sql_cache_cat = "SELECT * FROM tbl_cache_categories";
		$res_cache_cat = $db->get_results($sql_cache_cat);
?>
<div id="add-api-service-form-popup" class="white-popup zoom-anim-dialog">
	<h3>Add service</h3>
	<hr>
	
	<form name="add-api-service-form" id="add-api-service-form" action="#" method="post">
		<input type="hidden" name="command" value="add-api-service">
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="cache-category">Cache category</label>
					<select name="cache-category" class="form-control" id="cache-category" required>
						<option value="">Please select</option>
						<?php 
							if($res_cache_cat){
								foreach ($res_cache_cat as $res_cache_cat_single) {?>
									<option value="<?php echo $res_cache_cat_single->id;?>"><?php echo $res_cache_cat_single->title;?></option>
								<?php
								}
							}
						?>
					</select>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="service">Service</label>
				    	<input type="text" name="service" class="form-control" id="service" aria-describedby="serviceHelp" required>
				    	<span id="serviceHelp" class="help-block">Hint:<br> *For specific store, please add like <code>/api/home.php?store=en_ae</code><br>*For all stores, please add like<code>/api/home.php</code></span>
				</div>
			</div>
		</div>
		<hr>
		<button type="submit" id="submit-1" class="btn btn-primary">Save <span id="loader-1" style="display:none"><i class="fa fa-spinner fa-spin"></i></span></button>
	</form>
	<script>
		jQuery(document).ready(function($) {
			$.validator.addMethod('requiredNotBlank', function(value, element) {
				return $.validator.methods.required.call(this, $.trim(value), element);
			}, $.validator.messages.required);
		    	$("#add-api-service-form").validate({
				rules:{
					service:{
						required: true,
						minlength: 3,
						requiredNotBlank: true,
					}
				},
				submitHandler: function(form) {
					var options = {
						type: "form",
						formId: "#add-api-service-form",
						callback: {
							beforeSend: function(xhr) {
								$('#loader-1').fadeIn();
							},
							complete: function(XHR, textStatus) {
								var response = $.parseJSON(XHR.responseText);
								if( response.type != 'warn' ){
								    GET([{
								        type: 'link',
								        data: {
								            command: 'cache-management-list',
								        },
								        callback: {
								            complete: function(XHR, textStatus) {
							            		setTimeout(function(){
									            	if( $.magnificPopup ){
									            		$.magnificPopup.close();
									            	}
							            		},500);
								                $('.cache-management #list').html(XHR.responseText);
								            }
								        }
								    }]);
								}else{
									$.notify(response.message,{
										position: 'top right',
										className: response.type,
										showAnimation: 'fadeIn',
										hideAnimation: 'fadeOut',
									});									
								}
							},
							success: function(data, textStatus, XHR) {
								$('#loader-1').fadeOut();
							},
						}
					};
					Add(options);
				} 
			});
		});
	</script>
</div>
<?php
	}
	if( $_REQUEST['command'] == 'edit-api-service-form' ){

		$sql_cache_cat = "SELECT * FROM tbl_cache_categories";
		$res_cache_cat = $db->get_results($sql_cache_cat);

		$sql_services = "SELECT * FROM tbl_cache_services WHERE id='".$_POST['service_id']."'";
		$res_services = $db->get_row($sql_services);
?>
<div id="edit-api-service-form-popup" class="white-popup zoom-anim-dialog">
	<h3>Edit service</h3>
	<hr>
	<form name="edit-api-service-form" id="edit-api-service-form" action="#" method="post">
		<input type="hidden" name="command" value="edit-api-service">
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="cache-category">Cache category</label>
					<select name="cache-category" class="form-control" id="cache-category" required>
						<option value="">Please select</option>
						<?php 
							if($res_cache_cat){
								foreach ($res_cache_cat as $res_cache_cat_single) {?>
									<option value="<?php echo $res_cache_cat_single->id;?>" <?php if($res_services->cat_id==$res_cache_cat_single->id){ echo "selected";}?>><?php echo $res_cache_cat_single->title;?></option>
								<?php
								}
							}
						?>
					</select>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="service">Service</label>
				    	<input type="text" name="service" class="form-control" id="service" required aria-describedby="serviceHelp" value="<?php echo $res_services->title;?>">
				    	<span id="serviceHelp" class="help-block">Hint:<br> *For specific store, please add like <code>/api/home.php?store=en_ae</code><br>*For all stores, please add like<code>/api/home.php</code></span>
				</div>
			</div>
		</div>
		<hr>
		<input type="hidden" name="sid" value="<?php echo $res_services->id;?>">
		<button type="submit" id="submit-1" class="btn btn-primary">Save <span id="loader-1" style="display:none"><i class="fa fa-spinner fa-spin"></i></span></button>
	</form>
	<script>
		jQuery(document).ready(function($) {
			$.validator.addMethod('requiredNotBlank', function(value, element) {
				return $.validator.methods.required.call(this, $.trim(value), element);
			}, $.validator.messages.required);
		    	$("#edit-api-service-form").validate({
				rules:{
					service:{
						required: true,
						minlength: 3,
						requiredNotBlank: true,
					}
				},
				submitHandler: function(form) {
					var options = {
						type: "form",
						formId: "#edit-api-service-form",
						callback: {
							beforeSend: function(xhr) {
								$('#loader-1').fadeIn();
							},
							complete: function(XHR, textStatus) {
								var response = $.parseJSON(XHR.responseText);
								if( response.type != 'warn' ){
								    GET([{
								        type: 'link',
								        data: {
								            command: 'cache-management-list',
								        },
								        callback: {
								            complete: function(XHR, textStatus) {
							            		setTimeout(function(){
									            	if( $.magnificPopup ){
									            		$.magnificPopup.close();
									            	}
							            		},500);
								                $('.cache-management #list').html(XHR.responseText);
								            }
								        }
								    }]);
								}else{
									$.notify(response.message,{
										position: 'top right',
										className: response.type,
										showAnimation: 'fadeIn',
										hideAnimation: 'fadeOut',
									});									
								}
							},
							success: function(data, textStatus, XHR) {
								$('#loader-1').fadeOut();
							},
						}
					};
					Edit(options);
				} 
	    		});
		});
	</script>
</div>
<?php
	}
