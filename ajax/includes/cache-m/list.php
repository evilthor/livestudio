<?php
	if( $_REQUEST['command'] == 'cache-management-list' ){
?>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  	<?php
  		$sql_cache_cats = "SELECT * FROM tbl_cache_categories";
  		$res_cache_cats = $db->get_results($sql_cache_cats);
  		if($res_cache_cats){
      		$i=0;
      		foreach ($res_cache_cats as  $res_cache_cat) {
          		$i++;
                   	$sql_cache_services = "SELECT * FROM tbl_cache_services WHERE cat_id='".$res_cache_cat->id."'";
                   	$res_cache_services = $db->get_results($sql_cache_services);
  	?>
    	<div class="panel panel-default">
        	<div class="panel-heading" role="tab" id="heading-<?php echo $res_cache_cat->id;?>">
            	<h4 class="panel-title">
                	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#drp-<?php echo $res_cache_cat->id;?>" aria-expanded="true" aria-controls="collapseOne">
                    	<?php echo $res_cache_cat->title;?>
                	</a>
                	<?php if(count($res_cache_services)){ ?>
                    <script>
                         var clear_all_cache_<?php echo $res_cache_cat->id;?> = {
                              type: 'link',
                              data :{
                                   "command" : "clear-all-cache", 
                                   "id" : "<?php echo $res_cache_cat->id;?>"
                              },
                              confirm:{
                                   title: 'Confirm!',
                                   content: 'Are you sure to clear all cache in this <strong><?php echo $res_cache_cat->title;?></strong> group.',
                              },
                              callback: {
                                   beforeSend: function(xhr) {
                                        $('#clear-all-cache-<?php echo $res_cache_cat->id;?>').button('loading');
                                   },
                                   complete: function(XHR, textStatus) {
                                        $.alert('Selected group cache has been cleared!');
                                   },
                                   success: function(data, textStatus, XHR) {
                                        $('#clear-all-cache-<?php echo $res_cache_cat->id;?>').button('reset');
                                   },
                              }
                         }
                    </script>
                	<button
                         type="button" 
                         data-loading-text="Loading..."
                         id="clear-all-cache-<?php echo $res_cache_cat->id;?>"
                         class="btn btn-xs btn-success pull-right"
                         onClick="Delete(clear_all_cache_<?php echo $res_cache_cat->id;?>); return false;"
                    >Clear all</button>
                	<?php } ?>
            	</h4>
        	</div>
        	<div id="drp-<?php echo $res_cache_cat->id;?>" class="panel-collapse collapse <?php echo count($res_cache_services) > 0 ? 'in' : ''; ?>" role="tabpanel" aria-labelledby="heading-<?php echo $res_cache_cat->id;?>">
            	<div class="panel-body">
                	<ul class="list-group">
                         <?php
                             	if($res_cache_services){
                                 	foreach ($res_cache_services as $res_cache_service) {
                                     
                         ?>
                         <li class="list-group-item">
                         	<code><?php echo $res_cache_service->title;?></code> 
                         	<script>
                         		var del_option_<?php echo $res_cache_service->id;?> = {
                         			type: 'link',
                         			data :{
                         				"command" : "delete-api-service", 
                         				"id" : "<?php echo $res_cache_service->id;?>"
                         			},
								callback: {
									beforeSend: function(xhr) {
										$('#delete-api-service-<?php echo $res_cache_service->id;?>').fadeIn();
									},
									complete: function(XHR, textStatus) {
									    GET([{
									        type: 'link',
									        data: {
									            command: 'cache-management-list',
									        },
									        callback: {
									            complete: function(XHR, textStatus) {
									                $('.cache-management #list').html(XHR.responseText);
									            }
									        }
									    }]);
									},
									success: function(data, textStatus, XHR) {
										$('#delete-api-service-<?php echo $res_cache_service->id;?>').fadeOut();
									},
								}
                         		}
                         	</script>
                         	<a 
                         		style="margin-left: 6px;"
                         		href="#" 
                         		onClick="Delete(del_option_<?php echo $res_cache_service->id;?>); return false;"
                         		class="btn btn-xs btn-danger pull-right"
                         	>Delete <span style="display:none" id="delete-api-service-<?php echo $res_cache_service->id;?>"><i class="fa fa-spinner fa-spin"></i></span></a>
                         	<a 
                         		style="margin-left: 6px;"
                                  	data-data='{ "command" : "edit-api-service-form", "service_id" : "<?php echo $res_cache_service->id;?>" }' 
                                  	data-loader="#edit-api-service-form-<?php echo $res_cache_service->id;?>"
                                  	href="#" 
                         		class="btn btn-xs btn-primary pull-right __get_popup"
                         	>Edit <span style="display:none" id="edit-api-service-form-<?php echo $res_cache_service->id;?>"><i class="fa fa-spinner fa-spin"></i></span></a>
                              <script>
                                   var clear_service_cache_<?php echo $res_cache_service->id;?> = {
                                        type: 'link',
                                        data :{
                                             "command" : "clear-service-cache", 
                                             "id" : "<?php echo $res_cache_service->id;?>"
                                        },
                                        confirm:{
                                             title: 'Confirm!',
                                             content: 'Are you sure to clear cache from this service.',
                                        },
                                        callback: {
                                             beforeSend: function(xhr) {
                                                  $('#clear-service-cache-<?php echo $res_cache_service->id;?>').button('loading');
                                             },
                                             complete: function(XHR, textStatus) {
                                                 $.alert('Selected cache has been cleared!');
                                             },
                                             success: function(data, textStatus, XHR) {
                                                  $('#clear-service-cache-<?php echo $res_cache_service->id;?>').button('reset');
                                             },
                                        }
                                   }
                              </script>
                              <button
                                   type="button" 
                                   data-loading-text="Loading..."
                                   id="clear-service-cache-<?php echo $res_cache_service->id;?>"
                                   class="btn btn-xs btn-warning pull-right"
                                   onClick="Delete(clear_service_cache_<?php echo $res_cache_service->id;?>); return false;"
                              >Clear cache</button>
                         </li>
                    	<?php
                        			}
                    		}
                    	?>
          	 	</ul>
            	</div>
        	</div>
    	</div>
	<?php
				}
			}
		?>
</div>
<?php } ?>