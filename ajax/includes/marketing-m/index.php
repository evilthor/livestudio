<?php
	if( $_REQUEST['command'] == 'add-app-screen-form' ){
?>
<div id="add-app-screen-form-popup" class="white-popup zoom-anim-dialog">
	<h3>Add app screen name</h3>
	<hr>
	<form name="add-app-screen-form" id="add-app-screen-form" action="#" method="post">
		<input type="hidden" name="command" value="add-app-screen">
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="screen-name">Screen Name</label>
					<input type="text" name="screen-name" class="form-control" id="screen-name" required>
				</div>
			</div>
			<div class="col-lg-6"></div>
		</div>
		<hr>
		<button 
			type="submit" 
			id="submit" 
			class="btn btn-primary"
			data-loading-text="Loading..."
		>Save</button>
		<a 
			href="#" 
			class="btn btn-xs btn-link __get_popup"
			data-data='{ "command" : "app-screen" }' 
			data-loader="#app-screen"
		>Back <span style="display:none" id="app-screen"><i class="fa fa-spinner fa-spin"></i></span></a>

	</form>
	<script>
		jQuery(document).ready(function($) {
			$.validator.addMethod('requiredNotBlank', function(value, element) {
				return $.validator.methods.required.call(this, $.trim(value), element);
			}, $.validator.messages.required);
			$("#add-app-screen-form").validate({
				rules:{
					category:{
						required: true,
						minlength: 3,
						requiredNotBlank: true
					}
				},
				submitHandler: function(form) {
					var options = {
						type: "form",
						formId: "#add-app-screen-form",
						callback: {
							beforeSend: function(xhr) {
								$('#submit').button('loading');
							},
							complete: function(XHR, textStatus) {
								var response = $.parseJSON(XHR.responseText);
								if( response.type != 'warn' ){
									GET([{
										type: 'link',
										data:{
											command: 'app-screen',
										},
										callback: {
											complete: function(XHR, textStatus) {
											    var el = $(XHR.responseText);
											    callPopup(el);
											}
										 }
									},{
									   type: 'link',
									   data:{
										  command: 'marketing-messages-list',
									   },
									   callback: {
											 complete: function(XHR, textStatus) {
												$('.marketing-message-management #list').html(XHR.responseText);
											 }
										  }
								    }]);
								}else{
									$.notify(response.message,{
										position: 'top right',
										className: response.type,
										showAnimation: 'fadeIn',
										hideAnimation: 'fadeOut',
									});									
								}
							},
							success: function(data, textStatus, XHR) {
								$('#submit').button('reset');
							},
						}
					};
					Add(options);
				} 
			});
		});
	</script>
</div>
<?php
	}
	if( $_REQUEST['command'] == 'edit-app-screen-form' ){
		$sql_cache_cat_edit = "SELECT * FROM tbl_cache_categories WHERE id='".$_POST['id']."'";
		$res_cache_cat_edit = $db->get_row($sql_cache_cat_edit);
?>
<div id="edit-app-screen-form-popup" class="white-popup zoom-anim-dialog">
	<h3>Edit app screen name</h3>
	<hr>
	<form name="edit-app-screen-form" id="edit-app-screen-form" action="#" method="post">
		<input type="hidden" name="command" value="edit-app-screen">
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="screen-name">Screen Name</label>
					<input type="text" name="screen-name" class="form-control" id="screen-name" required value="<?php echo $res_cache_cat_edit->title;?>">
				</div>
			</div>
			<div class="col-lg-6"></div>
		</div>
		<hr>
		<input type="hidden" name="id" value="<?php echo $res_cache_cat_edit->id;?>">
		<button
			type="submit" 
			id="submit" 
			class="btn btn-primary"
			data-loading-text="Loading..."
		>Save <span id="loader-1" style="display:none"><i class="fa fa-spinner fa-spin"></i></span></button>
		<a 
			href="#" 
			class="btn btn-xs btn-link __get_popup"
			data-data='{ "command" : "app-screen" }' 
			data-loader="#app-screen"
		>Back <span style="display:none" id="app-screen"><i class="fa fa-spinner fa-spin"></i></span></a>	
	</form>
	<script>
		jQuery(document).ready(function($) {
			$.validator.addMethod('requiredNotBlank', function(value, element) {
				return $.validator.methods.required.call(this, $.trim(value), element);
			}, $.validator.messages.required);
			$("#edit-app-screen-form").validate({
				rules:{
					category:{
						required: true,
						minlength: 3,
						requiredNotBlank: true,
					}
				},
				submitHandler: function(form) {
					var options = {
						type: "form",
						formId: "#edit-app-screen-form",
						callback: {
							beforeSend: function(xhr) {
								$('#submit').button('loading');
							},
							complete: function(XHR, textStatus) {
								var response = $.parseJSON(XHR.responseText);
								if( response.type != 'warn' ){
									GET([{
										type: 'link',
										data:{
											command: 'app-screen',
										},
										callback: {
											complete: function(XHR, textStatus) {
											    var el = $(XHR.responseText);
											    callPopup(el);
											}
										 }
									},{
									   type: 'link',
									   data:{
										  command: 'marketing-messages-list',
									   },
									   callback: {
											 complete: function(XHR, textStatus) {
												$('.marketing-message-management #list').html(XHR.responseText);
											 }
										  }
								    }]);
								}else{
									$.notify(response.message,{
										position: 'top right',
										className: response.type,
										showAnimation: 'fadeIn',
										hideAnimation: 'fadeOut',
									});									
								}
							},
							success: function(data, textStatus, XHR) {
								$('#submit').button('reset');
							},
						}
					};
					Edit(options);
				} 
			});
		});
	</script>
</div>
<?php
	}
	if( $_REQUEST['command'] == 'app-screen' ){
		$sql_cache_cat = "SELECT * FROM tbl_cache_categories";
		$res_cache_cat = $db->get_results($sql_cache_cat);
?>
<div id="app-screen" class="white-popup zoom-anim-dialog">
	<div class="row">
		<div class="col-lg-8">
			<h3 class="pull-left">App screens</h3> 
			<a 
				style="margin-left: 12px; margin-top: 5px;"				
				href="#" 
				class="btn btn-xs btn-primary pull-left __get_popup"
				data-data='{ "command" : "add-app-screen-form" }' 
				data-loader="#add-app-screen-form"
			>Add app screen <span style="display:none" id="add-app-screen-form"><i class="fa fa-spinner fa-spin"></i></span></a>
			<?php if( count($res_cache_cat) > 0 ){ ?>
			<a 
				style="margin-left: 12px; margin-top: 5px;"				
				data-data='{ "command" : "add-marketing-message-form" }' 
				data-loader="#add-marketing-message-form"
				href="#" 
				class="btn btn-primary btn-xs pull-left __get_popup"
			>Add marketing message <span style="display:none" id="add-marketing-message-form"><i class="fa fa-spinner fa-spin"></i></span></a>
			<?php } ?>
		</div>
	</div>
	<hr>
	<div style="max-height: 300px; min-height: 300px; overflow: hidden; overflow-y: auto;">
		<?php 
			if($res_cache_cat){
				$res_cache_cat = array_chunk($res_cache_cat, 2);
				foreach($res_cache_cat as $res_cache_cat_single){
		?>
		<div class="row">
			<?php
					foreach ($res_cache_cat_single as $key => $value) {
			?>
			<div class="col-lg-6">
				<div>
					<h4><?php echo $value->title;?></h4>
				</div>
				<div>
					<a 
						href="#" 
						class="btn btn-xs btn-primary __get_popup"
						data-data='{ "command" : "edit-app-screen-form", "id" : "<?php echo $value->id;?>"}'
						data-loader="#edit-app-screen-loader-<?php echo $value->id;?>"
					>Edit <span style="display:none" id="edit-app-screen-loader-<?php echo $value->id;?>"><i class="fa fa-spinner fa-spin"></i></span></a>
					<script>
						var del_option_<?php echo $value->id;?> = {
							type: 'link',
							data :{
								"command" : "delete-app-screen", 
								"id" : "<?php echo $value->id;?>"
							},
							callback : {
								beforeSend: function(xhr) {
									$('#delete-app-screen-<?php echo $value->id;?>').fadeIn();
								},
								complete: function(XHR, textStatus) {
									GET([{
										type: 'link',
										data:{
											command: 'app-screen',
										},
										callback: {
											complete: function(XHR, textStatus) {
											    var el = $(XHR.responseText);
											    callPopup(el);
											}
										 }
									},{
									   type: 'link',
									   data:{
										  command: 'marketing-messages-list',
									   },
									   callback: {
											 complete: function(XHR, textStatus) {
												$('.marketing-message-management #list').html(XHR.responseText);
											 }
										  }
								    }]);
								},
								success: function(data, textStatus, XHR) {
									$('#delete-app-screen-<?php echo $value->id;?>').fadeOut();
								}
							}
						}
					</script>					
					<a 
						href="#" 
						class="btn btn-xs btn-danger"
						onClick="Delete(del_option_<?php echo $value->id;?>)"
					>Delete <span style="display:none" id="delete-app-screen-<?php echo $value->id;?>"><i class="fa fa-spinner fa-spin"></i></span></a>
				</div>
				<hr>
			</div>
			<?php
					}
			?>
		</div>
		<?php 
		}	
	}
		?>
	</div>
</div>
<?php
	}
	if( $_REQUEST['command'] == 'add-marketing-message-form' ){

		$sql_cache_cat = "SELECT * FROM tbl_cache_categories";
		$res_cache_cat = $db->get_results($sql_cache_cat);
?>
<div id="add-marketing-message-form-popup" class="white-popup zoom-anim-dialog">
	<h3>Add Marketing Message</h3>
	<hr>
	<form name="add-marketing-message-form" id="add-marketing-message-form" action="#" method="post">
		<input type="hidden" name="command" value="add-marketing-message">
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="app-screen">App Screen</label>
					<select name="app-screen" class="form-control" id="app-screen" required>
						<option value="">Please select</option>
						<?php 
							if($res_cache_cat){
								foreach ($res_cache_cat as $res_cache_cat_single) {?>
									<option value="<?php echo $res_cache_cat_single->id;?>"><?php echo $res_cache_cat_single->title;?></option>
								<?php
								}
							}
						?>
					</select>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="message">Message</label>
				    	<textarea 
				    		name="message" 
				    		class="form-control" 
				    		id="message" 
				    		required
				    	></textarea>
				</div>
			</div>
		</div>
		<hr>
		<button type="submit" id="submit-1" class="btn btn-primary">Save <span id="loader-1" style="display:none"><i class="fa fa-spinner fa-spin"></i></span></button>
	</form>
	<script>
		jQuery(document).ready(function($) {
			$.validator.addMethod('requiredNotBlank', function(value, element) {
				return $.validator.methods.required.call(this, $.trim(value), element);
			}, $.validator.messages.required);
		    	$("#add-marketing-message-form").validate({
				rules:{
					service:{
						required: true,
						minlength: 3,
						requiredNotBlank: true,
					}
				},
				submitHandler: function(form) {
					var options = {
						type: "form",
						formId: "#add-marketing-message-form",
						callback: {
							beforeSend: function(xhr) {
								$('#loader-1').fadeIn();
							},
							complete: function(XHR, textStatus) {
								var response = $.parseJSON(XHR.responseText);
								if( response.type != 'warn' ){
								    GET([{
								        type: 'link',
								        data: {
								            command: 'marketing-messages-list',
								        },
								        callback: {
								            complete: function(XHR, textStatus) {
							            		setTimeout(function(){
									            	if( $.magnificPopup ){
									            		$.magnificPopup.close();
									            	}
							            		},500);
								                $('.marketing-message-management #list').html(XHR.responseText);
								            }
								        }
								    }]);
								}else{
									$.notify(response.message,{
										position: 'top right',
										className: response.type,
										showAnimation: 'fadeIn',
										hideAnimation: 'fadeOut',
									});									
								}
							},
							success: function(data, textStatus, XHR) {
								$('#loader-1').fadeOut();
							},
						}
					};
					Add(options);
				} 
			});
		});
	</script>
</div>
<?php
	}
	if( $_REQUEST['command'] == 'edit-marketing-message-form' ){

		$sql_cache_cat = "SELECT * FROM tbl_cache_categories";
		$res_cache_cat = $db->get_results($sql_cache_cat);

		$sql_services = "SELECT * FROM tbl_cache_services WHERE id='".$_POST['service_id']."'";
		$res_services = $db->get_row($sql_services);
?>
<div id="edit-marketing-message-form-popup" class="white-popup zoom-anim-dialog">
	<h3>Edit Marketing Message</h3>
	<hr>
	<form name="edit-marketing-message-form" id="edit-marketing-message-form" action="#" method="post">
		<input type="hidden" name="command" value="edit-marketing-message">
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="app-screen">App Screen</label>
					<select name="app-screen" class="form-control" id="app-screen" required>
						<option value="">Please select</option>
						<?php 
							if($res_cache_cat){
								foreach ($res_cache_cat as $res_cache_cat_single) {?>
									<option value="<?php echo $res_cache_cat_single->id;?>" <?php if($res_services->cat_id==$res_cache_cat_single->id){ echo "selected";}?>><?php echo $res_cache_cat_single->title;?></option>
								<?php
								}
							}
						?>
					</select>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<label for="message">Message</label>
				    	<textarea 
				    		name="message" 
				    		class="form-control" 
				    		id="message" 
				    		required
				    	><?php echo $res_services->title;?></textarea>
				</div>
			</div>
		</div>
		<hr>
		<input type="hidden" name="sid" value="<?php echo $res_services->id;?>">
		<button type="submit" id="submit-1" class="btn btn-primary">Save <span id="loader-1" style="display:none"><i class="fa fa-spinner fa-spin"></i></span></button>
	</form>
	<script>
		jQuery(document).ready(function($) {
			$.validator.addMethod('requiredNotBlank', function(value, element) {
				return $.validator.methods.required.call(this, $.trim(value), element);
			}, $.validator.messages.required);
		    	$("#edit-marketing-message-form").validate({
				rules:{
					service:{
						required: true,
						minlength: 3,
						requiredNotBlank: true,
					}
				},
				submitHandler: function(form) {
					var options = {
						type: "form",
						formId: "#edit-marketing-message-form",
						callback: {
							beforeSend: function(xhr) {
								$('#loader-1').fadeIn();
							},
							complete: function(XHR, textStatus) {
								var response = $.parseJSON(XHR.responseText);
								if( response.type != 'warn' ){
								    GET([{
								        type: 'link',
								        data: {
								            command: 'marketing-messages-list',
								        },
								        callback: {
								            complete: function(XHR, textStatus) {
							            		setTimeout(function(){
									            	if( $.magnificPopup ){
									            		$.magnificPopup.close();
									            	}
							            		},500);
								                $('.marketing-message-management #list').html(XHR.responseText);
								            }
								        }
								    }]);
								}else{
									$.notify(response.message,{
										position: 'top right',
										className: response.type,
										showAnimation: 'fadeIn',
										hideAnimation: 'fadeOut',
									});									
								}
							},
							success: function(data, textStatus, XHR) {
								$('#loader-1').fadeOut();
							},
						}
					};
					Edit(options);
				} 
	    		});
		});
	</script>
</div>
<?php
	}
