<?php
	if( $_REQUEST['command'] == 'marketing-messages-list' ){
?>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  	<?php
  		$sql_cache_cats = "SELECT * FROM tbl_cache_categories";
  		$res_cache_cats = $db->get_results($sql_cache_cats);
  		if($res_cache_cats){
      		$i=0;
      		foreach ($res_cache_cats as  $res_cache_cat) {
          		$i++;
                   	$sql_cache_services = "SELECT * FROM tbl_cache_services WHERE cat_id='".$res_cache_cat->id."'";
                   	$res_cache_services = $db->get_results($sql_cache_services);
  	?>
    	<div class="panel panel-default">
        	<div class="panel-heading" role="tab" id="heading-<?php echo $res_cache_cat->id;?>">
            	<h4 class="panel-title">
                	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#drp-<?php echo $res_cache_cat->id;?>" aria-expanded="true" aria-controls="collapseOne">
                    	<?php echo $res_cache_cat->title;?>
                	</a>
            	</h4>
        	</div>
        	<div id="drp-<?php echo $res_cache_cat->id;?>" class="panel-collapse collapse <?php echo count($res_cache_services) > 0 ? 'in' : ''; ?>" role="tabpanel" aria-labelledby="heading-<?php echo $res_cache_cat->id;?>">
            	<div class="panel-body">
                	<ul class="list-group">
                         <?php
                             	if($res_cache_services){
                                 	foreach ($res_cache_services as $res_cache_service) {
                                     
                         ?>
                         <li class="list-group-item">
                         	<code><?php echo $res_cache_service->title;?></code> 
                         	<script>
                         		var del_option_<?php echo $res_cache_service->id;?> = {
                         			type: 'link',
                         			data :{
                         				"command" : "delete-marketing-message", 
                         				"id" : "<?php echo $res_cache_service->id;?>"
                         			},
								callback: {
									beforeSend: function(xhr) {
										$('#delete-marketing-message-<?php echo $res_cache_service->id;?>').fadeIn();
									},
									complete: function(XHR, textStatus) {
									    GET([{
									        type: 'link',
									        data: {
									            command: 'marketing-messages-list',
									        },
									        callback: {
									            complete: function(XHR, textStatus) {
									                $('.marketing-message-management #list').html(XHR.responseText);
									            }
									        }
									    }]);
									},
									success: function(data, textStatus, XHR) {
										$('#delete-marketing-message-<?php echo $res_cache_service->id;?>').fadeOut();
									},
								}
                         		}
                         	</script>
                         	<a 
                         		style="margin-left: 6px;"
                         		href="#" 
                         		onClick="Delete(del_option_<?php echo $res_cache_service->id;?>); return false;"
                         		class="btn btn-xs btn-danger pull-right"
                         	>Delete <span style="display:none" id="delete-marketing-message-<?php echo $res_cache_service->id;?>"><i class="fa fa-spinner fa-spin"></i></span></a>
                         	<a 
                         		style="margin-left: 6px;"
                                  	data-data='{ "command" : "edit-marketing-message-form", "service_id" : "<?php echo $res_cache_service->id;?>" }' 
                                  	data-loader="#edit-marketing-message-form-<?php echo $res_cache_service->id;?>"
                                  	href="#" 
                         		class="btn btn-xs btn-primary pull-right __get_popup"
                         	>Edit <span style="display:none" id="edit-app-screen-form-<?php echo $res_cache_service->id;?>"><i class="fa fa-spinner fa-spin"></i></span></a>
                         </li>
                    	<?php
                        			}
                    		}
                    	?>
          	 	</ul>
            	</div>
        	</div>
    	</div>
	<?php
				}
			}
		?>
</div>
<?php } ?>