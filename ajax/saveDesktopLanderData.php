<?php
include "../config.php";

if (isset($_POST['command']) && $_POST['command'] == 'desktopLanderLayoutSort') {
    $sort = null;
    if (isset($_POST['row_order']) && !empty($_POST['row_order'])) {
        $sort = explode(',', $_POST['row_order']);
        $sort = array_flip($sort);
        $lander_cat_id = intval($_POST['lander_cat_id']);
        $app_id = intval($_POST['app_id']);
        $lander_type = trim($_POST['lander_type']);

        if($lander_type=='slider'){
            $tbl = 'tbl_desktop_lander_slider';
        }else{
            $tbl = 'tbl_desktop_lander_rows';
        }

        $sql = "";
        $message = null;
        $layout = '';
        foreach ($sort as $id => $order) {
            $sql .= "UPDATE $tbl SET row_order = '" . $order . "' WHERE app_id = '".$app_id."' AND id = '" . $id . "'; ";
        }
        if ($db->query($sql) !== false) {
            $message = array(
                'type' => 'success',
                'message' => 'Layouts successfully sorted.',
            );
            $sql = "SELECT * FROM $tbl WHERE app_id = '".$app_id."' AND cat_id = '" . $lander_cat_id . "' ORDER BY row_order ASC";
            $results = $db->get_results($sql);
            $layout = perviewLayouts($results); // this function in common_php.php
        } else {
            $message = array(
                'type' => 'error',
                'message' => 'Layouts not sort, try again.',
            );
        }
    } else {
        $message = array(
            'type' => 'warning',
            'message' => 'Order isn\'t set properly.',
        );
    }
    echo json_encode(array(
        'html' => $layout,
        'message' => $message,
    ));
}
if (isset($_POST['command']) && $_POST['command'] == 'saveRow') {

     if($_POST['app_id'] == GLOBAL_EN || $_POST['app_id'] == GLOBAL_AR){
     $is_global = '1';
     }else{
          $is_global = '0';
     }

    $images = $_POST['images'];
    $attachments = $_POST['attachments'];
    $cat_id = $_POST['cat_id'];
    $layout = $_POST['layout'];
    $app_id = $_POST['app_id'];
    $row_id = $_POST['row_id'];
    $lander_type = $_POST['lander_type'];

    if($lander_type=='slider'){
        $tbl = 'desktop_lander_slider';
    }else{
        $tbl = 'desktop_lander_rows';
    }

    $error = false;
     if ($row_id == '') {
          $sql = "INSERT INTO tbl_".$tbl." SET
               cat_id = '" . $cat_id . "',
               app_id = '".$app_id."',
               layout = '" . $layout . "',
               images = '" . $images . "',
               attachment = '" . $attachments . "',
               is_global = '".$is_global."',
               show_in_app = '1'
               ";
          if ($db->query($sql) !== false) {
               $did = $db->insert_id;
               $sql = "INSERT INTO tbl_sort_rows SET
                    app_id = '$app_id',
                    row_id = '$db->insert_id',
                    row_type = '$tbl',
                    row_type_id = '$cat_id'
                    ";
               $db->query($sql);

               $check_app = "SELECT id FROM tbl_apps WHERE parent_app = '".$app_id."'";
               $res_app = $db->get_results($check_app);
               if($res_app){
                    foreach ($res_app  as $res_app_single) {
                         $sql = "INSERT INTO tbl_sort_rows SET
                         app_id = '$res_app_single->id',
                         row_id = '$did',
                         row_type = '$tbl',
                         row_type_id = '$cat_id'
                         ";
                         $db->query($sql);
                    }
               }

          }else{
               $error = true;
          }
     } else {
          $sql_images = "SELECT images FROM tbl_".$tbl." WHERE id = '" . $row_id . "'";
          $result_images = $db->get_row($sql_images);
          $old_images = json_decode($result_images->images);
          $new_images = json_decode($images);
          for ($i = 0; $i < sizeof($new_images); $i++) {
               if ($new_images[$i] != $old_images[$i] && file_exists($old_images[$i])) {
                    unlink($old_images[$i]);
               }
          }
          $sql = "UPDATE tbl_".$tbl." SET
               cat_id = '" . $cat_id . "',
               layout = '" . $layout . "',
               images = '" . $images . "',
               attachment = '" . $attachments . "'
               WHERE id = '" . $row_id . "'
               ";
          if ($db->query($sql) === false) {
               $error = true;
          }
     }
     if ($error) {
          echo "error";
     } else {
          echo $cat_id;
     }
}