<?php
include "../config.php";
if (isset($_POST['command']) && $_POST['command'] == 'saveRow') {
     $images = $_POST['images'];
     $attachments = $_POST['attachments'];
     $cat_id = $_POST['id'];
     $layout = $_POST['layout'];
     $app_id = $_POST['app_id'];
     $row_id = $_POST['row_id'];
     $error = false;

     if($_POST['app_id'] == GLOBAL_EN || $_POST['app_id'] == GLOBAL_AR){
     $is_global = '1';
     }else{
          $is_global = '0';
     }

     if ($row_id == '') {
          $sql = "INSERT INTO tbl_cat_rows_new SET
			        cat_id = '" . $cat_id . "',
                    app_id = '".$app_id."',
			        layout = '" . $layout . "',
			        images = '" . $images . "',
			        attachment = '" . $attachments . "',
                    is_global = '".$is_global."',
                    show_in_app = '1'";
          if ($db->query($sql) !== false) {
               $mid = $db->insert_id;
               $sql = "INSERT INTO tbl_sort_rows SET
                    app_id = '$app_id',
                    row_id = '$db->insert_id',
                    row_type = 'search_category_new',
                    row_type_id = '$cat_id'";
               $db->query($sql);

               $check_app = "SELECT id FROM tbl_apps WHERE parent_app = '".$app_id."'";
               $res_app = $db->get_results($check_app);
               if($res_app){
                    foreach ($res_app  as $res_app_single) {
                         $sql = "INSERT INTO tbl_sort_rows SET
                         app_id = '$res_app_single->id',
                         row_id = '$mid',
                         row_type = 'search_category_new',
                         row_type_id = '$cat_id'
                         ";
                         $db->query($sql);
                    }
               }
          }else{
               $error = true;
          }
     } else {
          $sql_images = "SELECT images FROM tbl_cat_rows_new WHERE id = '" . $row_id . "'";
          $result_images = $db->get_row($sql_images);
          $old_images = json_decode($result_images->images);
          $new_images = json_decode($images);
          for ($i = 0; $i < sizeof($new_images); $i++) {
               if ($new_images[$i] != $old_images[$i] && file_exists($old_images[$i])) {
                    unlink($old_images[$i]);
               }
          }
          $sql = "UPDATE tbl_cat_rows_new SET
			                cat_id = '" . $cat_id . "',
			                layout = '" . $layout . "',
			                images = '" . $images . "',
			                attachment = '" . $attachments . "'
			                WHERE id = '" . $row_id . "'";
          if ($db->query($sql) === false) {
               $error = true;
          }
     }

     if ($error) {
          echo "error";
     } else {
          echo $cat_id;
     }
}
