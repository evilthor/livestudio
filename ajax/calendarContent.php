<a href="#" data-hide=".contentDdPopup" class="close-popup"><i class="fa fa-times"></i></a>
<div id="datepicker"></div>
<button type="button" style="border-top-right-radius: 0;border-top-left-radius: 0;position: absolute;bottom: 0;" data-hide=".contentDdPopup" class="close-popup btn btn-primary btn-block btn-sm">DONE</button>
<script type="text/javascript">
     $(function () {
        $('#datepicker').datetimepicker({
			inline: true,
			minDate: moment(),
               showClear: true,
		  }).on('dp.change', function(e) {
				var dateTime =  moment(e.date).tz('Asia/Dubai').unix();
                    var timeDisply = moment(e.date).tz('Asia/Dubai').format('DD MMMM-hh:mm a');

                    timeDisply = timeDisply.split("-");
                    $('#selected-date').text(timeDisply[0]).animate({'color':'#f00'}, 1);
                    $('#selected-time').text(timeDisply[1]).animate({'color':'#f00'}, 1);
				notification.require('dateTime', dateTime);

                    var timeDisply = moment(e.date).tz('Asia/Dubai').format('YYYY-MM-DD|HH:mm');
                    timeDisply = timeDisply.split("|");
                    notification.require('date', timeDisply[0]);
                    notification.require('time', timeDisply[1]);
		 });
     });
</script>