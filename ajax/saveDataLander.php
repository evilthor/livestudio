<?php
include "../config.php";
ini_set('display_errors',0);
if (isset($_POST['command']) && $_POST['command'] == 'layoutSort') {
     $sort = null;
     if (isset($_POST['row_order']) && !empty($_POST['row_order'])) {
          $sort = explode(',', $_POST['row_order']);
          $lander_id = intval($_POST['lander_id']);
          $sort = array_flip($sort);
          $sql = "";
          $message = null;
          $layout = '';
          foreach ($sort as $id => $order) {
               $sql .= "UPDATE tbl_lander_rows SET row_order = '" . $order . "' WHERE id = '" . $id . "'; ";
          }
          if ($db->query($sql) !== false) {
               $message = array(
                    'type' => 'Success',
                    'message' => 'Layouts successfully sorted.',
               );
               $sql = "SELECT * FROM tbl_lander_rows WHERE lander_id = '" . $lander_id . "' ORDER BY row_order ASC";
               $results = $db->get_results($sql);
               if (@$results) {
                    foreach ($results as $result) {
                         $iamges = json_decode($result->images);
                         if ($result->layout == "single" || $result->layout == "singleBar") {
                              $layout .= '<div class="row single-template-layout">
									<div class="col-md-12">
										<div class="preview"><img src="' . $iamges[0] . '"></div>
									</div>
								</div>';
                         } elseif ($result->layout == "double" || $result->layout == "doubleShort") {
                              $doubleShort = ($result->layout == 'doubleShort') ? 'short' : '';
                              $layout .= '<div class="row double-template-layout ' . $doubleShort . '">
										<div class="col-md-6">
											<div class="preview"><img src="' . $iamges[0] . '"></div>
										</div>
										<div class="col-md-6">
											<div class="preview"><img src="' . $iamges[1] . '"></div>
										</div>
									</div>';
                         } elseif ($result->layout == "tripleLeft") {
                              $layout .= '<div class="row preview-triple-left">
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-12">
												<div class="preview _1024X704"><img src="' . $iamges[0] . '"></div>
											</div>

											<div class="col-md-12">
												<div class="preview _1024X704"><img src="' . $iamges[1] . '"></div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="preview _1024X1310"><img src="' . $iamges[2] . '"></div>
									</div>
								</div>';
                         } elseif ($result->layout == "tripleRight") {
                              $layout .= '<div class="row preview-triple-right">
									<div class="col-md-6">
										<div class="preview _1024X1310"><img src="' . $iamges[0] . '"></div>
									</div>
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-12">
												<div class="preview _1024X704"><img src="' . $iamges[1] . '"></div>
											</div>

											<div class="col-md-12">
												<div class="preview _1024X704"><img src="' . $iamges[2] . '"></div>
											</div>
										</div>
									</div>
								</div>';
                         }
                    }
               }
          } else {
               $message = array(
                    'type' => 'Error',
                    'message' => 'Layouts not sort, try again.',
               );
          }
     } else {
          $message = array(
               'type' => 'Warning',
               'message' => 'Order isn\'t set properly.',
          );
     }
     echo json_encode(array(
     	'html'=> $layout,
     	'message' => $message,
     ));
}
if (isset($_POST['command']) && $_POST['command'] == 'layoutSortCat') {
     $sort = null;
     if (isset($_POST['row_order']) && !empty($_POST['row_order'])) {
          $sort = explode(',', $_POST['row_order']);
          $lander_id = intval($_POST['lander_id']);
          $sort = array_flip($sort);
          $sql = "";
          $message = null;
          $layout = '';
          foreach ($sort as $id => $order) {
               $sql .= "UPDATE tbl_cat_rows SET row_order = '" . $order . "' WHERE id = '" . $id . "'; ";
          }
          if ($db->query($sql) !== false) {
               $message = array(
                    'type' => 'Success',
                    'message' => 'Layouts successfully sorted.',
               );
               $sql = "SELECT * FROM tbl_cat_rows WHERE merchandise_id = '" . $merchandise_id . "' ORDER BY row_order ASC";
               $results = $db->get_results($sql);
               if (@$results) {
                    foreach ($results as $result) {
                         $iamges = json_decode($result->images);
                         if ($result->layout == "double") {
                              $doubleShort = ($result->layout == 'doubleShort') ? 'short' : '';
                              $layout .= '<div class="row double-template-layout ' . $doubleShort . '">
										<div class="col-md-6">
											<div class="preview"><img src="' . $iamges[0] . '"></div>
										</div>
										<div class="col-md-6">
											<div class="preview"><img src="' . $iamges[1] . '"></div>
										</div>
									</div>';
                         }
                    }
               }
          } else {
               $message = array(
                    'type' => 'Error',
                    'message' => 'Layouts not sort, try again.',
               );
          }
     } else {
          $message = array(
               'type' => 'Warning',
               'message' => 'Order isn\'t set properly.',
          );
     }
     echo json_encode(array(
     	'html'=> $layout,
     	'message' => $message,
     ));
}
if (isset($_POST['command']) && $_POST['command'] == 'saveRow') {
     $images = $_POST['images'];
     $attachments = $_POST['attachments'];
     $lander_id = $_POST['id'];
     $layout = $_POST['layout'];
     $app_id = $_POST['app_id'];
     $row_id = $_POST['row_id'];
     $error = false;
     if ($row_id == '') {
          $sql = "INSERT INTO tbl_lander_rows SET
			lander_id = '" . $lander_id . "',
			layout = '" . $layout . "',
			images = '" . $images . "',
			attachment = '" . $attachments . "',
               is_global = '0',
               show_in_app = '1'
			";
     } else {
          $sql_images = "SELECT images FROM tbl_lander_rows WHERE id = '" . $row_id . "'";
          $result_images = $db->get_row($sql_images);
          $old_images = json_decode($result_images->images);
          $new_images = json_decode($images);
          for ($i = 0; $i < sizeof($new_images); $i++) {
               if ($new_images[$i] != $old_images[$i] && file_exists($old_images[$i])) {
                    unlink($old_images[$i]);
               }
          }
          $sql = "UPDATE tbl_lander_rows SET
			lander_id = '" . $lander_id . "',
			layout = '" . $layout . "',
			images = '" . $images . "',
			attachment = '" . $attachments . "'
			WHERE id = '" . $row_id . "'
			";
     }
     if ($db->query($sql) !== false) {
          //$merchandise_id = $db->insert_id;
     } else {
          $error = true;
     }

     if ($error) {
          echo "error";
     } else {
          echo $lander_id;
     }
}
