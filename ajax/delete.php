<?php
include "../config.php";
include('../cache/memcache.php');

if(isset($_POST['command']) && $_POST['command'] == 'delete-scheme'){
   $res = false; 
   $checkDB = "SELECT * FROM tbl_color_schemes WHERE id = '".$_POST['id']."' and active != '1'";
   $checkDB = $db->get_row($checkDB);
   if($checkDB){
       $file = dirname(getcwd())."/color-schemes/".$checkDB->slug;
       if( file_exists($file) ){
           unlink($file);
       }
       $sql_del = "DELETE FROM tbl_color_schemes WHERE id = '".$_POST['id']."'";
       $db->query($sql_del);
       $res = true;
   }
} 

if (isset($_POST['command']) && $_POST['command'] == 'delete-api-service') {

	$sql = "DELETE FROM tbl_cache_services WHERE id='".$_POST['id']."'";
	$db->query($sql);

}

if (isset($_POST['command']) && $_POST['command'] == 'delete-cache-category') {

	$sql = "DELETE FROM tbl_cache_categories WHERE id='".$_POST['id']."'";
	$db->query($sql);
	$sql = "DELETE FROM tbl_cache_services WHERE cat_id='".$_POST['id']."'";
	$db->query($sql);

}

// clear services cache
if (isset($_POST['command']) && $_POST['command'] == 'clear-service-cache'){

	$id = trim($_POST['id']);
    $domain_name = $_SERVER['SERVER_NAME'];
	$sql_services = "SELECT * FROM tbl_cache_services WHERE id='".$id."'";
	$res_services = $db->get_row($sql_services);
	$service_title = $res_services->title;
	if($service_title){

		$service_title_arr = explode("?",$service_title);
		//if query string is added from admin
		if(!empty($service_title_arr['1'])){
			// generate key
 			$del_key = sha1($domain_name.$service_title_arr['0']."?".$service_title_arr['1']);
 			//delete cache based on key
 			$cacheObject->delData($del_key);
		}else{
			$title_temp_arr = explode("/",$service_title_arr['0']);
			// check if there is lander api then attach id in query string otherwise attach store in query string
			if(in_array('lander.php', $title_temp_arr) || in_array('landerv2.php', $title_temp_arr)){
				$sql_lander_pages = "SELECT * FROM tbl_lander_pages";
				$res_lander_pages = $db->get_results($sql_lander_pages);
				foreach ($res_lander_pages as $res_lander_page) {
					// generate key
 					$del_key = sha1($domain_name.$service_title_arr['0']."?id=".$res_lander_page->id);
 					//delete cache based on key
 					$cacheObject->delData($del_key);
				}
			}else{
			
				//if query string is not added from admin
				$sql_stores = "SELECT * FROM tbl_stores";
				$res_stores = $db->get_results($sql_stores);
				if($res_stores){
					foreach ($res_stores as  $store) {
						if($store->store_lang=='both'){

                            $title_temp_arr_sl = explode("/",$service_title_arr['0']);
                            if(in_array('siteLanders.php', $title_temp_arr_sl)){
                                $del_key_en = sha1($domain_name.$service_title_arr['0']."?store=en_".$store->store_code.'&lander=kids');
                                $del_key_ar = sha1($domain_name.$service_title_arr['0']."?store=ar_".$store->store_code.'&lander=kids');
                                $cacheObject->delData($del_key_en);
                                $cacheObject->delData($del_key_ar);

                                $del_key_en = sha1($domain_name.$service_title_arr['0']."?store=en_".$store->store_code.'&lander=women');
                                $del_key_ar = sha1($domain_name.$service_title_arr['0']."?store=ar_".$store->store_code.'&lander=women');
                                $cacheObject->delData($del_key_en);
                                $cacheObject->delData($del_key_ar);

                                $del_key_en = sha1($domain_name.$service_title_arr['0']."?store=en_".$store->store_code.'&lander=men');
                                $del_key_ar = sha1($domain_name.$service_title_arr['0']."?store=ar_".$store->store_code.'&lander=men');
                                $cacheObject->delData($del_key_en);
                                $cacheObject->delData($del_key_ar);
                            }else{
                                // generate keys for both en and ar
                                $del_key_en = sha1($domain_name.$service_title_arr['0']."?store=en_".$store->store_code);
                                $del_key_ar = sha1($domain_name.$service_title_arr['0']."?store=ar_".$store->store_code);
                                //delete cache based on keys
                                $cacheObject->delData($del_key_en);
                                $cacheObject->delData($del_key_ar);
                            }
						}else{
                            $title_temp_arr_sl = explode("/",$service_title_arr['0']);
                            if(in_array('siteLanders.php', $title_temp_arr_sl)){
                                $del_key = sha1($domain_name.$service_title_arr['0']."?store=".$store->store_lang."_".$store->store_code.'&lander=kids');
                                //delete cache based on key
                                $cacheObject->delData($del_key);

                                $del_key = sha1($domain_name.$service_title_arr['0']."?store=".$store->store_lang."_".$store->store_code.'&lander=women');
                                //delete cache based on key
                                $cacheObject->delData($del_key);

                                $del_key = sha1($domain_name.$service_title_arr['0']."?store=".$store->store_lang."_".$store->store_code.'&lander=men');
                                //delete cache based on key
                                $cacheObject->delData($del_key);

                            }else{
                                // generate key
                                $del_key = sha1($domain_name.$service_title_arr['0']."?store=".$store->store_lang."_".$store->store_code);
                                //delete cache based on key
                                $cacheObject->delData($del_key);
                            }
						}
					}//foreach ($res_stores as  $store)
				}//if($res_stores)
			}//if(in_array('lander.php', $title_temp_arr))esle
		}//if(!empty($service_title_arr['1']))else
	}//if($service_title)
}//if (isset($_POST['command']) && $_POST['command'] == 'clear-service-cache')

// clear all category services cache
if (isset($_POST['command']) && $_POST['command'] == 'clear-all-cache'){

	$id = trim($_POST['id']);
    $domain_name = $_SERVER['SERVER_NAME'];
	$sql_services = "SELECT * FROM tbl_cache_services WHERE cat_id='".$id."'";
	$res_services = $db->get_results($sql_services);
	if($res_services){
		foreach ($res_services as $service) {
			$service_title_arr = explode("?",$service->title);
			//if query string is added from admin
			if(!empty($service_title_arr['1'])){
				// generate key
 				$del_key = sha1($domain_name.$service_title_arr['0']."?".$service_title_arr['1']);
 				//delete cache based on key
 				$cacheObject->delData($del_key);
			}else{
                $title_temp_arr = explode("/",$service_title_arr['0']);
				// check if there is lander api then attach id in query string otherwise attach store in query string
				if(in_array('lander.php', $title_temp_arr) || in_array('landerv2.php', $title_temp_arr)){
					$sql_lander_pages = "SELECT * FROM tbl_lander_pages";
					$res_lander_pages = $db->get_results($sql_lander_pages);
					foreach ($res_lander_pages as $res_lander_page) {
						
						// generate key
	 					$del_key = sha1($domain_name.$service_title_arr['0']."?id=".$res_lander_page->id);
	 					//delete cache based on key
	 					$cacheObject->delData($del_key);
					}
				}else{

					//if query string is not added from admin
					$sql_stores = "SELECT * FROM tbl_stores";
					$res_stores = $db->get_results($sql_stores);
					if($res_stores){
						foreach ($res_stores as  $store) {
							if($store->store_lang=='both'){
								// generate keys for both en and ar
                                $title_temp_arr_sl = explode("/",$service_title_arr['0']);
                                if(in_array('siteLanders.php', $title_temp_arr_sl)){

                                    $del_key_en = sha1($domain_name.$service_title_arr['0']."?store=en_".$store->store_code.'&lander=kids');
                                    $del_key_ar = sha1($domain_name.$service_title_arr['0']."?store=ar_".$store->store_code.'&lander=kids');
                                    $cacheObject->delData($del_key_en);
                                    $cacheObject->delData($del_key_ar);

                                    $del_key_en = sha1($domain_name.$service_title_arr['0']."?store=en_".$store->store_code.'&lander=women');
                                    $del_key_ar = sha1($domain_name.$service_title_arr['0']."?store=ar_".$store->store_code.'&lander=women');
                                    $cacheObject->delData($del_key_en);
                                    $cacheObject->delData($del_key_ar);

                                    $del_key_en = sha1($domain_name.$service_title_arr['0']."?store=en_".$store->store_code.'&lander=men');
                                    $del_key_ar = sha1($domain_name.$service_title_arr['0']."?store=ar_".$store->store_code.'&lander=men');
                                    $cacheObject->delData($del_key_en);
                                    $cacheObject->delData($del_key_ar);
                                }else{
                                    $del_key_en = sha1($domain_name.$service_title_arr['0']."?store=en_".$store->store_code);
                                    $del_key_ar = sha1($domain_name.$service_title_arr['0']."?store=ar_".$store->store_code);
                                    //delete cache based on keys
                                    $cacheObject->delData($del_key_en);
                                    $cacheObject->delData($del_key_ar);
                                }

							}else{
								// generate key
                                $title_temp_arr_sl = explode("/",$service_title_arr['0']);
                                if(in_array('siteLanders.php', $title_temp_arr_sl)){
                                    $del_key = sha1($domain_name.$service_title_arr['0']."?store=".$store->store_lang."_".$store->store_code.'&lander=kids');
                                    //delete cache based on key
                                    $cacheObject->delData($del_key);

                                    $del_key = sha1($domain_name.$service_title_arr['0']."?store=".$store->store_lang."_".$store->store_code.'&lander=women');
                                    //delete cache based on key
                                    $cacheObject->delData($del_key);

                                    $del_key = sha1($domain_name.$service_title_arr['0']."?store=".$store->store_lang."_".$store->store_code.'&lander=men');
                                    //delete cache based on key
                                    $cacheObject->delData($del_key);

                                }else{
                                    $del_key = sha1($domain_name.$service_title_arr['0']."?store=".$store->store_lang."_".$store->store_code);
                                    //delete cache based on key
                                    $cacheObject->delData($del_key);
                                }
							}
						}//foreach ($res_stores as  $store)
					}//if($res_stores)
				}//if(in_array('lander.php', $title_temp_arr))esle
			}//if(!empty($service_title_arr['1']))esle
		}//foreach ($res_services as $service)
	}//if($res_services)
}//if (isset($_POST['command']) && $_POST['command'] == 'clear-all-cache')

if (isset($_POST['command']) && $_POST['command'] == 'deleteTopSectionButtons'){

	$id = trim($_POST['id']);
	$app_id = trim($_POST['app_id']);
	$btn_type = trim($_POST['btnType']);
	if($btn_type=='global'){
		$sql = "INSERT INTO tbl_delete_data SET
                              app_id = '" . $app_id . "',
                              row_id = '" . $id . "',
                              row_type = 'top_section_buttons',
                              show_in_app = '0'";
    	$db->query($sql);
	}else{
		$sql = "DELETE FROM tbl_attachments WHERE id='" . $id . "'";
    	$db->query($sql);

    	$sql = "DELETE FROM tbl_delete_data WHERE row_id='" . $id . "' AND row_type='top_section_buttons'";
    	$db->query($sql);

	}
	
    echo $id;
}


if (isset($_POST['command']) && $_POST['command'] == 'delete-lander-pages') {
error_reporting(0);
	$attached='';
	$lander_id = trim($_POST['id']);
	$sql_merch = "SELECT attachment FROM tbl_rows";
	$res_merch = $db->get_results($sql_merch);
	if($res_merch){
		foreach ($res_merch as  $res_merch_single) {
			
			$attachments = json_decode($res_merch_single->attachment,true);
			if(!is_array($attachments[0])){
			    if($attachments[1] == 6 && $attachments[2]==$lander_id){
				  $attached.='attached';
				}
			}else{
    			foreach($attachments as $attach){
    				if($attach[1] == 6 && $attach[2]==$lander_id){
    				  $attached.='attached';
    				}
    			}
			}
		}
	}

	$sql_cat = "SELECT attachment FROM tbl_cat_rows";
	$res_cat = $db->get_results($sql_cat);
	if($res_cat){
		foreach ($res_cat as  $res_cat_single) {
			
			$attachments = json_decode($res_cat_single->attachment,true);
			if(!is_array($attachments[0])){
			    if($attachments[1] == 6 && $attachments[2]==$lander_id){
				  $attached.='attached';
				}
			}else{
    			foreach($attachments as $attach){
    				if($attach[1] == 6 && $attach[2]==$lander_id){
    				  $attached.='attached';
    				}
    			}
			}
		}
	}

	$sql_tops = "SELECT attachment FROM tbl_attachments WHERE section_type='top_section'";
	$res_tops = $db->get_results($sql_tops);
	if($res_tops){
		foreach ($res_tops as  $res_tops_single) {
			
			$attachments = json_decode($res_tops_single->attachment,true);
			if(!is_array($attachments[0])){
			    if($attachments[1] == 6 && $attachments[2]==$lander_id){
				  $attached.='attached';
				}
			}else{
    			foreach($attachments as $attach){
    				if($attach[1] == 6 && $attach[2]==$lander_id){
    				  $attached.='attached';
    				}
    			}
			}
		}
	}
	if($attached==''){

		$sql = "DELETE FROM tbl_lander_rows WHERE lander_id='".$lander_id."'";
    	$db->query($sql);

		$sql = "DELETE FROM tbl_lander_pages WHERE id='".$lander_id."'";
        $db->query($sql);

    	echo "Lander deleted successfully!";
	}else{
		echo "This lander is already attached, can't be deleted. Thank you!";
	}

}

if (isset($_POST['command']) && $_POST['command'] == 'delete-app-screen') {

    $sql = "DELETE FROM tbl_messages_cat WHERE id='".$_POST['id']."'";
    $db->query($sql);
    $sql = "DELETE FROM tbl_messages WHERE cat_id='".$_POST['id']."'";
    $db->query($sql);

}

if (isset($_POST['command']) && $_POST['command'] == 'delete-marketing-message') {

    $sql = "DELETE FROM tbl_messages WHERE id='".$_POST['id']."'";
    $db->query($sql);

}