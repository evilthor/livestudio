<?php
include('../connection.php');

ini_set('display_errors',1);
ini_set('max_execution_time', 0);
date_default_timezone_set('Asia/Dubai');
$date_new= date('Y-m-d');
$date_new;
$date = date('H:i');
$time = strtotime($date);
$time = $time - (10 * 60);
$previous_time = date("H:i", $time);

$sql_notifications = "SELECT * FROM tbl_notifications WHERE schedule_date = '".$date_new."' AND schedule_time = '".$date."' AND is_sent = '0' ";
$res = $db->get_results($sql_notifications);
$db->query("INSERT INTO tbl_crons SET status =  'Cron called at $date_new ".date('H:i')."'"); 
if($res){
	$db->query("INSERT INTO tbl_crons SET status =  'Record found at cron started $date_new ".date('H:i')."'"); 
	foreach($res as $r){

		$apps = $r->apps;
		$mtitle = $r->title;
		$mmessage = $r->message;
		$content = $r->attachment;
		//$dateTime = $_REQUEST['dateTime'];
		$schedule_date = $r->schedule_date;
		$schedule_time = $r->schedule_time;
		$apps_arr = explode(',',$apps);
		$id = $r->id;
		$android_apps = "";
		$sent = false;
		$ios_apps = array();
		$list="";
		$list_ios = "";

		foreach($apps_arr as $aa){
			if($aa == "all" || $aa == "android" || $aa == "ios" || $aa == "android-all-EN" || $aa == "android-all-AR" || $aa == "ios-all-EN" || $aa == "ios-all-AR"){
				continue;
			}else{
				if(strstr($aa,'ios-')){
					$ios_apps[] = trim(str_replace('ios-','',$aa));
				}else{
					$android_apps .= trim(str_replace('and-','',$aa)).",";
				}
			}
		}
		$android_apps = trim($android_apps,",");

		$file_new = $mtitle."_".$schedule_date."_".$id.".csv";
		$file_path = "../log_files/".$mtitle."_".$schedule_date."_".$id.".csv";
		$file = fopen("../log_files/".$mtitle."_".$schedule_date."_".$id.".csv","w+");
		$columns = "Token,mode,type,devicename,appname,status";
		fputcsv($file, explode(',',$columns));
		fclose($file);
		if($android_apps != ""){
					$sql = "SELECT DISTINCT token,`devicename`,`type`,`appname`, `mode`FROM tbl_devices WHERE type = 'android' AND app_id IN(".$android_apps.") AND push_woosh_enabled='0'";
					$results = $db->get_results($sql);
					if($results){
						$registration_ids = array();
						foreach($results as $r){
							array_push($registration_ids, $r->token);
							$list[] = "$r->token,$r->mode,$r->type,$r->devicename,$r->appname";
							}
						$push_url = "";
						$cid="";
						$pid="";
						$message = array('message' => $mmessage);
						
						$attachment = json_decode($content);
						$type = $attachment[0];
						$title = $attachment[2];
						if($type==1){
							$cid = $attachment[1];
							$message['type'] = 'category';
							$message['value'] = $cid;
							$message['title'] = $title;
							}elseif($type==2){
								$pid = $attachment[1];
								$message['type'] = 'product';
								$message['value'] = $pid;
								$message['title'] = $title;
							}elseif($type==3){
								$push_url = $attachment[1];
								$message['type'] = 'url';
								$message['value'] = $push_url;
								$message['title'] = $title;
							}elseif($type == '4'){
								$message['type'] = 'static';
								$message['value'] = '';
								$message['title'] = $title;
							}
						$sql_settings = "SELECT app_and_key FROM tbl_settings WHERE id='1'";
						$res_settings = $db->get_row($sql_settings);
						$apikey = $res_settings->app_and_key;
						$sent = true;
						$pushStatus = sendPushNotification($registration_ids, $message, $apikey,$list, $file_path);
						
					}
				}
		if(!empty($ios_apps)){
			$attachment = json_decode($content);
			$type = $attachment[0];
			$title = $attachment[2];
			$body['aps'] = array(
				'alert' => $mmessage,
				'sound' => 'default',
				);
			if($type == '1'){
				$cid = $attachment[1];
				$body['aps']['type'] = 'category';
				$body['aps']['value'] = $cid;
				$body['aps']['title'] = $title;
			}elseif($type == '2'){
				$pid = $attachment[1];
				$body['aps']['type'] = 'product';
				$body['aps']['value'] = $pid;
				$body['aps']['title'] = $title;
			}elseif($type == '3'){
				$push_url = $attachment[1];
				$body['aps']['type'] = 'url';
				$body['aps']['value'] = $push_url;
				$body['aps']['title'] = $title;
			}elseif($type == '4'){
				$body['aps']['type'] = 'static';
				$body['aps']['value'] = '';
				 $body['aps']['title'] = $title;
			}
			$mode = "production";
			if(isset($_REQUEST['mode']) && trim($_REQUEST['mode']) != ""){
				$mode = trim($_REQUEST['mode']);
				}
			foreach($ios_apps as $ia){
				$sql = "SELECT * FROM tbl_devices WHERE type = 'ios' AND mode = '".$mode."' AND app_id = '".$ia."' AND push_woosh_enabled='0'";
				$results = $db->get_results($sql);
				if($results){
					$tokens = array();
					foreach($results as $r){
						$tokens[] = $r->token;
						$list_ios[] = "$r->token,$r->mode,$r->type,$r->devicename,$r->appname";
						}
					if(!empty($tokens)){
						$sent = true;
						$sql_settings = "SELECT * FROM tbl_settings WHERE id='1'";
						$res_settings = $db->get_row($sql_settings);	
						sendPushNotificationsIos($tokens,$body, $mode, $res_settings ,$list_ios, $file_path);
					}
				}
			sleep(5);
			}
		}
		$sql = "UPDATE tbl_notifications SET
				is_sent      =  '1'
				WHERE  id = $id;
				";
		$db->query($sql);
		sendEmail($file_new);
	}
	$db->query("INSERT INTO tbl_crons SET status =  'Cron finished at $date_new ".date('H:i')."'");
}
    
function sendPushNotification($registration_ids, $message, $apikey, $list, $file_new) {
	if(sizeof($registration_ids) > 500){
		$registrations = array_chunk($registration_ids, 500);
		}else{
			$registrations[0] = $registration_ids;
			}
	$i = 0;
	define('GOOGLE_API_KEY', $apikey);
	foreach($registrations as $ra){
		$url = 'https://android.googleapis.com/gcm/send';
		$fields = array(
			'registration_ids' => $ra,
			'data' => $message,
		);
		$headers = array(
			'Authorization:key=' . GOOGLE_API_KEY,
			'Content-Type: application/json'
		);
		//echo json_encode($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$output = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
	
		$result = json_decode($result);
		foreach($result->results as $r){
			if(isset($r->error)){
			  $list[$i] .=",".$r->error;
			}
			else{
				$list[$i] .=",Sent";
			}
			$file = fopen($file_new,"a");
			fputcsv($file, explode(',',$list[$i]));
			fclose($file);
			$i++;
		}
		curl_close($ch);   
    
	}
}

function sendPushNotificationsIos($tokens,$body, $mode, $res_settings, $list_ios,$file_new){
	$fp = openConnectionToApple($mode, $res_settings);
	$i=0;
	if ($fp){
		foreach($tokens as $token){
			try{
			$payload = json_encode($body);
			$msg = chr(0) . pack('n', 32) . pack('H*', $token) . pack('n', strlen($payload)) . $payload;
			$result = fwrite($fp, $msg, strlen($msg));
			if (!$result){
				$list_ios[$i] .= ",Not Sent";
				fclose($fp);
				sleep(5);
				$fp = openConnectionToApple($mode, $res_settings);
			}
			else{
				$list_ios[$i] .= ",Sent";
			}
			$file = fopen($file_new,"a");
			fputcsv($file, explode(',',$list_ios[$i]));
			fclose($file);
			$i++;
			}catch(Exception $e){
				$i++;
				$list_ios[$i] .= ",".$e->getMessage();
				$file = fopen($file_new,"a");
				fputcsv($file, explode(',',$list_ios[$i]));
				fclose($file);
				fclose($fp);
				sleep(5);
				$fp = openConnectionToApple($mode, $res_settings);
				}
		}
	fclose($fp);
}	
}

function openConnectionToApple($mode, $res_settings){
	$dev_ssl = "../".$res_settings->dev_ssl;
	$dev_pass = $res_settings->dev_passphrase;
	
	$pro_ssl = "../".$res_settings->pro_ssl;
	$pro_pass = $res_settings->pro_passphrase;
	// Put your alert message here:
	$ctx = stream_context_create();
	////////////////////////////////////////////////////////////////////////////////
	if($mode == 'development'){	
		stream_context_set_option($ctx, 'ssl', 'local_cert', $dev_ssl);
		stream_context_set_option($ctx, 'ssl', 'passphrase', $dev_pass);
	}
	if($mode == 'production'){
		stream_context_set_option($ctx, 'ssl', 'local_cert', $pro_ssl);
		stream_context_set_option($ctx, 'ssl', 'passphrase', $pro_pass);
	}
	if($mode == "development"):
		$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		elseif($mode == "production"):
		$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
	endif;
	return $fp;
	}

function sendEmail($file_new){ 
 $file = "../log_files/".$file_new;
 $file_size = filesize($file);
 $handle = fopen($file, "r");
 $content = fread($handle, $file_size);
 $subject = "Testing Email";
 $mailto = "naveed.abbas@progos.org";
 fclose($handle);
 $content = chunk_split(base64_encode($content));
 $uid = md5(uniqid(time()));
 $eol = PHP_EOL;
 $header = "From: webmaster@example.com \r\n";
 $header .= "Reply-To: webmaster@example.com \r\n";
 $header .= "MIME-Version: 1.0\r\n";
 $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"";
 $message = "This is a multi-part message in MIME format.\r\n";
 $message .= "--".$uid.$eol;
 $message .= "Content-type:text/plain; charset=iso-8859-1".$eol;
 $message .= "Content-Transfer-Encoding: 7bit".$eol;
 $message .= "--".$uid.$eol;
 $message .= "Content-Type: application/octet-stream; name=\"".$file_new.$eol; // use different content types here
 $message .= "Content-Transfer-Encoding: base64\r\n";
 $message .= "Content-Disposition: attachment; filename=\"".$file_new.$eol;
 $message .= $content.$eol;
 $message .= "--".$uid."--";
 if (mail($mailto, $subject, $message, $header)) {
 
 } else {
 
 }
}

?>