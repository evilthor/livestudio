<?php
include "../config.php";
if (isset($_POST['command']) && $_POST['command'] == 'edit-store') {

     //$country_flag = trim($_POST['country-flag']);
     $country_code = trim($_POST['country-code']);
     $country_name = trim($_POST['country-name']);
     $country_name_arabic = trim($_POST['country-name-arabic']);
     $country_name_analytics = trim($_POST['country-name-analytics']);
     $currency_analytics = trim($_POST['currency-analytics']);
     $store_language = trim($_POST['store-language']);
     $fb_content_id_en = trim($_POST['fb-content-id-en']);
     $fb_content_id_ar = trim($_POST['fb-content-id-ar']);
     $store_code = strtolower($country_code);
     $store_status = trim($_POST['status']);
     $store_id = $_POST['store_id'];

     if($_FILES["country-flag"]["name"]!=''){

          $sql = "SELECT * FROM tbl_stores WHERE id='".$store_id."'";
          $res_edit_store = $db->get_row($sql);

          $destination_path = dirname(getcwd()) ."/images/countries_flags/";
          if( !file_exists($destination_path) ){
               mkdir($destination_path);
          }
          if( file_exists($destination_path."/".$res_edit_store->country_flag) ){
               unlink($destination_path."/".$res_edit_store->country_flag);
          }
          
          $filename = date('ymdhis').basename( $_FILES["country-flag"]["name"]);
          $target_path = $destination_path .$filename;
          move_uploaded_file($_FILES['country-flag']['tmp_name'], $target_path);
     }else{
         $filename =  $_POST['hid_image'];
     }
     
    if($store_language=='ar'){
        $fb_content_id_en = '';
    }
    if($store_language=='en'){
        $fb_content_id_ar = '';
    }
     $sql = "UPDATE tbl_stores SET
                         store_lang = '" . $store_language . "',
                         country_flag = '" . $filename . "',
                         country_code = '" . $country_code . "',
                         country_name = '" . $country_name . "',
                         country_name_arabic = '" .$country_name_arabic . "',
                         country_name_analytics = '" . $country_name_analytics . "',
                         currency_analytics = '" . $currency_analytics . "',
                         fb_content_id_en = '" . $fb_content_id_en . "',
                         fb_content_id_ar = '" . $fb_content_id_ar . "',
                         store_code = '" .$store_code . "',
                         status = '".$store_status."'
                         WHERE id = '".$store_id."'";

     if ($db->query($sql)) {
          $store_id = $store_id;
     } else {
          $error = true;
     }

}



//cache section

if (isset($_POST['command']) && $_POST['command'] == 'edit-cache-category') {
     $message = '';
     $category = trim($_POST['category']);
     $id = $_POST['catid'];

     // $sql = "SELECT * FROM `tbl_cache_categories` WHERE `title` = '".$category."'";
     // $cate = $db->get_results($sql);

     // if( count($cate) == 1 ){
          $sql = "UPDATE tbl_cache_categories SET title = '" . $category . "' WHERE id = '".$id."'";
          $db->query($sql);
          $message = array(
               'type' => 'success',
               'message' => 'cache category updated successfully.'
          );
     // }else{
     //      $message = array(
     //           'type' => 'warn',
     //           'message' => 'cache category already exists.'
     //      );
     // }
     echo json_encode($message);
}

if (isset($_POST['command']) && $_POST['command'] == 'edit-api-service') {
     $message = '';

     $category = trim($_POST['cache-category']);
     $service = trim($_POST['service']);
     $sid = $_POST['sid'];

     // $sql = "SELECT * FROM `tbl_cache_services` WHERE `cat_id` = '$category' AND  `title` = '$service'";
     // $serv = $db->get_results($sql);

     // if( count($serv) == 1 || count($serv) == 0 ){
          $sql = "UPDATE tbl_cache_services SET cat_id = '".$category."', title = '" . $service . "' WHERE id = '".$sid."'";
          $db->query($sql);
          $message = array(
               'type' => 'success',
               'message' => 'cache service updated successfully.'
          );
     // }else{
     //      $message = array(
     //           'type' => 'warn',
     //           'message' => 'cache service already exists.'
     //      );
     // }
     echo json_encode($message);
}

if (isset($_POST['command']) && $_POST['command'] == 'activate-scheme') {

     // deactive all color scheme
     $sql = "UPDATE tbl_color_schemes SET active = '0' WHERE 1=1";
     $db->query($sql);

     $id = $_POST['id'];
     $sql = "UPDATE tbl_color_schemes SET active = '1' WHERE id = '".$id."'";
     if ($db->query($sql)) {
          $sql_scheme = "SELECT title, slug FROM tbl_color_schemes WHERE active = '1' limit 1";
          $res_scheme = $db->get_results($sql_scheme);
          $res_scheme = $res_scheme[0];
          echo json_encode($res_scheme);
     } else {
          
     }
}

// Edit country

if (isset($_POST['command']) && $_POST['command'] == 'edit-country') {

    //$country_flag = trim($_POST['country-flag']);
    $country_code = trim($_POST['country-code']);
    $country_name = trim($_POST['country-name']);
    $short_code = trim($_POST['short-code']);
    $country_status = trim($_POST['status']);
    $country_id = $_POST['country_id'];

    $sql = "UPDATE tbl_countries SET
                         short_code = '" . $short_code . "',
                         country_code = '" . $country_code . "',
                         country_name = '" . $country_name . "',
                         status = '".$country_status."'
                         WHERE id = '".$country_id."'";

    if ($db->query($sql)) {
        $country_id = $country_id;
    } else {
        $error = true;
    }

}


if (isset($_POST['command']) && $_POST['command'] == 'edit-app-screen') {
    $message = '';
    $category = trim($_POST['screen-name']);
    $id = $_POST['id'];
    $sql = "UPDATE tbl_messages_cat SET title = '" . $category . "' WHERE id = '".$id."'";
    $db->query($sql);
    $message = array(
        'type' => 'success',
        'message' => 'App screen updated successfully.'
    );
    echo json_encode($message);
}

if (isset($_POST['command']) && $_POST['command'] == 'edit-marketing-message') {
    $message = '';

    $app_screen = trim($_POST['app-screen']);
    $message = trim($_POST['message']);
    $mid = $_POST['mid'];

    $sql = "UPDATE tbl_messages SET cat_id = '".$app_screen."', message = '" . $message . "' WHERE id = '".$mid."'";
    $db->query($sql);
    $message = array(
        'type' => 'success',
        'message' => 'Message updated successfully.'
    );
    echo json_encode($message);
}