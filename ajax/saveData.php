<?php
include "../config.php";

if (isset($_POST['command']) && $_POST['command'] == 'layoutSort') {
     if (isset($_POST['row_order']) && !empty($_POST['row_order'])) {

          $message = $sort = null;
          $layout = $sql_del = $sql = "";
          
          $sort = explode(',', $_POST['row_order']);
          $sort = array_flip($sort);

          $merchandise_id = intval($_POST['merchandise_id']);
          $app_id = intval($_POST['app_id']);
          $row_type = $_POST['row_type'];
          
          foreach ($sort as $id => $order) {

               $sql_del .= "DELETE FROM tbl_sort_rows WHERE 
                    app_id = '$app_id'
                    AND row_id = '$id'
                    AND row_type_id = '$merchandise_id'
                    AND row_type = '$row_type'; ";

               $sql .= "INSERT INTO tbl_sort_rows SET
                    app_id = '" . $app_id . "',
                    row_id = '" . $id . "',
                    row_type = '" . $row_type . "',
                    row_type_id = '" . $merchandise_id . "',
                    sort_order = '" . $order . "'; ";

               // $sql .= "UPDATE tbl_rows SET row_order = '" . $order . "' WHERE id = '" . $id . "'; ";
          }
          $db->query($sql_del);
          if ($db->query($sql) !== false) {

               
               $message = array(
                    'type' => 'success',
                    'message' => 'Layouts successfully sorted.',
               );
               $sort_sql = "SELECT * FROM tbl_sort_rows WHERE app_id = '" . $app_id . "' AND row_type_id = '" . $merchandise_id . "' AND row_type = '".$row_type."' ORDER BY sort_order ASC";
               $sort_results = $db->get_results($sort_sql);
               foreach ($sort_results as $sort_result) {
                    $sql = "SELECT * FROM tbl_rows WHERE id = '" . $sort_result->row_id . "'";
                    $results = $db->get_results($sql);
                    if (@$results) {
                         foreach ($results as $result) {
                              $images = json_decode($result->images);
                              if ($result->layout == "single" || $result->layout == "singleShort" || $result->layout == "singleBar") {
                                   $layout .= '<div class="row single-template-layout">';
                                   $layout .= '<div class="col-md-12">';
                                   $layout .= '<div class="preview"><img src="' . $images[0] . '"></div>';
                                   $layout .= '</div>';
                                   $layout .= '</div>';
                              } elseif ($result->layout == "double" || $result->layout == "doubleShort") {
                                   $doubleShort = ($result->layout == 'doubleShort') ? 'short' : '';
                                   $layout .= '<div class="row double-template-layout ' . $doubleShort . '">';
                                   $layout .= '<div class="col-md-6">';
                                   $layout .= '<div class="preview"><img src="' . $images[0] . '"></div>';
                                   $layout .= '</div>';
                                   $layout .= '<div class="col-md-6">';
                                   $layout .= '<div class="preview"><img src="' . $images[1] . '"></div>';
                                   $layout .= '</div>';
                                   $layout .= '</div>';
                              } elseif ($result->layout == "tripleLeft") {
                                   $layout .= '<div class="row preview-triple-left">';
                                   $layout .= '<div class="col-md-6">';
                                   $layout .= '<div class="row">';
                                   $layout .= '<div class="col-md-12">';
                                   $layout .= '<div class="preview _1024X704"><img src="' . $images[0] . '"></div>';
                                   $layout .= '</div>';
                                   $layout .= '<div class="col-md-12">';
                                   $layout .= '<div class="preview _1024X704"><img src="' . $images[1] . '"></div>';
                                   $layout .= '</div>';
                                   $layout .= '</div>';
                                   $layout .= '</div>';
                                   $layout .= '<div class="col-md-6">';
                                   $layout .= '<div class="preview _1024X1310"><img src="' . $images[2] . '"></div>';
                                   $layout .= '</div>';
                                   $layout .= '</div>';
                              } elseif ($result->layout == "tripleRight") {
                                   $layout .= '<div class="row preview-triple-right">';
                                   $layout .= '<div class="col-md-6">';
                                   $layout .= '<div class="preview _1024X1310"><img src="' . $images[0] . '"></div>';
                                   $layout .= '</div>';
                                   $layout .= '<div class="col-md-6">';
                                   $layout .= '<div class="row">';
                                   $layout .= '<div class="col-md-12">';
                                   $layout .= '<div class="preview _1024X704"><img src="' . $images[1] . '"></div>';
                                   $layout .= '</div>';
                                   $layout .= '<div class="col-md-12">';
                                   $layout .= '<div class="preview _1024X704"><img src="' . $images[2] . '"></div>';
                                   $layout .= '</div>';
                                   $layout .= '</div>';
                                   $layout .= '</div>';
                                   $layout .= '</div>';
                              }
                         }
                    }
               }
          } else {
               $message = array(
                    'type' => 'error',
                    'message' => 'Layouts not sort, try again.',
               );
          }
     } else {
          $message = array(
               'type' => 'warning',
               'message' => 'Order isn\'t set properly.',
          );
     }
     echo json_encode(array(
          'html' => $layout,
          'message' => $message,
     ));
}
if (isset($_POST['command']) && $_POST['command'] == 'layoutSortCat') {
     if (isset($_POST['row_order']) && !empty($_POST['row_order'])) {

          $message = $sort = null;
          $layout = $sql_del = $sql = "";
          
          $sort = explode(',', $_POST['row_order']);
          $sort = array_flip($sort);

          $search_category_id = intval($_POST['search_category_id']);
          $app_id = intval($_POST['app_id']);
          $row_type = $_POST['row_type'];

          foreach ($sort as $id => $order) {
               $sql_del .= "DELETE FROM tbl_sort_rows WHERE 
                    app_id = '$app_id'
                    AND row_id = '$id'
                    AND row_type_id = '$search_category_id'
                    AND row_type = '$row_type'; ";

               $sql .= "INSERT INTO tbl_sort_rows SET
                    app_id = '" . $app_id . "',
                    row_id = '" . $id . "',
                    row_type = '" . $row_type . "',
                    row_type_id = '" . $search_category_id . "',
                    sort_order = '" . $order . "'; ";

               //$sql .= "UPDATE tbl_cat_rows SET row_order = '" . $order . "' WHERE id = '" . $id . "'; ";
          }
          $db->query($sql_del);
          if ($db->query($sql) !== false) {
               $message = array(
                    'type' => 'success',
                    'message' => 'Layouts successfully sorted.',
               );
               $sort_sql = "SELECT * FROM tbl_sort_rows WHERE app_id = '" . $app_id . "' AND row_type_id = '" . $search_category_id . "' AND row_type = '".$row_type."' ORDER BY sort_order ASC";
               $sort_results = $db->get_results($sort_sql);
               foreach ($sort_results as $sort_result) {
                    $sql = "SELECT * FROM tbl_cat_rows WHERE id = '" . $sort_result->row_id . "'";
                    $results = $db->get_results($sql);
                    if (@$results) {
                         foreach ($results as $result) {
                              $images = json_decode($result->images);
                              if ($result->layout == "single" || $result->layout == "singleShort" || $result->layout == "singleBar") {
                                   $layout .= '<div class="row single-template-layout">';
                                   $layout .= '<div class="col-md-12">';
                                   $layout .= '<div class="preview"><img src="' . $images[0] . '"></div>';
                                   $layout .= '</div>';
                                   $layout .= '</div>';
                              } elseif ($result->layout == "double" || $result->layout == "doubleShort") {
                                   $doubleShort = ($result->layout == 'doubleShort') ? 'short' : '';
                                   $layout .= '<div class="row double-template-layout ' . $doubleShort . '">';
                                   $layout .= '<div class="col-md-6">';
                                   $layout .= '<div class="preview"><img src="' . $images[0] . '"></div>';
                                   $layout .= '</div>';
                                   $layout .= '<div class="col-md-6">';
                                   $layout .= '<div class="preview"><img src="' . $images[1] . '"></div>';
                                   $layout .= '</div>';
                                   $layout .= '</div>';
                              } elseif ($result->layout == "tripleLeft") {
                                   $layout .= '<div class="row preview-triple-left">';
                                   $layout .= '<div class="col-md-6">';
                                   $layout .= '<div class="row">';
                                   $layout .= '<div class="col-md-12">';
                                   $layout .= '<div class="preview _1024X704"><img src="' . $images[0] . '"></div>';
                                   $layout .= '</div>';
                                   $layout .= '<div class="col-md-12">';
                                   $layout .= '<div class="preview _1024X704"><img src="' . $images[1] . '"></div>';
                                   $layout .= '</div>';
                                   $layout .= '</div>';
                                   $layout .= '</div>';
                                   $layout .= '<div class="col-md-6">';
                                   $layout .= '<div class="preview _1024X1310"><img src="' . $images[2] . '"></div>';
                                   $layout .= '</div>';
                                   $layout .= '</div>';
                              } elseif ($result->layout == "tripleRight") {
                                   $layout .= '<div class="row preview-triple-right">';
                                   $layout .= '<div class="col-md-6">';
                                   $layout .= '<div class="preview _1024X1310"><img src="' . $images[0] . '"></div>';
                                   $layout .= '</div>';
                                   $layout .= '<div class="col-md-6">';
                                   $layout .= '<div class="row">';
                                   $layout .= '<div class="col-md-12">';
                                   $layout .= '<div class="preview _1024X704"><img src="' . $images[1] . '"></div>';
                                   $layout .= '</div>';
                                   $layout .= '<div class="col-md-12">';
                                   $layout .= '<div class="preview _1024X704"><img src="' . $images[2] . '"></div>';
                                   $layout .= '</div>';
                                   $layout .= '</div>';
                                   $layout .= '</div>';
                                   $layout .= '</div>';
                              }
                         }
                    }
               }
          } else {
               $message = array(
                    'type' => 'error',
                    'message' => 'Layouts not sort, try again.',
               );
          }
     } else {
          $message = array(
               'type' => 'warning',
               'message' => 'Order isn\'t set properly.',
          );
     }
     echo json_encode(array(
          'html' => $layout,
          'message' => $message,
     ));
}
if (isset($_POST['command']) && $_POST['command'] == 'saveRow') {

     if($_POST['app_id'] == GLOBAL_EN || $_POST['app_id'] == GLOBAL_AR){
     $is_global = '1';
     }else{
          $is_global = '0';
     }

     $images = $_POST['images'];
     $attachments = $_POST['attachments'];
     $merchandise_id = $_POST['id'];
     $layout = $_POST['layout'];
     $app_id = $_POST['app_id'];
     $row_id = $_POST['row_id'];
     $error = false;
     if ($row_id == '') {
          $sql = "INSERT INTO tbl_rows SET
               merchandise_id = '" . $merchandise_id . "',
               app_id = '".$app_id."',
               layout = '" . $layout . "',
               images = '" . $images . "',
               attachment = '" . $attachments . "',
               is_global = '".$is_global."',
               show_in_app = '1'
               ";
          if ($db->query($sql) !== false) {
               $mid = $db->insert_id;
               $sql = "INSERT INTO tbl_sort_rows SET
                    app_id = '$app_id',
                    row_id = '$db->insert_id',
                    row_type = 'merchandise',
                    row_type_id = '$merchandise_id'
                    ";
               $db->query($sql);

               $check_app = "SELECT id FROM tbl_apps WHERE parent_app = '".$app_id."'";
               $res_app = $db->get_results($check_app);
               if($res_app){
                    foreach ($res_app  as $res_app_single) {
                         $sql = "INSERT INTO tbl_sort_rows SET
                         app_id = '$res_app_single->id',
                         row_id = '$mid',
                         row_type = 'merchandise',
                         row_type_id = '$merchandise_id'
                         ";
                         $db->query($sql);
                    }
               }

          }else{
               $error = true;
          }
     } else {
          $sql_images = "SELECT images FROM tbl_rows WHERE id = '" . $row_id . "'";
          $result_images = $db->get_row($sql_images);
          $old_images = json_decode($result_images->images);
          $new_images = json_decode($images);
          for ($i = 0; $i < sizeof($new_images); $i++) {
               if ($new_images[$i] != $old_images[$i] && file_exists($old_images[$i])) {
                    unlink($old_images[$i]);
               }
          }
          $sql = "UPDATE tbl_rows SET
               merchandise_id = '" . $merchandise_id . "',
               layout = '" . $layout . "',
               images = '" . $images . "',
               attachment = '" . $attachments . "'
               WHERE id = '" . $row_id . "'
               ";
          if ($db->query($sql) === false) {
               $error = true;
          }
     }
     if ($error) {
          echo "error";
     } else {
          echo $merchandise_id;
     }
}

if (isset($_POST['command']) && $_POST['command'] == 'landerLayoutSort') {
     $sort = null;
     if (isset($_POST['row_order']) && !empty($_POST['row_order'])) {
          $sort = explode(',', $_POST['row_order']);
          $sort = array_flip($sort);
          $lander_id = intval($_POST['lander_id']);
          $sql = "";
          $message = null;
          $layout = '';
          foreach ($sort as $id => $order) {
               $sql .= "UPDATE tbl_lander_rows SET row_order = '" . $order . "' WHERE id = '" . $id . "'; ";
          }
          if ($db->query($sql) !== false) {
               $message = array(
                    'type' => 'success',
                    'message' => 'Layouts successfully sorted.',
               );
               $sql = "SELECT * FROM tbl_lander_rows WHERE lander_id = '" . $lander_id . "' ORDER BY row_order ASC";
               $results = $db->get_results($sql);
               $layout = perviewLayouts($results); // this function in common_php.php
          } else {
               $message = array(
                    'type' => 'error',
                    'message' => 'Layouts not sort, try again.',
               );
          }
     } else {
          $message = array(
               'type' => 'warning',
               'message' => 'Order isn\'t set properly.',
          );
     }
     echo json_encode(array(
          'html' => $layout,
          'message' => $message,
     ));
}

//Search category new section with all layouts
if (isset($_POST['command']) && $_POST['command'] == 'layoutSortCatNew') {
    if (isset($_POST['row_order']) && !empty($_POST['row_order'])) {

        $message = $sort = null;
        $layout = $sql_del = $sql = "";

        $sort = explode(',', $_POST['row_order']);
        $sort = array_flip($sort);

        $search_category_id = intval($_POST['search_category_id']);
        $app_id = intval($_POST['app_id']);
        $row_type = $_POST['row_type'];

        foreach ($sort as $id => $order) {
            $sql_del .= "DELETE FROM tbl_sort_rows WHERE 
                    app_id = '$app_id'
                    AND row_id = '$id'
                    AND row_type_id = '$search_category_id'
                    AND row_type = '$row_type'; ";

            $sql .= "INSERT INTO tbl_sort_rows SET
                    app_id = '" . $app_id . "',
                    row_id = '" . $id . "',
                    row_type = '" . $row_type . "',
                    row_type_id = '" . $search_category_id . "',
                    sort_order = '" . $order . "'; ";
        }
        $db->query($sql_del);
        if ($db->query($sql) !== false) {
            $message = array(
                'type' => 'success',
                'message' => 'Layouts successfully sorted.',
            );
            $sort_sql = "SELECT * FROM tbl_sort_rows WHERE app_id = '" . $app_id . "' AND row_type_id = '" . $search_category_id . "' AND row_type = '".$row_type."' ORDER BY sort_order ASC";
            $sort_results = $db->get_results($sort_sql);
            foreach ($sort_results as $sort_result) {
                $sql = "SELECT * FROM tbl_cat_rows_new WHERE id = '" . $sort_result->row_id . "'";
                $results = $db->get_results($sql);
                if (@$results) {
                    foreach ($results as $result) {
                        $images = json_decode($result->images);
                        if ($result->layout == "single" || $result->layout == "singleShort" || $result->layout == "singleBar") {
                            $layout .= '<div class="row single-template-layout">';
                            $layout .= '<div class="col-md-12">';
                            $layout .= '<div class="preview"><img src="' . $images[0] . '"></div>';
                            $layout .= '</div>';
                            $layout .= '</div>';
                        } elseif ($result->layout == "double" || $result->layout == "doubleShort") {
                            $doubleShort = ($result->layout == 'doubleShort') ? 'short' : '';
                            $layout .= '<div class="row double-template-layout ' . $doubleShort . '">';
                            $layout .= '<div class="col-md-6">';
                            $layout .= '<div class="preview"><img src="' . $images[0] . '"></div>';
                            $layout .= '</div>';
                            $layout .= '<div class="col-md-6">';
                            $layout .= '<div class="preview"><img src="' . $images[1] . '"></div>';
                            $layout .= '</div>';
                            $layout .= '</div>';
                        } elseif ($result->layout == "tripleLeft") {
                            $layout .= '<div class="row preview-triple-left">';
                            $layout .= '<div class="col-md-6">';
                            $layout .= '<div class="row">';
                            $layout .= '<div class="col-md-12">';
                            $layout .= '<div class="preview _1024X704"><img src="' . $images[0] . '"></div>';
                            $layout .= '</div>';
                            $layout .= '<div class="col-md-12">';
                            $layout .= '<div class="preview _1024X704"><img src="' . $images[1] . '"></div>';
                            $layout .= '</div>';
                            $layout .= '</div>';
                            $layout .= '</div>';
                            $layout .= '<div class="col-md-6">';
                            $layout .= '<div class="preview _1024X1310"><img src="' . $images[2] . '"></div>';
                            $layout .= '</div>';
                            $layout .= '</div>';
                        } elseif ($result->layout == "tripleRight") {
                            $layout .= '<div class="row preview-triple-right">';
                            $layout .= '<div class="col-md-6">';
                            $layout .= '<div class="preview _1024X1310"><img src="' . $images[0] . '"></div>';
                            $layout .= '</div>';
                            $layout .= '<div class="col-md-6">';
                            $layout .= '<div class="row">';
                            $layout .= '<div class="col-md-12">';
                            $layout .= '<div class="preview _1024X704"><img src="' . $images[1] . '"></div>';
                            $layout .= '</div>';
                            $layout .= '<div class="col-md-12">';
                            $layout .= '<div class="preview _1024X704"><img src="' . $images[2] . '"></div>';
                            $layout .= '</div>';
                            $layout .= '</div>';
                            $layout .= '</div>';
                            $layout .= '</div>';
                        }
                    }
                }
            }
        } else {
            $message = array(
                'type' => 'error',
                'message' => 'Layouts not sort, try again.',
            );
        }
    } else {
        $message = array(
            'type' => 'warning',
            'message' => 'Order isn\'t set properly.',
        );
    }
    echo json_encode(array(
        'html' => $layout,
        'message' => $message,
    ));
}