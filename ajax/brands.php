<?php
ini_set("display_errors", "1");

if (@$_REQUEST['lang'] == 'en_global') {
     $lang = 'en_ae';
} elseif (@$_REQUEST['lang'] == 'ar_global') {
     $lang = 'ar_ae';
} elseif (!isset($_REQUEST['lang']) || empty($_REQUEST['lang'])) {
     $lang = 'en_ae';
} else {
     $lang = @$_REQUEST['lang'];
}
$cid = @$_REQUEST['cid'];
if ($cid) {
     $url = 'https://www.elabelz.com/' . $lang . '/emapi/index/brandsforstudio';
     $url .= "?cid=" . $cid;
     $ch = curl_init();
     curl_setopt($ch, CURLOPT_URL, $url);
     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
     $result = curl_exec($ch);
     curl_close($ch);
} else {
     $lang = explode('_', $lang);
     $file = '../brands_main_' . trim($lang[0]) . '.json';
     $result = file_get_contents($file);
     $result = json_decode($result, true);
     //$result = json_encode($result);
}
echo $result;
