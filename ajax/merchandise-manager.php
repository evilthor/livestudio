<?php
	if( isset($_REQUEST['command']) && $_REQUEST['command'] == 'img-upload' ){

		header('Content-type: application/json');

		echo json_encode($_REQUEST);

	} 
	if( isset($_REQUEST['command']) && $_REQUEST['command'] == 'merchandise' ){
		$id = intval($_REQUEST['id']);
		if( $id > 0 ){
?>
			<div class="layoutItem clearfix">
				<div class="leftItem pull-left">
					<div class="firstItem">
						<div class="imageBlock animated fadeIn">
							<img ng-src="https://cms-highstreetapp.imgix.net/elabelz-ensa/promotions/ypN0D7c4TS5QXTKElkcAkA_source?fit=crop&amp;fm=jpg&amp;q=50&amp;w=0.40&amp;h=0.40" loading-item="" src="https://cms-highstreetapp.imgix.net/elabelz-ensa/promotions/ypN0D7c4TS5QXTKElkcAkA_source?fit=crop&amp;fm=jpg&amp;q=50&amp;w=0.40&amp;h=0.40">
						</div>
					</div>
				</div>
				<div class="rightItem pull-right">
					<div class="secondItem">
						<div class="imageBlock animated fadeIn">
							<img ng-src="https://cms-highstreetapp.imgix.net/elabelz-ensa/promotions/wLHPpiVAcGOYXsi0mdh-Ow_source?fit=crop&amp;fm=jpg&amp;q=50&amp;w=0.40&amp;h=0.40" loading-item="" src="https://cms-highstreetapp.imgix.net/elabelz-ensa/promotions/wLHPpiVAcGOYXsi0mdh-Ow_source?fit=crop&amp;fm=jpg&amp;q=50&amp;w=0.40&amp;h=0.40">
						</div>
					</div>
				</div>
			</div>
<?php
		}
	}
?>