<?php include 'config.php';?>
<?php include 'includes/session_check.php';?>
<?php
if (isset($_POST["app_name"])):
     $appimage = $_POST['old_appimage'];
     if ($_FILES['appimage']['tmp_name'] != '') {
          $targetimage = "images/";
          $targetimage = $targetimage . basename($_FILES['appimage']['name']);
          $imageFileType = pathinfo($targetimage, PATHINFO_EXTENSION);
          if (move_uploaded_file($_FILES['appimage']['tmp_name'], $targetimage)) {
               $appimage = $targetimage;
          }
     }
     $sql = "UPDATE tbl_apps SET
                  app_name = '" . $_POST["app_name"] . "',
                  app_desc = '" . $_POST["app_desc"] . "',
                  app_store = '" . $_POST["app_store"] . "',
                  appimage = '" . $appimage . "'
                  WHERE id = '" . $_REQUEST['app_id'] . "'
                  ";
     $error = false;
     if ($db->query($sql) !== false) {
          header('Location:apps.php?message=update');exit;
     } else {
          $error = true;
     }
endif;
?>
    <?php
$sql = "SELECT * FROM tbl_apps WHERE id = '" . $_GET['app_id'] . "'";
$result = $db->get_row($sql);
if (!$result) {
     header('Location:apps.php?message=wrong');exit;
}
?>
        <?php include 'includes/header.php';?>
<body>
        
            <div id="wrapper">
                <?php include 'includes/navigation.php';?>
                <div id="page-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="apps">
                                    <div class="title">
                                        <h2>Edit <strong><?php echo $result->app_name; ?></strong></h2>
                                    </div>
                                    <?php
if (@$error) {
     ?>
                                        <div class="clearfix">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <p class="alert-danger">Error occured, Please try again later.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
}
?>
                                            <div class="clearfix">
                                                <div class="row">
                                                    <div class="col-md-2"></div>
                                                    <div class="col-md-8">
                                                        <form id="frmAddApp" class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-2 control-label">App name</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" id="app_name" name="app_name" required placeholder="App name" value="<?php echo $result->app_name; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-2 control-label">App Description</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" id="app_desc" name="app_desc" required placeholder="App description"><?php echo $result->app_desc; ?></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <?php
                                                          $sql = "SELECT * FROM tbl_stores 
                                                                                WHERE status='1' 
                                                                                order by store_lang";
                                                          $res_stores = $db->get_results($sql);
                                                                $stores = array();
                                                                foreach ($res_stores as $res_store) {
                                                                    if($res_store->store_lang == "en"){
                                                                        $stores['EN'][$res_store->country_code] = $res_store->country_code;
                                                                    }elseif($res_store->store_lang == "ar"){
                                                                        $stores['AR'][$res_store->country_code] = $res_store->country_code;
                                                                    }else{
                                                                        $stores['EN'][$res_store->country_code] = $res_store->country_code;
                                                                        $stores['AR'][$res_store->country_code] = $res_store->country_code;
                                                                    }
                                                                }
                                                       ?>
                                                                    <label for="" class="col-sm-2 control-label">Store</label>
                                                                    <div class="col-sm-10">
                                                                        <select id="app_store" name="app_store" class="form-control" required>
                                                                            <?php foreach ($stores as $index => $store) { ?>
                                                                                <optgroup label="<?php echo $index ?>">
                                                                                    <?php
                                                                                    foreach ($store as $code => $country) {
                                                                                        $value = strtolower($index). "_". strtolower($code);
                                                                                        $selected = ( $result->app_store == $value ) ? 'selected=""' : "";
                                                                                        ?>
                                                                                        <option value="<?php echo $value; ?>" <?php echo $selected; ?>>
                                                                                            <?php echo strtoupper($country); ?>
                                                                                        </option>
                                                                                    <?php } ?>
                                                                                </optgroup>
                                                                            <?php } ?>

                                                                        </select>

                                                                    </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-2"></div>
                                                                <div class="col-sm-10">
                                                                    <label for="" class="control-label">App image</label>
                                                                    <input type="file" class="form-control" id="appimage" name="appimage">
                                                                    <br>
                                                                    <img width="100" src="<?php echo $result->appimage; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-offset-2 col-sm-10">
                                                                    <input type="hidden" name="old_appimage" value="<?php echo $result->appimage; ?>">
                                                                    <button type="submit" class="btn btn-primary">Update</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="col-md-2"></div>
                                                </div>
                                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include 'includes/footer.php';?>
        </body>

        </html>
