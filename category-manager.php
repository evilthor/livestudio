<?php
	include 'config.php';
	include 'includes/session_check.php';

	if(isset($_GET['id']) && $_GET['id'] != ''){
		$checkDB = "SELECT id,status,app_id FROM tbl_categories WHERE id = '".$_GET['id']."'";
		$checkDB = $db->get_row($checkDB);
		if($checkDB){
			if($checkDB->status == 1){
				$res = false; $active = true;
			}else{

				if($yes_global==1){
					$sql = "INSERT INTO tbl_delete_data SET app_id = '".$checkDB->app_id."',
															row_id = '".$_GET['id']."',
															row_type = 'categories',
															show_in_app = '0'";
					$db->query($sql);
				}else{

					$sql_rows = "SELECT * FROM tbl_cat_rows WHERE merchandise_id = '".$_GET['id']."'";
					$result_rows = $db->get_results($sql_rows);
					if($result_rows){
						foreach($result_rows as $result_row){
							$images = $result_row->images;
							$images = json_decode($images);
							if( count($images) > 0 ){
								foreach($images as $image){
									if(file_exists($image)){
										unlink($image);
									}
								}
								$sql_del = "DELETE FROM tbl_cat_rows WHERE id = '".$result_row->id."'";
								$db->query($sql_del);
							}
						}
					}
					$sql_del = "DELETE FROM tbl_categories WHERE id = '".$_GET['id']."'";
					$db->query($sql_del);
					$res = true;
				}
				
			}
		}else{
			$res = false;
		}
	} 
?>
<?php include 'includes/header.php'; ?>
<script>
	<?php if(isset($res) && $res == true){ ?>
	window.messages = {
		type : "success",
		message: "Record deleted successfully.",
	}
	<?php }elseif(isset($res) && $res == false && $active == true){ ?>
	window.messages = {
		type : "warning",
		message: "This home layout is currently active.",
	}
	<?php }elseif(isset($res) && $res == false){ ?>
	window.messages = {
		type : "warning",
		message: "Invalid id provided, please try again later.",
	}
	<?php } ?>
</script>
<body>
<div id="wrapper">
	<?php include('includes/navigation.php'); ?>
	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="apps">
						<div class="title">
							<h2>Search Category manager</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<?php 
				$sql = "SELECT GROUP_CONCAT(row_id) as row_ids FROM tbl_delete_data WHERE app_id ='".$_GET['app_id']."' AND row_type='categories'";
				$skip_data = $db->get_row($sql);
				$row_ids_arr = $skip_data->row_ids;

			 		$sql = "SELECT * FROM tbl_categories  WHERE app_id IN(".@$_GET['app_id'].", ".$global_app_id." ) AND id NOT IN ('".$row_ids_arr."') ORDER BY app_id";

			 		$merchandises = $db->get_results($sql);

			 		$sql = "SELECT GROUP_CONCAT(row_id) as row_ids FROM tbl_delete_data WHERE app_id ='".$_GET['app_id']."' AND row_type='categories_row'";
							$skip_data = $db->get_row($sql);
							$row_ids_arr_temp = $skip_data->row_ids;

					if($row_ids_arr_temp){
						$row_ids_temp_arr = explode(',', $row_ids_arr_temp);
						$row_ids_arr = "'" . implode ( "', '", $row_ids_temp_arr ) . "'";
						$row_ids_arr = rtrim($row_ids_arr,"'");
						$row_ids_arr = ltrim($row_ids_arr,"'");
					}else{
						$row_ids_arr='';
					}

				 	if($merchandises){
						foreach($merchandises as $merchandise){
							$sql = "SELECT * FROM tbl_sort_rows WHERE app_id = '".@$_GET['app_id']."' AND row_type_id = '".$merchandise->id."'";
							$check = $db->get_results($sql);
							if(count($check) > 0){
								if($yes_global==1){
									$sql = "SELECT * FROM tbl_cat_rows JOIN tbl_sort_rows ON tbl_cat_rows.id=tbl_sort_rows.row_id AND tbl_sort_rows.row_type='search_category' AND tbl_cat_rows.merchandise_id = '$merchandise->id' AND tbl_cat_rows.show_in_app = '1' AND tbl_cat_rows.is_global='1' AND tbl_cat_rows.app_id=tbl_sort_rows.app_id AND tbl_cat_rows.app_id IN(".@$_GET['app_id'].", ".$global_app_id." ) ORDER BY tbl_sort_rows.sort_order ASC";
								}else{
									$sql = "SELECT * FROM tbl_cat_rows JOIN tbl_sort_rows ON tbl_cat_rows.id=tbl_sort_rows.row_id AND tbl_sort_rows.row_type='search_category' AND tbl_cat_rows.merchandise_id = '$merchandise->id' AND tbl_cat_rows.show_in_app = '1' AND tbl_cat_rows.id NOT IN('".$row_ids_arr."') AND tbl_cat_rows.app_id=tbl_sort_rows.app_id AND tbl_cat_rows.app_id IN(".@$_GET['app_id'].", ".$global_app_id." ) ORDER BY tbl_sort_rows.sort_order ASC";
								}								
								// $sql = "SELECT * FROM tbl_cat_rows JOIN tbl_sort_rows ON tbl_cat_rows.id=tbl_sort_rows.row_id AND tbl_sort_rows.row_type='search_category' AND tbl_cat_rows.merchandise_id = '$merchandise->id' AND tbl_cat_rows.show_in_app = '1' ORDER BY tbl_sort_rows.sort_order ASC";
							}else{
								if($yes_global==1){
									$sql = "SELECT * FROM tbl_cat_rows WHERE merchandise_id = '".$merchandise->id."' AND show_in_app = '1' AND is_global='1' AND id NOT IN('".$row_ids_arr."')";
								}else{
									$sql = "SELECT * FROM tbl_cat_rows WHERE merchandise_id = '".$merchandise->id."' AND show_in_app = '1' AND id NOT IN('".$row_ids_arr."')";
								}
							}
							$results = $db->get_results($sql);
							$result = $results[0];
							if(@$result->layout == "singleBar"){
								$result = @$results[1];
							}
				?>
				<div class="col-md-3">
					<div class="box">
						<div class="merchandise-box" id="merchandise-<?php echo $merchandise->id; ?>" data-command="merchandise" data-id="<?php echo $merchandise->id; ?>">
							<div class="head">
								<div class="row">
									<div class="col-md-9">
										<div class="title"><?php echo $merchandise->title; ?></div>
										<div class="sub-title"><?php echo count($results); ?> Rows</div>
									</div>
									<div class="col-md-3">
										 
									</div>
								</div>
							</div>
					   		<div class="content" onClick="window.location.href='category-overview.php?app_id=<?php echo $_GET['app_id'] ?>&id=<?php echo $merchandise->id; ?>';">
								<?php
									$images = json_decode(@$result->images);
									if(@$result->layout == "single" || @$result->layout == "singleBar"){
								?>
								<div class="single">
									<div class="row">
										<div class="col-md-12">
											<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
										</div>
									</div>
								</div>
								<?php }elseif(@$result->layout == "double" || @$result->layout == "doubleShort"){ ?>
								<div class="double">
									<div class="row">
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
										</div>
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
										</div>
									</div>
								</div>
								<?php }elseif(@$result->layout == "tripleLeft"){ ?>
								<div class="triple">
									<div class="row">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[2]; ?>" class="img-responsive"> </div>
										</div>
									</div>
								</div>
								<?php }elseif(@$result->layout == "tripleRight"){ ?>
								<div class="triple">
									<div class="row">
										<div class="col-md-6">
											<div class="img"> <img src="<?php echo $images[0]; ?>" class="img-responsive"> </div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[1]; ?>" class="img-responsive"> </div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="img"> <img src="<?php echo $images[2]; ?>" class="img-responsive"> </div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<?php
						}
				 	}else{
			 	?>
				<div class="col-md-12">
					<p class="alert alert-success">No record found.</p>
				</div>
				<?php
				 	}
			 	?>
			</div>
		</div>
	</div>
</div>
<?php include 'includes/footer.php'; ?>
</body>
</html>